﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true"  Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true" CodeBehind="sync.aspx.cs" Inherits="Contact.sync" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="block-header">
       <span><p class="fa fa-home m-r-10"></p>Sync</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Sync data</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder">
                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                <div class="row">
                                    <div class="col-md-3 btn-fix-width">
                                        <asp:LinkButton ID="btnBeginSync" CssClass="btn btn-primary btn-sm" runat="server" OnClick="btnBeginSync_Click"><p class="fa fa-recycle m-r-10"></p> Begin Sync</asp:LinkButton><br /><br />
                                    </div>
                                    <div class="col-md-12">
                                        <asp:Label ID="Label2" CssClass="text-success" Visible="false" runat="server" Text="Sync completed successfully."></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
