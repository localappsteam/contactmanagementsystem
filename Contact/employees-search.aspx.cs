﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Security.Principal;

namespace Contact
{
    public partial class employees_search : System.Web.UI.Page
    {
        public override void VerifyRenderingInServerForm(Control control)
        {
            //Confirms that an HtmlForm control is rendered for the
            //specified ASP.NET server control at run time.
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int RoleID = 17;
            int AdminRoleID = 3;

            access obj = new access();
            if (obj.checkRoles(UserName, RoleID, AdminRoleID))
            {
                lkDataSync.Visible = true;
            }
            else
            {
                lkDataSync.Visible = false;
            }

        }

        protected void gridEmployees_DataBound(object sender, EventArgs e)
        {
            gridEmployees.SelectedIndex = -1;
        }

        void exportToExcel()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=Employees.xls");
            gridEmployees.GridLines = GridLines.Both;
            gridEmployees.HeaderStyle.Font.Bold = true;
            gridEmployees.HeaderStyle.ForeColor = Color.Black;
            gridEmployees.HeaderStyle.BackColor = Color.Silver;
            gridEmployees.HeaderStyle.Font.Underline = true;

            gridEmployees.AllowPaging = false;
            gridEmployees.AllowSorting = false;
            gridEmployees.DataBind();
            gridEmployees.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }

        void releaseObject(object obj)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            exportToExcel();
        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.CommandTimeout = 0;
        }

        protected void SqlDataSource1_Selected(object sender, SqlDataSourceStatusEventArgs e)
        {
            lblRows.Text = e.AffectedRows.ToString();
        }

        protected void gridEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            viewEmployee();
        }
        void viewEmployee()
        {
            Session["SelectedEmployeeID"] = gridEmployees.SelectedRow.Cells[1].Text.Trim();
            Response.Redirect("employees-search-detailed.aspx");
        }

        void dataSync()
        {
            try
            {
                access obj = new access();
                obj.employeeDataSync(Convert.ToInt32(Session["MillID"].ToString()));
                gridEmployees.DataBind();
                ShowAlert(1, "Data synchronization complete.");
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
        }

        protected void btnDataSync_Click(object sender, EventArgs e)
        {
            dataSync();
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        
    }
}