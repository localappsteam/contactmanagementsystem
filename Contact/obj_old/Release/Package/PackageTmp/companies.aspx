﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true"   Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true" CodeBehind="companies.aspx.cs" Inherits="Contact.companies" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Companies</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Contracting Companies</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-6">
                                 <asp:TextBox ID="txtSearch" CssClass="form-control form-control-sm  m-l-10" Width="75%" Style="float:left" Placeholder="Search company..." runat="server"></asp:TextBox>
                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 cont-holder" style="padding: 0px">
                                <asp:GridView ID="gridCompanies" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="gridCompanies_SelectedIndexChanged" >
                                    <AlternatingRowStyle BackColor="#f5f5f5" />
                                    
                                    <RowStyle Height="41px"></RowStyle>
                                    <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" SelectText="Select" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                        <asp:BoundField DataField="Company" HeaderText="Company" SortExpression="Company" />
                                        <asp:BoundField DataField="Employees" HeaderText="Employees" SortExpression="Employees" ReadOnly="True" />
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Companies_View" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="Mill" SessionField="SelectedOffice" Type="String" />
                                        <asp:ControlParameter ControlID="txtSearch" DefaultValue=" " Name="Searchvalue" PropertyName="Text" Type="String" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
