﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true" CodeBehind="employees-search-detailed.aspx.cs" Inherits="Contact.employees_search_detailed" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Employees / Employee search / Detailed</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Employee Details</h2>
                    <ul class="header-dropdown m-t--10 m-r-10" id="divOptions" runat="server">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <h4 class="material-icons" style="font-weight:normal; font-size:medium"><p class="fa fa-ellipsis-v m-r-10"></p>Options </h4>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <asp:LinkButton ID="btnViewEdit" runat="server" OnClick="btnViewEdit_Click"><span class="fa fa-edit m-r-10"></span>Edit</asp:LinkButton>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="cont-holder-plain">
                            <div class="row">
                                <div class="col-md-2 btn-fix-width">
                                    <a href="employees-search.aspx" class="btn btn-default btn-sm btn-block">
                                        <p class="fa fa-reply m-r-10"></p>
                                        Go back</a>
                                </div>
                            </div>
                            <div class="row" id="divEdit" runat="server" visible="false">
                                <div class="col-md-12">
                                    <h3>Editing enabled!</h3>
                                </div>
                                <div class="col-md-2 btn-fix-width">
                                    <asp:LinkButton ID="btnSaveChanges1" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSaveChanges1_Click"><p class="fa fa-save m-r-10"></p>Save changes</asp:LinkButton>
                                </div>
                                <div class="col-md-2 btn-fix-width">
                                    <asp:LinkButton ID="btnCancel1" CssClass="btn btn-default btn-block btn-sm" runat="server" OnClick="btnCancel1_Click"><p class="fa fa-times m-r-10"></p>Cancel editing</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="cont-holder-plain">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Personal details</h5>
                                </div>
                                <div class="col-md-4">
                                    <p>Surname <span class="text-danger">*</span></p>
                                    <asp:TextBox ID="txtSurname" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>First names <span class="text-danger">*</span></p>
                                    <asp:TextBox ID="txtFirstnames" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Nick name</p>
                                    <asp:TextBox ID="txtNickname" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>User name</p>
                                    <asp:TextBox ID="txtUsername" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Email<span class="text-danger">*</span></p>
                                    <asp:TextBox ID="txtEmail" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Mobile number <span class="text-danger">*</span></p>
                                    <asp:TextBox ID="txtCellphone" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Office number</p>
                                    <asp:TextBox ID="txtWorkPhone" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Ext</p>
                                    <asp:TextBox ID="txtExt" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Speed dail</p>
                                    <asp:TextBox ID="txtSpeedDial" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                 <div class="col-md-8">
                                    <p>Home address</p>
                                    <asp:TextBox ID="txtHomeAddress" Enabled="false" CssClass="form-control input-disabled" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Occupational details</h5>
                                </div>
                                <div class="col-md-4">
                                    <p>Company</p>
                                    <asp:TextBox ID="txtCompany" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Title</p>
                                    <asp:TextBox ID="txtTitle" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Employee number</p>
                                    <asp:TextBox ID="txtEmployeeNumber" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Department</p>
                                    <asp:TextBox ID="txtDepartment" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Office</p>
                                    <asp:TextBox ID="txtOffice" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Section</p>
                                    <asp:TextBox ID="txtSection" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Site</p>
                                    <asp:TextBox ID="txtSite" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Manager <span class="text-danger">*</span></p>
                                    <asp:TextBox ID="txtManager" Enabled="false" CssClass="form-control input-disabled" runat="server"></asp:TextBox>
                                </div>
                             </div>
                            <div class="row p-t-20" id="divEdit2" runat="server" visible="false">
                                <div class="col-md-2 btn-fix-width">
                                    <asp:LinkButton ID="btnSaveChanges2" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSaveChanges1_Click"><p class="fa fa-save m-r-10"></p>Save changes</asp:LinkButton>
                                </div>
                                <div class="col-md-2 btn-fix-width">
                                    <asp:LinkButton ID="btnCancel2" CssClass="btn btn-default btn-block btn-sm" runat="server" OnClick="btnCancel1_Click"><p class="fa fa-times m-r-10"></p>Cancel editing</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
