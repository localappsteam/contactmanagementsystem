﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="callout-contact.aspx.cs" Inherits="Contact.callout.callout_contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Create callout</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Create Callout
                        <asp:HyperLink ID="btnBack" class="btn btn-primary pull-right" style="margin-top: -5px" runat="server"><p class="fa fa-reply icon-mr"></p>Go back</asp:HyperLink>
                    </h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row" id="divDetails" runat="server">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-12" id="divActiveCallouts" visible="false" runat="server">
                                        <p class="text-danger font-16">NB. This Person is already on-site and has been contacted for the following callouts:</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <asp:LinkButton ID="tab1" runat="server">
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="tab2" runat="server">
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="tab3" runat="server">
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 p-l-40" id="divInput1" runat="server" visible="true">
                                        <h4>What is the outcome of contacting the Person?</h4>
                                        <hr />
                                        <div class="row">
                                            <div class="col-md-4" style="margin-top: 25px;"><b>Date/Time person contacted:</b></div>
                                            <div class="col-md-4">
                                                <small>Date</small>
                                                <asp:TextBox ID="txtDateContacted" CssClass="form-control" TextMode="Date" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <small>Time</small>
                                                <asp:TextBox ID="txtTimeContacted" CssClass="form-control" TextMode="Time" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="col-md-12">
                                                <asp:RadioButtonList ID="rdContactOutcome" runat="server">
                                                    <asp:ListItem Value="1" Selected="True">Person has been contacted</asp:ListItem>
                                                    <asp:ListItem Value="2">Person cannot come</asp:ListItem>
                                                    <asp:ListItem Value="3">Cannot get hold of Person</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <div class="col-md-12">
                                                <asp:LinkButton ID="btnSave" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnSave_Click"><p class="fa fa-save icon-mr"></p>Save</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 p-l-40" id="divInput2" runat="server" visible="false">
                                        <h4>Has the Person arrived on site?</h4>
                                        <hr />
                                        <div class="row">
                                            <div class="col-md-2 p-t-10">
                                                <p>Time arrived:</p>
                                            </div>
                                            <div class="col-md-4 p-t-10">
                                                <asp:Label ID="lblTimeArrived" Font-Bold="true" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">If person has arrived, print welcome note below.</div>
                                            <div class="col-md-12">
                                                <asp:LinkButton ID="btnPrintWelcomeNote" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnPrintWelcomeNote_Click"><p class="fa fa-print icon-mr"></p>Print welcome note</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 p-l-40" id="divInput3" runat="server" visible="false">
                                        <h4>Is the callout completed?</h4>
                                        <hr />
                                        <div class="row">
                                            <div class="col-md-4" style="margin-top: 25px;"><b>Date/Time completed:</b></div>
                                            <div class="col-md-4">
                                                <small>Date</small>
                                                <asp:TextBox ID="txtDateCompleted" CssClass="form-control" TextMode="Date" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <small>Time</small>
                                                <asp:TextBox ID="txtTimeCompleted" CssClass="form-control" TextMode="Time" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="col-md-12">
                                                <asp:LinkButton ID="btnCloseCallout" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseCallout_Click"><p class="fa fa-check icon-mr"></p>Close callout</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6  p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5>Basic Information</h5>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Callout number:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblCalloutNumber" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Standby duty:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblStandbyDuty" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Reason:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblReason" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Area:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblArea" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Status:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblStatus" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Notification number:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblNotification" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5>Contact notes</h5>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-12 m-t--20">
                                                <asp:GridView ID="gridCalloutNotes" Width="100%" CssClass="table table-condensed" Font-Size="12px" AllowSorting="True" BorderStyle="None" GridLines="None" HeaderStyle-Font-Bold="true"   runat="server" AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                                                    <AlternatingRowStyle BackColor="#f5f5f5" />
                                                    <RowStyle Height="25px"></RowStyle>
                                                    <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                                    <Columns>
                                                        <asp:BoundField DataField="Date/Time Contacted" HeaderText="Date/Time Contacted" SortExpression="Date/Time Contacted"/>
                                                        <asp:BoundField DataField="Person Contacted" HeaderText="Person Contacted" SortExpression="Person Contacted" />
                                                        <asp:BoundField DataField="Outcome" HeaderText="Outcome" SortExpression="Outcome" />
                                                        <asp:BoundField DataField="Contacted By" HeaderText="Contacted By" SortExpression="Contacted By" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_ViewCalloutAttempts" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="lblCalloutNumber" Name="CalloutID" PropertyName="Text" Type="Decimal" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5>Date/Time Information</h5>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Date captured:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblDateTimeCaptured" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Incident date/time::</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblIncidentTime" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Date/Time person called:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblTimePersonCalled" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Date/Time on-site:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblTimeOnSite" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Date/Time completed:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblTimeCompleted" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5>Person Called Out</h5>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Full name:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblPersonName" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Tel:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblPersonTel" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Ext:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblPersonExt" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Cell:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblPersonCell" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Address:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblPersonAddress" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5>Contact Person</h5>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Full name:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblContactName" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Tel:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblContactTel" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Ext:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblContactExt" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Cell:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblContactCell" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Address:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblContactAddress" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5>Authorised By</h5>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Full name:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblAuthorisedName" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Tel:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblAuthorisedTel" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Ext:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblAuthorisedExt" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Cell:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                               <p> <asp:Label ID="lblAuthorisedCell" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-6 field-name field-margin">
                                                <p><b><label>Address:</label></b></p>
                                            </div>
                                            <div class="col-md-6 field-margin" style="padding-top:8px">
                                                <p><asp:Label ID="lblAuthorisedAddress" runat="server" Text="Label"></asp:Label></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
