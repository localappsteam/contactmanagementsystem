﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Contact.callout.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Dashboard</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row text-center">
                            <div class="col-md-12 text-center" style="padding-right: 0px">
                                <div id="divCreateCallout" runat="server" class="menu-circle-container">
                                    <a href="create-callout.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/Status-mail-task-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Create Callout</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container">
                                    <a href="callouts.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/Devices-phone-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Callouts</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container">
                                    <a href="approval-request.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/Mimetypes-application-pkcs7-signature-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Callout Approval</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container">
                                    <a href="reports.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/custom-reports-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Reports</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container" id="divAdmin" runat="server">
                                    <a href="admin.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/mac-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Admin</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
