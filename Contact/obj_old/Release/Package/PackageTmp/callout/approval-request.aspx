﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="approval-request.aspx.cs" Inherits="Contact.callout.approval_request" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Callout Approval</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Callout Approval</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-12"><h5>To sign-off a completed callout, enter the details of the callout sent to you via email or cellphone.</h5></div>
                                </div>
                                <div class="row">
                                     <div class="col-md-12"><p>Select your role for the callout:</p>
                                         <asp:RadioButtonList ID="rdRole" Font-Bold="true" runat="server">
                                             <asp:ListItem Value="1"><b class="icon-mr">Person Called Out</b> (The person assigned to the callout)</asp:ListItem>
                                             <asp:ListItem Value="2"><b class="icon-mr">Supervisor, Manager, etc.</b> (The person who authorized the callout)</asp:ListItem>
                                             <asp:ListItem Value="3"><b class="icon-mr">Control Room</b> (Security)</asp:ListItem>
                                         </asp:RadioButtonList>
                                     </div>
                                </div>
                                <div class="row m-b-20">
                                     <div class="col-md-4">
                                         <p>Callout number:</p>
                                         <asp:TextBox ID="txtCalloutNumber" CssClass="form-control" TextMode="Number" runat="server"></asp:TextBox>
                                         <small><i>Enter the callout number that needs sign-off approval.</i></small>
                                         
                                     </div>
                                </div>
                                <div class="row">
                                     <div class="col-md-4">
                                         <p>Verification code:</p>
                                         <asp:TextBox ID="txtVerificationCode" MaxLength="4" CssClass="form-control" TextMode="Number" runat="server"></asp:TextBox>
                                         <small><i>Enter the verification code sent to you via email or cellphone.</i></small>
                                         <br />
                                     </div>
                                </div>
                                <div class="row">
                                     <div class="col-md-2 btn-fix-width">
                                         <asp:LinkButton ID="btnApprove" CssClass="btn btn-sm btn-block btn-primary" runat="server" OnClick="btnApprove_Click"><p class="fa fa-check icon-mr"></p>Approve</asp:LinkButton>
                                         </div>
                                     <div class="col-md-2 btn-fix-width">
                                         <a href="callouts-complete.aspx" class="btn btn-sm btn-block btn-default"><p class="fa fa-times icon-mr"></p>Cancel</a>
                                         </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
