﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="reports.aspx.cs" Inherits="Contact.callout.reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Reports</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Generate Reports</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain p-l-30 p-r-30">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h4>Report Filtering</h4>
                                    </div>
                                    <div class="col-md-12 text-center m-t--10">
                                        <p class="text-center bg-success m-l--15 m-r-20 p-t-10 p-b-10">Use this form to select the callout records you would like to see.</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4  p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5 class="margin-5">Basic Information</h5>
                                            </div>
                                            <div class="col-md-12 padding-0 m-t--20">
                                                <asp:ListBox ID="lstPersonCalledOut" CssClass="form-control" runat="server" DataSourceID="SqlDataSource1" DataTextField="Person" DataValueField="#" SelectionMode="Multiple" OnDataBound="lstPersonCalledOut_DataBound" ></asp:ListBox>
                                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_PersonCalledOut" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4  p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5 class="margin-5">Area / Control Room</h5>
                                            </div>
                                            <div class="col-md-12 padding-0 m-t--20">
                                                <asp:ListBox ID="lstArea" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Description" DataValueField="#" SelectionMode="Multiple" OnDataBound="lstArea_DataBound"></asp:ListBox>
                                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_SelectArea" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4  p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5 class="margin-5">Contact Person</h5>
                                            </div>
                                            <div class="col-md-12 padding-0 m-t--20">
                                                <asp:ListBox ID="lstContactPerson" CssClass="form-control" runat="server" DataSourceID="SqlDataSource3" DataTextField="Person" DataValueField="#" SelectionMode="Multiple" OnDataBound="lstContactPerson_DataBound"></asp:ListBox>
                                                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_PersonCalledOut" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4  p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5 class="margin-5">Authorized by</h5>
                                            </div>
                                            <div class="col-md-12 padding-0 m-t--20">
                                                <asp:ListBox ID="lstAuthorisedBy" CssClass="form-control" runat="server" DataSourceID="SqlDataSource4" DataTextField="Person" DataValueField="#" SelectionMode="Multiple" OnDataBound="lstAuthorisedBy_DataBound"></asp:ListBox>
                                                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_PersonCalledOut" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4  p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5 class="margin-5">Notification</h5>
                                            </div>
                                            <div class="col-md-12 padding-0 m-t--20">
                                                <asp:ListBox ID="lstNotification" CssClass="form-control" runat="server" DataSourceID="SqlDataSource5" DataTextField="Notification" DataValueField="Notification" SelectionMode="Multiple" OnDataBound="lstNotification_DataBound"></asp:ListBox>
                                                <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_SelectNotifications" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4  p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5 class="margin-5">Callout Type</h5>
                                            </div>
                                            <div class="col-md-12">
                                                <asp:RadioButtonList ID="rdCalloutType" runat="server" Width="100%" RepeatDirection="Horizontal" DataSourceID="SqlDataSource6" DataTextField="Description" DataValueField="#" SelectionMode="Multiple" OnDataBound="rdCalloutType_DataBound"></asp:RadioButtonList>
                                                <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_SelectCalloutType" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4  p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5 class="margin-5">Callout Date</h5>
                                            </div>
                                            <div class="col-md-6 m-t--10">
                                                From:
                                                 <asp:TextBox ID="txtFromDate" CssClass="form-control" Placeholder="dd/mm/yyyy" TextMode="Date" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="col-md-6 m-t--10">
                                                To:
                                                 <asp:TextBox ID="txtToDate" CssClass="form-control" Placeholder="dd/mm/yyyy" TextMode="Date" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4  p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5 class="margin-5">Employee Type</h5>
                                            </div>
                                            <div class="col-md-12">
                                                <asp:RadioButtonList ID="rdEmployeeType" RepeatDirection="Horizontal" Width="100%" runat="server">
                                                    <asp:ListItem Value="0" Selected="True">All</asp:ListItem>
                                                    <asp:ListItem Value="1">Permanent</asp:ListItem>
                                                    <asp:ListItem Value="2">Contractors</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4  p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5 class="margin-5">Callout Status</h5>
                                            </div>
                                            <div class="col-md-12">
                                                <asp:RadioButtonList ID="rdStatus" RepeatDirection="Horizontal" Width="100%" runat="server">
                                                    <asp:ListItem Value="0" Selected="True">All</asp:ListItem>
                                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                                    <asp:ListItem Value="2">Complete</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12 m-l--15 m-t--30">
                                        <hr />
                                        <div class="row">
                                            <div class="col-md-5"></div>
                                            <div class="col-md-2 text-center">
                                                <h4 style="font-weight: normal">Report type</h4>
                                                <asp:RadioButtonList Width="100%" ID="rdReportType" runat="server">
                                                    <asp:ListItem Value="1" Selected="True">Full Report</asp:ListItem>
                                                    <asp:ListItem Value="2">Summary</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <div class="col-md-5"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-2 btn-fix-width">
                                                <asp:LinkButton ID="btnGeneratePDF" CssClass="btn btn-primary btn-block" runat="server" OnClick="btnGeneratePDF_Click"><p class="fa fa-file-pdf-o m-r-10"></p>Generate Pdf</asp:LinkButton>
                                            </div>
                                            <div class="col-md-2 btn-fix-width">
                                                <asp:LinkButton ID="btnGenerateExcel" CssClass="btn btn-primary btn-block" runat="server" OnClick="btnGenerateExcel_Click"><p class="fa fa-file-pdf-o m-r-10"></p>Generate Excel</asp:LinkButton>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <br />
                                                <p class="text-center bg-success">
                                                    <br /><b>Hint!</b><br />
                                                    You can select multiple entries from a list by pressing the 'Control(Ctrl)' key while clicking items.
                                                    <br />
                                                    <br />
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
