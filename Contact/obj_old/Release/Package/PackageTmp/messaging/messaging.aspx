﻿<%@ Page Title="" Language="C#" MasterPageFile="~/messaging/massaging.Master" AutoEventWireup="true" CodeBehind="messaging.aspx.cs" Inherits="Contact.messaging.messaging" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Messaging</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Messaging Menu</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-2 btn-fix-width">
                                <a href="new-message.aspx" class="btn btn-primary btn-sm btn-block">
                                    <p class="fa fa-envelope m-r-10"></p>
                                    New message</a>
                            </div>
                            <div class="col-md-2 btn-fix-width">
                                <a href="groups.aspx" class="btn btn-primary btn-sm btn-block">
                                    <p class="fa fa-users m-r-10"></p>
                                    Groups</a>
                            </div>
                            <div class="col-md-2 btn-fix-width">
                                <a href="history.aspx" class="btn btn-primary btn-sm btn-block">
                                    <p class="fa fa-history m-r-10"></p>
                                    Messaging history</a>
                            </div>
                        </div>
                        <div class="row">
                            <div id="divRecentMessages" runat="server" class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 header" style="border: none; margin: 0px">
                                        <h2 style="border: none">Recently sent messages</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 cont-holder" style="overflow: auto">
                                        <asp:GridView ID="gridRecentMessages" Width="100%" CssClass="table" Font-Size="12px" AllowSorting="True" BorderStyle="None" GridLines="None" runat="server" EmptyDataText="No roles assigned." AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                            <RowStyle Height="41px"></RowStyle>
                                            <Columns>
                                                <asp:BoundField DataField="DateSent" HeaderText="DateSent" SortExpression="DateSent" />
                                                <asp:BoundField DataField="Cellphone" HeaderText="Cellphone" SortExpression="Cellphone" />
                                                <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" />
                                                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" ProviderName="System.Data.SqlClient" SelectCommand="sp_Contact_Messaging_RecentlySent" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="EmployeeID" SessionField="EmployeeID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
