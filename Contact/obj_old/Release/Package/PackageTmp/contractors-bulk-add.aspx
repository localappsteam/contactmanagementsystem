﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true"  Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true" CodeBehind="contractors-bulk-add.aspx.cs" Inherits="Contact.contractors_bulk_add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Contractors / Bulk add contractors</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Bulk add contractors</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-2 btn-fix-width">
                                <a href="contractors-search.aspx" class="btn btn-default btn-sm btn-block">
                                    <p class="fa fa-reply m-r-10"></p>
                                    Go back</a>
                            </div>
                            <div class="col-md-2 btn-fix-width">
                                <a class="btn btn-primary btn-sm btn-block" download href="templates/Contractors.csv">
                                    <p class="fa fa-file-text-o m-r-10"></p>
                                    Download template
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cont-holder" style="padding: 0px">
                            </div>
                            <div class="col-md-12 cont-holder-plain">
                                <div id="divUpload" runat="server">
                                    <div class="row m-t-20">
                                        <div class="col-md-12">
                                            <asp:FileUpload ID="FileUpload1" runat="server" />
                                        </div>
                                        <div class="col-md-2 btn-fix-width-sm">
                                            <asp:LinkButton ID="btnUpload" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnUpload_Click"><p class="fa fa-upload m-r-10"></p>Upload</asp:LinkButton>
                                        </div>
                                        <div class="col-md-12">
                                            <small><b><span class="text-danger">Note!</span> Consider the following before uploading the file.</b></small>
                                            <ol>
                                                <li><small>Do not use commas (<b>,</b>) on your text.</small></li>
                                                <li><small>Do not rename, remove or change the headings on the template.</small></li>
                                                <li><small>Make sure all required fields are captured. eg ID number, Company number, Cellphone, etc...</small></li>
                                                <li><small>Save the file as CSV (comma delimited) format.</small></li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div id="divVerification" runat="server" visible="false">
                                    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr />
                                            <label>Data verification</label>
                                        </div>
                                    </div>
                                    <div class="row margin-0 padding-0">
                                        <div class="col-md-2">
                                            <p>
                                                <span>Total rows uploaded : </span>
                                                <asp:Label ID="lblTotalUploaded" runat="server" Text="0"></asp:Label>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <p>
                                                <span>Invalid rows skipped : </span>
                                                <asp:Label ID="lblSkipped" ForeColor="Red" runat="server" Text="0"></asp:Label>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <p>
                                                <span>Verified : </span>
                                                <asp:Label ID="lblVerified2" ForeColor="Green" runat="server" Text="0"></asp:Label>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <p>
                                                <span>Filtered out : </span>
                                                <asp:Label ID="lblFiltered2" ForeColor="Red" runat="server" Text="0"></asp:Label>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="cont-holder-plain">
                                                <label style="width: 100%; padding: 5px" class="bg-success p-l-10">
                                                    Verified: 
                                                <asp:Label ID="lblVerified" runat="server" Text="0"></asp:Label></label>
                                            </div>
                                            <div class="cont-holder">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gridVerified" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None" HeaderStyle-BackColor="#3a3a3a" HeaderStyle-Font-Bold="true"  runat="server" ShowHeaderWhenEmpty="True" AllowPaging="true" AutoGenerateColumns="False" DataKeyNames="#" DataSourceID="SqlDataSource1">
                                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                                            <HeaderStyle BackColor="#3A3A3A" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                                            <RowStyle Height="30px"></RowStyle>
                                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                                            <Columns>
                                                                <asp:BoundField DataField="#" HeaderText="#" InsertVisible="False" ReadOnly="True" SortExpression="#" />
                                                                <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" />
                                                                <asp:BoundField DataField="Firstnames" HeaderText="Firstnames" SortExpression="Firstnames" />
                                                                <asp:BoundField DataField="Initials" HeaderText="Initials" SortExpression="Initials" />
                                                                <asp:BoundField DataField="IDNumber" HeaderText="IDNumber" SortExpression="IDNumber" />
                                                                <asp:BoundField DataField="CompanyNumber" HeaderText="CompanyNumber" SortExpression="CompanyNumber" />
                                                                <asp:BoundField DataField="HomeAddress" HeaderText="HomeAddress" SortExpression="HomeAddress" />
                                                                <asp:BoundField DataField="PIGEONHOLE" HeaderText="PIGEONHOLE" SortExpression="PIGEONHOLE" />
                                                                <asp:BoundField DataField="Bleeper" HeaderText="Bleeper" SortExpression="Bleeper" />
                                                                <asp:BoundField DataField="TelHome" HeaderText="TelHome" SortExpression="TelHome" />
                                                                <asp:BoundField DataField="Cell" HeaderText="Cell" SortExpression="Cell" />
                                                                <asp:BoundField DataField="ADUsername" HeaderText="ADUsername" SortExpression="ADUsername" />
                                                                <asp:BoundField DataField="EXT" HeaderText="EXT" SortExpression="EXT" />
                                                                <asp:BoundField DataField="DecPhone" HeaderText="DecPhone" SortExpression="DecPhone" />
                                                                <asp:BoundField DataField="Occupation" HeaderText="Occupation" SortExpression="Occupation" />
                                                                <asp:BoundField DataField="SpeedDial" HeaderText="SpeedDial" SortExpression="SpeedDial" />
                                                                <asp:BoundField DataField="Company" HeaderText="Company" SortExpression="Company" />
                                                                <asp:BoundField DataField="Manager" HeaderText="Manager" SortExpression="Manager" />
                                                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                                <asp:BoundField DataField="MILL" HeaderText="MILL" SortExpression="MILL" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Contractors_VerifiedUploadAdd" SelectCommandType="StoredProcedure" OnSelected="SqlDataSource1_Selected">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="EmployeeID" SessionField="EmployeeID" Type="Decimal" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                        <div class="col-md-12 m-t-20">
                                            <div class="cont-holder-plain">
                                                <p>See the error message on the highlighted fields.</p>
                                                <label style="width: 100%; padding: 5px" class="bg-danger p-l-10">
                                                    Filtered out: 
                                                <asp:Label ID="lblFiltered" runat="server" Text="0"></asp:Label></label>
                                            </div>
                                            <div class="cont-holder">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gridFilteredOut" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="#" DataSourceID="SqlDataSource2" OnDataBound="gridFilteredOut_DataBound">
                                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                                            
                                                            <RowStyle Height="41px"></RowStyle>
                                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                                            <Columns>
                                                                <asp:BoundField DataField="#" HeaderText="#" InsertVisible="False" ReadOnly="True" SortExpression="#" />
                                                                <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" />
                                                                <asp:BoundField DataField="Firstnames" HeaderText="Firstnames" SortExpression="Firstnames" />
                                                                <asp:BoundField DataField="Initials" HeaderText="Initials" SortExpression="Initials" />
                                                                <asp:BoundField DataField="IDNumber" HeaderText="IDNumber" SortExpression="IDNumber" ReadOnly="True" />
                                                                <asp:BoundField DataField="CompanyNumber" HeaderText="CompanyNumber" SortExpression="CompanyNumber" ReadOnly="True" />
                                                                <asp:BoundField DataField="HomeAddress" HeaderText="HomeAddress" SortExpression="HomeAddress" />
                                                                <asp:BoundField DataField="PIGEONHOLE" HeaderText="PIGEONHOLE" SortExpression="PIGEONHOLE" />
                                                                <asp:BoundField DataField="Bleeper" HeaderText="Bleeper" SortExpression="Bleeper" />
                                                                <asp:BoundField DataField="TelHome" HeaderText="TelHome" SortExpression="TelHome" />
                                                                <asp:BoundField DataField="Cell" HeaderText="Cell" SortExpression="Cell" ReadOnly="True" />
                                                                <asp:BoundField DataField="ADUsername" HeaderText="ADUsername" SortExpression="ADUsername" />
                                                                <asp:BoundField DataField="EXT" HeaderText="EXT" SortExpression="EXT" />
                                                                <asp:BoundField DataField="DecPhone" HeaderText="DecPhone" SortExpression="DecPhone" />
                                                                <asp:BoundField DataField="Occupation" HeaderText="Occupation" SortExpression="Occupation" />
                                                                <asp:BoundField DataField="SpeedDial" HeaderText="SpeedDial" SortExpression="SpeedDial" />
                                                                <asp:BoundField DataField="Company" HeaderText="Company" SortExpression="Company" />
                                                                <asp:BoundField DataField="Manager" HeaderText="Manager" SortExpression="Manager" />
                                                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                                <asp:BoundField DataField="MILL" HeaderText="MILL" SortExpression="MILL" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Contractors_FilteredUploadAdd" SelectCommandType="StoredProcedure" OnSelected="SqlDataSource2_Selected">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="EmployeeID" SessionField="EmployeeID" Type="Decimal" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="cont-holder-plain">
                                                <div class="row m-t-30">
                                                    <div class="col-md-3 btn-fix-width">
                                                        <asp:LinkButton ID="btnSubmit" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSubmit_Click"><p class="fa fa-paper-plane m-r-10"></p>Submit</asp:LinkButton>
                                                    </div>
                                                    <div class="col-md-2 btn-fix-width">
                                                        <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-block btn-sm" runat="server" OnClick="btnCancel_Click"><p class="fa fa-times m-r-10"></p>Cancel</asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 text-center">
                                                        <small><b>NB! </b> Note that only the verified data displayed on the first table will be submitted.</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
