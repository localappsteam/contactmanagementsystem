﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true"  Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true" CodeBehind="access-denied.aspx.cs" Inherits="Contact.access_denied" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <div class="panel">
                        <div class="row">
                           <div class="col-md-12">
                                <h2 class="text-danger"><span class="fa fa-exclamation-triangle m-r-10"></span>Access denied</h2>
                                <p class="m-t-10">You do not have the roles to access this page.</p>
                                <h5>Contact/<asp:Label ID="lblPagename" runat="server" Text=""></asp:Label></h5>
                                <br />
                                <p>Contact your local system administrator or log a call to the I.T service desk to request access.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
