﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true"  Language="C#" MasterPageFile="~/index.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="emergency-contacts.aspx.cs" Inherits="Contact.emergency_contacts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
         <span><p class="fa fa-home m-r-10"></p>Emergency Contacts </span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Search</h2>
                    <ul class="header-dropdown m-t--10 m-r-10">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  <h4 class="material-icons" style="font-weight:normal; font-size:medium"><p class="fa fa-ellipsis-v m-r-10"></p>Menu </h4>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                 <li id="lkAddEmergency" runat="server">
                                    <a href="emergency-add.aspx"><span class="fa fa-user-plus m-r-10"></span>Add new emergency contact</a>
                                </li>
                                <li id="lkBulkAddEmergency" Visible="false" runat="server">
                                    <a href="contractors-bulk-add.aspx"><span class="fa fa-upload m-r-10"></span>Bulk add</a>
                                </li>
                                <li>
                                    <asp:LinkButton ID="btnExportToExcel" runat="server" OnClick="btnExportToExcel_Click"><span class="fa fa-file-excel-o m-r-10"></span>Export to excel</asp:LinkButton>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-2">
                                <a href="entire-directory-search.aspx" class="btn btn-primary btn-block btn-sm m-l-10" style="margin-top:4px; min-width:180px"><p class="fa fa-search-plus m-r-10" ></p>Entire directory search</a>
                            </div>
                             <div class="col-md-1">
                                 </div>
                            <div class="col-md-6">
                                 <asp:TextBox ID="txtSearch" CssClass="form-control form-control-sm  m-l-10" Width="75%" Style="float:left" Placeholder="Search emergency..." runat="server"></asp:TextBox>
                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cont-holder" style="padding:0px">
                                    <asp:GridView ID="gridEmergency" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataKeyNames="#" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="gridEmergency_SelectedIndexChanged">
                                    <AlternatingRowStyle BackColor="#f5f5f5" />
                                    <RowStyle Height="41px"></RowStyle>
                                    <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" ControlStyle-CssClass="btn-block" SelectText="View" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                         <asp:BoundField DataField="#" HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="0px" HeaderText="#" SortExpression="#" />
                                     
                                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                                        <asp:BoundField DataField="EXT" HeaderText="EXT" SortExpression="EXT" />
                                        <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                                        <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
                                   
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_EmergencySearch" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSource1_Selecting" OnSelected="SqlDataSource1_Selected">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="txtSearch" DefaultValue=" " Name="SearchValue" PropertyName="Text" Type="String" />
                                        <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                         
                                
                            </div>
                        </div>
                         <div class="row">
                             <div class="col-md-12" hidden="hidden">
                                  <p>Results: <asp:Label ID="lblRows" runat="server" Text="0"></asp:Label> rows</p>
                             </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
