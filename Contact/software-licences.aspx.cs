﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;

namespace Contact
{
    public partial class software_licences : System.Web.UI.Page
    {
        public override void VerifyRenderingInServerForm(Control control)
        {
            //Confirms that an HtmlForm control is rendered for the
            //specified ASP.NET server control at run time.
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        void exportToExcel()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=Softwares.xls");
            gridSoftwares.GridLines = GridLines.Both;
            gridSoftwares.HeaderStyle.Font.Bold = true;
            gridSoftwares.HeaderStyle.ForeColor = Color.Black;
            gridSoftwares.HeaderStyle.BackColor = Color.Silver;
            gridSoftwares.HeaderStyle.Font.Underline = true;

            gridSoftwares.AllowPaging = false;
            gridSoftwares.AllowSorting = false;
            gridSoftwares.DataBind();
            gridSoftwares.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }

        void releaseObject(object obj)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            exportToExcel();
        }
    }
}