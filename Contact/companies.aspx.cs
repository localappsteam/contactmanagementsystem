﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact
{
    public partial class companies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void gridCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["SelectedCompany"] = gridCompanies.SelectedRow.Cells[1].Text.Trim();
            Response.Redirect("companies-view-employees.aspx");
        }
    }
}