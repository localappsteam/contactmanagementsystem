﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact
{
    public partial class departments_view_employees : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblSelectedDepartment.Text = Session["SelectedDepartment"].ToString().Trim();

            if (Session["SelectedDepartment"] == null)
            {
                Response.Redirect("departments.aspx");
            }

        }
    }
}