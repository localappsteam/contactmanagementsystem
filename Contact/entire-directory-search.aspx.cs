﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;

namespace Contact
{
    public partial class entire_directory_search : System.Web.UI.Page
    {
        public override void VerifyRenderingInServerForm(Control control)
        {
            //Confirms that an HtmlForm control is rendered for the
            //specified ASP.NET server control at run time.
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void gridContractors_DataBound(object sender, EventArgs e)
        {
            gridEveryone.SelectedIndex = -1;
        }

        void exportToExcel()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=EmployeesAndContractors.xls");
            gridEveryone.GridLines = GridLines.Both;
            gridEveryone.HeaderStyle.Font.Bold = true;
            gridEveryone.HeaderStyle.ForeColor = Color.Black;
            gridEveryone.HeaderStyle.BackColor = Color.Silver;
            gridEveryone.HeaderStyle.Font.Underline = true;

            gridEveryone.AllowPaging = false;
            gridEveryone.AllowSorting = false;
            gridEveryone.DataBind();
            gridEveryone.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }

        void releaseObject(object obj)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            exportToExcel();
        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.CommandTimeout = 0;
        }

        protected void SqlDataSource1_Selected(object sender, SqlDataSourceStatusEventArgs e)
        {
            
        }

        void viewEmployee()
        {

            if(gridEveryone.SelectedRow.Cells[8].Text.Trim().ToLower() == "employee")
            {
                Session["SelectedEmployeeID"] = gridEveryone.SelectedRow.Cells[1].Text.Trim();
                Response.Redirect("employees-search-detailed.aspx");
            }
            else
            {
                Session["SelectedContractorID"] = gridEveryone.SelectedRow.Cells[1].Text.Trim();
                Response.Redirect("contractors-search-detailed.aspx");
            }

            
        }

        protected void gridEveryone_SelectedIndexChanged(object sender, EventArgs e)
        {
            viewEmployee();
        }
    }
}