﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true"  Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true" CodeBehind="entire-directory-search.aspx.cs" Inherits="Contact.entire_directory_search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Employees & Contractors / Directory search</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Entire directory search</h2>
                    <ul class="header-dropdown m-t--10 m-r-10">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <h4 class="material-icons" style="font-weight:normal; font-size:medium"><p class="fa fa-ellipsis-v m-r-10"></p>Menu </h4>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <asp:LinkButton ID="btnExportToExcel" runat="server" OnClick="btnExportToExcel_Click"><span class="fa  fa-file-excel-o m-r-10"></span>Export to excel</asp:LinkButton>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtSearch" CssClass="form-control form-control-sm  m-l-10" Width="75%" Style="float: left" Placeholder="Search employee or contractor..." runat="server"></asp:TextBox>
                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cont-holder" style="padding: 0px">
                                <asp:GridView ID="gridEveryone" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="gridEveryone_SelectedIndexChanged">
                                    <AlternatingRowStyle BackColor="#f5f5f5" />
                                    
                                    <RowStyle Height="41px"></RowStyle>
                                    <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" ControlStyle-CssClass="btn-block" SelectText="View" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                        <asp:BoundField DataField="#" HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="1px" InsertVisible="False" ReadOnly="True" SortExpression="#" />
                                        <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" ReadOnly="True" />
                                        <asp:BoundField DataField="Firstnames" HeaderText="Firstnames" SortExpression="Firstnames" ReadOnly="True" />
                                        <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" ReadOnly="True" />
                                        <asp:BoundField DataField="Cell" HeaderText="Cell" SortExpression="Cell" ReadOnly="True" />
                                        <asp:BoundField DataField="EXT" HeaderText="EXT" SortExpression="EXT" ReadOnly="True" />
                                        <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" ReadOnly="True" />
                                        <asp:BoundField DataField="EmpType" HeaderText="EmpType" SortExpression="EmpType" ReadOnly="True" />
                                        <asp:BoundField DataField="Office" HeaderText="Office" SortExpression="Office" ReadOnly="True" />
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_EntireDirectorySearch" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="txtSearch" DefaultValue="" Name="SearchValue" PropertyName="Text" Type="String" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </div>
                        </div>
                        <div class="row">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
