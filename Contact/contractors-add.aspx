﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true"  CodeBehind="contractors-add.aspx.cs" Inherits="Contact.contractors_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Contractors / Contractor search / Detailed</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add New Contractor</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="cont-holder-plain">
                            <div class="row">
                                <div class="col-md-2 btn-fix-width">
                                    <asp:LinkButton ID="btnAdd1" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnAdd1_Click"><p class="fa fa-paper-plane m-r-10"></p>Submit</asp:LinkButton>
                                </div>
                                <div class="col-md-2 btn-fix-width">
                                    <a href="contractors-search.aspx" class="btn btn-default btn-sm btn-block">
                                        <p class="fa fa-times m-r-10"></p>
                                       Cancel</a>
                                </div>
                            </div>
                        </div>
                        <div class="cont-holder-plain">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Personal details</h5>
                                </div>
                                <div class="col-md-4">
                                    <p>Surname <span class="text-danger">*</span></p>
                                    <asp:TextBox ID="txtSurname"   CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>First names <span class="text-danger">*</span></p>
                                    <asp:TextBox ID="txtFirstnames"  CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Initials</p>
                                    <asp:TextBox ID="txtInitials"  CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>User name</p>
                                    <asp:TextBox ID="txtUsername"  CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Email</p>
                                    <asp:TextBox ID="txtEmail"  CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Cellphone number <span class="text-danger">*</span></p>
                                    <asp:TextBox ID="txtCellphone"  CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Office number</p>
                                    <asp:TextBox ID="txtWorkPhone"  CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Ext</p>
                                    <asp:TextBox ID="txtExt"  CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Speed Dial</p>
                                    <asp:TextBox ID="txtSpeedDial"  CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-8">
                                    <p>Home address</p>
                                    <asp:TextBox ID="txtHomeAddress" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Occupational details</h5>
                                </div>
                                <div class="col-md-4">
                                    <p>Company <span class="text-danger">*</span></p>
                                    <asp:TextBox ID="txtCompany" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Title <span class="text-danger">*</span></p>
                                    <asp:TextBox ID="txtTitle"  CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Company number</p>
                                    <asp:TextBox ID="txtEmployeeNumber" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <p>Manager</p>
                                    <asp:TextBox ID="txtManager" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                             </div>
                            <div class="row">
                                <div class="col-md-2 btn-fix-width">
                                    <asp:LinkButton ID="btnAdd2" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnAdd1_Click"><p class="fa fa-paper-plane m-r-10"></p>Submit</asp:LinkButton>
                                </div>
                                <div class="col-md-2 btn-fix-width">
                                    <a href="contractors-search.aspx" class="btn btn-default btn-sm btn-block">
                                        <p class="fa fa-times m-r-10"></p>
                                       Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
