﻿using System;
using System.Data;
using System.Web;
using System.Security.Principal;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Data.SqlClient;
using System.Text;

namespace Contact
{
    public partial class index : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionBuilder start = new SessionBuilder();
            loadUserProfile();
        }
        void loadUserProfile()
        {
            if(Session["MillID"] !=null)
            {

                WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String UserName = UserSecurityPrincipal.Name;


                lblSelectedOffice.Text = Session["SelectedOffice"].ToString();
                lblUsername.Text = UserName;

                access obj = new access();
                DataSet ds = obj.getUserLogged(UserName);
                String employeeID = "0";

                if (ds.Tables[0].Rows.Count > 0)
                {
                    employeeID = ds.Tables[0].Rows[0]["EmployeeID"].ToString();
                }

                Session["EmployeeID"] = employeeID;
                Session["UserName"] = UserName;

                ADManager ad = new ADManager();
                UserName = UserName.Substring(UserName.LastIndexOf(@"\") + 1).Trim();
                lblUserOffice.Text = ad.getUserLocation(UserName);

                applyRoles();

            }
            else
            {
                Response.Redirect("mill.aspx");
            }
        }

        void applyRoles()
        {
           
            String UserName = Session["UserName"].ToString();

            access obj = new access();
            DataSet ds = obj.getUserRolesAssigned(UserName, 1);

            software.Visible = false;
            useraccounts.Visible = false;

            for (int i=0; i<ds.Tables[0].Rows.Count; i++)
            {
                
                if (Convert.ToInt32(ds.Tables[0].Rows[i]["SystemRoleID"]) == 3) //Administrator
                {
                    software.Visible = true;
                    useraccounts.Visible = true;
                    break;
                }
                else if (Convert.ToInt32(ds.Tables[0].Rows[i]["SystemRoleID"]) == 8) //Softwares
                {
                    software.Visible = true;
                }

            }

        }
    }
}