﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact
{
    public partial class companies_view_employees : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblSelectedCompany.Text = Session["SelectedCompany"].ToString().Trim();

            if (Session["SelectedCompany"] == null)
            {
                Response.Redirect("companies.aspx");
            }
        }
    }
}