﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;

namespace Contact
{
    public partial class departments : System.Web.UI.Page
    {
        public override void VerifyRenderingInServerForm(Control control)
        {
            //Confirms that an HtmlForm control is rendered for the
            //specified ASP.NET server control at run time.
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        void exportToExcel()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=Departments.xls");
            gridDepartments.GridLines = GridLines.Both;
            gridDepartments.HeaderStyle.Font.Bold = true;
            gridDepartments.HeaderStyle.ForeColor = Color.Black;
            gridDepartments.HeaderStyle.BackColor = Color.Silver;
            gridDepartments.HeaderStyle.Font.Underline = true;

            gridDepartments.AllowPaging = false;
            gridDepartments.AllowSorting = false;
            gridDepartments.DataBind();
            gridDepartments.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }

        void releaseObject(object obj)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            exportToExcel();
        }

        protected void gridDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["SelectedDepartment"] = gridDepartments.SelectedRow.Cells[1].Text.Trim();
            Response.Redirect("departments-view-employees.aspx");
        }
    }
}