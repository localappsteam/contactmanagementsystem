﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Globalization;
using System.Net.Mail;
using System.Net;
using System.Configuration;


namespace Contact
{
    public class access
    {
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------DATABASE ACCESS FUNCTIONS-----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        //DEFAULT DECLARATIONS AND MAIN METHODS ###############################################################################################################
        //MS SQL SERVER
        private static String sqlConString = ConfigurationManager.ConnectionStrings["EmployeeContactConnectionString"].ConnectionString;
        private static SqlConnection sqlconnection;
        private SqlDataAdapter sqlda = null;
        private SqlCommand sqlcommand = null;
        private DataSet sqlds = null;

        //ORACLE
        private static String oracleConString = "Provider=OraOLEDB.Oracle;Data Source=NGXVOS02;User Id=standby;Password=standby;";
        private static OleDbConnection oracleconnection;
        private OleDbCommand oraclecommand = null;
        private OleDbDataReader oracledr = null;
        private DataSet oracleds = null;

        public access()
        {
            sqlconnection = new SqlConnection();
            sqlconnection.ConnectionString = sqlConString;
            sqlda = new SqlDataAdapter();
            sqlcommand = new SqlCommand();
            sqlds = new DataSet();

            oracleconnection = new OleDbConnection();
            oracleconnection.ConnectionString = oracleConString;
            oracledr = null;
            oraclecommand = new OleDbCommand();
            oracleds = new DataSet();
        }

        //SQL COMMANDS#####################################################################################################################
        private void runSqlCommand()
        {
            try
            {
                sqlconnection.Open();
            }
            catch (Exception)
            {
                sqlconnection.Close();
                sqlconnection.Open();
            }
            sqlcommand.ExecuteNonQuery();
            sqlconnection.Close();
        }

        public DataSet selectSqlCommand()
        {
            try
            {
                sqlconnection.Open();
            }
            catch (Exception)
            {
                sqlconnection.Close();
                sqlconnection.Open();
            }

            sqlda = new SqlDataAdapter(sqlcommand);
            sqlda.Fill(sqlds);
            sqlconnection.Close();

            return sqlds;
        }
        //END SQL COMMANDS#####################################################################################################################

        //ORACLE COMMANDS#########################################################################################################################
        private void runOracleCommand()
        {
            try
            {
                oracleconnection.Open();
            }
            catch (Exception)
            {
                oracleconnection.Close();
                oracleconnection.Open();
            }
            oraclecommand.ExecuteNonQuery();
            oracleconnection.Close();
        }

        public DataSet selectOracleCommand()
        {
            try
            {
                oracleconnection.Open();
            }
            catch (Exception)
            {

                oracleconnection.Close();
                oracleconnection.Open();
            }

            oracledr = oraclecommand.ExecuteReader();
            oracleds.Tables.Add("TABLE0");
            oracleds.Tables[0].Load(oracledr);

            oracleconnection.Close();

            return oracleds;
        }

        //END ORACLE COMMANDS####################################################################################################################################################


        // ------------------------------------------------------END DATABASE ACCESS FUNCIONS-------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ AUTHENTICATION -------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


        public DataSet getUserLogged(String ADUserName)
        {
            sqlcommand = new SqlCommand("sp_Contact_GetUserLogged", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@ADUserName", ADUserName);

            return selectSqlCommand();
        }

        public Boolean hasRoles(String UserName, String RoleName)
        {
            sqlcommand = new SqlCommand("sp_Contact_AuthenticateUser", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@ADUserName", UserName);
            sqlcommand.Parameters.AddWithValue("@RoleName", RoleName);

            DataSet result = selectSqlCommand();

            if (result.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean checkRoles(String ADUserName, int SystemRoleID, int SystemAdminRoleID)
        {
            sqlcommand = new SqlCommand("sp_Contact_CheckUserRoles", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@ADUserName", ADUserName);
            sqlcommand.Parameters.AddWithValue("@SystemRoleID", SystemRoleID);
            sqlcommand.Parameters.AddWithValue("@SystemAdminRoleID", SystemAdminRoleID);

            DataSet result = selectSqlCommand();

            if (result.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // ------------------------------------------------------END AUTHENTICATION -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ FORMATING AND CONVERTING-----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        public static String convertToDateString(String Date)
        {
            string[] DateTimeformats = { "dd/MM/yyyy", "yyyy-MM-dd" };
            DateTime value = DateTime.ParseExact(Date, DateTimeformats, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None);

            return value.ToString("yyyy-MM-dd");
        }

        public static String convertToDateTimeString(String Datetime)
        {
            string[] DateTimeformats = { "dd/MM/yyyy HH:mm", "yyyy-MM-dd HH:mm" };
            DateTime value = DateTime.ParseExact(Datetime, DateTimeformats, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None);
            return value.ToString("yyyy-MM-dd HH:mm");
        }

        public static Boolean isDate(String Date)
        {
            DateTime dd;
            string[] DateTimeformats = { "dd/MM/yyyy", "yyyy-MM-dd" };
            return DateTime.TryParseExact(Date, DateTimeformats, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None, out dd);
        }

        public static Boolean isDateTime(String Datetime)
        {
            DateTime dd;
            string[] DateTimeformats = { "dd/MM/yyyy HH:mm", "yyyy-MM-dd HH:mm" };
            return DateTime.TryParseExact(Datetime, DateTimeformats, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None, out dd);
        }

        // ------------------------------------------------------ END FORMATING AND CONVERTING-----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ MILLS -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        public DataSet viewMills(String SearchValue)
        {
            sqlcommand = new SqlCommand("sp_Contact_ViewMills", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SearchValue", SearchValue + " ");

            return selectSqlCommand();
        }

        public DataSet GetMill(String MillName)
        {
            sqlcommand = new SqlCommand("sp_Contact_GetMill", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MillName", MillName.Trim());

            return selectSqlCommand();
        }

        // ------------------------------------------------------ END MILLS -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ EMPLOYEES  -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        public DataSet getSelectedEmployeeDetails(String EmployeeID)
        {
            sqlcommand = new SqlCommand("sp_Contact_EmployeeSearch_Details", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);

            return selectSqlCommand();
        }

        public void employeeDataSync(int MillID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Employees_DataSynchronize", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);

            runSqlCommand();
        }

        public void editEmployeeDetails(String SystemUsername, int EmployeeID, String Surname, String Firstnames, String Nickname, String Email,
            String Cell, String OfficeNumber, String Ext, String SpeedDial, String HomeAddress, String Manager)
        {
            sqlcommand = new SqlCommand("sp_Contact_Employees_EditDetails", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@Surname", Surname);
            sqlcommand.Parameters.AddWithValue("@Firstnames", Firstnames);
            sqlcommand.Parameters.AddWithValue("@Nickname", Nickname);
            sqlcommand.Parameters.AddWithValue("@Email", Email);
            sqlcommand.Parameters.AddWithValue("@Cell", Cell);
            sqlcommand.Parameters.AddWithValue("@OfficeNumber", OfficeNumber);
            sqlcommand.Parameters.AddWithValue("@Ext", Ext);
            sqlcommand.Parameters.AddWithValue("@SpeedDial", SpeedDial);
            sqlcommand.Parameters.AddWithValue("@HomeAddress", HomeAddress);
            sqlcommand.Parameters.AddWithValue("@Manager", Manager);


            runSqlCommand();
        }
        // ------------------------------------------------------ END EMPLOYEES -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ CONTRACTORS  -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        public DataSet getSelectedContractorDetails(String ContractorID)
        {
            sqlcommand = new SqlCommand("sp_Contact_ContractorSearch_Details", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@ContractorID", ContractorID);

            return selectSqlCommand();
        }

        public void addNewContractor(String SystemUsername, String Surname, String Firstnames, String Initials, String CompanyNumber, String HomeAddress
            , String Cell, String ADUsername, String EXT, String SpeedDial, String DecPhone, String Occupation, String Company
            , String Manager, String Email, int MillID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Contractors_Add", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@Surname", Surname);
            sqlcommand.Parameters.AddWithValue("@Firstnames", Firstnames);
            sqlcommand.Parameters.AddWithValue("@Initials", Initials);
            sqlcommand.Parameters.AddWithValue("@CompanyNumber", CompanyNumber);
            sqlcommand.Parameters.AddWithValue("@HomeAddress", HomeAddress);
            sqlcommand.Parameters.AddWithValue("@Cell", Cell);
            sqlcommand.Parameters.AddWithValue("@ADUsername", ADUsername);
            sqlcommand.Parameters.AddWithValue("@EXT", EXT);
            sqlcommand.Parameters.AddWithValue("@DecPhone", DecPhone);
            sqlcommand.Parameters.AddWithValue("@Occupation", Occupation);
            sqlcommand.Parameters.AddWithValue("@SpeedDial", SpeedDial);
            sqlcommand.Parameters.AddWithValue("@Company", Company);
            sqlcommand.Parameters.AddWithValue("@Manager", Manager);
            sqlcommand.Parameters.AddWithValue("@Email", Email);
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);

            runSqlCommand();
        }

        public void editContractorDetails(String SystemUsername, int ContractorID, String Surname, String Firstnames,
            String Initials, String Username, String Email, String Cell, String OfficeNumber, String Ext, String SpeedDial, String HomeAddress,
            String Company, String Title, String CompanyNumber, String Manager)
        {
            sqlcommand = new SqlCommand("sp_Contact_Contractors_EditDetails", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@ContractorID", ContractorID);
            sqlcommand.Parameters.AddWithValue("@Surname", Surname);
            sqlcommand.Parameters.AddWithValue("@Firstnames", Firstnames);
            sqlcommand.Parameters.AddWithValue("@Initials", Initials);
            sqlcommand.Parameters.AddWithValue("@Username", Username);
            sqlcommand.Parameters.AddWithValue("@Email", Email);
            sqlcommand.Parameters.AddWithValue("@Cell", Cell);
            sqlcommand.Parameters.AddWithValue("@OfficeNumber", OfficeNumber);
            sqlcommand.Parameters.AddWithValue("@Ext", Ext);
            sqlcommand.Parameters.AddWithValue("@SpeedDial", SpeedDial);
            sqlcommand.Parameters.AddWithValue("@HomeAddress", HomeAddress);
            sqlcommand.Parameters.AddWithValue("@Company", Company);
            sqlcommand.Parameters.AddWithValue("@Title", Title);
            sqlcommand.Parameters.AddWithValue("@CompanyNumber", CompanyNumber);
            sqlcommand.Parameters.AddWithValue("@Manager", Manager);


            runSqlCommand();
        }

        public void deleteContractor(String SystemUsername, int ContractorID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Contractors_Delete", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@ContractorID", ContractorID);

            runSqlCommand();
        }

        public void deleteTempBulkAddContractors(Int64 UploadedByEmployeeID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Contractors_DeleteTempAdd", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@UploadedByEmployeeID", UploadedByEmployeeID);

            runSqlCommand();
        }

        public void bulkAddContractorToTemp(String UploadedByEmployeeID, String Surname, String Firstnames, String Initials, String CompanyNumber, String HomeAddress, String PIGEONHOLE, String Bleeper, String TelHome, String Cell, String ADUsername, String EXT, String DecPhone, String Occupation, String SpeedDial, String Company, String Manager, String Email, String Mill)
        {
            sqlcommand = new SqlCommand("sp_Contact_Contractors_AddToTemp", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@UploadedByEmployeeID", UploadedByEmployeeID);
            sqlcommand.Parameters.AddWithValue("@Surname", Surname);
            sqlcommand.Parameters.AddWithValue("@Firstnames", Firstnames);
            sqlcommand.Parameters.AddWithValue("@Initials", Initials);
            sqlcommand.Parameters.AddWithValue("@CompanyNumber", CompanyNumber);
            sqlcommand.Parameters.AddWithValue("@HomeAddress", HomeAddress);
            sqlcommand.Parameters.AddWithValue("@PIGEONHOLE", PIGEONHOLE);
            sqlcommand.Parameters.AddWithValue("@Bleeper", Bleeper);
            sqlcommand.Parameters.AddWithValue("@TelHome", TelHome);
            sqlcommand.Parameters.AddWithValue("@Cell", Cell);
            sqlcommand.Parameters.AddWithValue("@ADUsername", ADUsername);
            sqlcommand.Parameters.AddWithValue("@EXT", EXT);
            sqlcommand.Parameters.AddWithValue("@DecPhone", DecPhone);
            sqlcommand.Parameters.AddWithValue("@Occupation", Occupation);
            sqlcommand.Parameters.AddWithValue("@SpeedDial", SpeedDial);
            sqlcommand.Parameters.AddWithValue("@Company", Company);
            sqlcommand.Parameters.AddWithValue("@Manager", Manager);
            sqlcommand.Parameters.AddWithValue("@Email", Email);
            sqlcommand.Parameters.AddWithValue("@Mill", Mill);

            runSqlCommand();
        }

        public void bulkAddContractors(Int64 EmployeeID, String UserName)
        {
            sqlcommand = new SqlCommand("sp_Contact_Contractors_BulkAdd", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@UserName", UserName);

            runSqlCommand();
        }


        // ------------------------------------------------------ END CONTRACTORS -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ EMERGENCY  -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        public DataSet getSelectedEmergencyDetails(String EmergencyContactID)
        {
            sqlcommand = new SqlCommand("sp_Contact_EmergencySearch_Details", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@EmergencyContactID", EmergencyContactID);

            return selectSqlCommand();
        }


        public void addNewEmgergencyContact(String SystemUsername, String Name, String EXT,
            String Phone, String Title,
           int MillID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Emergency_Add", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
          
            sqlcommand.Parameters.AddWithValue("@Name", Name);
            sqlcommand.Parameters.AddWithValue("@EXT", EXT);
            sqlcommand.Parameters.AddWithValue("@Phone", Phone);
            sqlcommand.Parameters.AddWithValue("@Title", Title);
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
          

            runSqlCommand();
        }

        public void editEmergencyDetails(String SystemUsername, int EmergencyContactID, String Name,
             String Phone, String Ext, String Title)
        {
            sqlcommand = new SqlCommand("sp_Contact_Emergency_EditDetails", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@EmergencyContactID", EmergencyContactID);   
            sqlcommand.Parameters.AddWithValue("@Name", Name);         
            sqlcommand.Parameters.AddWithValue("@Phone", Phone);     
            sqlcommand.Parameters.AddWithValue("@Ext", Ext);     
            sqlcommand.Parameters.AddWithValue("@Title", Title);
            

            runSqlCommand();
        }

        public void deleteEmergencyContact(String SystemUsername, int EmergencyContactID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Emergency_Delete", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@EmergencyContactID", EmergencyContactID);

            runSqlCommand();
        }


        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ END EMERGENCY  -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$





        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ STANDBY -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        public void autoUpdateStandbyList()
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_UpdateStandbyListChanges", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;

            runSqlCommand();
        }

        public DataSet getStandyListDates(String Date)
        {
            sqlcommand = new SqlCommand("sp_Contact_StandBy_GetDates", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@Date", Date);
            return selectSqlCommand();
        }

        public DataSet getPrimaryUpdaters(int MillID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Updaters_ViewPrimary", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            return selectSqlCommand();

        }
        public DataSet getStandyListEmergencyNumbers(int MillID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_GetEmergencyNumbers", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            return selectSqlCommand();
        }
        public DataSet viewStandyListDepartment(int MillID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_ViewStandbyListDepartments", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            return selectSqlCommand();
        }

        public DataSet viewStandyListContactPersons(int MillID, int DepartmentID, String Date)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_ViewList", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            sqlcommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);
            sqlcommand.Parameters.AddWithValue("@Date", Date);
            return selectSqlCommand();
        }

        public void addEmployeeToStandbyList(String SystemUsername, Int64 EmployeeID, String EmpType, int MillID, String Date, String Time, int StandbyDutyID, int WeeklyRecurrence)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_AddEmployeeToList", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@EmpType", EmpType);
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            sqlcommand.Parameters.AddWithValue("@Date", Date);
            sqlcommand.Parameters.AddWithValue("@Time", Time);
            sqlcommand.Parameters.AddWithValue("@WeeklyRecurrence", WeeklyRecurrence);
            sqlcommand.Parameters.AddWithValue("@StandbyDutyID", StandbyDutyID);

            runSqlCommand();
        }

        public DataSet getSelectedStandbyPerson(Int64 StandbyID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_GetSelectedPerson", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@StandbyID", StandbyID);

            return selectSqlCommand();
        }

        public void deleteStandby(String SystemUsername, Int64 StandbyID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_DeleteStandby", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@StandbyID", StandbyID);

            runSqlCommand();
        }

        public DataSet getStandbyScheduleDates(int MillID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_SelectSchedule", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            return selectSqlCommand();
        }

        public void updateRecurrence(String SystemUsername, Int64 StandbyID, int Recurrence)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_UpdateRecurrence", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@StandbyID", StandbyID);
            sqlcommand.Parameters.AddWithValue("@Recurrence", Recurrence);

            runSqlCommand();
        }

        public String updateStartingDate(String SystemUsername, Int64 StandbyID, String Date, String Time)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_UpdateStartingDate", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@StandbyID", StandbyID);
            sqlcommand.Parameters.AddWithValue("@Date", Date);
            sqlcommand.Parameters.AddWithValue("@Time", Time);

            return selectSqlCommand().Tables[0].Rows[0]["StartDate"].ToString().Trim();
        }

        public void changeStandbyPerson(String SystemUsername, Int64 StandbyID, Int64 EmployeeID, String EmpType)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_ChangePerson", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@StandbyID", StandbyID);
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@EmpType", EmpType);

            runSqlCommand();
        }

        public void swapStandbyPerson(String SystemUsername, Int64 StandbyID1, Int64 StandbyID2, int ApplyToSeries)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_UpdateList_Swap", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@StandbyID1", StandbyID1);
            sqlcommand.Parameters.AddWithValue("@StandbyID2", StandbyID2);
            sqlcommand.Parameters.AddWithValue("@ApplyToSeries", ApplyToSeries);

            runSqlCommand();
        }

        public Boolean checkIfStandbyUpdaterExist(Int64 EmployeeID, String EmpType)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Updaters_CheckExist", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@EmpType", EmpType);

            if (selectSqlCommand().Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addNewStandbyUpdater(String SystemUsername, Int64 EmployeeID, String EmpType, int DepartmentID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Updaters_AddNew", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@EmpType", EmpType);
            sqlcommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);

            runSqlCommand();
        }

        public void updateStandbyUpdaterSection(String SystemUsername, int StandbyUpdaterID, int SectionID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Updaters_UpdateSection", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@StandbyUpdaterID", StandbyUpdaterID);
            sqlcommand.Parameters.AddWithValue("@SectionID", SectionID);

            runSqlCommand();
        }

        public void deleteStandbyUpdater(String SystemUsername, int StandbyUpdaterID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Updaters_Delete", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@StandbyUpdaterID", StandbyUpdaterID);

            runSqlCommand();
        }

        public void addStandbySection(String SystemUsername, int MillID, String Description, int SortingOrder)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Sections_Add", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            sqlcommand.Parameters.AddWithValue("@Description", Description);
            sqlcommand.Parameters.AddWithValue("@SortingOrder", SortingOrder);

            runSqlCommand();
        }

        public void editStandbySection(String SystemUsername, int SectionID, String Description, int SortingOrder)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Sections_Edit", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@SectionID", SectionID);
            sqlcommand.Parameters.AddWithValue("@Description", Description);
            sqlcommand.Parameters.AddWithValue("@SortingOrder", SortingOrder);

            runSqlCommand();
        }

        public void deleteStandbySection(String SystemUsername, int SectionID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Sections_Delete", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@SectionID", SectionID);

            runSqlCommand();
        }

        public Boolean checkExistStandbyDuty(String DutyNo, int DepartmentID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Duties_Add_CheckExist", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@DutyNo", DutyNo);
            sqlcommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);

            if (selectSqlCommand().Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addStandbyDuty(String SystemUsername, String DutyNo, String Description, String Bleeper, int DepartmentID, String DutyContactCellphone, int ShowTelephoneNumber, int ActiveOnList)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Duties_Add", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@DutyNo", DutyNo);
            sqlcommand.Parameters.AddWithValue("@Description", Description);
            sqlcommand.Parameters.AddWithValue("@ActiveOnList", ActiveOnList);
            sqlcommand.Parameters.AddWithValue("@ShowTelephoneNumber", ShowTelephoneNumber);

            if (Bleeper == String.Empty)
            {
                sqlcommand.Parameters.AddWithValue("@Bleeper", DBNull.Value);
            }
            else
            {
                sqlcommand.Parameters.AddWithValue("@Bleeper", Bleeper);
            }

            if (DutyContactCellphone == String.Empty)
            {
                sqlcommand.Parameters.AddWithValue("@DutyContactCellphone", DBNull.Value);
            }
            else
            {
                sqlcommand.Parameters.AddWithValue("@DutyContactCellphone", DutyContactCellphone);
            }

            sqlcommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);


            runSqlCommand();

        }

        public DataSet getSelectStandbyDuty(int DutyID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Duties_GetSelected", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@DutyID", DutyID);

            return selectSqlCommand();
        }

        public void editStandbyDuty(String SystemUsername, int DutyID, String DutyNo, String Description, String Bleeper, int DepartmentID, String DutyContactCellphone, int ShowTelephoneNumber, int ActiveOnList)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Duties_Edit", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@DutyID", DutyID);
            sqlcommand.Parameters.AddWithValue("@DutyNo", DutyNo);
            sqlcommand.Parameters.AddWithValue("@Description", Description);
            sqlcommand.Parameters.AddWithValue("@ActiveOnList", ActiveOnList);
            sqlcommand.Parameters.AddWithValue("@ShowTelephoneNumber", ShowTelephoneNumber);

            if (Bleeper == String.Empty)
            {
                sqlcommand.Parameters.AddWithValue("@Bleeper", DBNull.Value);
            }
            else
            {
                sqlcommand.Parameters.AddWithValue("@Bleeper", Bleeper);
            }

            if (DutyContactCellphone == String.Empty)
            {
                sqlcommand.Parameters.AddWithValue("@DutyContactCellphone", DBNull.Value);
            }
            else
            {
                sqlcommand.Parameters.AddWithValue("@DutyContactCellphone", DutyContactCellphone);
            }

            sqlcommand.Parameters.AddWithValue("@DepartmentID", DepartmentID);

            runSqlCommand();
        }

        public DataSet deleteStandbyDuty(String SystemUsername, int DutyID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_Duties_Delete", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@DutyID", DutyID);

            return selectSqlCommand();
        }

        public DataSet getStandbySchedules(int MillID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Standby_GetSchedules", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);

            return selectSqlCommand();
        }

        // ------------------------------------------------------ END STANDBY -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ CALLOUT -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


        public String getEmployeeIDonStandby(Int64 StandbyID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_EmployeeIDonStandby", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@StandbyID", StandbyID);

            return selectSqlCommand().Tables[0].Rows[0]["EmployeeID"].ToString();
        }

        public int createCallout(String SystemUsername, int MillID, int EmployeeID, int EmployeeTypeID, int StandbyAreaID, String Reason, int ContactID, int ContactEmpTypeID
            ,int AuthorisedByID, int AuthorisedByEmpTypeID, String Notification, int ParentCalloutID, int StandbyID, int StandbyIncorrect, int CalloutTypeID, String IncidentTime)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_Add", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@EmployeeTypeID", EmployeeTypeID);
            sqlcommand.Parameters.AddWithValue("@StandbyAreaID", StandbyAreaID);
            sqlcommand.Parameters.AddWithValue("@Reason", Reason);
            sqlcommand.Parameters.AddWithValue("@ContactID", ContactID);
            sqlcommand.Parameters.AddWithValue("@ContactEmpTypeID", ContactEmpTypeID);
            sqlcommand.Parameters.AddWithValue("@AuthorisedByID", AuthorisedByID);
            sqlcommand.Parameters.AddWithValue("@AuthorisedByEmpTypeID", AuthorisedByEmpTypeID);
            if(!String.IsNullOrEmpty(Notification))
            {
                sqlcommand.Parameters.AddWithValue("@Notification", Notification);
            }
            else
            {
                sqlcommand.Parameters.AddWithValue("@Notification", DBNull.Value);
            }

            if (ParentCalloutID > 0)
            {
                sqlcommand.Parameters.AddWithValue("@ParentCalloutID", ParentCalloutID);
            }
            else
            {
                sqlcommand.Parameters.AddWithValue("@ParentCalloutID", DBNull.Value);
            }

            sqlcommand.Parameters.AddWithValue("@StandbyID", StandbyID);
            sqlcommand.Parameters.AddWithValue("@StandbyIncorrect", StandbyIncorrect);
            sqlcommand.Parameters.AddWithValue("@CalloutTypeID", CalloutTypeID);

            if (IncidentTime != "1999-01-01 00:00")
            {
                sqlcommand.Parameters.AddWithValue("@IncidentTime", IncidentTime);
            }
            else
            {
                sqlcommand.Parameters.AddWithValue("@IncidentTime", DBNull.Value);
            }

            return Convert.ToInt32(selectSqlCommand().Tables[0].Rows[0][0].ToString());

        }

        public void cancelCallout(String SystemUsername, int CalloutID, String Reason)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_CancelCallout", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);
            sqlcommand.Parameters.AddWithValue("@Reason", Reason);

            runSqlCommand();
        }

        public DataSet getActiveCalloutsOnSite(int CalloutID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_ViewOtherActiveCallouts", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);

            return selectSqlCommand();
        }

        public DataSet getCalloutProcessInfo(int CalloutID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_ViewCalloutProcessDetails", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);

            return selectSqlCommand();
        }
        public void saveCalloutAttempt(String SystemUsername, int CalloutID, String DateTimeContacted)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_UpdateCallout_PersonCalled", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);
            sqlcommand.Parameters.AddWithValue("@DateTimeContacted", DateTimeContacted);

            runSqlCommand();
        }

        public void saveCalloutAttempt(String SystemUsername, int CalloutID, int OptionID, String OptionDescription, String DateTimeContacted)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_AddCalloutAttempt", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);
            sqlcommand.Parameters.AddWithValue("@OptionID", OptionID);
            sqlcommand.Parameters.AddWithValue("@OptionDescription", OptionDescription);
            sqlcommand.Parameters.AddWithValue("@DateTimeContacted", DateTimeContacted);

            runSqlCommand();
        }

        public void savePersonOnsite(String SystemUsername, int CalloutID, String TimeOnsite)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_UpdateCallout_PersonArrived", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);
            sqlcommand.Parameters.AddWithValue("@TimeOnsite", TimeOnsite);

            runSqlCommand();
        }

        public DataSet getCalloutPdfData(int CalloutID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_GetPdfValues", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);
            
            return selectSqlCommand();
        }

        public void closeCallout(String SystemUsername, int CalloutID, String DateTimeClosed)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_UpdateCallout_Close", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);
            sqlcommand.Parameters.AddWithValue("@DateTimeClosed", DateTimeClosed);

            runSqlCommand();
        }

        public DataSet viewCalloutDetails(int MillID, int CalloutID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_ViewCalloutDetails", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);

            return selectSqlCommand();
        }

        public DataSet generateApprovalRequest(int CalloutID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_CreateApprovalRequest", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);

            return selectSqlCommand();
        }
        public DataSet viewApprovaRequestDetails(int CalloutID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_ViewApprovalRequest", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);

            return selectSqlCommand();
        }

        public void approveCallout(int CalloutID, int UniqueCode, int Type, String SystemUserName)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_ApproveCallout", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);
            sqlcommand.Parameters.AddWithValue("@UniqueCode", UniqueCode);
            sqlcommand.Parameters.AddWithValue("@Type", Type);
            sqlcommand.Parameters.AddWithValue("@SystemUserName", SystemUserName);

            runSqlCommand();
        }

        public void updateCallout(String SystemUsername, int CalloutID, String PersonCalledOutID, int StandbyAreaID, String ContactPersonID, 
            String AuthorisedByID, String Notification, int CalloutTypeID, String IncidentTime, String Reason)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_Update", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@CalloutID", CalloutID);
            sqlcommand.Parameters.AddWithValue("@PersonCalledOutID", @PersonCalledOutID);
            sqlcommand.Parameters.AddWithValue("@StandbyAreaID", StandbyAreaID);
            sqlcommand.Parameters.AddWithValue("@ContactID", ContactPersonID);
            sqlcommand.Parameters.AddWithValue("@AuthorisedByID", AuthorisedByID);
            if (!String.IsNullOrEmpty(Notification))
            {
                sqlcommand.Parameters.AddWithValue("@Notification", Notification);
            }
            else
            {
                sqlcommand.Parameters.AddWithValue("@Notification", DBNull.Value);
            }
            sqlcommand.Parameters.AddWithValue("@CalloutTypeID", CalloutTypeID);
            sqlcommand.Parameters.AddWithValue("@IncidentTime", IncidentTime);
            sqlcommand.Parameters.AddWithValue("@Reason", Reason);

            runSqlCommand();
        }

        public DataSet viewCalloutsFullReportPdf(int MillID, String SqlFilter)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_ViewCalloutReport", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            sqlcommand.Parameters.AddWithValue("@SqlFilter", SqlFilter);

            return selectSqlCommand();
        }

        public DataSet viewCalloutsSummaryReportPdf(int MillID, String SqlFilter)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_ViewCalloutSummaryReport", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            sqlcommand.Parameters.AddWithValue("@SqlFilter", SqlFilter);

            return selectSqlCommand();
        }

        public DataSet viewCalloutsReportExcel(int MillID, String SqlFilter, String Sp)
        {
            String olbConStr = "Provider=SQLNCLI11;"+ConfigurationManager.ConnectionStrings["EmployeeContactConnectionString"].ConnectionString;

            DataSet tblDs = new DataSet("Report_DataSet");
            DataTable dt = new DataTable("Report_DataTable");

            //Set the locale for each
            tblDs.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
            tblDs.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;

            //Open a DB connection (in this example with OleDB)
            OleDbConnection OlbCon = new OleDbConnection(olbConStr);
            try
            {
                OlbCon.Open();
            }
            catch (Exception)
            {
                OlbCon.Close();
                OlbCon.Open();
            }

            //Create a query and fill the data table with the data from the DB
            OleDbCommand cmd = new OleDbCommand(Sp, OlbCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MillID", MillID);
            cmd.Parameters.AddWithValue("@SqlFilter", SqlFilter);

            OleDbDataAdapter adptr = new OleDbDataAdapter(cmd);
            adptr.Fill(dt);
            OlbCon.Close();

            //Add the table to the data set
            tblDs.Tables.Add(dt);

            return tblDs;
        }

        public void addArea(String SystemUsername, int MillID, String Description, String Ext)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_Areas_Add", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            sqlcommand.Parameters.AddWithValue("@Description", Description);
            sqlcommand.Parameters.AddWithValue("@Ext", Ext);

            runSqlCommand();
        }

        public void updateArea(String SystemUsername, int StandbyAreaID, String Description, String Ext)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_Areas_Edit", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@StandbyAreaID", StandbyAreaID);
            sqlcommand.Parameters.AddWithValue("@Description", Description);
            sqlcommand.Parameters.AddWithValue("@Ext", Ext);

            runSqlCommand();
        }

        public void deleteArea(String SystemUsername, int StandbyAreaID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_Areas_Delete", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@StandbyAreaID", StandbyAreaID);

            runSqlCommand();
        }

        public void addCalloutType(String SystemUsername, String Description)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_Types_Add", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@Description", Description);

            runSqlCommand();
        }

        public void updateCalloutType(String SystemUsername, int CalloutTypeID, String Description)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_Types_Edit", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@CalloutTypeID", CalloutTypeID);
            sqlcommand.Parameters.AddWithValue("@Description", Description);

            runSqlCommand();
        }

        public void deleteCalloutType(String SystemUsername, int CalloutTypeID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Callout_Types_Delete", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@CalloutTypeID", CalloutTypeID);

            runSqlCommand();
        }

        // ------------------------------------------------------ END CALLOUT -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ MESSAGING -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        public DataSet messagingGetContact(String EmployeeNumber)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_GetContacts", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@EmployeeNumber", EmployeeNumber);

            return selectSqlCommand();
        }

        public void saveSentMessage(String Message, String Cellphone, Int64 EmployeeID, String Status)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_SentMessages_Add", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@Message", Message);
            sqlcommand.Parameters.AddWithValue("@Cellphone", Cellphone);
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@Status", Status);

            runSqlCommand();
        }

        public DataSet viewMessagingGroups(Int64 EmployeeID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_ViewGroups", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);

            return selectSqlCommand();
        }

        public int createMessagingGroup(String Description, Int64 EmployeeID, int isCustom)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_CreateGroup", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@Description", Description);
            sqlcommand.Parameters.AddWithValue("@CreatedByEmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@isCustom", isCustom);

            return Convert.ToInt32(selectSqlCommand().Tables[0].Rows[0][0].ToString());
        }

        public void addMessagingGroupMembers(int MessagingGroupID, Int64 EmployeeID, int isEmployee)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_AddGroupMembers", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MessagingGroupID", MessagingGroupID);
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@isEmployee", isEmployee);

            runSqlCommand();
        }

        public void changeMessagingGroupName(int MessagingGroupID, String Description)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_ChangeGroupName", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MessagingGroupID", MessagingGroupID);
            sqlcommand.Parameters.AddWithValue("@Description", Description);

            runSqlCommand();
        }

        public void addCustomMessagingGroupMembers(String Firstname, String Surname, String Cellphone, int MessagingGroupID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_AddCustomGroupMembers", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@Firstname", Firstname);
            sqlcommand.Parameters.AddWithValue("@Surname", Surname);
            sqlcommand.Parameters.AddWithValue("@Cellphone", Cellphone);
            sqlcommand.Parameters.AddWithValue("@MessagingGroupID", MessagingGroupID);

            runSqlCommand();
        }

        public void removeMessagingGroupMembers(int MessagingGroupID, Int64 EmployeeID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_RemoveGroupMembers", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MessagingGroupID", MessagingGroupID);
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);

            runSqlCommand();
        }
        public void removeCustomMessagingGroupMembers(Int64 MessagingGroupMemberID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_RemoveCustomGroupMembers", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MessagingGroupMemberID", MessagingGroupMemberID);

            runSqlCommand();
        }


        public DataSet getMessagingGroupRecipients(int MessagingGroupID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_GetMessagingGroupRecipients", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MessagingGroupID", MessagingGroupID);

            return selectSqlCommand();
        }

        public void deleteMessagingGroup(int MessagingGroupID)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_DeleteGroup", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@MessagingGroupID", MessagingGroupID);

            runSqlCommand();
        }

        public void sendSMS(String body)
        {

            String to = "sms@messaging.clickatell.com";
            String from = "no-reply@sappi.com";

            String username = ConfigurationManager.AppSettings["username"].ToString();
            String password = ConfigurationManager.AppSettings["password"].ToString();
            String smtp = ConfigurationManager.AppSettings["smtp"].ToString();
            int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"].ToString());

            MailMessage mail = new MailMessage(from, to, "Sappi Contact System", body);
            mail.From = new System.Net.Mail.MailAddress(from);

            mail.IsBodyHtml = false;

            SmtpClient client = new SmtpClient(smtp, port);
            client.Credentials = new NetworkCredential(username, password);
            client.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSSL"].ToString());
            client.Send(mail);
        }

        public void sendEmail(String to, String subject, String body, String DisplayName)
        {
            String from = "no-reply@sappi.com";

            String username = ConfigurationManager.AppSettings["username"].ToString();
            String password = ConfigurationManager.AppSettings["password"].ToString();
            String smtp = ConfigurationManager.AppSettings["smtp"].ToString();
            int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"].ToString());

            String[] recipient = to.Split(';');

            for (int i=0; i<recipient.Length; i++)
            {
                if (recipient[i].Trim() != String.Empty)
                {
                    MailMessage mail = new MailMessage(from, recipient[i].Trim(), subject, body);
                    mail.From = new System.Net.Mail.MailAddress(from, DisplayName);

                    mail.IsBodyHtml = true;

                    SmtpClient client = new SmtpClient(smtp, port);
                    client.Credentials = new NetworkCredential(username, password);
                    client.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSSL"].ToString());
                    client.Send(mail);
                }
            }

        }

        public String getUserEmail(String UserName)
        {
            sqlcommand = new SqlCommand("sp_Contact_Messaging_GetUserEmail", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@UserName", UserName);

            DataSet ds = selectSqlCommand();
          
            if(ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0]["Email"].ToString().Trim();
            }
            else
            {
                return "";
            }
        }

        // ------------------------------------------------------ END MESSAGING -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ ADMINISTRATION -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        public DataSet getUserRolesAssigned(String Username, int ApplicationID)
        {
            sqlcommand = new SqlCommand("sp_Contact_System_Users_GetRolesAssigned", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@Username", Username);
            sqlcommand.Parameters.AddWithValue("@ApplicationID", ApplicationID);

            return selectSqlCommand();
        }

        public void assignUserRoles(String SystemUsername, int UserID, int RoleID, int ApplicationID)
        {
            sqlcommand = new SqlCommand("sp_Contact_System_Users_AddRoles", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@UserID", UserID);
            sqlcommand.Parameters.AddWithValue("@RoleID", RoleID);
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@ApplicationID", ApplicationID);

            runSqlCommand();
        }
        public void removeUserRoles(String SystemUsername, Int64 UserRoleID)
        {
            sqlcommand = new SqlCommand("sp_Contact_System_Users_RemoveRoles", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@UserRoleID", UserRoleID);

            runSqlCommand();
        }

        public Boolean addUserCheckExist(Int64 EmployeeID, String ADUserName, int EmpTypeID)
        {
            sqlcommand = new SqlCommand("sp_Contact_System_Users_Add_CheckExist", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@ADUserName", ADUserName);
            sqlcommand.Parameters.AddWithValue("@EmpTypeID", EmpTypeID);

            if (selectSqlCommand().Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addUser(String SystemUsername, Int64 EmployeeID, String ADUserName, int EmpTypeID, int MillID, int ApplicationID)
        {
            sqlcommand = new SqlCommand("sp_Contact_System_Users_Add", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            sqlcommand.Parameters.AddWithValue("@ADUserName", ADUserName);
            sqlcommand.Parameters.AddWithValue("@EmpTypeID", EmpTypeID);
            sqlcommand.Parameters.AddWithValue("@MillID", MillID);
            sqlcommand.Parameters.AddWithValue("@ApplicationID", ApplicationID);

            runSqlCommand();        
        }

        public void deleteUser(String SystemUsername, int UserID, int ApplicationID)
        {
            sqlcommand = new SqlCommand("sp_Contact_System_Users_Delete", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@UserID", UserID);
            sqlcommand.Parameters.AddWithValue("@ApplicationID", ApplicationID);

            runSqlCommand();
        }

        public void updateUsername(String SystemUsername, int UserID, String NewUserName, int ApplicationID)
        {
            sqlcommand = new SqlCommand("sp_Contact_System_Users_EditUsername", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@SystemUsername", SystemUsername);
            sqlcommand.Parameters.AddWithValue("@UserID", UserID);
            sqlcommand.Parameters.AddWithValue("@NewUserName", NewUserName);
            sqlcommand.Parameters.AddWithValue("@ApplicationID", ApplicationID);

            runSqlCommand();
        }

        // ------------------------------------------------------ END ADMINISTRATION -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // ------------------------------------------------------ SYNC -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        public DataSet getUserNamesFromDB()
        {
            sqlcommand = new SqlCommand("sp_Contact_GetAllUserNames", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;

            return selectSqlCommand();
        }

        public DataSet getStaffWithoutCellphoneNumbers()
        {
            sqlcommand = new SqlCommand("sp_Contact_Sync_GetStaffWithoutCellphoneNumbers", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;

            return selectSqlCommand();
        }

        public void updateCellphoneNumber(int EmpTypeID, String UserName, String Cellphone)
        {
            sqlcommand = new SqlCommand("sp_Contact_Sync_UpdateCellphoneNumber", sqlconnection);
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.Parameters.AddWithValue("@EmpTypeID", EmpTypeID);
            sqlcommand.Parameters.AddWithValue("@UserName", UserName);
            sqlcommand.Parameters.AddWithValue("@Cellphone", Cellphone);

            runSqlCommand();
        }




        // ------------------------------------------------------ END SYNC -----------------------------------------------------------------------------------------------------
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


    }
}