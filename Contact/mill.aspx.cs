﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Security.Principal;

namespace Contact
{
    public partial class mill : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadMills();
           
        }

        void loadMills()
        {

            Session.RemoveAll();

            access obj = new access();

            DataSet ds = obj.viewMills(txtSearch.Text.Trim());

            int count = ds.Tables[0].Rows.Count;

            LinkButton[] btn = new LinkButton[count];
            Literal[] html = new Literal[count];

            for (int i = 0; i < count; i++)
            {

                html[i] = new Literal();
                html[i].Text = "<div class='menu-circle menu-circle-lg text-center'>";
                html[i].Text = html[i].Text + "<img src='images/coal-power-plant-icon.png' class='img-menu' />";
                html[i].Text = html[i].Text + "<p class='text-center m-t-10'>" + ds.Tables[0].Rows[i]["Description"].ToString().ToUpper() + "</p></div>";

                btn[i] = new LinkButton();
                btn[i].ID = ds.Tables[0].Rows[i]["MillID"].ToString();
                btn[i].ToolTip = ds.Tables[0].Rows[i]["Description"].ToString();
                btn[i].CssClass = "dash-link";
                btn[i].Style.Add("float", "left");
                btn[i].Controls.Add(html[i]);

                //ADD ONCLICK EVENT
                btn[i].Click += (s, e) => {

                    LinkButton lbtn = (LinkButton)s;

                    Session["MillID"] = lbtn.ID.ToString();
                    Session["SelectedOffice"] = lbtn.ToolTip.ToString();

                    Response.Redirect("index.aspx");
                };

                divMills.Controls.Add(btn[i]);

            }

            txtSearch.Focus();
        }
    }
}