﻿using System;
using System.Drawing;
using System.Web.UI.WebControls;

namespace Contact.messaging
{
    public partial class groups_create : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
            loadSelected();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {

                String UserName = Session["UserName"].ToString();
                int RoleID = 7;
                int AdminRoleID = 14;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }

            }
            else
            {
                Response.Redirect("messaging.aspx");
            }

        }

        void loadSelected()
        {
            for (int i = 0; i < gridEmployees.Rows.Count; i++)
            {
                if (gridEmployees.Rows[i].Cells[6].Text.Replace("&nbsp;", "").Trim() == String.Empty)
                {
                    ((Button)gridEmployees.Rows[i].Cells[0].Controls[0]).Enabled = false;
                    ((Button)gridEmployees.Rows[i].Cells[0].Controls[0]).Text = "------";
                    gridEmployees.Rows[i].ForeColor = Color.Silver;
                }
                else
                {
                    ((Button)gridEmployees.Rows[i].Cells[0].Controls[0]).Enabled = true;
                    ((Button)gridEmployees.Rows[i].Cells[0].Controls[0]).Text = "Select";
                    gridEmployees.Rows[i].ForeColor = ColorTranslator.FromHtml("#555555");
                }

                for (int j = 0; j < BulletedList1.Items.Count; j++)
                {

                    if (gridEmployees.Rows[i].Cells[1].Text.Trim() == BulletedList1.Items[j].Value.Trim())
                    {
                        ((Button)gridEmployees.Rows[i].Cells[0].Controls[0]).Text = "Remove";
                    }
                }
            }

        }

        protected void gridEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectEmployee();
        }


        void selectEmployee()
        {
            if (((Button)gridEmployees.SelectedRow.Cells[0].Controls[0]).Text.Trim() == "Select")
            {
                ListItem lt = new ListItem();

                if (gridEmployees.SelectedRow.Cells[8].Text.Trim().ToLower() == "employee")
                {
                    lt.Value = gridEmployees.SelectedRow.Cells[1].Text;
                }
                else
                {
                    lt.Value = '-' + gridEmployees.SelectedRow.Cells[1].Text;
                }

                lt.Text = gridEmployees.SelectedRow.Cells[2].Text + ", " + gridEmployees.SelectedRow.Cells[3].Text;
                BulletedList1.Items.Add(lt);
            }
            else
            {
                for (int i = 0; i < BulletedList1.Items.Count; i++)
                {
                    if (gridEmployees.SelectedRow.Cells[1].Text.Trim() == BulletedList1.Items[i].Value.Trim())
                    {
                        BulletedList1.Items.RemoveAt(i);
                        ((Button)gridEmployees.SelectedRow.Cells[0].Controls[0]).Text = "Select";
                    }
                }
            }

            loadSelected();
        }

        protected void gridEmployees_DataBound(object sender, EventArgs e)
        {
            loadSelected();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            submit();
        }

        void submit()
        {
            if (txtGroupName.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Group name is required.");
            }
            else if (BulletedList1.Items.Count < 1)
            {
                ShowAlert(2, "No recipients selected.");
            }
            else
            {
                //BEGIN SUBMITTION

                try
                {
                    access obj = new access();
                    int MessagingGroupID = obj.createMessagingGroup(txtGroupName.Text.Trim(), Convert.ToInt64(Session["EmployeeID"].ToString()), 0);

                    obj = new access();
                    String EmployeeID = "";

                    for (int i = 0; i < BulletedList1.Items.Count; i++)
                    {
                        EmployeeID = BulletedList1.Items[i].Value.Trim();
                        int isEmployee = 1;

                        if (EmployeeID.Contains("-"))
                        {
                            isEmployee = 0;
                        }
                        else
                        {
                            isEmployee = 1;
                        }

                        obj.addMessagingGroupMembers(MessagingGroupID, Convert.ToInt64(EmployeeID.Replace("-", "").Trim()), isEmployee);

                    }
                    Session["GroupCreated"] = true;
                    ShowAlert(1, "Group created successfully.");
                }
                catch (Exception ex)
                {
                    ShowAlert(2, ex.Message);
                }
            }
        }


        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["GroupCreated"] != null)
            {
                Session.Remove("GroupCreated");

                Response.Redirect("groups.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }
}