﻿<%@ Page Title="" Language="C#" MasterPageFile="~/messaging/massaging.Master" AutoEventWireup="true" CodeBehind="new-message.aspx.cs" Inherits="Contact.messaging.new_message" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Messaging / New message</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>New Message</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-2 btn-fix-width">
                                <a href="messaging.aspx" class="btn btn-default btn-sm btn-block">
                                    <p class="fa fa-reply m-r-10"></p>
                                    Go back</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cont-holder">
                            </div>
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-5">
                                        <h5>Options<span style="color: red; margin-left: 5px">*</span></h5>
                                        <asp:RadioButtonList ID="rdOptions" Width="100%" Style="min-width: 400px" AutoPostBack="true" RepeatDirection="Horizontal" runat="server" BorderColor="Red" OnSelectedIndexChanged="rdOptions_SelectedIndexChanged">
                                            <asp:ListItem Value="1" Text="Groups" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Individual"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Company number (File upload)"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="divGroups" runat="server" class="col-md-6">
                                        <h5>Group selection<span style="color: red; margin-left: 5px">*</span></h5>
                                        <asp:CheckBoxList ID="ckGroups" runat="server" DataSourceID="SqlDataSource1" DataTextField="Description" DataValueField="MessagingGroupID">
                                        </asp:CheckBoxList>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Messaging_SelectMessagingGroups" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="EmployeeID" SessionField="EmployeeID" Type="Decimal" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                        <a href="groups-types.aspx" class="font-12">Create new group?</a>
                                    </div>
                                    <div id="divIndividual" runat="server" visible="false" class="col-md-5">
                                        <h5>Phone number<span style="color: red; margin-left: 5px">*</span></h5>
                                        <asp:TextBox ID="txtPhoneNumber" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                        <p>
                                            <small>To add multiple numbers use a semicolone ';'. Example 0820008880;0761115550;0645621025</small>
                                        </p>
                                    </div>
                                    <div id="divFileUpload" runat="server" visible="false" class="col-md-12">
                                        <h5>Company number (File upload)<span style="color: red; margin-left: 5px">*</span></h5>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                                <asp:LinkButton ID="btnUpload" CssClass="btn btn-primary btn-sm m-t-10" Font-Size="X-Small" runat="server" OnClick="btnUpload_Click"><p class="fa fa-upload"></p> Upload</asp:LinkButton>
                                            </div>
                                            <div class="col-md-4">
                                               <a href="../templates/EmployeeNumbers.csv" download >Download template</a>
                                            </div>
                                        </div>
                                        <div id="divContactFind" runat="server" visible="false" style="margin-right: 0px; margin-left: 0px; width: 100%">
                                            <!-- Basic Table -->
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <div class="card">
                                                        <div class="header">
                                                            <h2>
                                                                <p class="text-success">Found:
                                                                    <asp:Label ID="lblCountFound" runat="server" Text="0"></asp:Label></p>
                                                            </h2>
                                                        </div>
                                                        <div class="body table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>COMPANY_NO#</th>
                                                                        <th>CELL</th>
                                                                        <th>FIRST NAMES</th>
                                                                        <th>NICKNAME</th>
                                                                        <th>SURNAME</th>
                                                                        <th>DEPARTMENT</th>
                                                                        <th>OFFICE</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="tblFoundContacts"  runat="server" enableviewstate="True">
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="card">
                                                        <div class="header">
                                                            <h2>
                                                                <p class="text-danger actions">Filtered out:
                                                                    <asp:Label ID="lblCountFiltered" runat="server" Text="0"></asp:Label></p>
                                                            </h2>
                                                        </div>
                                                        <div class="body table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>COMPANY_NO#</th>
                                                                        <th>STATUS</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="tblFilteredContacts" runat="server">
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- #END# Basic Table -->
                                            <!-- Striped Rows -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <h5>Message<span style="color: red; margin-left: 5px">*</span></h5>
                                        <asp:TextBox ID="txtMessage" CssClass="form-control" Placeholder="Type your message here..." ForeColor="Black" TextMode="MultiLine" Height="150px" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <asp:LinkButton ID="btnProceed" CssClass="btn btn-primary btn-block btn-sm m-t-10" runat="server" OnClick="btnProceed_Click"><p class="fa fa-check m-r-10"></p> Proceed</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
