﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.messaging
{
    public partial class groups_members_custom : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
            loadGroup();
        }
        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {

                String UserName = Session["UserName"].ToString();
                int RoleID = 7;
                int AdminRoleID = 14;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    btnDeleteGroup.Visible = false;
                    btnAddMembers.Visible = false;
                    gridMembers.Columns[0].Visible = false;
                    btnChangeGroupName.Visible = false;
                }
                else
                {
                    btnDeleteGroup.Visible = true;
                    btnAddMembers.Visible = true;
                    gridMembers.Columns[0].Visible = true;
                    btnChangeGroupName.Visible = true;
                }
            }
            else
            {
                Response.Redirect("messaging.aspx");
            }

        }

        void loadGroup()
        {
            if (Session["SelectedMessagingGroupID"] == null)
            {
                Response.Redirect("groups.aspx");
            }
            else
            {
                lblGroupName.Text = Session["SelectedMessagingGroupName"].ToString();
            }
        }

        protected void btnAddMembers_Click(object sender, EventArgs e)
        {
            showAddMembers();
        }

        void showAddMembers()
        {
            if (Session["UserName"] != null)
            {

                divAddmembers1.Visible = true;
                divAddmembers2.Visible = true;
                txtFirstname.Focus();

            }
            else
            {
                Response.Redirect("groups.aspx");
            }
        }


        protected void btnChangeGroupName_Click(object sender, EventArgs e)
        {
            divChangeName.Visible = true;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtChangeName.Text = string.Empty;
            divChangeName.Visible = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            changeGroupName();
        }

        void changeGroupName()
        {

            if (Session["SelectedMessagingGroupID"] != null)
            {
                int GroupID = Convert.ToInt32(Session["SelectedMessagingGroupID"].ToString());

                if (Session["UserName"] != null)
                {
                    if (txtChangeName.Text.Trim() == String.Empty)
                    {
                        ShowAlert(2, "Group name is required!");
                    }
                    else
                    {
                        access obj = new access();
                        obj.changeMessagingGroupName(GroupID, txtChangeName.Text.Trim());
                        Session["SelectedMessagingGroupName"] = txtChangeName.Text.Trim();
                        txtChangeName.Text = string.Empty;
                        divChangeName.Visible = false;
                        ShowAlert(1, "Group name changed successfully.");
                    }

                }
                else
                {
                    Response.Redirect("groups.aspx");
            }
        }
            else
            {
                Response.Redirect("groups.aspx");
            }
        }

        protected void btnCloseAdd_Click(object sender, EventArgs e)
        {
            hideAddmembers();
        }

        void hideAddmembers()
        {
            divAddmembers1.Visible = false;
            divAddmembers2.Visible = false;
        }

        protected void gridMembers_SelectedIndexChanged(object sender, EventArgs e)
        {
            removeMember();
        }

        void removeMember()
        {
            if (Session["UserName"] != null)
            {
                access obj = new access();

                Int64 MemberID = Convert.ToInt64(gridMembers.SelectedRow.Cells[1].Text.Trim());

                obj.removeCustomMessagingGroupMembers(MemberID);
                gridMembers.DataBind();

            }
            else
            {
                Response.Redirect("groups.aspx");
            }
        }


        protected void btnAddRecipient_Click(object sender, EventArgs e)
        {
            addMember();
        }

        void addMember()
        {
            if (Session["SelectedMessagingGroupID"] != null)
            {

                if (txtFirstname.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "First name(s) is required!");
                }
                else if (txtSurname.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Surname is required!");
                }
                else if (txtCellphone.Text.Trim() == String.Empty)
                {

                    ShowAlert(2, "Cellpphone number is required!");
                }
                else
                {
                    bool memeberExist = false;

                    for (int i = 0; i < gridMembers.Rows.Count; i++)
                    {
                        String txtCell = txtCellphone.Text.Trim().Replace("+27", "0");
                        String gridCell = gridMembers.Rows[i].Cells[4].Text.Trim().Replace("+27", "0");

                        if (txtCell.Length > 10)
                        {
                            txtCell = "0" + txtCell.Substring(2);
                        }
                        if (gridCell.Length > 10)
                        {
                            gridCell = "0" + gridCell.Substring(2);
                        }

                        if (txtCell.Trim() == gridCell.Trim())
                        {
                            memeberExist = true;
                            break;
                        }

                    }

                    if (memeberExist)
                    {
                        ShowAlert(2, "The cellphone number is already assigned to a recipient on this group.");
                    }
                    else
                    {
                        access obj = new access();

                        int GroupID = Convert.ToInt32(Session["SelectedMessagingGroupID"].ToString());
                        String fn = txtFirstname.Text.Trim();
                        String ln = txtSurname.Text.Trim();
                        String cell = txtCellphone.Text.Trim();

                        obj.addCustomMessagingGroupMembers(fn, ln, cell, GroupID);
                        gridMembers.DataBind();

                        txtCellphone.Text = "";
                        txtSurname.Text = "";
                        txtFirstname.Text = "";
                    }
                }

            }
            else
            {
                Response.Redirect("groups.aspx");
            }

        }
        protected void btnPromptYes_Click(object sender, EventArgs e)
        {
            deleteGroup();
        }

        protected void btnPromptNo_Click(object sender, EventArgs e)
        {
            MessageBoxPrompt.Visible = false;
        }

        void deleteGroup()
        {
            if (Session["SelectedMessagingGroupID"] != null)
            {
                int GroupID = Convert.ToInt32(Session["SelectedMessagingGroupID"].ToString());

                access obj = new access();
                obj.deleteMessagingGroup(GroupID);

                MessageBoxPrompt.Visible = false;
                Session["GroupDeleted"] = true;
                ShowAlert(1, "Messaging group deleted.");
            }
        }

        protected void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            MessageBoxPrompt.Visible = true;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["GroupDeleted"] != null)
            {
                Session.Remove("GroupDeleted");
                Session.Remove("SelectedMessagingGroupID");
                Response.Redirect("groups.aspx");
            }

        }
    }
}