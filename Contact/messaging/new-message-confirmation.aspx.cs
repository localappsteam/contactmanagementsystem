﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Contact.messaging
{
    public partial class new_message_confirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
           
        }

        void AuthenticateUser()
        {

            if (Session["UserName"] != null)
            {

                String UserName = Session["UserName"].ToString();
                int RoleID = 15;
                int AdminRoleID = 14;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
                else
                {
                    load();
                }
            }
            else
            {
                Response.Redirect("messaging.aspx");
            }
        }

        void load()
        {
            if (Session["MessagingOption"] != null)
            {
                lblOption.Text = Session["MessagingOption"].ToString().Trim();
                lblMessage.Text = Session["Message"].ToString().Trim();

                String[] recipient = Session["Recipient"].ToString().Split(';');
                recipient = recipient.Distinct().ToArray();
                Literal[] html = new Literal[recipient.Length];

                for (int i = 0; i < html.Length; i++)
                {
                    html[i] = new Literal();
                    html[i].Text = html[i].Text + "<p>" + recipient[i] + "</p>";

                    divRecipients.Controls.Add(html[i]);
                }
            }
            else
            {
                Response.Redirect("new-message.aspx");
            }
        }
        String sendSMS(String message, String cellphone, String ReplyEmail)
        {

            int apiId = 3652497;
            String username = "sappitug";
            String Password = "SappiClick01";


            String text = "";
            text = text + "api_id:" + apiId.ToString() + Environment.NewLine;
            text = text + "user:" + username + Environment.NewLine;
            text = text + "password:" + Password + Environment.NewLine;
            text = text + cellphone;
            text = text + "msg_type:SMS_FLASH" + Environment.NewLine;
            text = text + "text:" + message + Environment.NewLine;
            text = text + "reply:lunga.dlamini@sappi.com";


            access obj = new access();
            obj.sendSMS(text);

            return "Success";






            //String api = "https://api.clickatell.com/http/sendmsg?user=sappitug&password=SappiClick01&api_id=3649105&to=" + cellphone + "&text=" + message;

            //WebClient cl = new WebClient();
            //cl.DownloadString(api);

            //// Create a request using a URL that can receive a post.   
            //WebRequest request = WebRequest.Create(");
            //// Set the Method property of the request to POST.  
            //request.Method = "POST";

            //// Get the response.  
            //WebResponse response = request.GetResponse();
            //response.Close();

            // Return the status.

        }

        void submitMessage()
        {

            Int64 EmployeeID = Convert.ToInt32(Session["EmployeeID"].ToString());
            String[] recipient = Session["Recipient"].ToString().Split(';');
            recipient = recipient.Distinct().ToArray();
            String[] cleanNumber = new String[recipient.Length];
            String To = "";

            int countSent = 0;

            //BEGIN SENDING MESSAGES

            access obj = new access();

            for (int i = 0; i < recipient.Length; i++)
            {
                cleanNumber[i] = recipient[i];

                if (cleanNumber[i].Contains("["))
                {
                    cleanNumber[i] = ((cleanNumber[i].Substring(cleanNumber[i].IndexOf('[') + 1)).Replace("]", "")).Trim();
                }

                cleanNumber[i] = ((cleanNumber[i].Replace("+", "")).Replace(" ", "")).Trim();

                String saCode = cleanNumber[i].Substring(0, 2);

                if (saCode == "06" || saCode == "07" || saCode == "08")
                {
                    cleanNumber[i] = ("27" + cleanNumber[i].Substring(1)).Trim();
                }

                To = To + "to:" + cleanNumber[i] + Environment.NewLine;
            }

            try
            {

                obj = new access();
                String Status = sendSMS(lblMessage.Text, To, obj.getUserEmail(Session["UserName"].ToString()));

                obj = new access();
                obj.saveSentMessage(lblMessage.Text, To.Replace("to:", " ").Trim(), EmployeeID, Status);
                Session["MessageSent"] = true;
                countSent++;
            }
            catch (Exception ex)
            {
                obj.saveSentMessage(lblMessage.Text, To.Replace("to:", " ").Trim(), EmployeeID, "FALIED: " + ex.Message);
            }

            if (countSent > 0)
            {
                ShowAlert(1, "Message sent.");
            }
            else
            {
                ShowAlert(2, "no message sent");
            }

        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["MessageSent"] != null)
            {
                Session.Remove("MessageSent");

                Response.Redirect("messaging.aspx");
            }
        }

        protected void btnSendMessage_Click(object sender, EventArgs e)
        {
            submitMessage();
        }
    }
}