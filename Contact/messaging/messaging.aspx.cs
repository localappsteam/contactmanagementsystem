﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.messaging
{
    public partial class messaging : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionBuilder start = new SessionBuilder();
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            String UserName = Session["UserName"].ToString();
            int RoleID = 1;
            int AdminRoleID = 14;

            access obj = new access();
            if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
            {
                Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                Response.Redirect("access-denied.aspx");
            }
        }
    }
}