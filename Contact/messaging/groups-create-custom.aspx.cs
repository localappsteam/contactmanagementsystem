﻿using System;
using System.Web.UI.WebControls;

namespace Contact.messaging
{
    public partial class groups_create_custom : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {

                String UserName = Session["UserName"].ToString();
                int RoleID = 7;
                int AdminRoleID = 14;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }

            }
            else
            {
                Response.Redirect("messaging.aspx");
            }

        }

        protected void btnAddRecipient_Click(object sender, EventArgs e)
        {
            addRecipient();
        }

        void addRecipient()
        {
            Session.Remove("GroupCreated");

            if (txtFirstname.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "First name(s) is required!");
            }
            else if (txtSurname.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Surname is required!");
            }
            else if (txtCellphone.Text.Trim() == String.Empty)
            {

                ShowAlert(2, "Cellpphone number is required!");
            }
            else
            {
                bool exist = false;

                for (int i = 0; i < chRecipients.Items.Count; i++)
                {
                    if (txtCellphone.Text.Trim() == chRecipients.Items[i].Value.Trim())
                    {
                        exist = true;
                        break;
                    }
                }

                if (exist)
                {
                    ShowAlert(2, "The cellphone number is already assigned to a recipient on this group.");
                }
                else
                {
                    ListItem lst = new ListItem();

                    lst.Value = txtCellphone.Text.Trim();
                    lst.Text = txtSurname.Text.Trim() + " | " + txtFirstname.Text.Trim() + " | ..." + txtCellphone.Text.Substring(7).Trim();
                    chRecipients.Items.Add(lst);

                    txtFirstname.Text = String.Empty;
                    txtSurname.Text = String.Empty;
                    txtCellphone.Text = String.Empty;
                }

            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            removeRecipient();
        }

        void removeRecipient()
        {
            for (int i = 0; i < chRecipients.Items.Count; i++)
            {
                if (chRecipients.Items[i].Selected)
                {
                    chRecipients.Items.RemoveAt(i);
                    i = i - 1;
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            submit();
        }
        void submit()
        {
            Session.Remove("GroupCreated");

            if (txtGroupName.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Group name is required.");
            }
            else if (chRecipients.Items.Count < 1)
            {
                ShowAlert(2, "No recipients added.");
            }
            else
            {
                //BEGIN SUBMITTION

                try
                {
                    access obj = new access();
                    int MessagingGroupID = obj.createMessagingGroup(txtGroupName.Text.Trim(), Convert.ToInt64(Session["EmployeeID"].ToString()), 1);

                    obj = new access();
                    String fn = "";
                    String ln = "";
                    String cell = "";

                    for (int i = 0; i < chRecipients.Items.Count; i++)
                    {

                        ln = chRecipients.Items[i].Text.Substring(0, chRecipients.Items[i].Text.IndexOf('|')).Trim();
                        fn = chRecipients.Items[i].Text.Substring(chRecipients.Items[i].Text.IndexOf('|') + 1).Trim();
                        fn = fn.Replace(fn.Substring(fn.IndexOf('|')), "");
                        fn = fn.Trim();
                        cell = chRecipients.Items[i].Value.Trim();

                        obj.addCustomMessagingGroupMembers(fn, ln, cell, MessagingGroupID);
                    }

                    Session["GroupCreated"] = true;
                    ShowAlert(1, "Group created successfully.");
                }
                catch (Exception ex)
                {
                    ShowAlert(2, ex.Message);
                }
            }
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["GroupCreated"] != null)
            {
                Session.Remove("GroupCreated");

                Response.Redirect("groups.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }
}