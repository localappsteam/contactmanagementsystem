﻿using System;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace Contact.messaging
{
    public partial class new_message : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {

            if (Session["UserName"] != null)
            {

                String UserName = Session["UserName"].ToString();
                int RoleID = 15;
                int AdminRoleID = 14;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
                else
                {
                    if (!Page.IsPostBack)
                    {
                        Session.Remove("MessagingOption");
                        Session.Remove("Recipient");
                        Session.Remove("Message");
                    }
                }
            }
            else
            {
                Response.Redirect("messaging.aspx");
            }
        }

        protected void rdOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeOption();
        }

        void changeOption()
        {
            divGroups.Visible = false;
            divIndividual.Visible = false;
            divFileUpload.Visible = false;

            if (rdOptions.SelectedValue == "1")
            {
                divGroups.Visible = true;
            }
            else if (rdOptions.SelectedValue == "2")
            {
                divIndividual.Visible = true;
            }
            else if (rdOptions.SelectedValue == "3")
            {
                divFileUpload.Visible = true;
            }
        }


        protected void btnUpload_Click(object sender, EventArgs e)
        {
            uploadFile();
        }

        void uploadFile()
        {
            Session.Remove("Recipient");

            if (!FileUpload1.HasFile)
            {
                ShowAlert(2, "Select a file first.");
            }
            else if (FileUpload1.FileName.Trim() != "EmployeeNumbers.csv")
            {
                ShowAlert(2, "Upload the EmployeeNumbers.csv file.");
            }
            else
            {
                //CREATE UPLOAD FILE DIRECTORY AND SAVE FILE AS TXT FORMAT

                String un = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                un = un.Substring(un.IndexOf(@"\") + 1);
                String dir = Server.MapPath("uploads") + @"\" + un + @"\messaging";
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                dir = dir + @"\emp_numbers.txt";

                FileUpload1.SaveAs(dir);


                //BEGIN CONTACT SEARCH
                StreamReader rd = new StreamReader(dir);
                int countLines = File.ReadAllLines(dir).Length;

                int countFound = 0;
                int countFiltered = 0;
                String Ricipient = "";


                Literal[] html = new Literal[countLines];

                for (int i = 0; i < countLines; i++)
                {
                    String empnumber = rd.ReadLine().Trim();

                    Int64 outval;

                    html[i] = new Literal();

                    divContactFind.Visible = true;

                    if (empnumber.ToUpper() != "EMPLOYEE NUMBER")
                    {

                        if (Int64.TryParse(empnumber, out outval) == true)
                        {
                            access obj = new access();
                            DataSet ds = obj.messagingGetContact(empnumber);

                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                if (ds.Tables[0].Rows[0]["Cell"].ToString().Trim() != String.Empty) //ADD TO FOUND CONTACTS
                                {
                                    html[i].Text = "<tr><th scope='row'>" + ds.Tables[0].Rows[0]["EmpID"].ToString() + "</th>";
                                    html[i].Text = html[i].Text + "<td>" + ds.Tables[0].Rows[0]["Cell"].ToString() + "</td>";
                                    html[i].Text = html[i].Text + "<td>" + ds.Tables[0].Rows[0]["Firstnames"].ToString() + "</td>";
                                    html[i].Text = html[i].Text + "<td>" + ds.Tables[0].Rows[0]["Nickname"].ToString() + "</td>";
                                    html[i].Text = html[i].Text + "<td>" + ds.Tables[0].Rows[0]["Surname"].ToString() + "</td>";
                                    html[i].Text = html[i].Text + "<td>" + ds.Tables[0].Rows[0]["Department"].ToString() + "</td>";
                                    html[i].Text = html[i].Text + "<td>" + ds.Tables[0].Rows[0]["Office"].ToString() + "</td></tr>";
                                    tblFoundContacts.Controls.Add(html[i]);
                                    countFound++;

                                    Ricipient = Ricipient +
                                                ds.Tables[0].Rows[0]["Firstnames"].ToString() + " " +
                                                ds.Tables[0].Rows[0]["Surname"].ToString() + " [" +
                                                     ds.Tables[0].Rows[0]["Cell"].ToString().Trim() + "];";
                                }
                                else //ADD TO FILTERED OUT CONTACTS | NOT NUMBER
                                {
                                    html[i].Text = "<tr><td>" + empnumber + "</td>";
                                    html[i].Text = html[i].Text + "<td>No number</td></tr>";
                                    tblFilteredContacts.Controls.Add(html[i]);
                                    countFiltered++;
                                }

                            }
                            else //ADD TO FILTERED OUT CONTACTS | NOT FOUND
                            {
                                html[i].Text = "<tr><td>" + empnumber + "</td>";
                                html[i].Text = html[i].Text + "<td>Not found</td></tr>";
                                tblFilteredContacts.Controls.Add(html[i]);
                                countFiltered++;
                            }


                        }
                        else //ADD TO FILTERED OUT CONTACTS | INVALID EMPLOYEE NUMBER
                        {
                            html[i].Text = "<tr><td>" + empnumber + "</td>";
                            html[i].Text = html[i].Text + "<td>Invalid</td></tr>";
                            tblFilteredContacts.Controls.Add(html[i]);
                            countFiltered++;
                        }
                    }
                }

                rd.Close();

                lblCountFound.Text = countFound.ToString();
                lblCountFiltered.Text = countFiltered.ToString();

                Session["Recipient"] = Ricipient.Substring(0, Ricipient.LastIndexOf(";"));

            }
        }

        void proceed()
        {

            if (rdOptions.SelectedValue == "1")
            {
                if (ckGroups.SelectedIndex < 0)
                {
                    ShowAlert(2, "A minimum of one group is required.");
                }
                else if (txtMessage.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "A message is required.");
                }
                else
                {
                    String strRecipients = "";

                    for (int i = 0; i < ckGroups.Items.Count; i++)
                    {
                        if (ckGroups.Items[i].Selected)
                        {

                            int MessagingGroupID = Convert.ToInt32(ckGroups.Items[i].Value.Trim());

                            access obj = new access();
                            DataSet ds = obj.getMessagingGroupRecipients(MessagingGroupID);

                            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                            {
                                strRecipients = strRecipients + ds.Tables[0].Rows[j]["Recipients"].ToString() + ";";
                            }
                        }
                    }

                    if (strRecipients.Trim() != String.Empty)
                    {
                        Session["MessagingOption"] = "Groups";
                        Session["Recipient"] = strRecipients.Substring(0, strRecipients.LastIndexOf(";"));
                        Session["Message"] = txtMessage.Text.Trim();

                        Response.Redirect("new-message-confirmation.aspx");
                    }
                    else
                    {
                        ShowAlert(2, "There are no members on this group.");
                    }

                }
            }
            else if (rdOptions.SelectedValue == "2")
            {
                if (txtPhoneNumber.Text.Trim().Length < 10)
                {
                    ShowAlert(2, "A cellphone number is required.");
                }
                else if (txtPhoneNumber.Text.Trim().Length < 10)
                {
                    ShowAlert(2, "Enter a valid cellphone number.");
                }
                else if (txtMessage.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "A message is required.");
                }
                else
                {
                    Session["MessagingOption"] = "Individual";
                    Session["Recipient"] = txtPhoneNumber.Text.Trim();
                    Session["Message"] = txtMessage.Text.Trim();

                    Response.Redirect("new-message-confirmation.aspx");
                }
            }
            else if (rdOptions.SelectedValue == "3")
            {
                int countRicipeints = 0;

                if (Session["Recipient"] == null)
                {
                    ShowAlert(2, "Upload the employee number file first.");
                }
                else
                {
                    countRicipeints = Session["Recipient"].ToString().Split(';').Length;

                    if (countRicipeints < 1)
                    {
                        ShowAlert(2, "There are no recipients to send a message to.");
                    }
                    else if (txtMessage.Text.Trim() == String.Empty)
                    {
                        ShowAlert(2, "A message is required.");
                    }
                    else
                    {
                        Session["MessagingOption"] = "Company numbers (File upload)";
                        Session["Message"] = txtMessage.Text.Trim();

                        Response.Redirect("new-message-confirmation.aspx");
                    }
                }
            }

        }

        protected void btnProceed_Click(object sender, EventArgs e)
        {
            proceed();
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }

}