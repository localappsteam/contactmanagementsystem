﻿using System;
using System.Data;
using System.Web;
using System.Security.Principal;

namespace Contact.messaging
{
    public partial class massaging : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionBuilder start = new SessionBuilder();
            AuthenticateUser();
            loadUserProfile();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int AdminRoleID = 14;
            access obj = new access();

            if (!obj.checkRoles(UserName, 1, AdminRoleID))
            {
                Response.Redirect("../access-denied.aspx");
            }
            else
            {
                obj = new access();
                if (!obj.checkRoles(UserName, 15, AdminRoleID)) //Send Messages
                {
                    lknewmessage.Visible = false;
                }
                else
                {
                    lknewmessage.Visible = true;
                }

                obj = new access();
                if (!obj.checkRoles(UserName, 0, AdminRoleID)) //System Users
                {
                    useraccounts.Visible = false;
                }
                else
                {
                    useraccounts.Visible = true;
                }

            }
        }
       

        void loadUserProfile()
        {
            if (Session["MillID"] != null)
            {

                WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String UserName = UserSecurityPrincipal.Name;


                lblSelectedOffice.Text = Session["SelectedOffice"].ToString();
                lblUsername.Text = UserName;

                access obj = new access();
                DataSet ds = obj.getUserLogged(UserName);
                String employeeID = "0";

                if (ds.Tables[0].Rows.Count > 0)
                {
                    employeeID = ds.Tables[0].Rows[0]["EmployeeID"].ToString();
                }

                Session["EmployeeID"] = employeeID;
                Session["UserName"] = UserName;

                ADManager ad = new ADManager();
                UserName = UserName.Substring(UserName.LastIndexOf(@"\") + 1).Trim();
                lblUserOffice.Text = ad.getUserLocation(UserName);

            }
            else
            {
                Response.Redirect("../mill.aspx");
            }
        }
    }
}