﻿<%@ Page Title="" Language="C#" MasterPageFile="~/messaging/massaging.Master" AutoEventWireup="true" CodeBehind="groups-members.aspx.cs" Inherits="Contact.messaging.groups_members" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="block-header">
                <span>
                    <p class="fa fa-home m-r-10"></p>
                    Messaging / Groups / Members</span>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Group Members - Employees & Contractors</h2>
                        </div>
                        <div class="body">
                            <div class="panel">
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:LinkButton ID="btnAddMembers" class="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnAddMembers_Click">
                                    <p class="fa fa-plus m-r-10"></p>
                                    Add Members</asp:LinkButton>
                                    </div>
                                    <div class="col-md-2 btn-fix-width">
                                       <asp:LinkButton ID="btnDeleteGroup" class="btn btn-danger btn-sm btn-block" runat="server" OnClick="btnDeleteGroup_Click">
                                            <p class="fa fa-trash-alt m-r-10"></p>
                                            Delete Group</asp:LinkButton>
                                    </div>
                                    <div class="col-md-2 btn-fix-width">
                                        <a href="groups.aspx" class="btn btn-default btn-sm btn-block">
                                            <p class="fa fa-reply m-r-10"></p>
                                            Go back</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 cont-holder">
                                    </div>
                                    <div class="col-md-12 cont-holder-plain" style="margin-top: 0px">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-bottom: 0px">
                                                <div style="width: 100px; float: left">
                                                    <img src="../images/Group-Folder-Ash-icon.png" class="img-menu-sm" style="width: 100%" />
                                                </div>
                                                <div style="float: left; padding-top: 40px; padding-left: 10px">
                                                    <h4 style="font-weight: normal">
                                                        <asp:Label ID="lblGroupName" Font-Underline="true" runat="server" Text=""></asp:Label><br /><small>
                                                            <asp:LinkButton ID="btnChangeGroupName" runat="server" OnClick="btnChangeGroupName_Click">Change name?</asp:LinkButton></small></h4>
                                                </div>
                                            </div>
                                            <div id="divChangeName" runat="server" visible="false">
                                            <div class="row">
                                                <div class="col-md-2" style="width: 110px;">
                                                </div>
                                                <div class="col-md-5" style="padding-left: 30px;">
                                                    <asp:TextBox ID="txtChangeName" placeholder="Enter group name..." CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2" style="width: 110px; height:0px; padding:0px; margin:0px;">
                                                </div>
                                                <div class="col-md-2 btn-fix-width-sm" style="padding-left: 30px;">
                                                     <asp:LinkButton ID="btnSave" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnSave_Click">Save</asp:LinkButton>
                                                </div>
                                                <div class="col-md-2 btn-fix-width-sm" style="padding-left: 30px;">
                                                     <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-sm btn-block" runat="server" OnClick="btnCancel_Click">Cancel</asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                            </div>
                                    </div>
                                    <div class="col-md-12 cont-holder-plain" style="margin-bottom: 0px; margin-top: 0px">
                                        <div class="row">
                                            <div class="col-md-6" style="padding-right: 0px; margin-right: 0px; padding-top: 0px">
                                                <asp:TextBox ID="txtSearch" CssClass="form-control form-control-sm" Width="78%" Style="float: left" Placeholder="Search contact..." runat="server"></asp:TextBox>
                                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-primary btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 cont-holder">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:GridView ID="gridMembers" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AllowPaging="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="gridMembers_SelectedIndexChanged">
                                                    <AlternatingRowStyle BackColor="#f5f5f5" />
                                                    
                                                    <RowStyle Height="41px"></RowStyle>
                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True" SelectText="Remove" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                                        <asp:BoundField DataField="#" HeaderText="#" ReadOnly="True" SortExpression="#" />
                                                        <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" ReadOnly="True" />
                                                        <asp:BoundField DataField="Firstnames" HeaderText="Firstnames" SortExpression="Firstnames" ReadOnly="True" />
                                                        <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" ReadOnly="True" />
                                                        <asp:BoundField DataField="Cell" HeaderText="Cell" SortExpression="Cell" ReadOnly="True" />
                                                        <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" ReadOnly="True" />
                                                        <asp:BoundField DataField="EmpType" HeaderText="EmpType" SortExpression="EmpType" ReadOnly="True" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Messaging_ViewGroupMembers" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="MessagingGroupID" SessionField="SelectedMessagingGroupID" Type="Int32" />
                                                        <asp:ControlParameter ControlID="txtSearch" DefaultValue=" " Name="SearchValue" PropertyName="Text" Type="String" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 cont-holder-plain" style="margin-bottom: 0px" id="divAddmembers1" visible="false" runat="server">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-bottom: 10px">
                                                <h5 style="font-weight: normal">Add Group Members</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1" style="padding-right: 0px; padding-top:2px">
                                                <asp:LinkButton ID="btnCloseAdd" class="btn btn-default btn-sm btn-block" runat="server" OnClick="btnCloseAdd_Click1">
                                                    Close
                                                </asp:LinkButton>
                                            </div>
                                            <div class="col-md-6" style="padding-right: 0px; margin-right: 0px; padding-top: 0px">
                                                <asp:TextBox ID="txtSearch2" CssClass="form-control form-control-sm  m-l-10" Width="75%" Style="float:left" Placeholder="Search employee..." runat="server"></asp:TextBox>
                                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 cont-holder" id="divAddmembers2" visible="false" runat="server">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:GridView ID="gridEmployees" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AllowPaging="False" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" OnSelectedIndexChanged="gridEmployees_SelectedIndexChanged" OnDataBound="gridEmployees_DataBound">
                                                    <AlternatingRowStyle BackColor="#f5f5f5" />
                                                    
                                                    <RowStyle Height="41px"></RowStyle>
                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True" SelectText="Add" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                                        <asp:BoundField DataField="#" HeaderText="#" ReadOnly="True" SortExpression="#" />
                                                        <asp:BoundField DataField="Surname" HeaderText="Surname" ReadOnly="True" SortExpression="Surname" />
                                                        <asp:BoundField DataField="Firstnames" HeaderText="Firstnames" ReadOnly="True" SortExpression="Firstnames" />
                                                        <asp:BoundField DataField="Title" HeaderText="Title" ReadOnly="True" SortExpression="Title" />
                                                        <asp:BoundField DataField="Cell" HeaderText="Cell" ReadOnly="True" SortExpression="Cell" />
                                                        <asp:BoundField DataField="Department" HeaderText="Department" ReadOnly="True" SortExpression="Department" />
                                                        <asp:BoundField DataField="EmpType" HeaderText="EmpType" ReadOnly="True" SortExpression="EmpType" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Messaging_EmployeeSearch" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="txtSearch2" DefaultValue="" Name="SearchValue" PropertyName="Text" Type="String" />
                                                        <asp:Parameter DefaultValue=" " Name="LIST" Type="String" />
                                                        <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="MessageBox" visible="false" class="MessageBox" runat="server">
                <div id="AlertBox" class="AlertBox"></div>
                <div class="AlertBoxContent">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div id="AlertHeader" runat="server" class="modal-header">
                                <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                                    <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                            </div>
                            <div class="modal-footer">
                                <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="MessageBoxPrompt" visible="false" class="MessageBox" runat="server">
                <div id="AlertBoxPrompt" class="AlertBox"></div>
                <div class="AlertBoxContent">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div id="AlertHeaderPrompt" runat="server" class="modal-header">
                                <h4 class="modal-title"><span id="spanAlertPrompt" runat="server"></span>
                                    <asp:Label ID="lblAlertTitlePrompt" runat="server" Text="Delete Group?"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <asp:Label ID="lblAlertMessagePrompt" runat="server" Font-Bold="false" Font-Size="Medium" Text="Are you sure you want to delete this group?"></asp:Label>
                            </div>
                            <div class="modal-footer">
                                <asp:LinkButton ID="btnPromptYes" CssClass="btn btn-sm btn-danger m-r-10" runat="server" OnClick="btnPromptYes_Click"><p class="fa fa-check icon-mr"></p>Yes</asp:LinkButton>
                                 <asp:LinkButton ID="btnPromptNo" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnPromptNo_Click"><p class="fa fa-times icon-mr"></p>No</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
