﻿using System;
using System.Web.UI.WebControls;
using System.Data;

namespace Contact.messaging
{
    public partial class groups : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
            loadGroups();
        }

        void AuthenticateUser()
        { 
            if (Session["UserName"] != null)
            {

                String UserName = Session["UserName"].ToString();
                int RoleID = 7;
                int AdminRoleID = 14;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    btnCreateGroup.Visible = false;
                }
                else
                {
                    btnCreateGroup.Visible = true;
                }
            }
            else
            {
                Response.Redirect("messaging.aspx");
            }
        }

        void loadGroups()
        {
            Session.Remove("SelectedMessagingGroupID");

            access obj = new access();
            DataSet ds = obj.viewMessagingGroups(Convert.ToInt32(Session["EmployeeID"]));

            LinkButton[] btn = new LinkButton[ds.Tables[0].Rows.Count];
            Literal[] html = new Literal[ds.Tables[0].Rows.Count];
            String imgUrl = "";
            String redirectUrl = "";


            for (int i = 0; i < html.Length; i++)
            {

                if (ds.Tables[0].Rows[i]["GroupType"].ToString().Trim().ToLower() == "(custom)")
                {
                    imgUrl = "../images/Group-Folder-icon.png";
                    redirectUrl = "groups-members-custom.aspx";
                }
                else
                {
                    imgUrl = "../images/Group-Folder-Ash-icon.png";
                    redirectUrl = "groups-members.aspx";
                }

                html[i] = new Literal();
                html[i].Text = "<div class='messaging-group-container'>";
                html[i].Text = html[i].Text + "<div class='messaging-group text-center'>";
                html[i].Text = html[i].Text + "<img src='" + imgUrl + "' class='img-menu-sm' />";
                html[i].Text = html[i].Text + "<p class='text-center'>" + ds.Tables[0].Rows[i]["Description"].ToString();
                html[i].Text = html[i].Text + "<br/><small>Members: " + ds.Tables[0].Rows[i]["Members"].ToString() + "<br/>";
                html[i].Text = html[i].Text + ds.Tables[0].Rows[i]["GroupType"].ToString() + "</small></p></div></div>";

                btn[i] = new LinkButton();
                btn[i].ID = ds.Tables[0].Rows[i]["MessagingGroupID"].ToString();
                btn[i].ToolTip = ds.Tables[0].Rows[i]["Description"].ToString();
                btn[i].CssClass = "dash-link col-md-2 text-center";
                btn[i].SkinID = redirectUrl;
                btn[i].Controls.Add(html[i]);

                //ADD ONCLICK EVENT
                btn[i].Click += (s, e) =>
                {
                    LinkButton lbtn = (LinkButton)s;
                    Session["SelectedMessagingGroupID"] = lbtn.ID.ToString();
                    Session["SelectedMessagingGroupName"] = lbtn.ToolTip.ToString();

                    Response.Redirect(lbtn.SkinID);

                };

                divGroups.Controls.Add(btn[i]);
            }

        }
    }
}