﻿<%@ Page Title="" Language="C#" MasterPageFile="~/messaging/massaging.Master" AutoEventWireup="true" CodeBehind="groups-create-custom.aspx.cs" Inherits="Contact.messaging.groups_create_custom" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="block-header">
                <span>
                    <p class="fa fa-home m-r-10"></p>
                    Messaging / Groups / Create</span>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Create Custom Recipients Group</h2>
                        </div>
                        <div class="body">
                            <div class="panel">
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                        <a href="groups-types.aspx" class="btn btn-default btn-sm btn-block">
                                            <p class="fa fa-reply m-r-10"></p>
                                            Go back</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 cont-holder">
                                    </div>
                                    <div class="col-md-12 cont-holder-plain">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>Group name<span class="text-danger icon-ml">*</span></p>
                                                <asp:TextBox ID="txtGroupName" Placeholder="Enter group name..." CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12" style="padding-left: 6px">
                                                <div class="row">
                                                    <div class="col-md-8" style="padding-left: 0px">
                                                        <div class="cont-holder" style="padding: 25px;">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <p>First name(s)<span class="text-danger icon-ml">*</span></p>
                                                                    <asp:TextBox ID="txtFirstname" CssClass="form-control form-group-sm" runat="server"></asp:TextBox>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <p>Surname<span class="text-danger icon-ml">*</span></p>
                                                                    <asp:TextBox ID="txtSurname" CssClass="form-control form-group-sm" runat="server"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <p>Cellphone<span class="text-danger icon-ml">*</span></p>
                                                                    <asp:TextBox ID="txtCellphone" TextMode="Number" CssClass="form-control form-group-sm" runat="server"></asp:TextBox>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <asp:LinkButton ID="btnAddRecipient" CssClass="btn btn-primary btn-sm btn-block" Style="margin-top: 32px" runat="server" OnClick="btnAddRecipient_Click"><p class="fa fa-plus m-r-10"></p>Add recipient</asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" style="padding-left: 0px">
                                                        <div class=" cont-holder-plain" style="padding: 0px; padding-top: 0px">
                                                            <h5><span style="float: left">Selected recipients</span>
                                                                <asp:LinkButton ID="btnRemove" CssClass="btn btn-default btn-sm" Style="float: right; margin-top: -7px" runat="server" OnClick="btnRemove_Click"><p class="fa fa-times icon-mr"></p>Remove</asp:LinkButton>
                                                                <h5></h5>
                                                                <br />
                                                                <h5></h5>
                                                                <h5></h5>
                                                            </h5>
                                                        </div>
                                                        <div class="cont-holder" style="margin-top: 15px; padding: 0px; padding-left: 5px; padding-top: 0px; width: 100%">
                                                            <asp:CheckBoxList ID="chRecipients" Width="100%" runat="server"></asp:CheckBoxList>
                                                        </div>
                                                        <div class="cont-holder-plain" style="padding: 0px; padding-top: 0px; margin-top: 30px">
                                                            <asp:LinkButton ID="btnSubmit" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnSubmit_Click"><p class="fa fa-paper-plane m-r-10"></p>Submit</asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="MessageBox" visible="false" class="MessageBox" runat="server">
                <div id="AlertBox" class="AlertBox"></div>
                <div class="AlertBoxContent">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div id="AlertHeader" runat="server" class="modal-header">
                                <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                                    <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                            </div>
                            <div class="modal-footer">
                                <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
