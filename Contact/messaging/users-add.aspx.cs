﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.messaging
{
    public partial class users_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 0; //Admin only
                int AdminRoleID = 14;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("list.aspx");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            gridEveryone.SelectedIndex = -1;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            addUser();
        }

        void addUser()
        {
            if (gridEveryone.SelectedIndex < 0)
            {
                ShowAlert(2, "Select a person first.");
            }
            else if (txtUsername.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Enter a user name first.");
            }
            else
            {
                int EmpTypeID;
                Int64 EmployeeID = Convert.ToInt64(gridEveryone.SelectedRow.Cells[1].Text.Trim());
                String Username = txtUsername.Text.Trim();
                int MillID = Convert.ToInt32(Session["MillID"].ToString());

                if (gridEveryone.SelectedRow.Cells[9].Text.Trim().ToLower() == "employee")
                {
                    EmpTypeID = 1;
                }
                else
                {
                    EmpTypeID = 2;
                }


                access obj = new access();
                if (obj.addUserCheckExist(EmployeeID, Username, EmpTypeID))
                {
                    ShowAlert(2, "This user account already exist on the system.");
                }
                else
                {
                    try
                    {
                        System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String SystemUserName = UserSecurityPrincipal.Name;

                        obj = new access();
                        obj.addUser(SystemUserName, EmployeeID, Username, EmpTypeID, MillID, 3);

                        txtSearch.Text = "";
                        gridEveryone.DataBind();
                        gridEveryone.SelectedIndex = -1;
                        txtUsername.Text = "";

                        ShowAlert(1, "User added successfully.");
                    }
                    catch (Exception ex)
                    {

                        ShowAlert(2, ex.Message);
                    }

                }
            }
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }
}