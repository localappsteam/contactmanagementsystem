﻿<%@ Page Title="" Language="C#" MasterPageFile="~/messaging/massaging.Master" AutoEventWireup="true" CodeBehind="access-denied.aspx.cs" Inherits="Contact.messaging.access_denied" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2 class="text-danger text-center"><span class="fa fa-exclamation-triangle m-r-10"></span>Access denied</h2>
                                <p class="m-t-10 text-center">You do not have the roles to access this page.</p>
                                <h5 class="text-center">Messaging/<asp:Label ID="lblPagename" runat="server" Text=""></asp:Label></h5>
                                <br />
                                <p class="text-center">Contact your local system administrator or log a call to the I.T service desk to request access.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
