﻿<%@ Page Title="" Language="C#" MasterPageFile="~/messaging/massaging.Master" AutoEventWireup="true" CodeBehind="new-message-confirmation.aspx.cs" Inherits="Contact.messaging.new_message_confirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span><p class="fa fa-home m-r-10"></p>Messaging / New message / Confirmation</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Confirm Message</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-2 btn-fix-width">
                                <a href="new-message.aspx" class="btn btn-default btn-sm btn-block">
                                    <p class="fa fa-reply m-r-10"></p>
                                    Go back</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cont-holder">
                            </div>
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row m-t-20">
                                    <div class="col-md-2">
                                        <p><b>Messaging option:</b></p>
                                    </div>
                                    <div class="col-md-10">
                                        <p><asp:Label ID="lblOption" runat="server" Text=""></asp:Label></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <p><b>Recipients:</b></p>
                                    </div>
                                    <div class="col-md-10">
                                        <div id="divRecipients" runat="server" class="cont-holder-plain" style="margin-left:0px; max-width:500px">
                                             
                                        </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-6">
                                        <hr />
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <p><b>Message:</b></p>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="cont-holder-plain" style="margin-left:0px; max-width:500px">
                                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                       
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <asp:LinkButton ID="btnSendMessage" CssClass="btn btn-primary btn-block btn-sm m-t-10" runat="server" OnClick="btnSendMessage_Click"><p class="fa fa-paper-plane m-r-10"></p> Send message</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click" ><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
