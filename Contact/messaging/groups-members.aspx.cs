﻿using System;
using System.Web.UI.WebControls;
using System.Drawing;

namespace Contact.messaging
{
    public partial class groups_members : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
            loadGroup();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {

                String UserName = Session["UserName"].ToString();
                int RoleID = 7;
                int AdminRoleID = 14;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    btnDeleteGroup.Visible = false;
                    btnAddMembers.Visible = false;
                    gridMembers.Columns[0].Visible = false;
                    btnChangeGroupName.Visible = false;
                }
                else
                {
                    btnDeleteGroup.Visible = true;
                    btnAddMembers.Visible = true;
                    gridMembers.Columns[0].Visible = true;
                    btnChangeGroupName.Visible = true;
                }
            }
            else
            {
                Response.Redirect("messaging.aspx");
            }

        }
        void loadGroup()
        {
            if (Session["SelectedMessagingGroupID"] == null)
            {
                Response.Redirect("groups.aspx");
            }
            else
            {
                lblGroupName.Text = Session["SelectedMessagingGroupName"].ToString();
            }
        }
        void loadSelected()
        {
            for (int i = 0; i < gridEmployees.Rows.Count; i++)
            {
                gridEmployees.Rows[i].Visible = true;

                if (gridEmployees.Rows[i].Cells[5].Text.Replace("&nbsp;", "").Trim() == String.Empty)
                {
                    ((Button)gridEmployees.Rows[i].Cells[0].Controls[0]).Enabled = false;
                    ((Button)gridEmployees.Rows[i].Cells[0].Controls[0]).Text = "------";
                    gridEmployees.Rows[i].ForeColor = Color.Silver;
                }
                else
                {
                    ((Button)gridEmployees.Rows[i].Cells[0].Controls[0]).Enabled = true;
                    ((Button)gridEmployees.Rows[i].Cells[0].Controls[0]).Text = "Add";
                    gridEmployees.Rows[i].ForeColor = ColorTranslator.FromHtml("#555555");
                }
            }

            for (int i = 0; i < gridMembers.Rows.Count; i++)
            {
                for (int j = 0; j < gridEmployees.Rows.Count; j++)
                {
                    if (gridMembers.Rows[i].Cells[1].Text.Trim() == gridEmployees.Rows[j].Cells[1].Text.Trim() && gridMembers.Rows[i].Cells[gridMembers.Columns.Count-1].Text.Trim() == gridEmployees.Rows[j].Cells[gridEmployees.Columns.Count-1].Text.Trim())
                    {
                        gridEmployees.Rows[j].Visible = false;
                        break;
                    }
                }
            }
        }
        protected void btnAddMembers_Click(object sender, EventArgs e)
        {
            showAddMembers();
        }

        protected void btnCloseAdd_Click1(object sender, EventArgs e)
        {
            hideAddmembers();
        }


        void showAddMembers()
        {
            if (Session["UserName"] != null)
            {
                divAddmembers1.Visible = true;
                divAddmembers2.Visible = true;
                loadSelected();
                gridEmployees.Focus();
            }
            else
            {
                Response.Redirect("groups.aspx");
            }
        }

        void hideAddmembers()
        {
            divAddmembers1.Visible = false;
            divAddmembers2.Visible = false;
        }


        protected void gridEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            addMember();
        }

        void addMember()
        {

            if (Session["SelectedMessagingGroupID"] != null)
            {
                access obj = new access();

                int GroupID = Convert.ToInt32(Session["SelectedMessagingGroupID"].ToString());
                Int64 EmployeeID = Convert.ToInt64(gridEmployees.SelectedRow.Cells[1].Text.Trim());
                int isEmployee = 1;

                if (gridEmployees.SelectedRow.Cells[7].Text.Trim().ToLower() == "employee")
                {
                    isEmployee = 1;
                }
                else
                {
                    isEmployee = 0;
                }

                obj.addMessagingGroupMembers(GroupID, EmployeeID, isEmployee);
                gridMembers.DataBind();
                gridEmployees.DataBind();

            }
            else
            {
                Response.Redirect("groups.aspx");
            }

        }

        protected void gridMembers_SelectedIndexChanged(object sender, EventArgs e)
        {
            removeMember();
        }
        void removeMember()
        {
            if (Session["UserName"] != null)
            {
                if (Session["SelectedMessagingGroupID"] != null)
                {
                    access obj = new access();

                    int GroupID = Convert.ToInt32(Session["SelectedMessagingGroupID"].ToString());
                    Int64 EmployeeID = Convert.ToInt64(gridMembers.SelectedRow.Cells[1].Text.Trim());

                    obj.removeMessagingGroupMembers(GroupID, EmployeeID);
                    gridMembers.DataBind();
                    gridEmployees.DataBind();
                }
                else
                {
                    Response.Redirect("groups.aspx");
                }


            }
            else
            {
                Response.Redirect("groups.aspx");
            }
        }

        protected void gridEmployees_DataBound(object sender, EventArgs e)
        {
            loadSelected();
        }

        protected void btnPromptYes_Click(object sender, EventArgs e)
        {
            deleteGroup();
        }

        protected void btnPromptNo_Click(object sender, EventArgs e)
        {
            MessageBoxPrompt.Visible = false;
        }

        protected void btnChangeGroupName_Click(object sender, EventArgs e)
        {
            divChangeName.Visible = true;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtChangeName.Text = string.Empty;
            divChangeName.Visible = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            changeGroupName();
        }

        void changeGroupName()
        {

            if (Session["SelectedMessagingGroupID"] != null)
            {
                int GroupID = Convert.ToInt32(Session["SelectedMessagingGroupID"].ToString());

                if (Session["UserName"] != null)
                {
                    if (txtChangeName.Text.Trim() == String.Empty)
                    {
                        ShowAlert(2, "Group name is required!");
                    }
                    else
                    {
                        access obj = new access();
                        obj.changeMessagingGroupName(GroupID, txtChangeName.Text.Trim());
                        Session["SelectedMessagingGroupName"] = txtChangeName.Text.Trim();
                        txtChangeName.Text = string.Empty;
                        divChangeName.Visible = false;
                        ShowAlert(1, "Group name changed successfully.");
                    }

                }
                else
                {
                    Response.Redirect("groups.aspx");
                }
            }
            else
            {
                Response.Redirect("groups.aspx");
            }
        }

        void deleteGroup()
        {
            if (Session["SelectedMessagingGroupID"] != null)
            {
                int GroupID = Convert.ToInt32(Session["SelectedMessagingGroupID"].ToString());

                access obj = new access();
                obj.deleteMessagingGroup(GroupID);

                MessageBoxPrompt.Visible = false;
                Session["GroupDeleted"] = true;
                ShowAlert(1, "Messaging group deleted.");
            }
            else
            {
                Response.Redirect("groups.aspx");
            }
        }

        protected void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            MessageBoxPrompt.Visible = true;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["GroupDeleted"] != null)
            {
                Session.Remove("GroupDeleted");
                Session.Remove("SelectedMessagingGroupID");
                Response.Redirect("groups.aspx");
            }

        }
    }
}