﻿<%@ Page Title="" Language="C#" MasterPageFile="~/messaging/massaging.Master" AutoEventWireup="true" CodeBehind="groups.aspx.cs" Inherits="Contact.messaging.groups" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Messaging / Groups</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Messaging Groups</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-2 btn-fix-width" id="btnCreateGroup" runat="server">
                                <a href="groups-types.aspx" class="btn btn-primary btn-sm btn-block">
                                    <p class="fa fa-plus m-r-10"></p>
                                    Create new group</a>
                            </div>
                           <div class="col-md-2 btn-fix-width">
                                <a href="messaging.aspx" class="btn btn-default btn-sm btn-block">
                                    <p class="fa fa-reply m-r-10"></p>
                                    Go back</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cont-holder">
                            </div>
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row" id="divGroups" runat="server">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
