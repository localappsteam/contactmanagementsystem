﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true"  Language="C#" MasterPageFile="~/index.Master"  AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Contact.index1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <h2>HOME</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header" style="background-color:#eeeeee">
                    <h2>Telephone List - Quick Access</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row text-center">
                            <div class="col-md-12 text-center" style="padding-right: 0px">
                                <div class="menu-circle-container">
                                    <a href="employees-search.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="images/Office-Client-Male-Dark-icon.png" class="img-menu m-l-15" />
                                            <p class="text-center m-t-10">Employees</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container">
                                    <a href="contractors-search.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="images/contractor-man-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Contractors</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container" id="divSystemUsers" runat="server">
                                    <a href="users.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="images/user-group-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">System Users</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
   
                <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header" style="background-color:#eeeeee">
                    <h2>Sub-Systems</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row text-center">
                            <div class="col-md-12 text-center" style="padding-right: 0px">
                                <div class="menu-circle-container">
                                    <a href="standby/index.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="images/Apps-preferences-contact-list-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Standby System</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container">
                                    <a href="callout/index.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="images/Phone-List-icon.png" class="img-menu m-l-25" />
                                            <p class="text-center m-t-10">Call Out System</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container">
                                    <a href="messaging/messaging.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="images/SMS-bubble-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Messaging System</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container">
                                    <a href="http://ngxviis03/CommunicationsInventorySystem" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="images/packing-icon.png" class="img-menu m-l-15" />
                                            <p class="text-center">Communications Inventory System</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
