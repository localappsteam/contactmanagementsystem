﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="callout-update-helpers.aspx.cs" Inherits="Contact.callout.callout_update_helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Update / Add helpers</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add Helpers</h2>
                </div>
                <div class="body">
                    <div class="panel">
                         <div class="row" id="divHelpers" runat="server">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 1:<span class="m-l-10 text-danger">*</span></p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper1" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_PersonCalledOut" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 2:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper2" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 3:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper3" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 4:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper4" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 5:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper5" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 6:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper6" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 7:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper7" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 8:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper8" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 9:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper9" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 10:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper10" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="row m-t-40">
                                    <div class="col-md-2 btn-fix-width">
                                         <asp:LinkButton ID="btnSubmit" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnSubmit_Click" ><p class="fa fa-save icon-mr"></p>Submit</asp:LinkButton>
                                     </div>
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-sm btn-block" runat="server" OnClick="btnCancel_Click" ><p class="fa fa-times icon-mr"></p>Cancel</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click" ><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
