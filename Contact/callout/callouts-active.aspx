﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="callouts-active.aspx.cs" Inherits="Contact.callout.callouts_active" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Active callouts</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Active callouts</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-3">
                                        <p>Date from: (dd/MM/yyyy)</p>
                                        <asp:TextBox ID="txtDateFrom" CssClass="form-control form-control-sm" Placeholder="dd/MM/yyyy" TextMode="Date" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <p>Date to: (dd/MM/yyyy)</p>
                                        <asp:TextBox ID="txtDateTo" CssClass="form-control form-control-sm" Placeholder="dd/MM/yyyy" TextMode="Date" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2 btn-fix-width">
                                        <p style="color: white; margin-bottom:12px">.</p>
                                        <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-block btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 cont-holder" style="overflow:auto">
                                        <asp:HiddenField ID="hiddenCalloutID" runat="server" />
                                        <asp:GridView ID="gridActiveCallouts" Width="100%" CssClass="table" Font-Size="12px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                            
                                            <RowStyle Height="41px"></RowStyle>
                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                            <Columns>
                                                <asp:TemplateField ControlStyle-CssClass="image-button" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnUpdateCallout" runat="server" AlternateText='<%#Eval("Callout Number") %>' ToolTip="Contact the person or update callout time details." OnClick="btnUpdateCallout_Click" CausesValidation="False" CommandName="Select" ImageUrl="~/images/small/Phone-icon.png" Text="Select" />
                                                    </ItemTemplate>
                                                    <ItemStyle BackColor="White" BorderColor="White" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ControlStyle-CssClass="image-button" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnOpenCallout" runat="server" AlternateText='<%#Eval("Callout Number") %>' ToolTip="View or edit callout details." OnClick="btnOpenCallout_Click" CausesValidation="False" CommandName="Select" ImageUrl="~/images/small/Folder-Open-icon.png" Text="Select" />
                                                    </ItemTemplate>
                                                    <ItemStyle BackColor="White" BorderColor="White" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ControlStyle-CssClass="image-button" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnPrintCallout" runat="server" AlternateText='<%#Eval("Callout Number") %>' ToolTip="Print callout report." OnClick="btnPrintCallout_Click" CausesValidation="False" CommandName="Select" ImageUrl="~/images/small/Apps-printer-icon.png" Text="Select" />
                                                    </ItemTemplate>
                                                    <ItemStyle BackColor="White" BorderColor="White" />
                                                </asp:TemplateField>
                                                 <asp:TemplateField ControlStyle-CssClass="image-button" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnCancelCallout"  runat="server" AlternateText='<%#Eval("Callout Number") %>' ToolTip="Cancel this callout." OnClick="btnCancelCallout_Click" CausesValidation="False" CommandName="Select" ImageUrl="~/images/small/user-trash-full-icon.png" Text="Select" />
                                                    </ItemTemplate>
                                                    <ItemStyle BackColor="White" BorderColor="White" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Callout Number" HeaderText="Callout Number" ReadOnly="True" SortExpression="Callout Number" />
                                                <asp:BoundField DataField="Person Called Out" HeaderText="Person Called Out" SortExpression="Person Called Out" ReadOnly="True" />
                                                <asp:BoundField DataField="Incident Time" HeaderText="Incident Time" SortExpression="Incident Time" ReadOnly="True" />
                                                <asp:BoundField DataField="Contact Person" HeaderText="Contact Person" SortExpression="Contact Person" ReadOnly="True" />
                                                <asp:BoundField DataField="Reason" HeaderText="Reason" SortExpression="Reason" ReadOnly="True" />
                                                <asp:BoundField DataField="Notification" HeaderText="Notification" ReadOnly="True" SortExpression="Notification" />
                                                <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_ViewCallouts" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="MillID" SessionField="MillID" DefaultValue="" Type="Int32" />
                                               <asp:ControlParameter ControlID="txtDateFrom" Name="StartingDate" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="txtDateTo" Name="EndingDate" PropertyName="Text" Type="String" />
                                                <asp:Parameter DefaultValue="ACTIVE" Name="Status" Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PromptBox" visible="false" class="MessageBox" runat="server">
        <div id="Prompt" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="PromptHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanPrompt" runat="server"></span>
                            <asp:Label ID="lblPrompTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblPromptMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                         <asp:TextBox ID="txtDeleteReason" Placeholder="Reason..." CssClass="form-control" TextMode="MultiLine" Text="" runat="server"></asp:TextBox>
                        <small id="DeleteRequired" runat="server" visible="false" class="text-danger">This field is required!</small>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnSubmitDelete" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnSubmitDelete_Click"><p class="fa fa-check icon-mr"></p>Sumbit</asp:LinkButton>
                        <asp:LinkButton ID="btnClosePromptBox" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnClosePromptBox_Click"><p class="fa fa-times icon-mr"></p>Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
