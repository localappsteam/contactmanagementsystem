﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="callouts-complete.aspx.cs" Inherits="Contact.callout.callouts_complete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Completed callouts</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Completed callouts</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-3">
                                        <p>Date from: (dd/MM/yyyy)</p>
                                        <asp:TextBox ID="txtDateFrom" CssClass="form-control form-control-sm" Placeholder="dd/MM/yyyy" TextMode="Date" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <p>Date to: (dd/MM/yyyy)</p>
                                        <asp:TextBox ID="txtDateTo" CssClass="form-control form-control-sm" Placeholder="dd/MM/yyyy" TextMode="Date" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2 btn-fix-width">
                                        <p style="color: white; margin-bottom:12px">.</p>
                                        <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-block btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 cont-holder" style="overflow:auto">
                                        <asp:GridView ID="gridCompletedCallouts" Width="100%" CssClass="table" Font-Size="12px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                            
                                            <RowStyle Height="41px"></RowStyle>
                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                            <Columns>
                                                <asp:TemplateField ControlStyle-CssClass="image-button" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnOpenCallout" runat="server" AlternateText='<%#Eval("Callout Number") %>' ToolTip="View or edit callout details." OnClick="btnOpenCallout_Click" CausesValidation="False" CommandName="Select" ImageUrl="~/images/small/Folder-Open-icon.png" Text="Select" />
                                                    </ItemTemplate>
                                                    <ItemStyle BackColor="White" BorderColor="White" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ControlStyle-CssClass="image-button" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnPrintCallout" runat="server" AlternateText='<%#Eval("Callout Number") %>' ToolTip="Print callout report." OnClick="btnPrintCallout_Click" CausesValidation="False" CommandName="Select" ImageUrl="~/images/small/Apps-printer-icon.png" Text="Select" />
                                                    </ItemTemplate>
                                                    <ItemStyle BackColor="White" BorderColor="White" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Callout Number" HeaderText="Callout Number" ReadOnly="True" SortExpression="Callout Number" />
                                                <asp:BoundField DataField="Person Called Out" HeaderText="Person Called Out" SortExpression="Person Called Out" ReadOnly="True" />
                                                <asp:BoundField DataField="Incident Time" HeaderText="Incident Time" SortExpression="Incident Time" ReadOnly="True" />
                                                <asp:BoundField DataField="Time Completed" HeaderText="Time Completed" SortExpression="Time Completed" ReadOnly="True" />
                                                <asp:BoundField DataField="Contact Person" HeaderText="Contact Person" SortExpression="Contact Person" ReadOnly="True" />
                                                <asp:BoundField DataField="Reason" HeaderText="Reason" SortExpression="Reason" ReadOnly="True" />
                                                <asp:BoundField DataField="Notification" HeaderText="Notification" ReadOnly="True" SortExpression="Notification" />
                                                <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" />
                                            </Columns>
                                        </asp:GridView>
                                         <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_ViewCallouts" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="MillID" SessionField="MillID" DefaultValue="" Type="Int32" />
                                               <asp:ControlParameter ControlID="txtDateFrom" Name="StartingDate" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="txtDateTo" Name="EndingDate" PropertyName="Text" Type="String" />
                                                <asp:Parameter DefaultValue="COMPLETED" Name="Status" Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
