﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class types_add : System.Web.UI.Page
    {

            protected void Page_Load(object sender, EventArgs e)
            {
                AuthenticateUser();
            }

            void AuthenticateUser()
            {
                if (Session["UserName"] != null)
                {
                    String UserName = Session["UserName"].ToString();
                    int RoleID = 0;//ADMIN ONLY
                    int AdminRoleID = 19;

                    access obj = new access();
                    if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                    {
                        Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                        Response.Redirect("access-denied.aspx");
                    }
                }
                else
                {
                    Response.Redirect("index.aspx");
                }
            }

            protected void btnSubmit_Click(object sender, EventArgs e)
            {
                addType();
            }

            void addType()
            {
                if (txtDescription.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "A callout type description is required!");
                }
                else
                {
                try
                {
                    String Description = txtDescription.Text.Trim();

                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    access obj = new access();
                    obj.addCalloutType(SystemUserName, txtDescription.Text.Trim());

                    Session["TypeAdded"] = true;

                    ShowAlert(1, "Callout type added successfully.");
                }
                catch (Exception ex)
                {

                    ShowAlert(2, ex.Message);
                }
                }
            }

            void ShowAlert(int title, String message)
            {
                if (title == 1)
                {
                    lblAlertTitle.Text = "Success";
                    lblAlertTitle.Attributes.Add("class", "text-success");
                    spanAlert.Attributes.Add("class", "fa fa-check text-success");
                }
                else if (title == 2)
                {
                    lblAlertTitle.Text = "Alert";
                    lblAlertTitle.Attributes.Add("class", "text-danger");
                    spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
                }

                lblAlertMessage.Text = message;
                MessageBox.Visible = true;
            }

            void HideAlert()
            {
                lblAlertTitle.Text = "";
                lblAlertMessage.Text = "";
                MessageBox.Visible = false;

                if (Session["TypeAdded"] != null)
                {
                    Session.Remove("TypeAdded");
                    Response.Redirect("types.aspx");
                }
            }

            protected void btnCloseMessageBox_Click(object sender, EventArgs e)
            {
                HideAlert();
            }


        
    }
}