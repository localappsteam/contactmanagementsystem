﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;

namespace Contact.callout
{
    public class CalloutNotifications
    {

        public String sendSignOfRequest(int CalloutID, String FilePath)
        {
            String errorMsg = "";

            //Fetching Email Body Text from EmailTemplate File. 
            StreamReader str = new StreamReader(FilePath);
            String MailText = str.ReadToEnd();
            str.Close();

            String PersonMail = MailText;
            String AuthorizerMail = MailText;
            String SecurityMail = MailText;

            access obj = new access();
            obj.generateApprovalRequest(CalloutID);

            obj = new access();
            DataSet ds = obj.viewApprovaRequestDetails(CalloutID);

            String fullPath = (HttpContext.Current.Request.Url.AbsoluteUri);
            String url = fullPath.Substring(0, fullPath.LastIndexOf("/") + 1);


            if (ds.Tables[0].Rows.Count > 0)
            {
                obj = new access();

                String hasError = "";

                //PERSON CALLED OUT ################################################################################

                String calloutUrl = url + "callout-update.aspx?ID=" + CalloutID + "&Source=callouts-complete";
                String approveUrl = url + "approval-confirmation.aspx?ID=" + CalloutID + "&Type=1&Code=" + ds.Tables[0].Rows[0]["PersonCalledOutCode"].ToString();
                String homeUrl = url + "index.aspx";

                String Email = ds.Tables[0].Rows[0]["PersonCalledOutEmail"].ToString();

                PersonMail = PersonMail.Replace("$Name", ds.Tables[0].Rows[0]["PersonCalledOutName"].ToString());
                PersonMail = PersonMail.Replace("$CalloutNumber", CalloutID.ToString());
                PersonMail = PersonMail.Replace("$CalloutUrl", calloutUrl);
                PersonMail = PersonMail.Replace("$ApproveUrl", approveUrl);
                PersonMail = PersonMail.Replace("$AlternativeUrl", approveUrl);
                PersonMail = PersonMail.Replace("$Code", ds.Tables[0].Rows[0]["PersonCalledOutCode"].ToString());
                PersonMail = PersonMail.Replace("$HomeUrl", homeUrl);

                try
                {
                    obj.sendEmail(Email, "Callout: " + CalloutID + " - Sign-off Approval Request", PersonMail, "Sappi, Callout System");
                }
                catch (Exception)
                {
                    hasError = hasError + ds.Tables[0].Rows[0]["PersonCalledOutName"].ToString() + " '" + Email + "'<br/>";
                }
                //END PERSON CALLED OUT ################################################################################

                //AUTHORIZER / SUPERVISOR ################################################################################

                approveUrl = url + "approval-confirmation.aspx?ID=" + CalloutID + "&Type=2&Code=" + ds.Tables[0].Rows[0]["AuthorisedByCode"].ToString();

                Email = ds.Tables[0].Rows[0]["AuthorizerEmail"].ToString();

                AuthorizerMail = AuthorizerMail.Replace("$Name", ds.Tables[0].Rows[0]["AuthorizerName"].ToString());
                AuthorizerMail = AuthorizerMail.Replace("$CalloutNumber", CalloutID.ToString());
                AuthorizerMail = AuthorizerMail.Replace("$CalloutUrl", calloutUrl);
                AuthorizerMail = AuthorizerMail.Replace("$ApproveUrl", approveUrl);
                AuthorizerMail = AuthorizerMail.Replace("$AlternativeUrl", approveUrl);
                AuthorizerMail = AuthorizerMail.Replace("$Code", ds.Tables[0].Rows[0]["AuthorisedByCode"].ToString());
                AuthorizerMail = AuthorizerMail.Replace("$HomeUrl", homeUrl);

                try
                {
                    obj.sendEmail(Email, "Callout: " + CalloutID + " - Sign-off Approval Request", AuthorizerMail, "Sappi, Callout System");
                }
                catch (Exception)
                {
                    hasError = hasError + ds.Tables[0].Rows[0]["AuthorizerName"].ToString() + " '" + Email + "'<br/>";
                }
                //END AUTHORIZER / SUPERVISOR ################################################################################

                //SECURITY ################################################################################

                approveUrl = url + "approval-confirmation.aspx?ID=" + CalloutID + "&Type=3&Code=" + ds.Tables[0].Rows[0]["SecurityCode"].ToString();

                Email = ds.Tables[0].Rows[0]["SecurityEmail"].ToString();

                SecurityMail = SecurityMail.Replace("$Name", ds.Tables[0].Rows[0]["SecurityName"].ToString());
                SecurityMail = SecurityMail.Replace("$CalloutNumber", CalloutID.ToString());
                SecurityMail = SecurityMail.Replace("$CalloutUrl", calloutUrl);
                SecurityMail = SecurityMail.Replace("$ApproveUrl", approveUrl);
                SecurityMail = SecurityMail.Replace("$AlternativeUrl", approveUrl);
                SecurityMail = SecurityMail.Replace("$Code", ds.Tables[0].Rows[0]["SecurityCode"].ToString());
                SecurityMail = SecurityMail.Replace("$HomeUrl", homeUrl);

                try
                {
                    obj.sendEmail(Email, "Callout: " + CalloutID + " - Sign-off Approval Request", SecurityMail, "Sappi, Callout System");
                }
                catch (Exception)
                {
                    hasError = hasError + ds.Tables[0].Rows[0]["SecurityName"].ToString() + " '" + Email + "'<br/>";
                }
                //END SECURITY ################################################################################


                if (hasError != String.Empty)
                {
                    errorMsg = "<b class='text-danger'><span class='fa fa-exclamation-circle icon-mr'></span>Error sending emails to:</b><br/>" + hasError + " <br/>Please verify email addresses on the <a href='../index.aspx' target='_blank'>Contact system</a>.";
                }

            }

            return errorMsg;
        }
        public String sendSignOfRequest(int CalloutID, String FilePath, int Option)
        {
            String errorMsg = "";

            //Fetching Email Body Text from EmailTemplate File. 
            StreamReader str = new StreamReader(FilePath);
            String MailText = str.ReadToEnd();
            str.Close();

            String PersonMail = MailText;
            String AuthorizerMail = MailText;
            String SecurityMail = MailText;

            access obj = new access();
            obj.generateApprovalRequest(CalloutID);

            obj = new access();
            DataSet ds = obj.viewApprovaRequestDetails(CalloutID);

            String fullPath = (HttpContext.Current.Request.Url.AbsoluteUri);
            String url = fullPath.Substring(0, fullPath.LastIndexOf("/") + 1);


            if (ds.Tables[0].Rows.Count > 0)
            {
                obj = new access();

                String hasError = "";

                String calloutUrl = "";
                String approveUrl = "";
                String homeUrl = "";
                String Email = "";


                if (Option == 1)
                {

                    //PERSON CALLED OUT ################################################################################
                    calloutUrl = url + "callout-update.aspx?ID=" + CalloutID + "&Source=callouts-complete";
                    approveUrl = url + "approval-confirmation.aspx?ID=" + CalloutID + "&Type=1&Code=" + ds.Tables[0].Rows[0]["PersonCalledOutCode"].ToString();
                    homeUrl = url + "index.aspx";

                    Email = ds.Tables[0].Rows[0]["PersonCalledOutEmail"].ToString();

                    PersonMail = PersonMail.Replace("$Name", ds.Tables[0].Rows[0]["PersonCalledOutName"].ToString());
                    PersonMail = PersonMail.Replace("$CalloutNumber", CalloutID.ToString());
                    PersonMail = PersonMail.Replace("$CalloutUrl", calloutUrl);
                    PersonMail = PersonMail.Replace("$ApproveUrl", approveUrl);
                    PersonMail = PersonMail.Replace("$AlternativeUrl", approveUrl);
                    PersonMail = PersonMail.Replace("$Code", ds.Tables[0].Rows[0]["PersonCalledOutCode"].ToString());
                    PersonMail = PersonMail.Replace("$HomeUrl", homeUrl);

                    try
                    {
                        obj.sendEmail(Email, "Callout: " + CalloutID + " - Sign-off Approval Request", PersonMail, "Sappi, Callout System");
                    }
                    catch (Exception)
                    {
                        hasError = hasError + ds.Tables[0].Rows[0]["PersonCalledOutName"].ToString() + " '" + Email + "'<br/>";
                    }
                    //END PERSON CALLED OUT ################################################################################
                }
                else if (Option == 2)
                {

                    //AUTHORIZER / SUPERVISOR ################################################################################

                    approveUrl = url + "approval-confirmation.aspx?ID=" + CalloutID + "&Type=2&Code=" + ds.Tables[0].Rows[0]["AuthorisedByCode"].ToString();

                    Email = ds.Tables[0].Rows[0]["AuthorizerEmail"].ToString();

                    AuthorizerMail = AuthorizerMail.Replace("$Name", ds.Tables[0].Rows[0]["AuthorizerName"].ToString());
                    AuthorizerMail = AuthorizerMail.Replace("$CalloutNumber", CalloutID.ToString());
                    AuthorizerMail = AuthorizerMail.Replace("$CalloutUrl", calloutUrl);
                    AuthorizerMail = AuthorizerMail.Replace("$ApproveUrl", approveUrl);
                    AuthorizerMail = AuthorizerMail.Replace("$AlternativeUrl", approveUrl);
                    AuthorizerMail = AuthorizerMail.Replace("$Code", ds.Tables[0].Rows[0]["AuthorisedByCode"].ToString());
                    AuthorizerMail = AuthorizerMail.Replace("$HomeUrl", homeUrl);

                    try
                    {
                        obj.sendEmail(Email, "Callout: " + CalloutID + " - Sign-off Approval Request", AuthorizerMail, "Sappi, Callout System");
                    }
                    catch (Exception)
                    {
                        hasError = hasError + ds.Tables[0].Rows[0]["AuthorizerName"].ToString() + " '" + Email + "'<br/>";
                    }
                    //END AUTHORIZER / SUPERVISOR ################################################################################
                }
                else if (Option == 3)
                {


                    //SECURITY ################################################################################

                    approveUrl = url + "approval-confirmation.aspx?ID=" + CalloutID + "&Type=3&Code=" + ds.Tables[0].Rows[0]["SecurityCode"].ToString();

                    Email = ds.Tables[0].Rows[0]["SecurityEmail"].ToString();

                    SecurityMail = SecurityMail.Replace("$Name", ds.Tables[0].Rows[0]["SecurityName"].ToString());
                    SecurityMail = SecurityMail.Replace("$CalloutNumber", CalloutID.ToString());
                    SecurityMail = SecurityMail.Replace("$CalloutUrl", calloutUrl);
                    SecurityMail = SecurityMail.Replace("$ApproveUrl", approveUrl);
                    SecurityMail = SecurityMail.Replace("$AlternativeUrl", approveUrl);
                    SecurityMail = SecurityMail.Replace("$Code", ds.Tables[0].Rows[0]["SecurityCode"].ToString());
                    SecurityMail = SecurityMail.Replace("$HomeUrl", homeUrl);

                    try
                    {
                        obj.sendEmail(Email, "Callout: " + CalloutID + " - Sign-off Approval Request", SecurityMail, "Sappi, Callout System");
                    }
                    catch (Exception)
                    {
                        hasError = hasError + ds.Tables[0].Rows[0]["SecurityName"].ToString() + " '" + Email + "'<br/>";
                    }
                    //END SECURITY ################################################################################

                }
                else if(Option == 4)
                {
                    errorMsg = sendSignOfRequest(CalloutID, FilePath);
                }


                if (hasError != String.Empty && Option != 4)
                {
                    errorMsg = "<b class='text-danger'><span class='fa fa-exclamation-circle icon-mr'></span>Error sending emails to:</b><br/>" + hasError + " <br/>Please verify email addresses on the <a href='../index.aspx' target='_blank'>Contact system</a>.";
                }

            }

            return errorMsg;
        }


    }
}