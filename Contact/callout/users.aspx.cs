﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class users : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 0; //Admin only
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void gridStandbyUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            editUser();
        }

        void editUser()
        {
            Session["SelectedUserID"] = gridUsers.SelectedRow.Cells[1].Text.Trim();
            Session["SelectedFullname"] = gridUsers.SelectedRow.Cells[2].Text.Trim();
            Session["SelectedUsername"] = gridUsers.SelectedRow.Cells[3].Text.Trim();
            Response.Redirect("users-edit.aspx");
        }
    }
}