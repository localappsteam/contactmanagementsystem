﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class callout_update_helpers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["MillID"] != null)
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;
                int RoleID = 20;
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(SystemUserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            addHelpers();
        }

        void addHelpers()
        {
            try
            {
                if (drdHelper1.SelectedIndex < 1 && drdHelper2.SelectedIndex < 1 && drdHelper3.SelectedIndex < 1 && drdHelper4.SelectedIndex < 1 && drdHelper5.SelectedIndex < 1
                    && drdHelper6.SelectedIndex < 1 && drdHelper7.SelectedIndex < 1 && drdHelper8.SelectedIndex < 1 && drdHelper9.SelectedIndex < 1 && drdHelper10.SelectedIndex < 1)
                {
                    ShowAlert(2, "Select atleast 1 helper.");
                }
                else
                {
                    string[] DateTimeformats = { "dd/MM/yyyy HH:mm", "yyyy-MM-dd HH:mm" };

                    access obj = new access();
                    System.Data.DataSet ds = obj.viewCalloutDetails(Convert.ToInt32(Session["MillID"].ToString()), Convert.ToInt32(Request.QueryString["ID"]));

                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        int ParentCalloutID = Convert.ToInt32(ds.Tables[0].Rows[0]["CalloutID"].ToString());
                        int MillID = Convert.ToInt32(Session["MillID"]);
                        int EmployeeID = Convert.ToInt32(ds.Tables[0].Rows[0]["PersonCalledOutID"].ToString().Substring(ds.Tables[0].Rows[0]["PersonCalledOutID"].ToString().IndexOf("-") + 1));
                        int EmployeeTypeID = Convert.ToInt32(ds.Tables[0].Rows[0]["PersonCalledOutID"].ToString().Substring(0, ds.Tables[0].Rows[0]["PersonCalledOutID"].ToString().IndexOf("-")));
                        int ControlRoomID = Convert.ToInt32(ds.Tables[0].Rows[0]["StandbyAreaID"].ToString());
                        String Reason = ds.Tables[0].Rows[0]["Reason"].ToString().Trim();

                        int ContactID = Convert.ToInt32(ds.Tables[0].Rows[0]["ContactPersonID"].ToString().Substring(ds.Tables[0].Rows[0]["ContactPersonID"].ToString().IndexOf("-") + 1));
                        int ContactEmpTypeID = Convert.ToInt32(ds.Tables[0].Rows[0]["ContactPersonID"].ToString().Substring(0, ds.Tables[0].Rows[0]["ContactPersonID"].ToString().IndexOf("-")));
                        int AuthorisedByID = Convert.ToInt32(ds.Tables[0].Rows[0]["AuthorizingPersonID"].ToString().Substring(ds.Tables[0].Rows[0]["AuthorizingPersonID"].ToString().IndexOf("-") + 1));
                        int AuthorisedByEmpTypeID = Convert.ToInt32(ds.Tables[0].Rows[0]["AuthorizingPersonID"].ToString().Substring(0, ds.Tables[0].Rows[0]["AuthorizingPersonID"].ToString().IndexOf("-")));
                        String Notification = ds.Tables[0].Rows[0]["Notification"].ToString().Trim();
                        int StandbyID = Convert.ToInt32(ds.Tables[0].Rows[0]["StandbyID"].ToString().Trim());
                        int StandbyIncorrect = Convert.ToInt32(ds.Tables[0].Rows[0]["StandbyIncorrect"].ToString());
                        int CalloutTypeID = Convert.ToInt32(ds.Tables[0].Rows[0]["CalloutTypeID"].ToString());

                        obj = new access();

                        if (drdHelper1.SelectedIndex > 0)
                        {
                            int HelperID = Convert.ToInt32(drdHelper1.SelectedValue.Substring(drdHelper1.SelectedValue.IndexOf("-") + 1));
                            int HelperEmpTypeID = Convert.ToInt32(drdHelper1.SelectedValue.Substring(0, drdHelper1.SelectedValue.IndexOf("-")));

                            obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                                ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                                StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                        }
                        if (drdHelper2.SelectedIndex > 0)
                        {
                            int HelperID = Convert.ToInt32(drdHelper2.SelectedValue.Substring(drdHelper2.SelectedValue.IndexOf("-") + 1));
                            int HelperEmpTypeID = Convert.ToInt32(drdHelper2.SelectedValue.Substring(0, drdHelper2.SelectedValue.IndexOf("-")));

                            obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                                ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                                StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                        }
                        if (drdHelper3.SelectedIndex > 0)
                        {
                            int HelperID = Convert.ToInt32(drdHelper3.SelectedValue.Substring(drdHelper3.SelectedValue.IndexOf("-") + 1));
                            int HelperEmpTypeID = Convert.ToInt32(drdHelper3.SelectedValue.Substring(0, drdHelper3.SelectedValue.IndexOf("-")));

                            obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                                ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                                StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                        }
                        if (drdHelper4.SelectedIndex > 0)
                        {
                            int HelperID = Convert.ToInt32(drdHelper4.SelectedValue.Substring(drdHelper4.SelectedValue.IndexOf("-") + 1));
                            int HelperEmpTypeID = Convert.ToInt32(drdHelper4.SelectedValue.Substring(0, drdHelper4.SelectedValue.IndexOf("-")));

                            obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                                ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                                StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                        }
                        if (drdHelper5.SelectedIndex > 0)
                        {
                            int HelperID = Convert.ToInt32(drdHelper5.SelectedValue.Substring(drdHelper5.SelectedValue.IndexOf("-") + 1));
                            int HelperEmpTypeID = Convert.ToInt32(drdHelper5.SelectedValue.Substring(0, drdHelper5.SelectedValue.IndexOf("-")));

                            obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                                ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                                StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                        }
                        if (drdHelper6.SelectedIndex > 0)
                        {
                            int HelperID = Convert.ToInt32(drdHelper6.SelectedValue.Substring(drdHelper6.SelectedValue.IndexOf("-") + 1));
                            int HelperEmpTypeID = Convert.ToInt32(drdHelper6.SelectedValue.Substring(0, drdHelper6.SelectedValue.IndexOf("-")));

                            obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                                ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                                StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                        }
                        if (drdHelper7.SelectedIndex > 0)
                        {
                            int HelperID = Convert.ToInt32(drdHelper7.SelectedValue.Substring(drdHelper7.SelectedValue.IndexOf("-") + 1));
                            int HelperEmpTypeID = Convert.ToInt32(drdHelper7.SelectedValue.Substring(0, drdHelper7.SelectedValue.IndexOf("-")));

                            obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                                ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                                StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                        }
                        if (drdHelper8.SelectedIndex > 0)
                        {
                            int HelperID = Convert.ToInt32(drdHelper8.SelectedValue.Substring(drdHelper8.SelectedValue.IndexOf("-") + 1));
                            int HelperEmpTypeID = Convert.ToInt32(drdHelper8.SelectedValue.Substring(0, drdHelper8.SelectedValue.IndexOf("-")));

                            obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                                ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                                StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                        }
                        if (drdHelper9.SelectedIndex > 0)
                        {
                            int HelperID = Convert.ToInt32(drdHelper9.SelectedValue.Substring(drdHelper9.SelectedValue.IndexOf("-") + 1));
                            int HelperEmpTypeID = Convert.ToInt32(drdHelper9.SelectedValue.Substring(0, drdHelper9.SelectedValue.IndexOf("-")));

                            obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                                ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                                StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                        }
                        if (drdHelper10.SelectedIndex > 0)
                        {
                            int HelperID = Convert.ToInt32(drdHelper10.SelectedValue.Substring(drdHelper10.SelectedValue.IndexOf("-") + 1));
                            int HelperEmpTypeID = Convert.ToInt32(drdHelper10.SelectedValue.Substring(0, drdHelper10.SelectedValue.IndexOf("-")));

                            obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                                ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                                StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                        }

                        Session["HelpersAdded"] = true;

                        ShowAlert(1, "Callout helpers added successfully.");
                    }
                    else
                    {
                        ShowAlert(2, "Invalid callout number.");
                    }
                }
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
            
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("callout-update.aspx?ID=" + Request.QueryString["ID"] + "&Source=" + Request.QueryString["Source"]);
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["HelpersAdded"] != null)
            {
                Session.Remove("HelpersAdded");
                Response.Redirect("callout-update.aspx?ID=" + Request.QueryString["ID"]+"&Source="+ Request.QueryString["Source"]);
            }
        }

       
    }
}