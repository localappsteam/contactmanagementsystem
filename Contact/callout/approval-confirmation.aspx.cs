﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class approval_confirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            approveCallout();
        }

        void approveCallout()
        {
            if (!Page.IsPostBack)
            {
                divSuccess.Visible = false;
                divError.Visible = false;

                if (Request.QueryString["ID"] != null && Request.QueryString["Type"] != null && Request.QueryString["Code"] != null)
                {
                    try
                    {
                        int CallouID = Convert.ToInt32(Request.QueryString["ID"].Trim());
                        int Type = Convert.ToInt32(Request.QueryString["Type"].Trim());
                        int UniqueCode = Convert.ToInt32(Request.QueryString["Code"].Trim());

                        System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String SystemUserName = UserSecurityPrincipal.Name;

                        access obj = new access();
                        obj.approveCallout(CallouID, UniqueCode, Type, SystemUserName);

                        divSuccess.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        lblError.Text = ex.Message;
                        divError.Visible = true;
                    }

                }
                else
                {
                    Response.Redirect("callouts-complete.aspx");
                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Write("<script>window.open ('pdf.aspx?ID=" + Request.QueryString["ID"].Trim() + "&RPT=1','_blank');</script>");
        }
    }
}