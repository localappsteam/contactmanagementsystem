﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="approval-confirmation.aspx.cs" Inherits="Contact.callout.approval_confirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Callout Approval</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Callout Approval</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row" id="divSuccess" runat="server" visible="false">
                                    <div class="col-md-12 text-center">
                                        <h3 class="text-success margin-0">Success<span class="fa fa-thumbs-up icon-ml"></span></h3>
                                        <br />
                                        <h4 style="font-weight:normal">The callout was successfully approved.<br /><br />Click on the "<b>Print callout</b>" button to view the callout information or click on "<b>Close</b>" to close this page.</h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5"></div>
                                        <div class="col-md-2 btn-fix-width">
                                            <asp:LinkButton ID="btnPrint" CssClass="btn btn-block btn-primary" runat="server" OnClick="btnPrint_Click"><p class="fa fa-print icon-mr"></p>Print callout</asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5"></div>
                                        <div class="col-md-2 btn-fix-width">
                                            <a href="callouts-complete.aspx" class="btn btn-block btn-default"><p class="fa fa-times icon-mr"></p>Close</a>
                                            </div>
                                        </div>
                                </div>
                                <div class="row" id="divError" runat="server" visible="false">
                                    <div class="col-md-12 text-center">
                                        <h3 class="text-danger margin-0">Denied<span class="fa fa-exclamation-triangle icon-ml"></span></h3>
                                        <br />
                                        <h4 style="font-weight:normal"><b class="text-danger">Error: </b><asp:Label ID="lblError" runat="server" Text="dsfsdfsdf"></asp:Label></h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5"></div>
                                        <div class="col-md-2 btn-fix-width">
                                            <a href="callouts-complete.aspx" class="btn btn-block btn-default"><p class="fa fa-times icon-mr"></p>Close</a>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
