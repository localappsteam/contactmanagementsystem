﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class create_callout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           AuthenticateUser();

        }

        void AuthenticateUser()
        {
            if (Session["MillID"] != null)
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;
                int RoleID = 20;
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(SystemUserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void gridDuties_SelectedIndexChanged(object sender, EventArgs e)
        {
            next();
        }

        void next()
        {
            try
            {
                access obj = new access();

                drdPersonToCall.SelectedValue = obj.getEmployeeIDonStandby(Convert.ToInt64(gridDuties.SelectedValue.ToString().Trim())).ToString();
                divDuty.Visible = false;
                divDetails.Visible = true;

                lblDuty.Text = (gridDuties.SelectedRow.Cells[2].Text + " - " + gridDuties.SelectedRow.Cells[3].Text).ToUpper();
            }
            catch (Exception ex)
            {
                ShowAlert(2, ex.Message);
            }
           

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            createCallout();
        }

        void createCallout()
        {
            try
            {
                if (Session["MillID"] == null)
                {
                    //Do nothing
                }
                else if (drdPersonToCall.SelectedIndex < 1)
                {
                    ShowAlert(2, "Select a person to callout first.");
                }
                else if (drdArea.SelectedIndex < 1)
                {
                    ShowAlert(2, "Select a Area first.");
                }
                else if (drdContact.SelectedIndex < 1)
                {
                    ShowAlert(2, "Select a contact person first.");
                }
                else if (drdAuthorised.SelectedIndex < 1)
                {
                    ShowAlert(2, "Select the person who authorised the callout first.");
                }
                else if (drdCalloutType.SelectedIndex < 1)
                {
                    ShowAlert(2, "Select a the callout type first.");
                }
                else if (txtDate.Text.Trim() == String.Empty || txtTime.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Enter the Incident date and time first.");
                }
                else if (!access.isDateTime(txtDate.Text.Trim() + " " + txtTime.Text.Trim()))
                {
                    ShowAlert(2, "Enter a valid Incident date and time. Use date format 'dd/mm/yyyy' or 'yyyy-mm-dd', and time format 'hh:mm'");
                }
                else if (txtReasonForCallout.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Enter a reason for the callout first.");
                }
                else
                {
                    if (rdHelpers.SelectedValue.Trim() == "0")
                    {

                        access obj = new access();

                        System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String SystemUserName = UserSecurityPrincipal.Name;

                        int MillID = Convert.ToInt32(Session["MillID"]);
                        int EmployeeID = Convert.ToInt32(drdPersonToCall.SelectedValue.Substring(drdPersonToCall.SelectedValue.IndexOf("-") + 1));
                        int EmployeeTypeID = Convert.ToInt32(drdPersonToCall.SelectedValue.Substring(0, drdPersonToCall.SelectedValue.IndexOf("-")));
                        int StandbyAreaID = Convert.ToInt32(drdArea.SelectedValue.Trim());
                        String Reason = txtReasonForCallout.Text.Trim();
                        int ContactID = Convert.ToInt32(drdContact.SelectedValue.Substring(drdContact.SelectedValue.IndexOf("-") + 1));
                        int ContactEmpTypeID = Convert.ToInt32(drdContact.SelectedValue.Substring(0, drdContact.SelectedValue.IndexOf("-")));
                        int AuthorisedByID = Convert.ToInt32(drdAuthorised.SelectedValue.Substring(drdAuthorised.SelectedValue.IndexOf("-") + 1));
                        int AuthorisedByEmpTypeID = Convert.ToInt32(drdAuthorised.SelectedValue.Substring(0, drdAuthorised.SelectedValue.IndexOf("-")));
                        String Notification = txtNotificationNo.Text.Trim();
                        int StandbyID = Convert.ToInt32(gridDuties.SelectedValue.ToString().Trim());
                        int StandbyIncorrect = chkIncorrectList.Checked ? 1 : 0;
                        int CalloutTypeID = Convert.ToInt32(drdCalloutType.SelectedValue.Trim());
                        String IncidentTime = access.convertToDateTimeString(txtDate.Text.Trim() + " " + txtTime.Text.Trim());

                        obj.createCallout(SystemUserName, MillID, EmployeeID, EmployeeTypeID, StandbyAreaID, Reason, ContactID, ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, 0, StandbyID, StandbyIncorrect, CalloutTypeID, IncidentTime);

                        Session["CalloutSubmitted"] = true;

                        ShowAlert(1, "Callout created successfully.");
                    }
                    else
                    {
                        divDetails.Visible = false;
                        divHelpers.Visible = true;
                    }


                }
            }
            catch (Exception ex)
            {
                ShowAlert(2, ex.Message);
            }
            
        }


        protected void btnSubmit2_Click(object sender, EventArgs e)
        {
            addHelpers();
        }

        void addHelpers()
        {
            if(drdHelper1.SelectedIndex < 1 && drdHelper2.SelectedIndex < 1 && drdHelper3.SelectedIndex < 1 && drdHelper4.SelectedIndex < 1 && drdHelper5.SelectedIndex < 1)
            {
                ShowAlert(2, "Select atleast 1 helper.");
            }
            else
            {
                string[] DateTimeformats = { "dd/MM/yyyy HH:mm", "yyyy-MM-dd HH:mm" };

                access obj = new access();

                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;

                int MillID = Convert.ToInt32(Session["MillID"]);
                int EmployeeID = Convert.ToInt32(drdPersonToCall.SelectedValue.Substring(drdPersonToCall.SelectedValue.IndexOf("-") + 1));
                int EmployeeTypeID = Convert.ToInt32(drdPersonToCall.SelectedValue.Substring(0, drdPersonToCall.SelectedValue.IndexOf("-")));
                int ControlRoomID = Convert.ToInt32(drdArea.SelectedValue.Trim());
                String Reason = txtReasonForCallout.Text.Trim();
                int ContactID = Convert.ToInt32(drdContact.SelectedValue.Substring(drdContact.SelectedValue.IndexOf("-") + 1));
                int ContactEmpTypeID = Convert.ToInt32(drdContact.SelectedValue.Substring(0, drdContact.SelectedValue.IndexOf("-")));
                int AuthorisedByID = Convert.ToInt32(drdAuthorised.SelectedValue.Substring(drdAuthorised.SelectedValue.IndexOf("-") + 1));
                int AuthorisedByEmpTypeID = Convert.ToInt32(drdAuthorised.SelectedValue.Substring(0, drdAuthorised.SelectedValue.IndexOf("-")));
                String Notification = txtNotificationNo.Text.Trim();
                int StandbyID = Convert.ToInt32(gridDuties.SelectedValue.ToString().Trim());
                int StandbyIncorrect = chkIncorrectList.Checked ? 1 : 0;
                int CalloutTypeID = Convert.ToInt32(drdCalloutType.SelectedValue.Trim());
                String IncidentTime = access.convertToDateTimeString(txtDate.Text.Trim() + " " + txtTime.Text.Trim());

                int ParentCalloutID = obj.createCallout(SystemUserName, MillID, EmployeeID, EmployeeTypeID, ControlRoomID, Reason, ContactID, ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, 0, StandbyID, StandbyIncorrect, CalloutTypeID, IncidentTime);


                if (drdHelper1.SelectedIndex > 0)
                {
                    int HelperID = Convert.ToInt32(drdHelper1.SelectedValue.Substring(drdHelper1.SelectedValue.IndexOf("-") + 1));
                    int HelperEmpTypeID = Convert.ToInt32(drdHelper1.SelectedValue.Substring(0, drdHelper1.SelectedValue.IndexOf("-")));
                    
                    obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                        ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                        StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                }
                if (drdHelper2.SelectedIndex > 0)
                {
                    int HelperID = Convert.ToInt32(drdHelper2.SelectedValue.Substring(drdHelper2.SelectedValue.IndexOf("-") + 1));
                    int HelperEmpTypeID = Convert.ToInt32(drdHelper2.SelectedValue.Substring(0, drdHelper2.SelectedValue.IndexOf("-")));

                    obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                        ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                        StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                }
                if (drdHelper3.SelectedIndex > 0)
                {
                    int HelperID = Convert.ToInt32(drdHelper3.SelectedValue.Substring(drdHelper3.SelectedValue.IndexOf("-") + 1));
                    int HelperEmpTypeID = Convert.ToInt32(drdHelper3.SelectedValue.Substring(0, drdHelper3.SelectedValue.IndexOf("-")));

                    obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                        ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                        StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                }
                if (drdHelper4.SelectedIndex > 0)
                {
                    int HelperID = Convert.ToInt32(drdHelper4.SelectedValue.Substring(drdHelper4.SelectedValue.IndexOf("-") + 1));
                    int HelperEmpTypeID = Convert.ToInt32(drdHelper4.SelectedValue.Substring(0, drdHelper4.SelectedValue.IndexOf("-")));

                    obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                        ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                        StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                }
                if (drdHelper5.SelectedIndex > 0)
                {
                    int HelperID = Convert.ToInt32(drdHelper5.SelectedValue.Substring(drdHelper5.SelectedValue.IndexOf("-") + 1));
                    int HelperEmpTypeID = Convert.ToInt32(drdHelper5.SelectedValue.Substring(0, drdHelper5.SelectedValue.IndexOf("-")));

                    obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                        ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                        StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                }
                if (drdHelper6.SelectedIndex > 0)
                {
                    int HelperID = Convert.ToInt32(drdHelper6.SelectedValue.Substring(drdHelper6.SelectedValue.IndexOf("-") + 1));
                    int HelperEmpTypeID = Convert.ToInt32(drdHelper6.SelectedValue.Substring(0, drdHelper6.SelectedValue.IndexOf("-")));

                    obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                        ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                        StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                }
                if (drdHelper7.SelectedIndex > 0)
                {
                    int HelperID = Convert.ToInt32(drdHelper7.SelectedValue.Substring(drdHelper7.SelectedValue.IndexOf("-") + 1));
                    int HelperEmpTypeID = Convert.ToInt32(drdHelper7.SelectedValue.Substring(0, drdHelper7.SelectedValue.IndexOf("-")));

                    obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                        ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                        StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                }
                if (drdHelper8.SelectedIndex > 0)
                {
                    int HelperID = Convert.ToInt32(drdHelper8.SelectedValue.Substring(drdHelper8.SelectedValue.IndexOf("-") + 1));
                    int HelperEmpTypeID = Convert.ToInt32(drdHelper8.SelectedValue.Substring(0, drdHelper8.SelectedValue.IndexOf("-")));

                    obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                        ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                        StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                }
                if (drdHelper9.SelectedIndex > 0)
                {
                    int HelperID = Convert.ToInt32(drdHelper9.SelectedValue.Substring(drdHelper9.SelectedValue.IndexOf("-") + 1));
                    int HelperEmpTypeID = Convert.ToInt32(drdHelper9.SelectedValue.Substring(0, drdHelper9.SelectedValue.IndexOf("-")));

                    obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                        ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                        StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                }
                if (drdHelper10.SelectedIndex > 0)
                {
                    int HelperID = Convert.ToInt32(drdHelper10.SelectedValue.Substring(drdHelper10.SelectedValue.IndexOf("-") + 1));
                    int HelperEmpTypeID = Convert.ToInt32(drdHelper10.SelectedValue.Substring(0, drdHelper10.SelectedValue.IndexOf("-")));

                    obj.createCallout(SystemUserName, MillID, HelperID, HelperEmpTypeID, ControlRoomID, Reason, ContactID,
                        ContactEmpTypeID, AuthorisedByID, AuthorisedByEmpTypeID, Notification, ParentCalloutID,
                        StandbyID, StandbyIncorrect, CalloutTypeID, "1999-01-01 00:00");
                }

                Session["CalloutSubmitted"] = true;

                ShowAlert(1, "Callout created successfully.");
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            divHelpers.Visible = false;
            divDetails.Visible = true;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["CalloutSubmitted"] != null)
            {
                Session.Remove("CalloutSubmitted");
                Response.Redirect("callouts-active.aspx");
            }
        }

       
    }
}