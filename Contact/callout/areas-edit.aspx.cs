﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class areas_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
            loadArea();
        }

        void AuthenticateUser()
        {
            if (Session["MillID"] != null)
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;
                int RoleID = 0; //ADMIN ONLY
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(SystemUserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        void loadArea()
        {
            if (Request.QueryString["ID"] != null && Request.QueryString["Description"] !=null && Request.QueryString["Ext"] != null)
            {
                if (!Page.IsPostBack)
                {
                    txtAreaName.Text = Request.QueryString["Description"].ToString().Trim();
                    txtExtention.Text = Request.QueryString["Ext"].ToString().Trim();
                }

            }
            else
            {
                Response.Redirect("areas.aspx");
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            divDelete.Visible = true;
        }

        protected void btnDeleteYes_Click(object sender, EventArgs e)
        {
            deleteArea();
        }

        protected void btnDeleteNo_Click(object sender, EventArgs e)
        {
            divDelete.Visible = false;
        }
        void deleteArea()
        {
            try
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;

                access obj = new access();
                obj.deleteArea(SystemUserName, Convert.ToInt32(Request.QueryString["ID"].ToString()));

                Session["AreaUpdated"] = true;
                ShowAlert(1, "Area deleted successfully.");
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            updateArea();
        }

        void updateArea()
        {
            try
            {
                if (txtAreaName.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "A area name is required.");
                }
                else
                {

                    int AreaID = Convert.ToInt32(Request.QueryString["ID"].ToString());
                    String Description = txtAreaName.Text.Trim();
                    String Ext = txtExtention.Text.Trim();

                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    access obj = new access();
                    obj.updateArea(SystemUserName, AreaID, Description, Ext);

                    Session["AreaUpdated"] = true;

                    ShowAlert(1, "Area updated successfully.");
                }

            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
                
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["AreaUpdated"] != null)
            {
                Session.Remove("AreaUpdated");
                Response.Redirect("areas.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }
}