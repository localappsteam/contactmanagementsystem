﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using System.IO;
using System.Data;

namespace Contact.callout
{
    public partial class pdf : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadPdf();
        }


        void loadPdf()
        {
            if (!Page.IsPostBack)
            {

                int CalloutID = 0;

                try
                {
                    CalloutID = Convert.ToInt32(Request.QueryString["ID"]);
                }
                catch (Exception)
                {
                }

                access obj = new access();
                DataSet ds = obj.getCalloutPdfData(CalloutID);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    MemoryStream memoryStream = new MemoryStream();

                    Document document = new Document(PageSize.A4, 35, 35, 10, 10);

                    Font lbfont = FontFactory.GetFont(FontFactory.HELVETICA, 8f);
                    Paragraph linebreak = new Paragraph("\n", lbfont);
                    Font headingFont = FontFactory.GetFont(FontFactory.HELVETICA, 14f);
                    Font headingFont2 = FontFactory.GetFont(FontFactory.HELVETICA, 12f);
                    Font subHeadingFont = FontFactory.GetFont(FontFactory.HELVETICA, 9f);

                    Font lineFont = FontFactory.GetFont(FontFactory.HELVETICA, 5f);
                    Paragraph underLine = new Paragraph("_____________________________________________________________________________________________" +
                                                        "_____________________________________________________________________________________________", lineFont);

                    //DOCUMENT HEADING ####################################################################################
                    PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                    document.Open();

                    Paragraph Heading = new Paragraph("Sappi " + ds.Tables[0].Rows[0]["Mill"] + "\n", headingFont);
                    Heading.Alignment = Element.ALIGN_CENTER;
                    document.Add(Heading);
                    Heading = new Paragraph("Callout Report", headingFont2);
                    Heading.Alignment = Element.ALIGN_CENTER;
                    document.Add(Heading);
                    //END DOCUMENT HEADING################################################################################

                    document.Add(linebreak);//LINE BREAK

                    //CALLOUT DETAILS SECTION ############################################################################
                    Paragraph subHeading = new Paragraph("Callout Details".ToUpper(), subHeadingFont);
                    document.Add(subHeading);

                    document.Add(underLine);
                    document.Add(linebreak);

                    PdfPTable table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    addCell(table, "Callout Number:", ds.Tables[0].Rows[0]["CalloutID"].ToString());
                    addCell(table, "Standby Duty:", ds.Tables[0].Rows[0]["StandbyDuty"].ToString());
                    addCell(table, "Control Room:", ds.Tables[0].Rows[0]["ControlRoom"].ToString());
                    addCell(table, "Reason:", ds.Tables[0].Rows[0]["Reason"].ToString());
                    addCell(table, "Notification:", ds.Tables[0].Rows[0]["Notification"].ToString());
                    addCell(table, "Authorised By:", ds.Tables[0].Rows[0]["AuthorisedBy"].ToString());
                    addCell(table, "Contact Person:", ds.Tables[0].Rows[0]["ContactPerson"].ToString());
                    addCell(table, "Extension:", ds.Tables[0].Rows[0]["ControlExt"].ToString());
                    document.Add(table);
                    document.Add(linebreak);
                    //END DETAILS SECTION ################################################################################

                    //TIME DETAILS SECTION ############################################################################
                    subHeading = new Paragraph("Time Details".ToUpper(), subHeadingFont);
                    document.Add(subHeading);

                    document.Add(underLine);
                    document.Add(linebreak);

                    table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    addCell(table, "Time Captured:", ds.Tables[0].Rows[0]["CreateDate"].ToString());
                    addCell(table, "Incident Time:", ds.Tables[0].Rows[0]["IncidentTime"].ToString());
                    addCell(table, "Time Called:", ds.Tables[0].Rows[0]["TimeArtisanCalled"].ToString());
                    addCell(table, "Time On-Site:", ds.Tables[0].Rows[0]["DateTimeIn"].ToString());
                    addCell(table, "Time Off-Site:", ds.Tables[0].Rows[0]["DateTimeOut"].ToString());
                    document.Add(table);
                    document.Add(linebreak);
                    //END TIME SECTION ################################################################################

                    //PERSON CALLED OUT SECTION ############################################################################
                    subHeading = new Paragraph("Person Called Out".ToUpper(), subHeadingFont);
                    document.Add(subHeading);

                    document.Add(underLine);
                    document.Add(linebreak);

                    table = new PdfPTable(2);
                    table.WidthPercentage = 100;
                    addCell(table, "Person Called Out:", ds.Tables[0].Rows[0]["PersonCalledOut"].ToString());
                    addCell(table, "Company Number:", ds.Tables[0].Rows[0]["EmployeeNumber"].ToString());
                    addCell(table, "Address:", ds.Tables[0].Rows[0]["Address"].ToString());
                    addCell(table, "Tel:", ds.Tables[0].Rows[0]["Tel"].ToString());
                    addCell(table, "Cell:", ds.Tables[0].Rows[0]["Cell"].ToString());
                    addCell(table, "Extension:", ds.Tables[0].Rows[0]["PersonCalledOutExt"].ToString());
                    document.Add(table);
                    document.Add(linebreak);
                    //END PERSON CALLED OUT SECTION ################################################################################

                    //SIGNATURES SECTION ############################################################################
                    subHeading = new Paragraph("Approvals".ToUpper(), subHeadingFont);
                    document.Add(subHeading);

                    document.Add(underLine);
                    document.Add(linebreak); document.Add(linebreak); document.Add(linebreak);

                    table = new PdfPTable(3);
                    table.WidthPercentage = 100;

                    addSignatureCell(table, ds.Tables[0].Rows[0]["PersonCalledOutApproved"].ToString(), ds.Tables[0].Rows[0]["PersonCalledOut"].ToString(), "(Person Called Out)");
                    addSignatureCell(table, ds.Tables[0].Rows[0]["AuthorisedByApproved"].ToString(), ds.Tables[0].Rows[0]["AuthorisedBy"].ToString(), "(Authorised by)");
                    addSignatureCell(table, ds.Tables[0].Rows[0]["SecurityApproved"].ToString(), ds.Tables[0].Rows[0]["SecurityName"].ToString(), "(Security)");

                    addEmptyCell(table);
                    addSignatureCell(table, ds.Tables[0].Rows[0]["TimeCompleted"].ToString().Replace(",", ""), "Date/Time Completed", "");
                    addEmptyCell(table);
                    document.Add(table);
                    //END SIGNATURES SECTION ################################################################################


                    //FOOTER SECTION ################################################################################
                    Paragraph footer = new Paragraph("Print date: " + ds.Tables[0].Rows[0]["PrintDate"].ToString(), FontFactory.GetFont(FontFactory.TIMES, 9, iTextSharp.text.Font.NORMAL));
                    footer.Alignment = Element.ALIGN_RIGHT;
                    PdfPTable footerTbl = new PdfPTable(1);
                    footerTbl.TotalWidth = 900;
                    footerTbl.HorizontalAlignment = Element.ALIGN_RIGHT;
                    PdfPCell cell = new PdfPCell(footer);
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    footerTbl.AddCell(cell);
                    footerTbl.WriteSelectedRows(0, -1, 415, 30, writer.DirectContent);
                    document.Add(footerTbl);
                    //END FOOTER SECTION ################################################################################

                    document.Close();
                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();
                    Response.Clear();
                    Response.ContentType = "application/pdf";

                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.Expires = -1;
                    Response.Buffer = true;
                    Response.BinaryWrite(bytes);
                    Response.End();
                }

            }
        }

        public void addCell(PdfPTable Tbl, String Heading, String Value)
        {
            Tbl.HorizontalAlignment = Element.ALIGN_LEFT;
            Font titleFont = FontFactory.GetFont(FontFactory.HELVETICA, 9f);
            Font valueFont = FontFactory.GetFont(FontFactory.HELVETICA, 9f);
            Paragraph title = new Paragraph(Heading, titleFont);
            Paragraph value = new Paragraph(Value, valueFont);
            PdfPCell cell = new PdfPCell(title);
            cell.Border = 0;
            cell.PaddingBottom = 5;
            Tbl.AddCell(cell);
            cell = new PdfPCell(value);
            cell.Border = 0;
            cell.PaddingBottom = 5;
            Tbl.AddCell(cell);
        }

        public void addSignatureCell(PdfPTable Tbl, String Value, String Name, String Heading)
        {
            Tbl.HorizontalAlignment = Element.ALIGN_LEFT;
            Font titleFont = FontFactory.GetFont(FontFactory.HELVETICA, 9f);
            Paragraph title = new Paragraph(Value + "\n_________________________\n\n" + Name + "\n\n"+ Heading, titleFont);
            PdfPCell cell = new PdfPCell(title);
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.PaddingBottom = 25;
            Tbl.AddCell(cell);

        }

        public void addEmptyCell(PdfPTable Tbl)
        {
            Tbl.HorizontalAlignment = Element.ALIGN_LEFT;
            Font titleFont = FontFactory.GetFont(FontFactory.HELVETICA, 9f);
            Paragraph title = new Paragraph("", titleFont);

            PdfPCell cell = new PdfPCell(title);
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            Tbl.AddCell(cell);
        }
    }
}