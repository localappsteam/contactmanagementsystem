﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using System.IO;
using System.Data;

namespace Contact.callout
{
    public partial class report_pdf : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadPdf();
        }

        void loadPdf()
        {
            if (!Page.IsPostBack)
            {

                if(Request.QueryString["Type"] != null && Session["ReportFilter"] != null && Session["MillID"] !=null)
                {

                    int ReportType = 0;
                    String Filter = "";

                    try
                    {
                        ReportType = Convert.ToInt32(Request.QueryString["Type"]);
                        Filter = Session["ReportFilter"].ToString();
                    }
                    catch (Exception)
                    {
                    }


                    if (ReportType == 1) //Full Report
                    {
                        generateFullReport(Filter);
                    }
                    else //Summary Report
                    {
                        generateSummaryReport(Filter);
                    }
                }
            }
        }

        void generateFullReport(String Filter)
        {
            access obj = new access();
            DataSet ds = obj.viewCalloutsFullReportPdf(Convert.ToInt32(Session["MillID"]), Filter);

            MemoryStream memoryStream = new MemoryStream();

            Document document = new Document(PageSize.A4, 30, 30, 30, 30);

            Font lbfont = FontFactory.GetFont(FontFactory.HELVETICA, 8f);
            Paragraph linebreak = new Paragraph("\n", lbfont);
            Font headingFont = FontFactory.GetFont(FontFactory.HELVETICA, 12f);
            Font headingFont2 = FontFactory.GetFont(FontFactory.HELVETICA, 12f);
            Font subHeadingFont = FontFactory.GetFont(FontFactory.HELVETICA, 8f);

            Font lineFont = FontFactory.GetFont(FontFactory.HELVETICA, 5f);
            Paragraph underLine = new Paragraph("_____________________________________________________________________________________________" +
                                                "_____________________________________________________________________________________________", lineFont);

            //DOCUMENT HEADING ####################################################################################
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            document.Open();

            Paragraph Heading = new Paragraph("Sappi " + Session["SelectedOffice"] + "\n", headingFont);
            Heading.Alignment = Element.ALIGN_CENTER;
            document.Add(Heading);
            Heading = new Paragraph("Callout List Report", headingFont2);
            Heading.Alignment = Element.ALIGN_CENTER;
            document.Add(Heading);
            //END DOCUMENT HEADING################################################################################

            document.Add(linebreak);//LINE BREAK
            document.Add(linebreak);

            //COLUMN HEADING SECTION ############################################################################

            PdfPTable table = new PdfPTable(7);
            table.WidthPercentage = 100;
            addHeadingCell(table, "Callout No");
            addHeadingCell(table, "Person Called-out");
            addHeadingCell(table, "Company No");
            addHeadingCell(table, "Contact Person");
            addHeadingCell(table, "Incident Time");
            addHeadingCell(table, "Date Completed");
            addHeadingCell(table, "Status");

            document.Add(table);
            document.Add(linebreak);
            //END COLUMN HEADING SECTION ################################################################################

            if (ds.Tables[0].Rows.Count > 0)
            {
                table = new PdfPTable(7);
                table.WidthPercentage = 100;

                for(int i=0; i< ds.Tables[0].Rows.Count; i++)
                {
                    for(int j=0; j<7; j++)
                    {
                        addValueCell(table, ds.Tables[0].Rows[i][j].ToString());
                    }
                    
                }

                document.Add(table);
                document.Add(linebreak);
            }

            document.Close();
            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";

            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            Response.BinaryWrite(bytes);
            Response.End();

        }


        void generateSummaryReport(String Filter)
        {
            access obj = new access();
            DataSet ds = obj.viewCalloutsSummaryReportPdf(Convert.ToInt32(Session["MillID"]), Filter);

            MemoryStream memoryStream = new MemoryStream();

            Document document = new Document(PageSize.A4, 30, 30, 30, 30);

            Font lbfont = FontFactory.GetFont(FontFactory.HELVETICA, 8f);
            Paragraph linebreak = new Paragraph("\n", lbfont);
            Font headingFont = FontFactory.GetFont(FontFactory.HELVETICA, 12f);
            Font headingFont2 = FontFactory.GetFont(FontFactory.HELVETICA, 12f);
            Font subHeadingFont = FontFactory.GetFont(FontFactory.HELVETICA, 8f);

            Font lineFont = FontFactory.GetFont(FontFactory.HELVETICA, 5f);
            Paragraph underLine = new Paragraph("_____________________________________________________________________________________________" +
                                                "_____________________________________________________________________________________________", lineFont);

            //DOCUMENT HEADING ####################################################################################
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            document.Open();

            Paragraph Heading = new Paragraph("Sappi " + Session["SelectedOffice"] + "\n", headingFont);
            Heading.Alignment = Element.ALIGN_CENTER;
            document.Add(Heading);
            Heading = new Paragraph("Callout Summary Report", headingFont2);
            Heading.Alignment = Element.ALIGN_CENTER;
            document.Add(Heading);
            //END DOCUMENT HEADING################################################################################

            document.Add(linebreak);//LINE BREAK
            document.Add(linebreak);

            //COLUMN HEADING SECTION ############################################################################

            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            addHeadingCell(table, "Callout Date");
            addHeadingCell(table, "Number of Callouts");

            document.Add(table);
            document.Add(linebreak);
            //END COLUMN HEADING SECTION ################################################################################

            if (ds.Tables[0].Rows.Count > 0)
            {
                table = new PdfPTable(2);
                table.WidthPercentage = 100;

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        addValueCell(table, ds.Tables[0].Rows[i][j].ToString());
                    }

                }

                document.Add(table);
                document.Add(linebreak);
            }

            document.Close();
            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";

            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;
            Response.BinaryWrite(bytes);
            Response.End();
        }

        public void addHeadingCell(PdfPTable Tbl, String Title)
        {
            Tbl.HorizontalAlignment = Element.ALIGN_LEFT;
            Font valueFont = FontFactory.GetFont(FontFactory.HELVETICA, 9f, Font.UNDERLINE, BaseColor.BLACK);
            Paragraph value = new Paragraph(Title, valueFont);
            PdfPCell cell = new PdfPCell(value);
            cell.Border = 0;
            cell.PaddingBottom = 5;
            Tbl.AddCell(cell);
        }

        public void addValueCell(PdfPTable Tbl, String text)
        {
            Tbl.HorizontalAlignment = Element.ALIGN_LEFT;
            Font valueFont = FontFactory.GetFont(FontFactory.HELVETICA, 8f);
            Paragraph value = new Paragraph(text.ToUpper(), valueFont);
            PdfPCell cell = new PdfPCell(value);
            cell.Border = 0;
            cell.PaddingBottom = 10;
            Tbl.AddCell(cell);
        }
    }
}