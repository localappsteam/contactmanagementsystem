﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class types : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 0;//ADMIN ONLY
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void btnAddTypes_Click(object sender, EventArgs e)
        {
            Response.Redirect("types-add.aspx");
        }

        protected void gridTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("types-edit.aspx?ID=" + gridTypes.SelectedRow.Cells[1].Text.Trim() + "&Description=" + gridTypes.SelectedRow.Cells[2].Text.Trim());
        }
    }
}