﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using ExcelLibrary;
using System.IO;

namespace Contact.callout
{
    public partial class reports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtFromDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                txtToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        protected void lstPersonCalledOut_DataBound(object sender, EventArgs e)
        {
            lstPersonCalledOut.Items[0].Text = "(All)";
        }

        protected void lstArea_DataBound(object sender, EventArgs e)
        {
            lstArea.Items[0].Text = "(All)";
        }

        protected void lstContactPerson_DataBound(object sender, EventArgs e)
        {
            lstContactPerson.Items[0].Text = "(All)";
        }

        protected void lstAuthorisedBy_DataBound(object sender, EventArgs e)
        {
            lstAuthorisedBy.Items[0].Text = "(All)";
        }

        protected void lstNotification_DataBound(object sender, EventArgs e)
        {
            lstNotification.Items[0].Text = "(All)";
        }

        protected void rdCalloutType_DataBound(object sender, EventArgs e)
        {
            rdCalloutType.Items[0].Text = "All";

            if(!Page.IsPostBack)
            {
                rdCalloutType.SelectedIndex = 0;
            }
        }

        void generateReport(int ReportFormat)
        {
            if (txtFromDate.Text.Trim() == String.Empty || txtToDate.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Enter a date range first.");
            }
            else if(!access.isDate(txtFromDate.Text.Trim()))
            {
                ShowAlert(2, "Enter a valid 'From date'. Use date format 'dd/mm/yyyy or yyyy-mm-dd'.");
            }
            else if (!access.isDate(txtToDate.Text.Trim()))
            {
                ShowAlert(2, "Enter a valid 'To date'. Use date format 'dd/mm/yyyy or yyyy-mm-dd'.");
            }
            else
            {
      
                String sql = " and c.CreateDate between '" + access.convertToDateString(txtFromDate.Text.Trim()) + "' and DATEADD(DAY,1,'" + access.convertToDateString(txtToDate.Text.Trim()) + "')";

                if (lstPersonCalledOut.SelectedIndex > 0)
                {
                    String values = "";

                    for (int i = 1; i < lstPersonCalledOut.Items.Count; i++)
                    {
                        if (lstPersonCalledOut.Items[i].Selected)
                        {
                            values = values + "'" + lstPersonCalledOut.Items[i].Value.Trim() + "',";
                        }
                    }

                    values = values.Substring(0, values.LastIndexOf(','));
                    sql = sql + " and CAST(c.EmpTypeID as varchar)+'-'+CAST(c.EmployeeID as varchar) in (" + values + ")";
                }

                if (lstArea.SelectedIndex > 0)
                {
                    String values = "";

                    for (int i = 1; i < lstArea.Items.Count; i++)
                    {
                        if (lstArea.Items[i].Selected)
                        {
                            values = values + lstArea.Items[i].Value.Trim() + ",";
                        }
                    }

                    values = values.Substring(0, values.LastIndexOf(','));
                    sql = sql + " and c.StandbyAreaID in (" + values + ")";
                }


                if (lstContactPerson.SelectedIndex > 0)
                {
                    String values = "";

                    for (int i = 1; i < lstContactPerson.Items.Count; i++)
                    {
                        if (lstContactPerson.Items[i].Selected)
                        {
                            values = values + "'" + lstContactPerson.Items[i].Value.Trim() + "',";
                        }
                    }

                    values = values.Substring(0, values.LastIndexOf(','));
                    sql = sql + " and CAST(c.ContactEmpTypeID as varchar)+'-'+CAST(c.ContactID as varchar) in (" + values + ")";
                }

                if (lstAuthorisedBy.SelectedIndex > 0)
                {
                    String values = "";

                    for (int i = 1; i < lstAuthorisedBy.Items.Count; i++)
                    {
                        if (lstAuthorisedBy.Items[i].Selected)
                        {
                            values = values + "'" + lstAuthorisedBy.Items[i].Value.Trim() + "',";
                        }
                    }

                    values = values.Substring(0, values.LastIndexOf(','));
                    sql = sql + " and CAST(c.AuthorisedByEmpTypeID as varchar)+'-'+CAST(c.AuthorisedByID as varchar) in (" + values + ")";
                }

                if (lstNotification.SelectedIndex > 0)
                {
                    String values = "";

                    for (int i = 1; i < lstNotification.Items.Count; i++)
                    {
                        if (lstNotification.Items[i].Selected)
                        {
                            values = values + "'" + lstNotification.Items[i].Value.Trim() + "',";
                        }
                    }

                    values = values.Substring(0, values.LastIndexOf(','));
                    sql = sql + " and c.Notification in (" + values + ")";
                }

                if (rdCalloutType.SelectedIndex > 0)
                {
                    sql = sql + " and c.CalloutTypeID =" + rdCalloutType.SelectedValue;
                }

                if (rdEmployeeType.SelectedIndex > 0)
                {
                    sql = sql + " and c.EmpTypeID =" + rdEmployeeType.SelectedValue;
                }

                if (rdStatus.SelectedIndex > 0)
                {
                    sql = sql + " and (case when c.CalloutStatusID in (1,2,3) then 1 else 2 end) =" + rdStatus.SelectedValue;
                }

                Session.Remove("ReportFilter");

                if (ReportFormat == 1)//Pdf
                {
                    Session["ReportFilter"] = sql;
                    Response.Write("<script>window.open ('report-pdf.aspx?Type=" + rdReportType.SelectedValue.Trim() + "','_blank');</script>");
                }
                else //Excel
                {
                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String un = UserSecurityPrincipal.Name;
                    un = un.Substring(un.IndexOf(@"\")).Replace(@"\", "");
                    String dir = Server.MapPath("~/Files/Callout Reports/" + un);

                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }

                    DataSet ds = new DataSet();
                    String FileName = "";

                    access obj = new access();

                    if (rdReportType.SelectedValue == "1") //Full Report
                    {
                        FileName = "Callout_Report.xls";
                        ds = obj.viewCalloutsReportExcel(Convert.ToInt32(Session["MillID"]), sql, "sp_Contact_Callout_ViewCalloutReport");
                    }
                    else //Summary Report
                    {
                        FileName = "Callout_Summary_Report.xls";
                        ds = obj.viewCalloutsReportExcel(Convert.ToInt32(Session["MillID"]), sql, "sp_Contact_Callout_ViewCalloutSummaryReport");
                    }

                    DataSetHelper.CreateWorkbook(dir + "/" + FileName, ds);

                    Response.Write("<script>window.open ('../Files/Callout Reports/" + un + "/" + FileName + "','_blank');</script>");

                }
            }
        }

        protected void btnGeneratePDF_Click(object sender, EventArgs e)
        {
            generateReport(1);
        }

        protected void btnGenerateExcel_Click(object sender, EventArgs e)
        {
            generateReport(2);
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }
    }
}