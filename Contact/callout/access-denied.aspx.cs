﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class access_denied : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadError();
        }

        void loadError()
        {
            if (Session["Pagename"] != null)
            {
                lblPagename.Text = Session["Pagename"].ToString();
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }
    }
}