﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class areas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["MillID"] != null)
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;
                int RoleID = 0; //ADMIN ONLY
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(SystemUserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void gridAreas_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("areas-edit.aspx?ID=" + gridAreas.SelectedRow.Cells[1].Text.Trim()+ "&Description="+ gridAreas.SelectedRow.Cells[2].Text.Trim()
                + "&Ext= " + gridAreas.SelectedRow.Cells[3].Text.Trim());
        }

        protected void btnAddArea_Click(object sender, EventArgs e)
        {
            Response.Redirect("areas-add.aspx");
        }
    }
}