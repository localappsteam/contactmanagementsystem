﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="users-edit.aspx.cs" Inherits="Contact.callout.users_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Users / Edit</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Callout System User</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:LinkButton ID="btnAddRoles" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnAddRoles_Click"><p class="fa fa-plus icon-mr"></p>Assign new roles</asp:LinkButton>
                                    </div>
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:LinkButton ID="btnDeleteUser" CssClass="btn btn-danger btn-sm btn-block" runat="server" OnClick="btnDeleteUser_Click"><p class="fa fa-trash icon-mr"></p>Delete account</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row" id="divDelete" runat="server" visible="false">
                                    <div class="col-md-12">
                                        <h5 class="text-danger">Delete user account?</h5>
                                        <p>Are you sure you want to delete this user account?</p>
                                        <asp:LinkButton ID="btnDeleteYes" CssClass="btn btn-danger btn-sm" runat="server" OnClick="btnDeleteYes_Click"><p class="fa fa-check icon-mr"></p>Yes</asp:LinkButton>
                                        <asp:LinkButton ID="btnDeleteNo" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnDeleteNo_Click"><p class="fa fa-times icon-mr"></p>No</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <h5>Full name:
                                            <asp:Label ID="lblFullname" runat="server" Text=""></asp:Label></h5>
                                        <h5>User name:
                                            <asp:Label ID="lblUsername" runat="server" Text=""></asp:Label><asp:LinkButton ID="btnEditUsername" CssClass="btn btn-xs btn-default m-l-10" style="margin-top:-5px;" runat="server" OnClick="btnEditUsername_Click"><p class="fa fa-edit icon-mr"></p>Edit</asp:LinkButton></h5>
                                        <div id="divEditUsername" visible="false" runat="server" class="row">
                                            <div class="col-md-12" style="min-width:320px">
                                                <h5 style="float:left; width:80px">SAPPISA\</h5><asp:TextBox ID="txtEditUsername" CssClass="form-control" style="float:left; width:200px;" runat="server"></asp:TextBox>
                                                <br /><br />
                                                <asp:LinkButton ID="btnSaveUsername" CssClass="btn btn-primary btn-sm m-l-80" runat="server" OnClick="btnSaveUsername_Click"><p class="fa fa-save icon-mr"></p>Save</asp:LinkButton>
                                                <asp:LinkButton ID="btnCancelUsername" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnCancelUsername_Click"><p class="fa fa-times icon-mr"></p>Cancel</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="divAddRoles" runat="server" visible="false">
                                    <div class="col-md-6">
                                         <h5>Add Roles</h5>
                                        <asp:CheckBoxList ID="chkRoles" CssClass="checkbox" runat="server" DataSourceID="SqlDataSource1" DataTextField="Description" DataValueField="#">
                                        </asp:CheckBoxList>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_System_Roles_ViewToAssign" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="UserID" SessionField="SelectedUserID" Type="Int32" />
                                                <asp:Parameter DefaultValue="4" Name="ApplicationID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-2 btn-fix-width">
                                                <asp:LinkButton ID="btnSubmit" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnSubmit_Click"><p class="fa fa-plus icon-mr"></p>Submit</asp:LinkButton>
                                            </div>
                                            <div class="col-md-2 btn-fix-width">
                                                <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-sm btn-block" runat="server" OnClick="btnCancel_Click"><p class="fa fa-times icon-mr"></p>Cancel</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 cont-holder" style="overflow:auto">
                                        <h5 style="margin-left:5px">User Roles</h5>
                                        <asp:GridView ID="gridRoles" Width="100%" CssClass="table" Font-Size="12px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No roles assigned." AutoGenerateColumns="False" DataSourceID="SqlDataSource6" DataKeyNames="#" OnSelectedIndexChanged="gridRoles_SelectedIndexChanged">
                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                            <RowStyle Height="41px"></RowStyle>
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" ControlStyle-CssClass="btn btn-block btn-xs btn-default" SelectText="Remove" ControlStyle-Font-Size="11px" ButtonType="Button" />
                                                <asp:BoundField DataField="#" HeaderText="#" ReadOnly="True" SortExpression="#" HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="1px" InsertVisible="False"/>
                                                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_System_Users_ViewRoles" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="UserID" SessionField="SelectedUserID" Type="Int32" />
                                                <asp:Parameter DefaultValue="4" Name="ApplicationID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                        <a href="users.aspx" class="btn btn-default btn-sm btn-block">
                                            <p class="fa fa-times icon-mr"></p>
                                            Close</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
