﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class approval_request : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            doApproval();
        }

        void doApproval()
        {
            try
            {
                if(rdRole.SelectedIndex < 0)
                {
                    ShowAlert(2, "Select your role for the callout first.");
                }
                else if(txtCalloutNumber.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Enter the callout number first.");
                }
                else if(txtVerificationCode.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Enter the verification code.");
                }
                else
                {
                    Response.Redirect("approval-confirmation.aspx?ID=" + txtCalloutNumber.Text.Trim() + "&Type=" + rdRole.SelectedValue.Trim() + "&Code=" + txtVerificationCode.Text.Trim());
                }
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
        }
        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["CalloutSubmitted"] != null)
            {
                Session.Remove("CalloutSubmitted");
                Response.Redirect("callouts-active.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

       
    }
}