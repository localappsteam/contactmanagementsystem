﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Principal;


namespace Contact.callout
{
    public partial class callout : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionBuilder start = new SessionBuilder();
            AuthenticateUser();
            loadUserProfile();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int AdminRoleID = 19;
            access obj = new access();

            if (!obj.checkRoles(UserName, 20, AdminRoleID)) //Create Callouts
            {
                lkcreatecallout.Visible = false;
            }
            else
            {
                lkcreatecallout.Visible = true;
            }

            obj = new access();
            if (!obj.checkRoles(UserName, 0, AdminRoleID)) //Admin
            {
                useraccounts.Visible = false;
            }
            else
            {
                useraccounts.Visible = true;
            }
        }

        void loadUserProfile()
        {
            if (Session["MillID"] != null)
            {

                WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String UserName = UserSecurityPrincipal.Name;


                lblSelectedOffice.Text = Session["SelectedOffice"].ToString();
                lblUsername.Text = UserName;

                access obj = new access();
                DataSet ds = obj.getUserLogged(UserName);
                String employeeID = "0";

                if (ds.Tables[0].Rows.Count > 0)
                {
                    employeeID = ds.Tables[0].Rows[0]["EmployeeID"].ToString();
                }

                Session["EmployeeID"] = employeeID;
                Session["UserName"] = UserName;

                ADManager ad = new ADManager();
                UserName = UserName.Substring(UserName.LastIndexOf(@"\") + 1).Trim();
                lblUserOffice.Text = ad.getUserLocation(UserName);

            }
        }
    }
}