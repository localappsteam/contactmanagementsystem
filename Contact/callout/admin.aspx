﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="Contact.callout.admin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Amin</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Admin</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row text-center">
                            <div class="col-md-12 text-center" style="padding-right: 0px">
                                <div class="menu-circle-container">
                                    <a href="areas.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/app-map-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Areas</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container">
                                    <a href="types.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/folder-iphone-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Callout Types</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container">
                                    <a href="users.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/user-group-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">System Users</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                         </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
