﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int AdminRoleID = 19;
            access obj = new access();

            if (!obj.checkRoles(UserName, 20, AdminRoleID)) //Create Callouts
            {
                divCreateCallout.Visible = false;
            }
            else
            {
                divCreateCallout.Visible = true;
            }

            obj = new access();
            if (!obj.checkRoles(UserName, 0, AdminRoleID)) //Admin
            {
                divAdmin.Visible = false;
            }
            else
            {
                divAdmin.Visible = true;
            }
        }
    }
}