﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;

namespace Contact.callout
{
    public partial class callouts_complete : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtDateFrom.Text = DateTime.Today.AddDays(-30).ToString("yyyy-MM-dd");
                txtDateTo.Text = DateTime.Now.ToString("yyyy-MM-dd");
                AuthenticateUser();
            }
        }

        void AuthenticateUser()
        {
            if (Session["MillID"] != null)
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;
                int RoleID = 20;
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(SystemUserName, RoleID, AdminRoleID))
                {
                    gridCompletedCallouts.Columns[0].Visible = false;
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }
        protected void btnOpenCallout_Click(object sender, ImageClickEventArgs e)
        {
            String pageName = Path.GetFileName(Request.Path);
            Response.Redirect("callout-update.aspx?ID=" + ((ImageButton)sender).AlternateText.Trim() + "&Source=" + pageName.Substring(0, pageName.LastIndexOf('.')));
        }

        protected void btnPrintCallout_Click(object sender, ImageClickEventArgs e)
        {
            Response.Write("<script>window.open ('pdf.aspx?ID=" + ((ImageButton)sender).AlternateText.Trim() + "&RPT=1','_blank');</script>");
        }
    }
}