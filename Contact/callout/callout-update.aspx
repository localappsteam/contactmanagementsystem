﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="callout-update.aspx.cs" Inherits="Contact.callout.callout_update" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Callout Details</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Details</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:HyperLink ID="btnCancel" class="btn btn-default btn-sm btn-block" runat="server"><p class="fa fa-reply icon-mr"></p>Back</asp:HyperLink>
                                    </div>
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:LinkButton ID="btnSave1" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnSave1_Click"><p class="fa fa-save icon-mr"></p>Save</asp:LinkButton>
                                    </div>
                                    <div class="col-md-4">
                                        <asp:HyperLink ID="btnResendApproval1" CssClass="btn btn-primary btn-sm" runat="server"><p class="fa fa-envelope-o icon-mr"></p>Resend approval request</asp:HyperLink>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7 p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5>Contact Information</h5>
                                            </div>
                                        </div>
                                        <div id="divParentCallout" runat="server" class="row field-margin">
                                            <div class="col-sm-12">
                                                <h5 class="text-danger">Please note that some of the information cannot be edit because this is a Helper callout.<br />
                                                    Click on the parent callout number below to make those changes on the original callout.</h5>
                                            </div>
                                            <div class="col-md-5 field-name field-margin">
                                                <p><b>Parent callout number:</b></p>
                                            </div>
                                            <div class="col-md-7 field-margin p-t-10">
                                                <p>
                                                    <asp:HyperLink ID="lblParentCalloutNumber" Font-Bold="true" runat="server">0</asp:HyperLink></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Callout number:</p>
                                            </div>
                                            <div class="col-md-7 field-margin p-t-10">
                                                <p>
                                                    <asp:Label ID="lblCalloutNumber" runat="server" Text=""></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Standby duty:</p>
                                            </div>
                                            <div class="col-md-7 field-margin">
                                                <asp:DropDownList ID="drdStandbyDuty" Enabled="false" CssClass="form-control input-disabled" runat="server" DataSourceID="SqlDataSource1" DataTextField="Description" DataValueField="StandbyDutyID">
                                                </asp:DropDownList>
                                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_Duties_Select" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Person called out:</p>
                                            </div>
                                            <div class="col-md-7 field-margin">
                                                <asp:DropDownList ID="drdPersonToCall" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                                </asp:DropDownList>
                                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_PersonCalledOut" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Area:</p>
                                            </div>
                                            <div class="col-md-7 field-margin">
                                                <asp:DropDownList ID="drdArea" CssClass="form-control" runat="server" DataSourceID="SqlDataSource3" DataTextField="Description" DataValueField="#">
                                                </asp:DropDownList>
                                                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_SelectArea" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Contact:</p>
                                            </div>
                                            <div class="col-md-7 field-margin">
                                                <asp:DropDownList ID="drdContact" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Authorised by:</p>
                                            </div>
                                            <div class="col-md-7 field-margin">
                                                <asp:DropDownList ID="drdAuthorised" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Notification number:</p>
                                            </div>
                                            <div class="col-md-7 field-margin">
                                                <asp:TextBox ID="txtNotificationNo" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Callout type:</p>
                                            </div>
                                            <div class="col-md-7 field-margin">
                                                <asp:DropDownList ID="drdCalloutType" CssClass="form-control" runat="server" DataSourceID="SqlDataSource4" DataTextField="Description" DataValueField="#">
                                                </asp:DropDownList>
                                                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_SelectCalloutType" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            </div>
                                        </div>
                                        <div id="divIncidentTime" runat="server" class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Time of incident:</p>
                                            </div>
                                            <div class="col-md-7 field-margin">
                                                <div class="row field-margin">
                                                    <div class="col-md-6 field-margin">
                                                        <asp:TextBox ID="txtDate" CssClass="form-control" TextMode="Date" runat="server"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-6 field-margin">
                                                        <asp:TextBox ID="txtTime" CssClass="form-control" TextMode="Time" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Status:</p>
                                            </div>
                                            <div class="col-md-7 field-margin p-t-10">
                                                <p><asp:Label ID="lblStatus" runat="server" Text=""></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5>Reason for callout</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtReasonForCallout" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5" id="divHelpers" runat="server">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5>Helpers</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 btn-fix-width pull-right">
                                                <asp:LinkButton ID="btnAddHelpers" CssClass="btn btn-sm btn-block btn-primary" runat="server" OnClick="btnAddHelpers_Click"><p class="fa fa-user-plus icon-mr"></p>Add helpers</asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 cont-holder" style="overflow:auto">
                                                <asp:HiddenField ID="hiddenCalloutID" runat="server" />
                                                <asp:GridView ID="gridHelpers" Width="100%" CssClass="table" Font-Size="12px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource5">
                                                    <AlternatingRowStyle BackColor="#f5f5f5" />
                                                    
                                                    <RowStyle Height="41px"></RowStyle>
                                                    <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                                    <Columns>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btnUpdateCallout" runat="server" OnClick="btnUpdateCallout_Click" AlternateText='<%#Eval("Callout Number") %>' ToolTip="Contact the person or update callout time details." CausesValidation="False" CommandName="Select" ImageUrl="~/images/small/Phone-icon.png" Text="Select" />
                                                            </ItemTemplate>
                                                            <ControlStyle Width="18px" />
                                                            <ItemStyle BackColor="White" BorderColor="White" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btnOpenCallout" runat="server" OnClick="btnOpenCallout_Click" AlternateText='<%#Eval("Callout Number") %>' ToolTip="View or edit callout details." CausesValidation="False" CommandName="Select" ImageUrl="~/images/small/Folder-Open-icon.png" Text="Select" />
                                                            </ItemTemplate>
                                                            <ControlStyle Width="18px" />
                                                            <ItemStyle BackColor="White" BorderColor="White" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btnPrintCallout" runat="server" OnClick="btnPrintCallout_Click" AlternateText='<%#Eval("Callout Number") %>' ToolTip="Print callout report." CausesValidation="False" CommandName="Select" ImageUrl="~/images/small/Apps-printer-icon.png" Text="Select" />
                                                            </ItemTemplate>
                                                            <ControlStyle Width="18px" />
                                                            <ItemStyle BackColor="White" BorderColor="White" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btnCancelCallout" runat="server" OnClick="btnCancelCallout_Click" AlternateText='<%#Eval("Callout Number") %>' ToolTip="Cancel this callout." CausesValidation="False" CommandName="Select" ImageUrl="~/images/small/user-trash-full-icon.png" Text="Select" />
                                                            </ItemTemplate>
                                                            <ControlStyle Width="18px" />
                                                            <ItemStyle BackColor="White" BorderColor="White" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Callout Number" HeaderText="Callout Number" ReadOnly="True" SortExpression="Callout Number" />
                                                        <asp:BoundField DataField="Person Called Out" HeaderText="Person Called Out" SortExpression="Person Called Out" ReadOnly="True" />
                                                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" />
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_ViewCalloutHelpers" SelectCommandType="StoredProcedure">
                                                    <SelectParameters>
                                                        <asp:SessionParameter Name="MillID" SessionField="MillID" DefaultValue="" Type="Int32" />
                                                        <asp:ControlParameter ControlID="lblCalloutNumber" Name="ParentCalloutID" PropertyName="Text" Type="Decimal" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7 p-r-50">
                                        <div class="row">
                                            <div class="col-md-12 bg-primary">
                                                <h5>Time Information</h5>
                                            </div>
                                        </div>
                                         <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Time captured:</p>
                                            </div>
                                            <div class="col-md-7 field-margin p-t-10">
                                                <p>
                                                    <asp:Label ID="lblCaptured" runat="server" Text=""></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Called:</p>
                                            </div>
                                            <div class="col-md-7 field-margin p-t-10">
                                                <p>
                                                    <asp:Label ID="lblCalled" runat="server" Text=""></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Arrived:</p>
                                            </div>
                                            <div class="col-md-7 field-margin p-t-10">
                                                <p>
                                                    <asp:Label ID="lblArrived" runat="server" Text=""></asp:Label></p>
                                            </div>
                                        </div>
                                        <div class="row field-margin">
                                            <div class="col-md-5 field-name field-margin">
                                                <p>Completed:</p>
                                            </div>
                                            <div class="col-md-7 field-margin p-t-10">
                                                <p>
                                                    <asp:Label ID="lblCompleted" runat="server" Text=""></asp:Label></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:HyperLink ID="btnCancel2" class="btn btn-default btn-sm btn-block" runat="server"><p class="fa fa-reply icon-mr"></p>Back</asp:HyperLink>
                                    </div>
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:LinkButton ID="btnSave2" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnSave1_Click"><p class="fa fa-save icon-mr"></p>Save</asp:LinkButton>
                                    </div>
                                    <div class="col-md-4">
                                        <asp:HyperLink ID="btnResendApproval2" CssClass="btn btn-primary btn-sm" runat="server"><p class="fa fa-envelope-o icon-mr"></p>Resend approval request</asp:HyperLink>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PromptBox" visible="false" class="MessageBox" runat="server">
        <div id="Prompt" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="PromptHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanPrompt" runat="server"></span>
                            <asp:Label ID="lblPrompTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblPromptMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                         <asp:TextBox ID="txtDeleteReason" Placeholder="Reason..." CssClass="form-control" TextMode="MultiLine" Text="" runat="server"></asp:TextBox>
                        <small id="DeleteRequired" runat="server" visible="false" class="text-danger">This field is required!</small>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnSubmitDelete" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnSubmitDelete_Click"><p class="fa fa-check icon-mr"></p>Sumbit</asp:LinkButton>
                        <asp:LinkButton ID="btnClosePromptBox" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnClosePromptBox_Click"><p class="fa fa-times icon-mr"></p>Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
