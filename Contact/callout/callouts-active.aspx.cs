﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Drawing;
using System.IO;

namespace Contact.callout
{
    public partial class callouts_active : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if(!Page.IsPostBack)
            {
                txtDateFrom.Text = DateTime.Today.AddDays(-30).ToString("yyyy-MM-dd");
                txtDateTo.Text = DateTime.Now.ToString("yyyy-MM-dd");
                AuthenticateUser();
            }

        }

        void AuthenticateUser()
        {
            if (Session["MillID"] != null)
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;
                int RoleID = 20;
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(SystemUserName, RoleID, AdminRoleID))
                {
                    gridActiveCallouts.Columns[0].Visible = false;
                    gridActiveCallouts.Columns[1].Visible = false;
                    gridActiveCallouts.Columns[3].Visible = false;
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void btnUpdateCallout_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("callout-contact.aspx?ID=" + ((ImageButton)sender).AlternateText.Trim());
        }

        protected void btnOpenCallout_Click(object sender, ImageClickEventArgs e)
        {
            String pageName = Path.GetFileName(Request.Path);
            Response.Redirect("callout-update.aspx?ID=" + ((ImageButton)sender).AlternateText.Trim() + "&Source=" + pageName.Substring(0, pageName.LastIndexOf('.')));
        }

        protected void btnPrintCallout_Click(object sender, ImageClickEventArgs e)
        {
            Response.Write("<script>window.open ('pdf.aspx?ID=" + ((ImageButton)sender).AlternateText.Trim() + "&RPT=1','_blank');</script>");
        }

        protected void btnCancelCallout_Click(object sender, ImageClickEventArgs e)
        {
            hiddenCalloutID.Value = ((ImageButton)sender).AlternateText.Trim();
            ShowDeletePrompt();
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }

        void ShowDeletePrompt()
        {
            txtDeleteReason.Text = "";
            lblPrompTitle.Text = "Cancel Callout";
            lblPrompTitle.Attributes.Add("class", "text-danger");
            spanPrompt.Attributes.Add("class", "fa fa-trash text-danger");
            lblPromptMessage.Text = "Enter a reason for cancelling this callout.";
            PromptBox.Visible = true;
        }

        void HideDeletePrompt()
        {
            lblPrompTitle.Text = "";
            lblPromptMessage.Text = "";
            txtDeleteReason.Text = "";
            PromptBox.Visible = false;
            gridActiveCallouts.SelectedIndex = -1;
        }

        protected void btnSubmitDelete_Click(object sender, EventArgs e)
        {
            deleteCallout();
        }

        void deleteCallout()
        {
            if (txtDeleteReason.Text.Trim() == String.Empty)
            {
                DeleteRequired.Visible = true;
                txtDeleteReason.Focus();
            }
            else
            {
                try
                {
                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    int CalloutID = Convert.ToInt32(hiddenCalloutID.Value);
                    String Reason = txtDeleteReason.Text.Trim();

                    access obj = new access();
                    obj.cancelCallout(SystemUserName, CalloutID, Reason);

                    gridActiveCallouts.DataBind();
                    ShowAlert(1, "Callout cancellation submitted.");
                }
                catch (Exception ex)
                {
                    ShowAlert(2, ex.Message);
                }

                HideDeletePrompt();
                gridActiveCallouts.SelectedIndex = -1;
            }
        }

        protected void btnClosePromptBox_Click(object sender, EventArgs e)
        {
            HideDeletePrompt();
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        
    }
}