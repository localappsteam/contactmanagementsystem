﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class types_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
            loadArea();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 0;//ADMIN ONLY
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        void loadArea()
        {
            if (Request.QueryString["ID"] != null && Request.QueryString["Description"] != null)
            {
                if (!Page.IsPostBack)
                {
                    txtDescription.Text = Request.QueryString["Description"].ToString().Trim();
                }

            }
            else
            {
                Response.Redirect("types.aspx");
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            divDelete.Visible = true;
        }

        protected void btnDeleteYes_Click(object sender, EventArgs e)
        {
            deleteCalloutType();
        }

        protected void btnDeleteNo_Click(object sender, EventArgs e)
        {
            divDelete.Visible = false;
        }
        void deleteCalloutType()
        {
            try
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;

                access obj = new access();
                obj.deleteCalloutType(SystemUserName, Convert.ToInt32(Request.QueryString["ID"].ToString()));

                Session["TypeUpdated"] = true;
                ShowAlert(1, "Callout type deleted successfully.");
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            updateArea();
        }

        void updateArea()
        {
            try
            {
                if (txtDescription.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "A callout type descritpion is required.");
                }
                else
                {

                    int CalloutTypeID = Convert.ToInt32(Request.QueryString["ID"].ToString());
                    String Description = txtDescription.Text.Trim();

                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    access obj = new access();
                    obj.updateCalloutType(SystemUserName, CalloutTypeID, Description);

                    Session["TypeUpdated"] = true;

                    ShowAlert(1, "Callout type updated successfully.");
                }

            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }

        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["TypeUpdated"] != null)
            {
                Session.Remove("TypeUpdated");
                Response.Redirect("types.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }
}