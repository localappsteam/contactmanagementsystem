﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="types.aspx.cs" Inherits="Contact.callout.types" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Types</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Types</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:LinkButton ID="btnAddType" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnAddTypes_Click"><p class="fa fa-plus icon-mr"></p>Add new callout type</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <asp:TextBox ID="txtSearch" CssClass="form-control form-control-sm" Width="75%" Style="float: left" Placeholder="Search..." runat="server"></asp:TextBox>
                                        <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 cont-holder" style="overflow:auto">
                                        <asp:GridView ID="gridTypes" Width="100%" CssClass="table" Font-Size="12px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AutoGenerateColumns="False" DataSourceID="SqlDataSource1" DataKeyNames="#" OnSelectedIndexChanged="gridTypes_SelectedIndexChanged">
                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                            
                                            <RowStyle Height="41px"></RowStyle>
                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" ControlStyle-CssClass="btn btn-block btn-xs btn-default" SelectText="Edit" ControlStyle-Font-Size="11px" ButtonType="Button" />
                                                <asp:BoundField DataField="#" HeaderText="#" ReadOnly="True" SortExpression="#" HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="1px" InsertVisible="False"/>
                                                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_Types_View" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="txtSearch" DefaultValue=" " Name="SearchValue" PropertyName="Text" Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
