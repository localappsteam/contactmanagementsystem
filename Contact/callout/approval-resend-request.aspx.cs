﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class approval_resend_request : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadCalloutInfo();
        }

        void loadCalloutInfo()
        {
            
            if(!Page.IsPostBack)
            {
                if (Request.QueryString["ID"] != null && Session["MillID"] != null)
                {
                    
                    String CalloutNumber = Request.QueryString["ID"];
                    String Reason = Request.QueryString["Reason"];

                    lblCalloutID.Text = CalloutNumber;
                    lblReason.Text = Reason;

                    btnBack.NavigateUrl = "~/callout/callout-update.aspx?ID=" + CalloutNumber + "&Source=callouts-complete";

                }
                else
                {
                    Response.Redirect("callouts-completed.aspx");
                }
            }
           
        }
        protected void btnSendApproval_Click(object sender, EventArgs e)
        {
            resendRequest();
        }
        void resendRequest()
        {
            if (Request.QueryString["ID"] != null && Session["MillID"] != null)
            {
                try
                {
                    if(rdRole.SelectedIndex<0)
                    {
                        ShowAlert(2, "Select a person to send the request to.");
                    }
                    else
                    {
                        CalloutNotifications cn = new CalloutNotifications();

                        int CalloutID = Convert.ToInt32(Request.QueryString["ID"]);

                        String response = cn.sendSignOfRequest(CalloutID, Server.MapPath("~/callout/approval-request.html"), Convert.ToInt32(rdRole.SelectedValue));

                        Session["RequestSent"] = true;

                        if(response ==String.Empty)
                        {
                            ShowAlert(1, "Approval request sent successfully.");
                        }
                        else
                        {
                            ShowAlert(2, response);
                        }
                        
                    }
                   
                }
                catch (Exception ex)
                {

                    ShowAlert(2, ex.Message);
                }
                
            }
            else
            {
                Response.Redirect("callouts-complete.aspx");
            }
        }
        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["RequestSent"] != null)
            {
                Session.Remove("RequestSent");
                Response.Redirect("callout-update.aspx?ID=" + Request.QueryString["ID"] + "&Source=callouts-complete");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        
    }
}