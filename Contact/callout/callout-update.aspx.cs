﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;

namespace Contact.callout
{
    public partial class callout_update : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
            loadCalloutDetails();
        }

        void AuthenticateUser()
        {
            if (Session["MillID"] != null)
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;
                int RoleID = 20;
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(SystemUserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }


        protected void btnUpdateCallout_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("callout-contact.aspx?ID=" + ((ImageButton)sender).AlternateText.Trim());
        }

        protected void btnOpenCallout_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("callout-update.aspx?ID=" + ((ImageButton)sender).AlternateText.Trim());
        }

        protected void btnPrintCallout_Click(object sender, ImageClickEventArgs e)
        {
            Response.Write("<script>window.open ('pdf.aspx?ID=" + ((ImageButton)sender).AlternateText.Trim() + "&RPT=1','_blank');</script>");
        }

        protected void btnCancelCallout_Click(object sender, ImageClickEventArgs e)
        {
            hiddenCalloutID.Value = ((ImageButton)sender).AlternateText.Trim();
            ShowDeletePrompt();
        }

        void ShowDeletePrompt()
        {
            txtDeleteReason.Text = "";
            lblPrompTitle.Text = "Cancel Callout";
            lblPrompTitle.Attributes.Add("class", "text-danger");
            spanPrompt.Attributes.Add("class", "fa fa-trash text-danger");
            lblPromptMessage.Text = "Enter a reason for cancelling this callout.";
            PromptBox.Visible = true;
        }

        void HideDeletePrompt()
        {
            lblPrompTitle.Text = "";
            lblPromptMessage.Text = "";
            txtDeleteReason.Text = "";
            PromptBox.Visible = false;
            gridHelpers.SelectedIndex = -1;
        }

        protected void btnSubmitDelete_Click(object sender, EventArgs e)
        {
            deleteCallout();
        }

        void deleteCallout()
        {
            if (txtDeleteReason.Text.Trim() == String.Empty)
            {
                DeleteRequired.Visible = true;
                txtDeleteReason.Focus();
            }
            else
            {
                try
                {
                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    int CalloutID = Convert.ToInt32(hiddenCalloutID.Value);
                    String Reason = txtDeleteReason.Text.Trim();

                    access obj = new access();
                    obj.cancelCallout(SystemUserName, CalloutID, Reason);

                    gridHelpers.DataBind();
                    ShowAlert(1, "Callout cancellation submitted.");
                }
                catch (Exception ex)
                {
                    ShowAlert(2, ex.Message);
                }

                HideDeletePrompt();
                gridHelpers.SelectedIndex = -1;
            }
        }

        void loadCalloutDetails()
        {
            if (!Page.IsPostBack)
            {
                divParentCallout.Visible = false;
                divHelpers.Visible = false;

                drdStandbyDuty.Enabled = false;
                drdStandbyDuty.CssClass = "form-control input-disabled";

                drdPersonToCall.Enabled = false;
                drdPersonToCall.CssClass = "form-control input-disabled";

                drdArea.Enabled = false;
                drdArea.CssClass = "form-control input-disabled";

                drdContact.Enabled = false;
                drdContact.CssClass = "form-control input-disabled";

                drdAuthorised.Enabled = false;
                drdAuthorised.CssClass = "form-control input-disabled";

                txtNotificationNo.Enabled = false;
                txtNotificationNo.CssClass = "form-control input-disabled";

                drdCalloutType.Enabled = false;
                drdCalloutType.CssClass = "form-control input-disabled";

                txtDate.Enabled = false;
                txtDate.CssClass = "form-control input-disabled";

                txtTime.Enabled = false;
                txtTime.CssClass = "form-control input-disabled";

                txtReasonForCallout.Enabled = false;
                txtReasonForCallout.CssClass = "form-control input-disabled";

                btnSave1.Visible = true;
                btnSave2.Visible = true;
                btnAddHelpers.Visible = true;

                if (Request.QueryString["ID"] != null && Session["MillID"] != null && Request.QueryString["Source"] != null)
                {
                    int CalloutID = Convert.ToInt32(Request.QueryString["ID"]);
                    String SourcePage = Request.QueryString["Source"].Trim();

                    btnCancel.NavigateUrl = SourcePage + ".aspx";
                    btnCancel2.NavigateUrl = SourcePage + ".aspx";

                    CultureInfo culture = new CultureInfo("en-US");

                    access obj = new access();

                    DataSet ds = obj.viewCalloutDetails(Convert.ToInt32(Session["MillID"]), CalloutID);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblParentCalloutNumber.Text = ds.Tables[0].Rows[0]["ParentCalloutID"].ToString();
                        lblCalloutNumber.Text = ds.Tables[0].Rows[0]["CalloutID"].ToString();
                        drdStandbyDuty.SelectedValue = ds.Tables[0].Rows[0]["StandbyDutyID"].ToString();
                        drdPersonToCall.SelectedValue = ds.Tables[0].Rows[0]["PersonCalledOutID"].ToString();
                        drdArea.SelectedValue = ds.Tables[0].Rows[0]["StandbyAreaID"].ToString();
                        drdContact.SelectedValue = ds.Tables[0].Rows[0]["ContactPersonID"].ToString();
                        drdAuthorised.SelectedValue = ds.Tables[0].Rows[0]["AuthorizingPersonID"].ToString();
                        txtNotificationNo.Text = ds.Tables[0].Rows[0]["Notification"].ToString();
                        drdCalloutType.SelectedValue = ds.Tables[0].Rows[0]["CalloutTypeID"].ToString();
                        txtDate.Text = ds.Tables[0].Rows[0]["IncidentTime"].ToString().Substring(0, 10);
                        txtTime.Text = ds.Tables[0].Rows[0]["IncidentTime"].ToString().Substring(11, 5);
                        lblStatus.Text = ds.Tables[0].Rows[0]["CalloutProgress"].ToString();
                        txtReasonForCallout.Text = ds.Tables[0].Rows[0]["Reason"].ToString();
                        lblCaptured.Text = ds.Tables[0].Rows[0]["CreateDate"].ToString();
                        lblCalled.Text = ds.Tables[0].Rows[0]["TimeArtisanCalled"].ToString();
                        lblArrived.Text = ds.Tables[0].Rows[0]["DateTimeIn"].ToString();
                        lblCompleted.Text = ds.Tables[0].Rows[0]["DateTimeOut"].ToString();

                        if (ds.Tables[0].Rows[0]["CalloutProgress"].ToString().Trim().ToUpper() == "PENDING SIGN-OFF APPROVAL")
                        {
                            btnResendApproval1.Visible = true;
                            btnResendApproval2.Visible = true;

                            String Url = "~/callout/approval-resend-request.aspx?ID="
                                + ds.Tables[0].Rows[0]["CalloutID"].ToString()
                                + "&Reason=" + ds.Tables[0].Rows[0]["Reason"].ToString();

                            btnResendApproval1.NavigateUrl = Url;
                            btnResendApproval2.NavigateUrl = Url;
                        }
                        else
                        {
                            btnResendApproval1.Visible = false;
                            btnResendApproval2.Visible = false;
                        }

                        String status = ds.Tables[0].Rows[0]["Status"].ToString().ToUpper();

                        if (status == "OPEN" && lblParentCalloutNumber.Text.Trim() == String.Empty)
                        {
                            divHelpers.Visible = true;

                            drdPersonToCall.Enabled = true;
                            drdPersonToCall.CssClass = "form-control";

                            drdArea.Enabled = true;
                            drdArea.CssClass = "form-control";

                            drdContact.Enabled = true;
                            drdContact.CssClass = "form-control";

                            drdAuthorised.Enabled = true;
                            drdAuthorised.CssClass = "form-control";

                            txtNotificationNo.Enabled = true;
                            txtNotificationNo.CssClass = "form-control";

                            drdCalloutType.Enabled = true;
                            drdCalloutType.CssClass = "form-control";

                            txtDate.Enabled = true;
                            txtDate.CssClass = "form-control";

                            txtTime.Enabled = true;
                            txtTime.CssClass = "form-control";

                            txtReasonForCallout.Enabled = true;
                            txtReasonForCallout.CssClass = "form-control";
                        }
                        else if (status == "OPEN" && lblParentCalloutNumber.Text.Trim() != String.Empty)
                        {
                            divHelpers.Visible = false;

                            divParentCallout.Visible = true;
                            lblParentCalloutNumber.NavigateUrl = "callout-update.aspx?ID=" + lblParentCalloutNumber.Text + "&Source=callouts-active";

                            drdPersonToCall.Enabled = true;
                            drdPersonToCall.CssClass = "form-control";
                        }
                        else
                        {
                            btnSave1.Visible = false;
                            btnSave2.Visible = false;
                            btnAddHelpers.Visible = false;

                            for (int i = 0; i < gridHelpers.Rows.Count; i++)
                            {
                                ((ImageButton)gridHelpers.Rows[i].FindControl("btnUpdateCallout")).Visible = false;
                                ((ImageButton)gridHelpers.Rows[i].FindControl("btnCancelCallout")).Visible = false;
                            }

                            if (lblParentCalloutNumber.Text.Trim() == String.Empty)
                            {
                                divParentCallout.Visible = false;
                                divHelpers.Visible = true;
                            }
                            else
                            {
                                divParentCallout.Visible = true;
                                lblParentCalloutNumber.NavigateUrl = "callout-update.aspx?ID=" + lblParentCalloutNumber.Text + "&Source=callouts-complete";
                                divHelpers.Visible = false;
                            }

                            if (status == "PENDING")
                            {
                                drdAuthorised.Enabled = true;
                                drdAuthorised.CssClass = "form-control";
                                btnSave1.Visible = true;
                                btnSave2.Visible = true;
                            }
                        }
                    }

                }
            }
        }

        protected void btnAddHelpers_Click(object sender, EventArgs e)
        {
            Response.Redirect("callout-update-helpers.aspx?ID=" + Request.QueryString["ID"] + "&Source=" + Request.QueryString["Source"]);
        }
        protected void btnSave1_Click(object sender, EventArgs e)
        {
            updateCallout();
        }

        void updateCallout()
        {
            try
            {
                string[] DateTimeformats = { "dd/MM/yyyy HH:mm", "yyyy-MM-dd HH:mm" };

                if(drdPersonToCall.SelectedIndex < 1)
                {
                    ShowAlert(2, "A person called out is required.");
                }
                else if (drdArea.SelectedIndex < 1)
                {
                    ShowAlert(2, "An area is required.");
                }
                else if (drdContact.SelectedIndex < 1)
                {
                    ShowAlert(2, "A contact person is required.");
                }
                else if (drdAuthorised.SelectedIndex < 1)
                {
                    ShowAlert(2, "An authorizing person is required.");
                }
                else if (drdPersonToCall.SelectedIndex < 1)
                {
                    ShowAlert(2, "A person called out is required.");
                }
                else if (drdCalloutType.SelectedIndex < 1)
                {
                    ShowAlert(2, "Callout type is required.");
                }
                else if (txtDate.Text.Trim() == String.Empty || txtTime.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Date and time of incident is required.");
                }
                else if (!access.isDateTime(txtDate.Text.Trim() + " " + txtTime.Text.Trim()))
                {
                    ShowAlert(2, "Enter a valid Incident date and time. Use date format 'dd/mm/yyyy' or 'yyyy-mm-dd', and time format 'hh:mm'");
                }
                else if (txtReasonForCallout.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "A reason for the callout is required.");
                }
                else
                {
                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    // DateTime IncidentTime = DateTime.ParseExact(txtDate.Text.Trim() + " " + txtTime.Text.Trim(), DateTimeformats, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None);
                    String IncidentTime = access.convertToDateTimeString(txtDate.Text.Trim() + " " + txtTime.Text.Trim());

                    access obj = new access();
                    obj.updateCallout(SystemUserName, Convert.ToInt32(lblCalloutNumber.Text.Trim()), drdPersonToCall.SelectedValue.Trim()
                        , Convert.ToInt32(drdArea.SelectedValue.Trim()), drdContact.SelectedValue.Trim(), drdAuthorised.SelectedValue.Trim(), txtNotificationNo.Text.Trim(), Convert.ToInt32(drdCalloutType.SelectedValue.Trim())
                        , IncidentTime, txtReasonForCallout.Text.Trim());

                    Session["CalloutUpdated"] = true;
                    ShowAlert(1, "Changes saved successfully.");
                }

            }
            catch (Exception ex)
            {
                ShowAlert(2, ex.Message);
            }
        }
        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["CalloutUpdated"] != null)
            {
                Session.Remove("CalloutUpdated");

                String SourcePage = Request.QueryString["Source"].Trim();
                Response.Redirect(SourcePage + ".aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        protected void btnClosePromptBox_Click(object sender, EventArgs e)
        {
            HideDeletePrompt();
        }

        
    }
}