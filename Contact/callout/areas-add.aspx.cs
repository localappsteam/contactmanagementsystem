﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.callout
{
    public partial class areas_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["MillID"] != null)
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;
                int RoleID = 0; //ADMIN ONLY
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(SystemUserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            addArea();
        }

        void addArea()
        {
            if (txtArea.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Area name is required!");
            }
            else
            {
                try
                {
                    if (Session["MillID"] != null)
                    {
                        int MillID = Convert.ToInt32(Session["MillID"].ToString());
                        String Description = txtArea.Text.Trim();
                        String Ext = txtExt.Text.Trim();

                        System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String SystemUserName = UserSecurityPrincipal.Name;

                        access obj = new access();
                        obj.addArea(SystemUserName, MillID, Description, Ext);

                        Session["AreaAdded"] = true;

                        ShowAlert(1, "Area added successfully.");
                    }
                    else
                    {
                        Response.Redirect("areas.aspx");
                    }

                }
                catch (Exception ex)
                {

                    ShowAlert(2, ex.Message);
                }
            }
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if(Session["AreaAdded"] != null)
            {
                Session.Remove("AreaAdded");
                Response.Redirect("areas.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

    }
}