﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace Contact.callout
{
    public partial class callout_contact : System.Web.UI.Page
    {

        private int CalloutID;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
            loadCalloutInfo();
        }

        void AuthenticateUser()
        {
            if (Session["MillID"] != null)
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;
                int RoleID = 20;
                int AdminRoleID = 19;

                access obj = new access();
                if (!obj.checkRoles(SystemUserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        void loadCalloutInfo()
        {
           
            if (Request.QueryString["ID"] != null)
            {
                CalloutID = Convert.ToInt32(Request.QueryString["ID"]);
                CultureInfo culture = new CultureInfo("en-US");

                access obj = new access();
                DataSet ds;

                ds = obj.getActiveCalloutsOnSite(CalloutID);
                HyperLink[] link = new HyperLink[ds.Tables[0].Rows.Count];
                String CalloutNumber;
                divActiveCallouts.Visible = false;

                for (int i=0; i<ds.Tables[0].Rows.Count; i++)
                {
                    CalloutNumber = ds.Tables[0].Rows[i]["CalloutID"].ToString();

                    link[i] = new HyperLink();
                    link[i].Text = CalloutNumber;
                    link[i].NavigateUrl = "~/callout/callout-contact.aspx/?ID=" + CalloutNumber;
                    link[i].Attributes.Add("style", "margin-right:10px; font-size:16px; font-weight:bold");
                    divActiveCallouts.Controls.Add(link[i]);
                    divActiveCallouts.Visible = true;
                }

                

                obj = new access();
                ds = obj.getCalloutProcessInfo(CalloutID);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    if(ds.Tables[0].Rows[0]["DateTimeOut"].ToString().Trim() == String.Empty)
                    {
                        btnBack.NavigateUrl = "~/callout/callouts-active.aspx";
                    }
                    else
                    {
                        btnBack.NavigateUrl = "~/callout/callouts-complete.aspx";
                    }

                    //SET INPUT TABS ********************************************
                    txtDateContacted.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    txtTimeContacted.Text = DateTime.Now.ToString("HH:mm");
                    lblTimeArrived.Text = DateTime.Now.ToString("HH:mm");
                    txtDateCompleted.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    txtTimeCompleted.Text = DateTime.Now.ToString("HH:mm");

                    tab1.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block-disabled";
                    tab2.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block-disabled";
                    tab3.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block-disabled";

                    Literal lt1 = new Literal();
                    lt1.Text = "<h4>1. Contact Person <span class='fa fa-times pull-right'></span></h4>";

                    Literal lt2 = new Literal();
                    lt2.Text = "<h4>2. Welcome Note <span class='fa fa-times pull-right'></span></h4>";

                    Literal lt3 = new Literal();
                    lt3.Text = "<h4>3. Complete Callout <span class='fa fa-times pull-right'></span></h4>";

                    divInput1.Visible = false;
                    divInput2.Visible = false;
                    divInput3.Visible = false;

                    tab1.Controls.Clear();
                    tab2.Controls.Clear();
                    tab3.Controls.Clear();

                    if (ds.Tables[0].Rows[0]["TimeArtisanCalled"].ToString().Trim() == String.Empty)
                    {
                        tab1.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block-active";
                        lt1.Text = "<h4>1. Contact Person <span class='fa fa-arrow-right pull-right'></span></h4>";
                        divInput1.Visible = true;
                    }
                    else if (ds.Tables[0].Rows[0]["DateTimeIn"].ToString().Trim() == String.Empty)
                    {
                        tab1.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block";
                        tab2.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block-active";
                        lt1.Text = "<h4>1. Contact Person <span class='fa fa-check pull-right'></span></h4>";
                        lt2.Text = "<h4>2. Welcome Note <span class='fa fa-arrow-right pull-right'></span></h4>";
                        divInput2.Visible = true;
                    }
                    else if(ds.Tables[0].Rows[0]["DateTimeOut"].ToString().Trim() == String.Empty)
                    {
                        tab1.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block";
                        tab2.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block";
                        tab3.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block-active";
                        lt1.Text = "<h4>1. Contact Person <span class='fa fa-check pull-right'></span></h4>";
                        lt2.Text = "<h4>2. Welcome Note <span class='fa fa-check pull-right'></span></h4>";
                        lt3.Text = "<h4>3. Complete Callout <span class='fa fa-arrow-right pull-right'></span></h4>";
                        divInput3.Visible = true;
                    }
                    else
                    {
                        tab1.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block";
                        tab2.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block";
                        tab3.CssClass = "col-sm-12 p-t-10 p-b-10 navigation-btn-block";

                        lt1.Text = "<h4>1. Contact Person <span class='fa fa-check pull-right'></span></h4>";
                        lt2.Text = "<h4>2. Welcome Note <span class='fa fa-check pull-right'></span></h4>";
                        lt3.Text = "<h4>3. Complete Callout <span class='fa fa-check pull-right'></span></h4>";

                        txtDateCompleted.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["DateTimeOut"].ToString().Trim(), culture).ToString("yyyy-MM-dd");
                        txtTimeCompleted.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["DateTimeOut"].ToString().Trim(), culture).ToString("HH:mm");

                        btnCloseCallout.Enabled = false;
                        btnCloseCallout.CssClass = "btn btn-sm btn-default";
                        divInput3.Visible = true;
                    }
                    tab1.Controls.Add(lt1);
                    tab2.Controls.Add(lt2);
                    tab3.Controls.Add(lt3);

                    //SET CALOUT INFORMAION************************************************************
                    lblCalloutNumber.Text = ds.Tables[0].Rows[0]["CalloutID"].ToString().Trim();
                    lblStandbyDuty.Text = ds.Tables[0].Rows[0]["StandbyDuty"].ToString().Trim();
                    lblReason.Text = ds.Tables[0].Rows[0]["Reason"].ToString().Trim();
                    lblArea.Text = ds.Tables[0].Rows[0]["Area"].ToString().Trim();
                    lblStatus.Text = ds.Tables[0].Rows[0]["Status"].ToString().Trim();
                    lblNotification.Text = ds.Tables[0].Rows[0]["Notification"].ToString().Trim();

                    lblPersonName.Text = ds.Tables[0].Rows[0]["PersonCalledOutName"].ToString().Trim();
                    lblPersonTel.Text = ds.Tables[0].Rows[0]["PersonCalledOutTel"].ToString().Trim();
                    lblPersonExt.Text = ds.Tables[0].Rows[0]["PersonCalledOutExt"].ToString().Trim();
                    lblPersonCell.Text = ds.Tables[0].Rows[0]["PersonCalledOutCell"].ToString().Trim();
                    lblPersonAddress.Text = ds.Tables[0].Rows[0]["PersonCalledOutAddress"].ToString().Trim();

                    lblDateTimeCaptured.Text = ds.Tables[0].Rows[0]["CreateDate"].ToString().Trim();
                    lblIncidentTime.Text = ds.Tables[0].Rows[0]["IncidentTime"].ToString().Trim();
                    lblTimePersonCalled.Text = ds.Tables[0].Rows[0]["TimeArtisanCalled"].ToString().Trim();
                    lblTimeOnSite.Text = ds.Tables[0].Rows[0]["DateTimeIn"].ToString().Trim();
                    lblTimeCompleted.Text = ds.Tables[0].Rows[0]["DateTimeOut"].ToString().Trim();

                    lblContactName.Text = ds.Tables[0].Rows[0]["ContactPersonName"].ToString().Trim();
                    lblContactTel.Text = ds.Tables[0].Rows[0]["ContactPersonTel"].ToString().Trim();
                    lblContactExt.Text = ds.Tables[0].Rows[0]["ContactPersonExt"].ToString().Trim();
                    lblContactCell.Text = ds.Tables[0].Rows[0]["ContactPersonCell"].ToString().Trim();
                    lblContactAddress.Text = ds.Tables[0].Rows[0]["ContactPersonAddress"].ToString().Trim();

                    lblAuthorisedName.Text = ds.Tables[0].Rows[0]["AuthorisedName"].ToString().Trim();
                    lblAuthorisedTel.Text = ds.Tables[0].Rows[0]["AuthorisedTel"].ToString().Trim();
                    lblAuthorisedExt.Text = ds.Tables[0].Rows[0]["AuthorisedExt"].ToString().Trim();
                    lblAuthorisedCell.Text = ds.Tables[0].Rows[0]["AuthorisedCell"].ToString().Trim();
                    lblAuthorisedAddress.Text = ds.Tables[0].Rows[0]["AuthorisedAddress"].ToString().Trim();
                }

            }
  
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            saveContacted();
        }

        void saveContacted()
        {
            DateTime val;

            if(txtDateContacted.Text.Trim() == String.Empty || txtTimeContacted.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Date and time is required!");
            }
            else if (DateTime.TryParse(txtDateContacted.Text.Trim(), out val) == false)
            {
                ShowAlert(2, "Enter a valid date. Format 'dd/MM/yyyy OR yyyy-MM-dd'");
            }
            else if (DateTime.TryParse(txtDateContacted.Text.Trim() + " " + txtTimeContacted.Text.Trim(), out val) == false)
            {
                ShowAlert(2, "Enter a valid time. Format '00:00'");
            }
            else
            {
                CalloutID = Convert.ToInt32(Request.QueryString["ID"]);
                CultureInfo culture = new CultureInfo("en-US");

                String DateTimeContacted = "";

                if (txtDateContacted.Text.Trim().Contains("/"))
                {
                    DateTimeContacted = txtDateContacted.Text.Trim();
                    DateTimeContacted = DateTimeContacted.Substring(6) + "-" + DateTimeContacted.Substring(3, 2) + "-" + DateTimeContacted.Substring(0, 2) + " " + txtTimeContacted.Text.Trim();
                }
                else
                {
                    DateTimeContacted = Convert.ToDateTime(txtDateContacted.Text.Trim(), culture).ToString("yyyy-MM-dd") + " " + txtTimeContacted.Text.Trim();
                }

                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;

                if (rdContactOutcome.SelectedValue.Trim() == "1")
                {
                    access obj = new access();
                    obj.saveCalloutAttempt(SystemUserName, CalloutID, DateTimeContacted);
                }
                else
                {
                    access obj = new access();
                    obj.saveCalloutAttempt(SystemUserName, CalloutID, Convert.ToInt32(rdContactOutcome.SelectedValue.Trim()), rdContactOutcome.SelectedItem.Text.Trim(), DateTimeContacted);
                }

                Session["CalloutSaved"] = true;
                ShowAlert(1, "Callout saved successfully.");
            }
        }

        protected void btnPrintWelcomeNote_Click(object sender, EventArgs e)
        {
            savePersonOnSite();
        }

        void savePersonOnSite()
        {
            try
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;

                access obj = new access();
                obj.savePersonOnsite(SystemUserName, CalloutID, lblTimeArrived.Text.Trim());

                Session["CalloutSaved"] = true;
                ShowAlert(1, "Callout saved successfully.");

                Response.Write("<script>window.open ('pdf.aspx?ID=" + lblCalloutNumber.Text.Trim() + "&RPT=1','_blank');</script>");
                loadCalloutInfo();
            }
            catch (Exception ex)
            {
                ShowAlert(2, ex.Message);
            }

        }


        protected void btnCloseCallout_Click(object sender, EventArgs e)
        {
            closeCallout();
        }

        void closeCallout()
        {
            try
            {

                DateTime val;

                if (txtDateCompleted.Text.Trim() == String.Empty || txtTimeCompleted.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Date and time is required!");
                }
                else if (DateTime.TryParse(txtDateCompleted.Text.Trim(), out val) == false)
                {
                    ShowAlert(2, "Enter a valid date. Format 'dd/MM/yyyy OR yyyy-MM-dd'");
                }
                else if (DateTime.TryParse(txtDateCompleted.Text.Trim() + " " + txtTimeCompleted.Text.Trim(), out val) == false)
                {
                    ShowAlert(2, "Enter a valid time. Format '00:00'");
                }
                else
                {
                    CalloutID = Convert.ToInt32(Request.QueryString["ID"]);
                    CultureInfo culture = new CultureInfo("en-US");

                    String DateTimeCompleted = "";

                    if (txtDateCompleted.Text.Trim().Contains("/"))
                    {
                        DateTimeCompleted = txtDateCompleted.Text.Trim();
                        DateTimeCompleted = DateTimeCompleted.Substring(6) + "-" + DateTimeCompleted.Substring(3, 2) + "-" + DateTimeCompleted.Substring(0, 2) + " " + txtTimeCompleted.Text.Trim();
                    }
                    else
                    {
                        DateTimeCompleted = Convert.ToDateTime(txtDateCompleted.Text.Trim(), culture).ToString("yyyy-MM-dd") + " " + txtTimeCompleted.Text.Trim();
                    }

                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    access obj = new access();
                    obj.closeCallout(SystemUserName, CalloutID, DateTimeCompleted);

                    CalloutNotifications cn = new CalloutNotifications();
                    String response = cn.sendSignOfRequest(CalloutID, Server.MapPath("~/callout/approval-request.html"));

                    Session["CalloutSaved"] = true;

                    ShowAlert(1, "Callout closed successfully<br/><br/>" + response);

                }
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if(Session["CalloutSaved"] != null)
            {
                Session.Remove("CalloutSaved");
                Response.Redirect("callouts-active.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

       
    }
}