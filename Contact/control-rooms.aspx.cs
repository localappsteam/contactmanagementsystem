﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;

namespace Contact
{
    public partial class control_rooms : System.Web.UI.Page
    {
        public override void VerifyRenderingInServerForm(Control control)
        {
            //Confirms that an HtmlForm control is rendered for the
            //specified ASP.NET server control at run time.
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        void exportToExcel()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=ControlRooms.xls");
            gridControlRooms.GridLines = GridLines.Both;
            gridControlRooms.HeaderStyle.Font.Bold = true;
            gridControlRooms.HeaderStyle.ForeColor = Color.Black;
            gridControlRooms.HeaderStyle.BackColor = Color.Silver;
            gridControlRooms.HeaderStyle.Font.Underline = true;

            gridControlRooms.AllowPaging = false;
            gridControlRooms.AllowSorting = false;
            gridControlRooms.DataBind();
            gridControlRooms.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }

        void releaseObject(object obj)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            exportToExcel();
        }
    }
}