﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Security.Principal;

namespace Contact
{
    public partial class contractors_search : System.Web.UI.Page
    {
        public override void VerifyRenderingInServerForm(Control control)
        {
            //Confirms that an HtmlForm control is rendered for the
            //specified ASP.NET server control at run time.
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int RoleID = 18;
            int AdminRoleID = 3;

            access obj = new access();
            if (obj.checkRoles(UserName, RoleID, AdminRoleID))
            {
                lkAddContractor.Visible = true;
            }
            else
            {
                lkAddContractor.Visible = false;
            }

        }

        protected void gridContractors_DataBound(object sender, EventArgs e)
        {
            gridContractors.SelectedIndex = -1;
        }

        void exportToExcel()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=Contractors.xls");
            gridContractors.GridLines = GridLines.Both;
            gridContractors.HeaderStyle.Font.Bold = true;
            gridContractors.HeaderStyle.ForeColor = Color.Black;
            gridContractors.HeaderStyle.BackColor = Color.Silver;
            gridContractors.HeaderStyle.Font.Underline = true;

            gridContractors.AllowPaging = false;
            gridContractors.AllowSorting = false;
            gridContractors.DataBind();
            gridContractors.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }

        void releaseObject(object obj)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            exportToExcel();
        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.CommandTimeout = 0;
        }

        protected void SqlDataSource1_Selected(object sender, SqlDataSourceStatusEventArgs e)
        {
            lblRows.Text = e.AffectedRows.ToString();
        }

        void viewContractor()
        {
            Session["SelectedContractorID"] = gridContractors.SelectedRow.Cells[1].Text.Trim();
            Response.Redirect("contractors-search-detailed.aspx");
        }

        protected void gridContractors_SelectedIndexChanged(object sender, EventArgs e)
        {
            viewContractor();
        }
    }
}