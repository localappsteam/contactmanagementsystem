﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Principal;

namespace Contact
{
    public partial class emergency_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int RoleID = 30;
            int AdminRoleID = 3;

            access obj = new access();
            if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
            {
                Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                Response.Redirect("access-denied.aspx");
            }

        }

        protected void btnAdd1_Click(object sender, EventArgs e)
        {
            submit();
        }
        void submit()
        {
            
          if (txtName.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Name(s) required!");
            }
            else if (txtPhone.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Phone number required!");
            }
           
            else if (txtTitle.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Job title required!");
            }
            else
            {
                if(Session["MillID"] != null)
                {
                    try
                    {
                        WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String UserName = UserSecurityPrincipal.Name;

                        access obj = new access();
                        obj.addNewEmgergencyContact(UserName, txtName.Text.Trim(), txtExt.Text.Trim(),
                            txtPhone.Text.Trim(), txtTitle.Text.Trim(),   Convert.ToInt32(Session["MillID"]));

                        clearFields();
                        ShowAlert(1, "Emergency added successfully.");

                            
                    }
                    catch (Exception ex)
                    {

                        ShowAlert(2, ex.Message);
                    }
                }
            }

        }

        void clearFields()
        {
            txtName.Text = ""; txtExt.Text = ""; txtPhone.Text = "";  
            txtTitle.Text = "";
        }


        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }
}