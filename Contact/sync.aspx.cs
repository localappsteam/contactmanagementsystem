﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Contact
{
    public partial class sync : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBeginSync_Click(object sender, EventArgs e)
        {
            syncCellphoneNumber();
        }


        void syncCellphoneNumber()
        {
            Label2.Visible = false;

            access obj = new access();
            DataSet ds = obj.getStaffWithoutCellphoneNumbers();

            ADManager ad = new ADManager();

            int emptype;
            String un, email, cell, office;

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                emptype = Convert.ToInt32(ds.Tables[0].Rows[i]["EmpType"].ToString().Trim());
                un = ds.Tables[0].Rows[i]["UserName"].ToString().Trim();
                email = ds.Tables[0].Rows[i]["Email"].ToString().Trim();
                office = ad.getUserLocation(un).Trim();

                if (office != String.Empty)
                {
                    cell = ad.GetCellphoneFromAD(email, office).Trim();

                    if (cell != String.Empty)
                    {
                        obj = new access();
                        obj.updateCellphoneNumber(emptype, un, cell);
                    }
                }
                
            }

            Label2.Visible = true;
        }
    }
}