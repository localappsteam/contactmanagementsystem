﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Principal;

namespace Contact
{
    public partial class emergency_search_detailed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
            loadData();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int RoleID = 30;
            int AdminRoleID = 3;

            access obj = new access();
            if (obj.checkRoles(UserName, RoleID, AdminRoleID))
            {
                divOptions.Visible = true;
            }
            else
            {
                divOptions.Visible = false;
                divEdit.Visible = false;
                divEdit2.Visible = false;
                divDelete.Visible = false;
            }

        }

        void loadData()
        {
            if (Session["SelectedEmergencyID"] != null)
            {
                if (divEdit.Visible == false)
                {
                    access obj = new access();
                    DataSet ds = obj.getSelectedEmergencyDetails(Session["SelectedEmergencyID"].ToString());

               
                    txtName.Text = ds.Tables[0].Rows[0]["Name"].ToString();
                  
                    txtPhone.Text = ds.Tables[0].Rows[0]["Phone"].ToString();
               
                    txtExt.Text = ds.Tables[0].Rows[0]["Ext"].ToString();
                   
                    txtTitle.Text = ds.Tables[0].Rows[0]["Title"].ToString();
                   
                }

            }
            else
            {
                Response.Redirect("emergency-contacts.aspx");
            }
        }

        void enableEditing()
        {
            divEdit.Visible = true;
            divEdit2.Visible = true;

          
            txtName.Enabled = true; txtName.CssClass = "form-control";
            txtExt.Enabled = true; txtExt.CssClass = "form-control";
            txtPhone.Enabled = true; txtPhone.CssClass = "form-control";
            txtTitle.Enabled = true; txtTitle.CssClass = "form-control";
           

        }

        void disableEditing()
        {
            divEdit.Visible = false;
            divEdit2.Visible = false;

           
            txtName.Enabled = false; txtName.CssClass = "form-control input-disabled";
            txtExt.Enabled = false; txtExt.CssClass = "form-control input-disabled";
            txtPhone.Enabled = false; txtPhone.CssClass = "form-control input-disabled";
            txtExt.Enabled = false; txtExt.CssClass = "form-control input-disabled";  
            txtTitle.Enabled = false; txtTitle.CssClass = "form-control input-disabled";
            divDelete.Visible = false;

        }

        protected void btnViewEdit_Click(object sender, EventArgs e)
        {
            enableEditing();
        }

        protected void btnCancel1_Click(object sender, EventArgs e)
        {
            disableEditing();
        }

        protected void btnSaveChanges1_Click(object sender, EventArgs e)
        {
            saveChanges();
        }
        void saveChanges()
        {
            if (Session["SelectedEmergencyID"] != null)
            {

                
              if (txtName.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Name(s) required!");
                }
                else if (txtTitle.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Job title required!");
                }
      
                else
                {
                    try
                    {
                        WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String UserName = UserSecurityPrincipal.Name;

                        access obj = new access();
                        obj.editEmergencyDetails(UserName, Convert.ToInt32(Session["SelectedEmergencyID"]), txtName.Text.Trim(), 
                            txtPhone.Text.Trim(), txtExt.Text.Trim(),
                            txtTitle.Text.Trim());

                        ShowAlert(1, "Changes saved successfully.");
                        disableEditing();
                    }
                    catch (Exception ex)
                    {

                        ShowAlert(2, ex.Message);
                    }
                }
            }
            else
            {
                Response.Redirect("employees-search.aspx");
            }

        }

        protected void btnViewDelete_Click(object sender, EventArgs e)
        {
            divDelete.Visible = true;
        }

        protected void btnDeleteYes_Click(object sender, EventArgs e)
        {
            deleteEmergencyContact();
        }

        protected void btnDeleteNo_Click(object sender, EventArgs e)
        {
            divDelete.Visible = false;
        }

        void deleteEmergencyContact()
        {
            if (Session["SelectedEmergencyID"] != null)
            {
                try
                {
                    WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String UserName = UserSecurityPrincipal.Name;

                    access obj = new access();
                    obj.deleteEmergencyContact(UserName, Convert.ToInt32(Session["SelectedEmergencyID"]));
                    disableEditing();

                    Session.Remove("SelectedEmergencyID");

                    ShowAlert(1, "Emergency Contact deleted successfully.");
                }
                catch (Exception ex)
                {

                    ShowAlert(2, ex.Message);
                }
            }
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }
}