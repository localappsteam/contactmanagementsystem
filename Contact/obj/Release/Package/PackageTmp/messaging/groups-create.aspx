﻿<%@ Page Title="" Language="C#" MasterPageFile="~/messaging/massaging.Master" AutoEventWireup="true" CodeBehind="groups-create.aspx.cs" Inherits="Contact.messaging.groups_create" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Messaging / Groups / Create</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Create Employees & Contractor Group</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-2 btn-fix-width">
                                <a href="groups-types.aspx" class="btn btn-default btn-sm btn-block">
                                    <p class="fa fa-reply m-r-10"></p>
                                    Go back</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cont-holder">
                            </div>
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-5">
                                        <p>Group name<span class="text-danger icon-ml">*</span></p>
                                        <asp:TextBox ID="txtGroupName" Placeholder="Enter group name..." CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>Select contacts below<span class="text-danger icon-ml">*</span></p>
                                                <asp:TextBox ID="txtSearch" CssClass="form-control form-control-sm" Width="78%" Style="float: left" Placeholder="Search employee or contractor..." runat="server"></asp:TextBox>
                                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                                            </div>
                                            <div class="col-md-12" style="padding-left: 6px">
                                                <div class="row">
                                                    <div class="col-md-9" style="padding-left: 0px">
                                                        <div class="cont-holder" style="padding: 0px; padding-top: 0px">
                                                            <asp:GridView ID="gridEmployees" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AllowPaging="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="gridEmployees_SelectedIndexChanged" OnDataBound="gridEmployees_DataBound">
                                                                <AlternatingRowStyle BackColor="#f5f5f5" />
                                                                
                                                                <RowStyle Height="41px"></RowStyle>
                                                                <Columns>
                                                                    <asp:CommandField ShowSelectButton="True" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                                                    <asp:BoundField DataField="#" HeaderText="#" ReadOnly="True" SortExpression="#" />
                                                                    <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" ReadOnly="True" />
                                                                    <asp:BoundField DataField="Firstnames" HeaderText="Firstnames" SortExpression="Firstnames" ReadOnly="True" />
                                                                    <asp:BoundField DataField="Nickname" HeaderText="Nickname" SortExpression="Nickname" ReadOnly="True" />
                                                                    <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" ReadOnly="True" />
                                                                    <asp:BoundField DataField="Cell" HeaderText="Cell" SortExpression="Cell" ReadOnly="True" />
                                                                    <asp:BoundField DataField="Department" HeaderText="Department" ReadOnly="True" SortExpression="Department" />
                                                                    <asp:BoundField DataField="EmpType" HeaderText="EmpType" ReadOnly="True" SortExpression="EmpType" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Messaging_EmployeeSearch" SelectCommandType="StoredProcedure">
                                                                <SelectParameters>
                                                                    <asp:ControlParameter ControlID="txtSearch" DefaultValue="" Name="SearchValue" PropertyName="Text" Type="String" />
                                                                    <asp:Parameter DefaultValue=" " Name="LIST" Type="String" />
                                                                    <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" style="padding-left: 0px">
                                                        <div class=" cont-holder-plain" style="padding: 0px; padding-top: 0px">
                                                            <h5>Selected recipients</h5>
                                                        </div>

                                                        <div class="cont-holder" style="padding: 0px; padding-top: 0px">
                                                            <asp:BulletedList ID="BulletedList1" runat="server">
                                                            </asp:BulletedList>
                                                        </div>
                                                        <div class="cont-holder-plain" style="padding: 0px; padding-top: 0px; margin-top: 30px">
                                                            <asp:LinkButton ID="btnSubmit" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnSubmit_Click"><p class="fa fa-paper-plane m-r-10"></p>Submit</asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnSubmit" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
