﻿<%@ Page Title="" Language="C#" MasterPageFile="~/messaging/massaging.Master" AutoEventWireup="true" CodeBehind="history.aspx.cs" Inherits="Contact.messaging.history" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Messaging / Message history</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Messaging History</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-2 btn-fix-width">
                                <a href="messaging.aspx" class="btn btn-default btn-sm btn-block">
                                    <p class="fa fa-reply m-r-10"></p>
                                    Go back</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cont-holder">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <p>Start date <span class="text-danger icon-ml">*</span></p>
                                <asp:TextBox ID="txtStartDate" CssClass="form-control form-control-sm" TextMode="Date" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <p>Start date <span class="text-danger icon-ml">*</span></p>
                                <asp:TextBox ID="txtEndDate" CssClass="form-control form-control-sm" TextMode="Date" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-2 btn-fix-width" style="margin-top: 22px">
                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-block btn-sm m-t-10" runat="server">Search<p class="fa fa-search m-l-10"></p></asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div id="divRecentMessages" runat="server" class="col-md-12 cont-holder">
                                <asp:GridView ID="gridHistory" Width="100%" CssClass="table" Font-Size="12px" AllowSorting="True" BorderStyle="None" GridLines="None" runat="server" EmptyDataText="No roles assigned." AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                                    <AlternatingRowStyle BackColor="#f5f5f5" />
                                    <RowStyle Height="41px"></RowStyle>
                                    <Columns>
                                        <asp:BoundField DataField="DateSent" HeaderText="DateSent" SortExpression="DateSent" />
                                        <asp:BoundField DataField="Cellphone" HeaderText="Cellphone" SortExpression="Cellphone" />
                                        <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" />
                                        <asp:BoundField DataField="SentBy" HeaderText="SentBy" SortExpression="SentBy" ReadOnly="True" />
                                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" ProviderName="System.Data.SqlClient" SelectCommand="sp_Contact_Messaging_ViewHistory" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="EmployeeID" SessionField="EmployeeID" Type="Int32" />
                                        <asp:ControlParameter ControlID="txtStartDate" Name="StartDate" PropertyName="Text" Type="DateTime" />
                                        <asp:ControlParameter ControlID="txtEndDate" Name="EndDate" PropertyName="Text" Type="DateTime" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
