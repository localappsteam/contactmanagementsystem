﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="callouts.aspx.cs" Inherits="Contact.callout.callouts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Callouts</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Callouts</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row text-center">
                            <div class="col-md-12 text-center" style="padding-right: 0px">
                                <div class="menu-circle-container">
                                    <a href="callouts-active.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/folder-green-image-people-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Active Callouts</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container">
                                    <a href="callouts-complete.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/folder-yellow-documents-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Completed Callouts</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="menu-circle-container">
                                    <a href="callouts-cancelled.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/folder-red-visiting-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Cancelled Callouts</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                         </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
