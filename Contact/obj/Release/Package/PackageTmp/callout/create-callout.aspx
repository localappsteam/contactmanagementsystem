﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="create-callout.aspx.cs" Inherits="Contact.callout.create_callout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Create callout</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Create Callout</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row" id="divDuty" runat="server" visible="true">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h5>Select the standby duty you would like to call out.</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10">
                                        <asp:TextBox ID="txtSearch" CssClass="form-control form-control-sm" Width="75%" Style="float: left" Placeholder="Search..." runat="server"></asp:TextBox>
                                        <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 cont-holder" style="overflow:auto">
                                        <asp:GridView ID="gridDuties" Width="100%" CssClass="table" Font-Size="12px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AutoGenerateColumns="False" DataSourceID="SqlDataSource1" DataKeyNames="#" OnSelectedIndexChanged="gridDuties_SelectedIndexChanged">
                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                            
                                            <RowStyle Height="41px"></RowStyle>
                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" ControlStyle-CssClass="btn btn-block btn-xs btn-default" SelectText="Select" ControlStyle-Font-Size="11px" ButtonType="Button" />
                                                <asp:BoundField DataField="#" HeaderText="#" ReadOnly="True" SortExpression="#" HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="1px" InsertVisible="False" />
                                                <asp:BoundField DataField="Duty No" HeaderText="Duty No" SortExpression="Duty No" />
                                                <asp:BoundField DataField="Duty" HeaderText="Duty" SortExpression="Duty" />
                                                <asp:BoundField DataField="Person" HeaderText="Person" SortExpression="Person" />
                                                 <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_Duties_View" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="MillID" SessionField="MillID" DefaultValue="31" Type="Int32" />
                                                <asp:ControlParameter ControlID="txtSearch" DefaultValue=" " Name="SearchValue" PropertyName="Text" Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="divDetails" runat="server" visible="false">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-12">
                                        Standby Duty:<asp:Label ID="lblDuty" CssClass="m-l-10" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 bg-primary">
                                        <h5>Contact Information</h5>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-3 field-name field-margin">
                                        <p>Person to call:<span class="m-l-10 text-danger">*</span></p>
                                    </div>
                                    <div class="col-md-4 field-margin">
                                        <asp:DropDownList ID="drdPersonToCall" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_PersonCalledOut" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                    <div class="col-md-5 field-name field-margin">
                                        <small>Who do you want to call out? If the person is not inthe list please contact Security.</small>
                                    </div>
                                </div>
                                <div class="row field-margin p-t-10">
                                    <div class="col-md-3 field-name field-margin">
                                        <p>Standby list details incorrect:<span class="m-l-10 text-danger">*</span></p>
                                    </div>
                                    <div class="col-md-4 field-name field-margin">
                                        <asp:CheckBox ID="chkIncorrectList" Text=" " runat="server" />
                                    </div>
                                    <div class="col-md-5 field-name field-margin">
                                        <small>Please select this if the incorrect person was shown on the standby list. </small>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-3 field-name field-margin" >
                                        <p>Area:<span class="m-l-10 text-danger">*</span></p>
                                    </div>
                                    <div class="col-md-4 field-margin">
                                        <asp:DropDownList ID="drdArea" CssClass="form-control" runat="server"  DataSourceID="SqlDataSource3" DataTextField="Description" DataValueField="#">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_SelectArea" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                    <div class="col-md-5 field-name field-margin">
                                        <small>Which area must they report to?</small>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-3 field-name field-margin" >
                                        <p>Contact:<span class="m-l-10 text-danger">*</span></p>
                                    </div>
                                    <div class="col-md-4 field-margin">
                                        <asp:DropDownList ID="drdContact" CssClass="form-control" runat="server"  DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-5 field-name field-margin">
                                        <small>Who must the person contact when they arrive at the mill? </small>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-3 field-name field-margin" >
                                        <p>Authorised by:<span class="m-l-10 text-danger">*</span></p>
                                    </div>
                                    <div class="col-md-4 field-margin">
                                        <asp:DropDownList ID="drdAuthorised" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-5 field-name field-margin">
                                        <small>Who authorised this callout? - (Shift Manager)</small>
                                    </div>
                                </div>
                                 <div class="row field-margin">
                                    <div class="col-md-3 field-name field-margin" >
                                        <p>Notification number:</p>
                                    </div>
                                    <div class="col-md-4 field-margin">
                                        <asp:TextBox ID="txtNotificationNo" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-md-5 field-name field-margin">
                                        <small>SAP notification number</small>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-3 field-name field-margin" >
                                        <p>Do you require Helpers?</p>
                                    </div>
                                    <div class="col-md-4 field-name field-margin">
                                        <asp:RadioButtonList ID="rdHelpers" RepeatDirection="Horizontal" Width="200px" runat="server">
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                    <div class="col-md-5 field-name field-margin">
                                        <small>If helpers are required select "Yes"</small>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-3 field-name field-margin" >
                                        <p>Callout type:<span class="m-l-10 text-danger">*</span></p>
                                    </div>
                                    <div class="col-md-4 field-margin">
                                        <asp:DropDownList ID="drdCalloutType" CssClass="form-control" runat="server" DataSourceID="SqlDataSource4" DataTextField="Description" DataValueField="#">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Callout_SelectCalloutType" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                    </div>
                                    <div class="col-md-5 field-name field-margin">
                                        <small>What type of callout is this?</small>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-3 field-name field-margin" >
                                        <p>Time of Incident:<span class="m-l-10 text-danger">*</span></p>
                                    </div>
                                    <div class="col-md-4 field-margin">
                                        <div class="row field-margin">
                                             <div class="col-md-6 field-margin">
                                                 <asp:TextBox ID="txtDate" CssClass="form-control" Placeholder="dd/mm/yyyy" TextMode="Date" runat="server"></asp:TextBox>
                                                 </div>
                                             <div class="col-md-6 field-margin">
                                                 <asp:TextBox ID="txtTime" CssClass="form-control" Placeholder="hh:mm" TextMode="Time" runat="server"></asp:TextBox>
                                                 </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 field-name field-margin">
                                        <small>Date and Time of the callout</small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 bg-primary">
                                        <h5>Reason for Callout</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtReasonForCallout" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                         <asp:LinkButton ID="btnSubmit" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnSubmit_Click"><p class="fa fa-save icon-mr"></p>Submit</asp:LinkButton>
                                     </div>
                                    <div class="col-md-2 btn-fix-width">
                                        <a href="create-callout.aspx" class="btn btn-default btn-sm btn-block">
                                            <p class="fa fa-times icon-mr"></p>
                                            Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row" id="divHelpers" runat="server" visible="false">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 1:<span class="m-l-10 text-danger">*</span></p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper1" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 2:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper2" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 3:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper3" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 4:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper4" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 5:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper5" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 6:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper6" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 7:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper7" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 8:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper8" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 9:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper9" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row field-margin">
                                    <div class="col-md-2 field-name field-margin">
                                        <p>Helper 10:</p>
                                    </div>
                                    <div class="col-md-5 field-margin">
                                        <asp:DropDownList ID="drdHelper10" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Person" DataValueField="#">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row m-t-40">
                                    <div class="col-md-2 btn-fix-width">
                                         <asp:LinkButton ID="btnSubmit2" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnSubmit2_Click"><p class="fa fa-save icon-mr"></p>Submit</asp:LinkButton>
                                     </div>
                                    <div class="col-md-2 btn-fix-width">
                                         <asp:LinkButton ID="btnBack" CssClass="btn btn-default btn-sm btn-block" runat="server" OnClick="btnBack_Click"><p class="fa fa-reply icon-mr"></p>Back</asp:LinkButton>
                                     </div>
                                    <div class="col-md-2 btn-fix-width">
                                        <a href="create-callout.aspx" class="btn btn-default btn-sm btn-block">
                                            <p class="fa fa-times icon-mr"></p>
                                            Cancel</a>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
