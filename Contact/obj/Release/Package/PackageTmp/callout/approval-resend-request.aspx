﻿<%@ Page Title="" Language="C#" MasterPageFile="~/callout/callout.Master" AutoEventWireup="true" CodeBehind="approval-resend-request.aspx.cs" Inherits="Contact.callout.approval_resend_request" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Callout / Callout Approval / Resend Approval Request</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Resend Approval Request</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row" >
                                    <div class="col-md-4"">
                                        <h5>Callout No: <asp:Label ID="lblCalloutID" CssClass="m-l-10" Font-Bold="false" runat="server" Text="Label"></asp:Label></h5>
                                        <h5>Reason: <asp:Label ID="lblReason" CssClass="m-l-10" Font-Bold="false" runat="server" Text="Label"></asp:Label></h5>
                                        </div>
                                    </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>Use the option below to send the callout approval request.</h5>
                                        <p class="text-danger">NB: Make sure that the person's email address is correct on the <a href="../index.aspx" target="_blank">Contact system</a>.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Select the person you want to resend the callout approval request to:</p>
                                        <asp:RadioButtonList ID="rdRole" Font-Bold="true" runat="server">
                                            <asp:ListItem Value="1"><b class="icon-mr">Person Called Out</b> (The person assigned to the callout).</asp:ListItem>
                                            <asp:ListItem Value="2"><b class="icon-mr">Supervisor, Manager, etc</b> (The person who authorized the callout).</asp:ListItem>
                                            <asp:ListItem Value="3"><b class="icon-mr">Control Room</b> (Security).</asp:ListItem>
                                            <asp:ListItem Value="4"><b class="icon-mr">Send to All</b> (Send to all three options above).</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:LinkButton ID="btnSendApproval" CssClass="btn btn-sm btn-block btn-primary" runat="server" OnClick="btnSendApproval_Click"><p class="fa fa-send icon-mr"></p>Send</asp:LinkButton>
                                    </div>
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:HyperLink ID="btnBack" class="btn btn-sm btn-block btn-default" runat="server"><p class="fa fa-times icon-mr"></p>Cancel</asp:HyperLink>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
