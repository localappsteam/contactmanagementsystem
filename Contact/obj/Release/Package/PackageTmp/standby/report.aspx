﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="report.aspx.cs" Inherits="Contact.standby.report" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Standby</title>
    <!-- Favicon-->
    <link rel="icon" href="../images/sappi logo.png" type="image/x-icon">

    <!-- Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->

    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/custom.css" rel="stylesheet" />

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
</head>
<body class="theme-blue bg-white">
    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
    <form id="form1" runat="server">

        <div class="card">
            <div id="divList" runat="server" class="body">
                <div class="panel" style="min-height: 800px">
                    <div class="row">
                        <div class="col-md-12 text-center" style="margin-bottom: 10px">
                            <h3 class="m-b-20" style="margin-top: 0px">
                                <asp:Label ID="lblMill" runat="server" Text=""></asp:Label>
                                STANDBY LIST</h3>
                            <h4>
                                <asp:Label ID="lblMill2" runat="server" Text=""></asp:Label>
                                NEW SCHEDULE</h4>
                            <h4>
                                <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                                <span class="m-r-10"></span>
                                -
                                    <span class="m-r-10"></span>
                                <asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                            </h4>
                            <asp:LinkButton ID="btnPrevious" ToolTip="Click here to view alternate standby start and end times if it differs during the selected week above." runat="server" OnClick="btnPrevious_Click"><p class="fa fa-backward icon-mr"></p> Previous list</asp:LinkButton>
                            <asp:LinkButton ID="btnNext" ToolTip="Click here to view alternate standby start and end times if it differs during the selected week above." runat="server" OnClick="btnNext_Click">Next list<p class="fa fa-forward icon-ml"></p></asp:LinkButton>
                        </div>
                        <div class="col-md-12 cont-holder">
                        </div>
                        <div class="col-md-12 cont-holder-plain" style="margin-top: -10px">
                            <div class="row m-l-20 m-r-20">
                                <div class="col-md-12 text-center bg-red" style="margin-bottom: 10px; padding-bottom: 0px">
                                    <asp:Label ID="lblEmergencyNumber" Font-Size="14px" Font-Bold="false" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="body table-responsive" style="padding-top: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                <asp:Table ID="tblPerson" CssClass="table table-bordered" Style="font-size: 12px;" runat="server">
                                    <asp:TableHeaderRow CssClass="bg-indigo">
                                        <asp:TableHeaderCell Text="Duty no#" CssClass="list-table-cell">
                                        </asp:TableHeaderCell>
                                        <asp:TableHeaderCell Text="Contact Person" CssClass="list-table-cell">
                                        </asp:TableHeaderCell>
                                        <asp:TableHeaderCell Text="Position" CssClass="list-table-cell">
                                        </asp:TableHeaderCell>
                                        <asp:TableHeaderCell Text="Address" CssClass="list-table-cell">
                                        </asp:TableHeaderCell>
                                        <asp:TableHeaderCell Text="Tel / Ext" CssClass="list-table-cell">
                                        </asp:TableHeaderCell>
                                        <asp:TableHeaderCell Text="Cellphone" CssClass="list-table-cell">
                                        </asp:TableHeaderCell>
                                    </asp:TableHeaderRow>
                                </asp:Table>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-4 p-l-40">
                            <h4>Please select date to view standby list</h4>
                             <asp:TextBox ID="txtSearch" Placeholder="dd/mm/yyyy" CssClass="form-control form-control-sm" Width="75%" Style="float: left" TextMode="Date" runat="server"></asp:TextBox>
                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server">View<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                        </div>
                         <div class="col-md-4 p-l-40">
                             <asp:Label ID="lblDateError" Visible="false" ForeColor="Red" runat="server" Text="Enter a valid date. Formart(dd/mm/yyyy)"></asp:Label>
                           </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 p-l-40">
                            <h4 runat="server" id="lkPrimaryUpdaters">
                            </h4>
                            <h4>For changes on the CURRENT WEEKS STANDBY LIST please phone Security @ 6107 / 6108</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>


    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>
    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="../plugins/raphael/raphael.min.js"></script>
    <script src="../plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="../plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="../plugins/flot-charts/jquery.flot.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Icons Js-->
    <script src="../js/fontawesome.min.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/index.js"></script>
    <script src="../js/menu.js"></script>

</body>
</html>
