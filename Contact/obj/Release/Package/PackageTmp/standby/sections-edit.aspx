﻿<%@ Page Title="" Language="C#" MasterPageFile="~/standby/standby.Master" AutoEventWireup="true" CodeBehind="sections-edit.aspx.cs" Inherits="Contact.standby.sections_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Standby / Sections / Edit</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Standby Section</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:LinkButton ID="btnDelete" CssClass="btn btn-danger btn-sm btn-block" runat="server" OnClick="btnDelete_Click"><p class="fa fa-trash icon-mr"></p>Delete section</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row" id="divDelete" runat="server" visible="false">
                                    <div class="col-md-12">
                                        <h5 class="text-danger">Delete section?</h5>
                                        <p>Are you sure you want to delete this section?</p>
                                        <asp:LinkButton ID="btnDeleteYes" CssClass="btn btn-danger btn-sm" runat="server" OnClick="btnDeleteYes_Click"><p class="fa fa-check icon-mr"></p>Yes</asp:LinkButton>
                                        <asp:LinkButton ID="btnDeleteNo" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnDeleteNo_Click"><p class="fa fa-times icon-mr"></p>No</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <h5>Section name:
                                            <asp:TextBox ID="txtSectionName" CssClass="form-control" runat="server"></asp:TextBox></h5>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>Sorting order:
                                             <asp:TextBox ID="txtSortingOrder" TextMode="Number" CssClass="form-control" runat="server"></asp:TextBox></h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                            <asp:LinkButton ID="btnUpdate" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnUpdate_Click"><p class="fa fa-save icon-mr"></p>Update</asp:LinkButton>
                                            </div>
                                    <div class="col-md-2 btn-fix-width">
                                            <a href="sections.aspx" class="btn btn-default btn-sm btn-block"><p class="fa fa-times icon-mr"></p>Close</a>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
