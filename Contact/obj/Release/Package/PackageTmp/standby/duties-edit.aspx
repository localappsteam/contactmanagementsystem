﻿<%@ Page Title="" Language="C#" MasterPageFile="~/standby/standby.Master" AutoEventWireup="true" CodeBehind="duties-edit.aspx.cs" Inherits="Contact.standby.duties_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Standby / Duties / Edit</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Standby Duty</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                            <asp:LinkButton ID="btnDelete" CssClass="btn btn-danger btn-sm btn-block" runat="server" OnClick="btnDelete_Click"><p class="fa fa-trash icon-mr"></p>Delete duty</asp:LinkButton>
                                        </div>
                                    </div>
                                <div class="row" id="divDelete" runat="server" visible="false">
                                    <div class="col-md-12">
                                        <h5 class="text-danger">Delete duty?</h5>
                                        <p>Are you sure you want to delete this standby duty?</p>
                                        <asp:LinkButton ID="btnDeleteYes" CssClass="btn btn-danger btn-sm" runat="server" OnClick="btnDeleteYes_Click"><p class="fa fa-check icon-mr"></p>Yes</asp:LinkButton>
                                        <asp:LinkButton ID="btnDeleteNo" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnDeleteNo_Click"><p class="fa fa-times icon-mr"></p>No</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Duty No<span class="text-danger icon-ml">*</span></p>
                                        <asp:TextBox ID="txtDutyNo" CssClass="form-control" placeholder="Enter duty no..." runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Duty<span class="text-danger icon-ml">*</span></p>
                                        <asp:TextBox ID="txtDuty" CssClass="form-control" placeholder="Enter duty..." runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Bleeper<small class="text-muted m-l-10 icon-ml font-11"><i>Optional</i></small></p>
                                        <asp:TextBox ID="txtBleeper" CssClass="form-control" placeholder="Enter bleeper..." runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Standby cell<small class="text-muted m-l-10 icon-ml font-11"><i>Optional</i></small></p>
                                        <asp:TextBox ID="txtStandbyCell" CssClass="form-control" placeholder="Enter standby cell..." runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Department<span class="text-danger icon-ml">*</span></p>
                                       <asp:DropDownList ID="drdDepartment" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Description" DataValueField="DepartmentID">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Standby_SelectDepartments" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Show office Telephone/EXT number on standby list?<span class="text-danger icon-ml">*</span></p>
                                        <asp:RadioButtonList ID="rdShowOfficeTelephone" runat="server">
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Selected="True" Value="0">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Show duty on standby list template?<span class="text-danger icon-ml">*</span></p>
                                        <asp:RadioButtonList ID="rdActiveOnList" runat="server">
                                            <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="0">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:LinkButton ID="btnUpdate" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnUpdate_Click"><p class="fa fa-save icon-mr"></p>Update</asp:LinkButton>
                                    </div>
                                    <div class="col-md-2 btn-fix-width">
                                       <a href="duties.aspx" class="btn btn-default btn-sm btn-block"><p class="fa fa-times icon-mr"></p>Close</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
