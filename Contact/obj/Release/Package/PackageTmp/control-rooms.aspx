﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true"  Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true" CodeBehind="control-rooms.aspx.cs" Inherits="Contact.control_rooms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Control rooms</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Control Rooms</h2>
                    <ul class="header-dropdown m-t--10 m-r-10">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <h4 class="material-icons" style="font-weight:normal; font-size:medium"><p class="fa fa-ellipsis-v m-r-10"></p>Menu </h4>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <asp:LinkButton ID="btnExportToExcel" runat="server" OnClick="btnExportToExcel_Click"><span class="fa  fa-file-excel-o m-r-10"></span>Export to excel</asp:LinkButton>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-6">
                                 <asp:TextBox ID="txtSearch" CssClass="form-control form-control-sm  m-l-10" Width="75%" Style="float:left" Placeholder="Search..." runat="server"></asp:TextBox>
                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 cont-holder" style="padding: 0px">
                                <asp:GridView ID="gridControlRooms" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                                    <AlternatingRowStyle BackColor="#f5f5f5" />
                                    
                                    <RowStyle Height="41px"></RowStyle>
                                    <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                    <Columns>
                                        <asp:BoundField DataField="ControlRoom" HeaderText="ControlRoom" SortExpression="ControlRoom" />
                                        <asp:BoundField DataField="Ext" HeaderText="Ext" SortExpression="Ext" />
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_ControlRooms_View" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="Mill" SessionField="SelectedOffice" Type="String" />
                                        <asp:ControlParameter ControlID="txtSearch" DefaultValue=" " Name="SearchValue" PropertyName="Text" Type="String" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
