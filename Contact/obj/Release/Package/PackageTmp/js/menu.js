﻿window.onload = function () {
    var dir = window.location.href;
    var pagename = dir.substring(dir.lastIndexOf("/") + 1, dir.lastIndexOf("."));

    if (pagename.indexOf("-") > -1) {
        pagename = pagename.substring(0, pagename.indexOf("-"));
    }

    var menu = null; 

    if (pagename.toLowerCase() == "index") {
        menu = document.getElementById("home");
    }
    else if (pagename.toLowerCase() == "employees") {
        menu = document.getElementById("employees");
    }
    else if (pagename.toLowerCase() == "contractors") {
        menu = document.getElementById("contractors");
    }
    else if (pagename.toLowerCase() == "standby") {
        menu = document.getElementById("standby");
    }
    else if (pagename.toLowerCase() == "departments") {
        menu = document.getElementById("departments");
    }
    else if (pagename.toLowerCase() == "control") {
        menu = document.getElementById("controlrooms");
    }
    else if (pagename.toLowerCase() == "companies") {
        menu = document.getElementById("companies");
    }
    else if (pagename.toLowerCase() == "emergency") {
        menu = document.getElementById("emergency");
    }
    else if (pagename.toLowerCase() == "software") {
        menu = document.getElementById("software");
    }
    else if (pagename.toLowerCase() == "users" || pagename.toLowerCase() == "admin" || pagename.toLowerCase() == "types" || pagename.toLowerCase() == "areas") {
        menu = document.getElementById("useraccounts");
    }
    else if (pagename.toLowerCase() == "sync") {
        menu = document.getElementById("datasync");
    }
    else if (pagename.toLowerCase() == "list") {
        menu = document.getElementById("lklist");
    }
    else if (pagename.toLowerCase() == "update") {
        menu = document.getElementById("lkupdatelist");
    }
    else if (pagename.toLowerCase() == "updaters") {
        menu = document.getElementById("lkupdaters");
    }
    else if (pagename.toLowerCase() == "sections") {
        menu = document.getElementById("lksections");
    }
    else if (pagename.toLowerCase() == "duties") {
        menu = document.getElementById("lkduties");
    }
    else if (pagename.toLowerCase() == "messaging") {
        menu = document.getElementById("lkdashboard");
    }
    else if (pagename.toLowerCase() == "new") {
        menu = document.getElementById("lknewmessage");
    }
    else if (pagename.toLowerCase() == "groups") {
        menu = document.getElementById("lkgroups");
    }
    else if (pagename.toLowerCase() == "history") {
        menu = document.getElementById("lkhistory");
    }
    else if (pagename.toLowerCase() == "create") {
        menu = document.getElementById("lkcreatecallout");
    }
    else if (pagename.toLowerCase() == "callouts") {
        menu = document.getElementById("lkcallouts");
    }
    else if (pagename.toLowerCase() == "approval") {
        menu = document.getElementById("lkapproval");
    }
    else if (pagename.toLowerCase() == "reports") {
        menu = document.getElementById("lkreports");
    }
    else {
        menu = document.getElementById("home");
    }
    
    
    menu.setAttribute("class", "active");
}

