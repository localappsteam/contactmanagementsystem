﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Security.Principal;

namespace Contact
{
    public partial class emergency_contacts: System.Web.UI.Page
    {
        public override void VerifyRenderingInServerForm(Control control)
        {
            //Confirms that an HtmlForm control is rendered for the
            //specified ASP.NET server control at run time.
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int RoleID = 30;
            int AdminRoleID = 3;

            access obj = new access();
            if (obj.checkRoles(UserName, RoleID, AdminRoleID))
            {
                lkAddEmergency.Visible = true;
            }
            else
            {
                lkAddEmergency.Visible = false;
            }

        }

        protected void gridEmergency_DataBound(object sender, EventArgs e)
        {
            gridEmergency.SelectedIndex = -1;
        }

        void exportToExcel()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=Emergency Contacts.xls");
            gridEmergency.GridLines = GridLines.Both;
            gridEmergency.HeaderStyle.Font.Bold = true;
            gridEmergency.HeaderStyle.ForeColor = Color.Black;
            gridEmergency.HeaderStyle.BackColor = Color.Silver;
            gridEmergency.HeaderStyle.Font.Underline = true;

            gridEmergency.AllowPaging = false;
            gridEmergency.AllowSorting = false;
            gridEmergency.DataBind();
            gridEmergency.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }

        void releaseObject(object obj)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            exportToExcel();
        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.CommandTimeout = 0;
        }

        protected void SqlDataSource1_Selected(object sender, SqlDataSourceStatusEventArgs e)
        {
            lblRows.Text = e.AffectedRows.ToString();
        }

        void viewContractor()
        {
            Session["SelectedEmergencyID"] = gridEmergency.SelectedRow.Cells[1].Text.Trim();
            Response.Redirect("emergency-search-detailed.aspx");
        }

        protected void gridEmergency_SelectedIndexChanged(object sender, EventArgs e)
        {
            viewContractor();
        }
    }
}