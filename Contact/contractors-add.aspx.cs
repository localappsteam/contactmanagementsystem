﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Principal;

namespace Contact
{
    public partial class contractors_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int RoleID = 18;
            int AdminRoleID = 3;

            access obj = new access();
            if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
            {
                Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                Response.Redirect("access-denied.aspx");
            }

        }

        protected void btnAdd1_Click(object sender, EventArgs e)
        {
            submit();
        }
        void submit()
        {
            if(txtSurname.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Surname required!");
            }
            else if (txtFirstnames.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Firstname(s) required!");
            }
            else if (txtCellphone.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Cellphone number required!");
            }
            else if (txtCompany.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Company name required!");
            }
            else if (txtTitle.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Job title required!");
            }
            else
            {
                if(Session["MillID"] != null)
                {
                    try
                    {
                        WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String UserName = UserSecurityPrincipal.Name;

                        access obj = new access();
                        obj.addNewContractor(UserName, txtSurname.Text.Trim(), txtFirstnames.Text.Trim(),
                            txtInitials.Text.Trim(), txtEmployeeNumber.Text.Trim(), txtHomeAddress.Text.Trim(),
                            txtCellphone.Text.Trim(), txtUsername.Text.Trim(), txtExt.Text.Trim(),txtSpeedDial.Text.Trim(), txtWorkPhone.Text.Trim(),
                            txtTitle.Text.Trim(), txtCompany.Text.Trim(), txtManager.Text.Trim(), txtEmail.Text.Trim(), Convert.ToInt32(Session["MillID"]));

                        clearFields();
                        ShowAlert(1, "Contractor added successfully.");

                            
                    }
                    catch (Exception ex)
                    {

                        ShowAlert(2, ex.Message);
                    }
                }
            }

        }

        void clearFields()
        {
            txtSurname.Text = ""; txtFirstnames.Text = "";
            txtInitials.Text = ""; txtEmployeeNumber.Text = ""; txtHomeAddress.Text = "";
            txtCellphone.Text = ""; txtUsername.Text = ""; txtExt.Text = ""; txtWorkPhone.Text = "";
            txtTitle.Text = ""; txtCompany.Text = ""; txtManager.Text = ""; txtEmail.Text = "";
        }


        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }
}