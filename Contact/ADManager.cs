﻿using System;
using System.DirectoryServices.ActiveDirectory;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Protocols;
using Microsoft.ActiveDirectory.Management;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Security.Principal;



namespace Contact
{
    public class ADManager
    {
      
        public ADManager()
        {

        }
        
        public String getUserLocation(String UserName)
        {
            try
            {
                DirectorySearcher search = new DirectorySearcher();
                search.Filter = String.Format("(SAMAccountName={0})", UserName);
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                // String rr = "LDAP://CN=Dlamini\, Lunga(ADM),OU=Administrator Users,OU=Privileged Users,OU=Ngodwana,DC=ZA,DC=SAPPI,DC=com";

                String location = result.Path;
                location = location.Substring(location.LastIndexOf("OU=") + 3);
                location = location.Substring(0, location.IndexOf(",")).Trim();

                return location;
            }
            catch (Exception)
            {
                return "";
            }
            
        }


        public String GetCellphoneFromAD(String email, String office)
        {
            String value = "";

            try
            {
                SearchResultCollection results;
                DirectorySearcher ds = null;
                DirectoryEntry de = new DirectoryEntry("LDAP://ZA.SAPPI.com/OU=" + office + ",DC=ZA,DC=SAPPI,DC=com", @"SAPPISA\ADM-LDLAMINI", "Happy@083913");

                ds = new DirectorySearcher(de);
                ds.Filter = "(mail=" + email + ")";

                results = ds.FindAll();

                foreach (SearchResult sr in results)
                {
                    value = sr.Properties["mobile"][0].ToString();
                }
            }
            catch(Exception)
            {
            }

            return value;
        }
        
    }
}