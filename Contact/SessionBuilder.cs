﻿using System;
using System.Data;
using System.Web;
using System.Security.Principal;


namespace Contact
{
    public class SessionBuilder
    {
        public SessionBuilder()
        {

            WindowsIdentity UserSecurityPrincipal = System.Web.HttpContext.Current.Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            UserName = UserName.Substring(UserName.LastIndexOf(@"\") + 1).Trim();

            ADManager ad = new ADManager();
            String office = ad.getUserLocation(UserName);

            if (office != String.Empty)
            {
                if (System.Web.HttpContext.Current.Session["SelectedOffice"] == null)
                {
                    access obj = new access();
                    DataSet ds = obj.GetMill(office);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        System.Web.HttpContext.Current.Session["SelectedOffice"] = office;
                        System.Web.HttpContext.Current.Session["MillID"] = ds.Tables[0].Rows[0]["MillID"].ToString();
                        System.Web.HttpContext.Current.Session["UserName"] = UserSecurityPrincipal.Name;
                    }
                }
            }
        }
    }
}