﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Principal;

namespace Contact
{
    public partial class employees_search_detailed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
            loadData();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int RoleID = 17;
            int AdminRoleID = 3;

            access obj = new access();
            if (obj.checkRoles(UserName, RoleID, AdminRoleID))
            {
                divOptions.Visible = true;
            }
            else
            {
                divOptions.Visible = false;
                divEdit.Visible = false;
                divEdit2.Visible = false;
            }
        }

        void loadData()
        {
            if(Session["SelectedEmployeeID"] != null)
            {
                if (divEdit.Visible == false)
                {
                    access obj = new access();
                    DataSet ds = obj.getSelectedEmployeeDetails(Session["SelectedEmployeeID"].ToString());

                    txtSurname.Text = ds.Tables[0].Rows[0]["Surname"].ToString();
                    txtFirstnames.Text = ds.Tables[0].Rows[0]["Firstnames"].ToString();
                    txtNickname.Text = ds.Tables[0].Rows[0]["Nickname"].ToString();
                    txtUsername.Text = ds.Tables[0].Rows[0]["ADUsername"].ToString();
                    txtEmail.Text = ds.Tables[0].Rows[0]["EMail"].ToString();
                    txtCellphone.Text = ds.Tables[0].Rows[0]["Cell"].ToString();
                    txtWorkPhone.Text = ds.Tables[0].Rows[0]["Phone"].ToString();
                    txtExt.Text = ds.Tables[0].Rows[0]["Ext"].ToString();
                    txtSpeedDial.Text = ds.Tables[0].Rows[0]["SpeedDail"].ToString();
                    txtHomeAddress.Text = ds.Tables[0].Rows[0]["EmpAddress"].ToString();
                    txtCompany.Text = ds.Tables[0].Rows[0]["Company"].ToString();
                    txtTitle.Text = ds.Tables[0].Rows[0]["JobTitle"].ToString();
                    txtEmployeeNumber.Text = ds.Tables[0].Rows[0]["EmployeeNumber"].ToString();
                    txtDepartment.Text = ds.Tables[0].Rows[0]["Department"].ToString();
                    txtOffice.Text = ds.Tables[0].Rows[0]["Office"].ToString();
                    txtSection.Text = ds.Tables[0].Rows[0]["Section"].ToString();
                    txtSite.Text = ds.Tables[0].Rows[0]["Site"].ToString();
                    txtManager.Text = ds.Tables[0].Rows[0]["ManagerName"].ToString();
                }

            }
            else
            {
                Response.Redirect("employees-search.aspx");
            }
        }

        protected void btnViewEdit_Click(object sender, EventArgs e)
        {
            enableEditing();
        }

        void enableEditing()
        {
            divEdit.Visible = true;
            divEdit2.Visible = true;

            txtSurname.Enabled = true; txtSurname.CssClass = "form-control";
            txtFirstnames.Enabled = true; txtFirstnames.CssClass = "form-control";
            txtNickname.Enabled = true; txtNickname.CssClass = "form-control";
            txtEmail.Enabled = true; txtEmail.CssClass = "form-control";
            txtCellphone.Enabled = true; txtCellphone.CssClass = "form-control";
            txtWorkPhone.Enabled = true; txtWorkPhone.CssClass = "form-control";
            txtExt.Enabled = true; txtExt.CssClass = "form-control";
            txtSpeedDial.Enabled = true; txtSpeedDial.CssClass = "form-control";
            txtHomeAddress.Enabled = true; txtHomeAddress.CssClass = "form-control";
            txtManager.Enabled = true; txtManager.CssClass = "form-control";
        }

        void disableEditing()
        {
            divEdit.Visible = false;
            divEdit2.Visible = false;

            txtSurname.Enabled = false; txtSurname.CssClass = "form-control input-disabled";
            txtFirstnames.Enabled = false; txtFirstnames.CssClass = "form-control input-disabled";
            txtNickname.Enabled = false; txtNickname.CssClass = "form-control input-disabled";
            txtEmail.Enabled = false; txtEmail.CssClass = "form-control input-disabled";
            txtCellphone.Enabled = false; txtCellphone.CssClass = "form-control input-disabled";
            txtWorkPhone.Enabled = false; txtWorkPhone.CssClass = "form-control input-disabled";
            txtExt.Enabled = false; txtExt.CssClass = "form-control input-disabled";
            txtSpeedDial.Enabled = false; txtSpeedDial.CssClass = "form-control input-disabled";
            txtHomeAddress.Enabled = false; txtHomeAddress.CssClass = "form-control input-disabled";
            txtManager.Enabled = false; txtManager.CssClass = "form-control input-disabled";
        }

        protected void btnCancel1_Click(object sender, EventArgs e)
        {
            disableEditing();
        }

        protected void btnSaveChanges1_Click(object sender, EventArgs e)
        {
            saveChanges();
        }
        void saveChanges()
        {
            if (Session["SelectedEmployeeID"] != null)
            {

                if (txtSurname.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Surname required!");
                }
                else if (txtFirstnames.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Firstname(s) required!");
                }
                else if (txtEmail.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Email required!");
                }
                else if (txtCellphone.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Cellphone number required!");
                }
                else if (txtManager.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Manager required!");
                }
                else
                {
                    try
                    {
                        WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String UserName = UserSecurityPrincipal.Name;

                        access obj = new access();
                        obj.editEmployeeDetails(UserName, Convert.ToInt32(Session["SelectedEmployeeID"]), txtSurname.Text.Trim(), txtFirstnames.Text.Trim()
                            , txtNickname.Text.Trim(), txtEmail.Text.Trim(), txtCellphone.Text.Trim(),
                            txtWorkPhone.Text.Trim(), txtExt.Text.Trim(), txtSpeedDial.Text.Trim(), txtHomeAddress.Text.Trim(), txtManager.Text.Trim());

                        ShowAlert(1, "Changes saved successfully.");
                        disableEditing();
                    }
                    catch (Exception ex)
                    {

                        ShowAlert(2, ex.Message);
                    }
                }
            }
            else
            {
                Response.Redirect("employees-search.aspx");
            }

        }


        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        
    }
}