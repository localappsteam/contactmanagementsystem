﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true"  Language="C#" MasterPageFile="~/index.Master" AutoEventWireup="true" CodeBehind="departments-view-employees.aspx.cs" Inherits="Contact.departments_view_employees" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Departments / Employees</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2><asp:Label ID="lblSelectedDepartment" runat="server" Text=""></asp:Label> : Employees</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-2 btn-fix-width">
                                <a href="departments.aspx" class="btn btn-default btn-sm btn-block">
                                    <p class="fa fa-reply m-r-10"></p>
                                    Go back</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                 <asp:TextBox ID="txtSearch" CssClass="form-control form-control-sm  m-l-10" Width="75%" Style="float:left" Placeholder="Search department..." runat="server"></asp:TextBox>
                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 cont-holder" style="padding: 0px">
                                <asp:GridView ID="gridEmployees" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                                    <AlternatingRowStyle BackColor="#f5f5f5" />
                                    
                                    <RowStyle Height="41px"></RowStyle>
                                    <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                    <Columns>
                                        <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" />
                                        <asp:BoundField DataField="Firstnames" HeaderText="Firstnames" SortExpression="Firstnames" ReadOnly="True" />
                                         <asp:BoundField DataField="JobTitle" HeaderText="JobTitle" SortExpression="JobTitle" />
                                         <asp:BoundField DataField="EMail" HeaderText="EMail" SortExpression="EMail" />
                                         <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                                         <asp:BoundField DataField="Ext" HeaderText="Ext" SortExpression="Ext" />
                                         <asp:BoundField DataField="Cell" HeaderText="Cell" SortExpression="Cell" />
                                         <asp:BoundField DataField="ManagerName" HeaderText="ManagerName" SortExpression="ManagerName" />
                                    </Columns>
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Departments_View_Employees" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="Mill" SessionField="SelectedOffice" Type="String" />
                                        <asp:SessionParameter Name="Department" SessionField="SelectedDepartment" Type="String" />
                                        <asp:ControlParameter ControlID="txtSearch" DefaultValue=" " Name="Searchvalue" PropertyName="Text" Type="String" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
