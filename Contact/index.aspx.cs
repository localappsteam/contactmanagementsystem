﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Principal;

namespace Contact
{
    public partial class index1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            applyRoles();
        }

        void applyRoles()
        {

            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            access obj = new access();
            DataSet ds = obj.getUserRolesAssigned(UserName, 1);

            divSystemUsers.Visible = false;

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToInt32(ds.Tables[0].Rows[i]["SystemRoleID"]) == 3) //Administrator
                {
                    divSystemUsers.Visible = true;
                    break;
                }

            }

        }
    }
}