﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.standby
{
    public partial class duties : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 13;
                int AdminRoleID = 9;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void btnAddDuty_Click(object sender, EventArgs e)
        {
            Response.Redirect("duties-add.aspx");
        }

        protected void gridDuties_SelectedIndexChanged(object sender, EventArgs e)
        {
            editDuty();
        }

        void editDuty()
        {
            Session["SelectedStandbyDutyID"] = gridDuties.SelectedRow.Cells[1].Text.Trim();
            Response.Redirect("duties-edit.aspx");
        }
    }
}