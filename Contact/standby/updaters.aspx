﻿<%@ Page Title="" Language="C#" MasterPageFile="~/standby/standby.Master" AutoEventWireup="true" CodeBehind="updaters.aspx.cs" Inherits="Contact.standby.updaters" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Standby / Updaters</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Standby List Updaters</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-10">
                                        <p>
                                            To update information on the system, please contact the relevant person on the contact list below.
                                            <asp:LinkButton ID="btnAddUpdater" CssClass="btn btn-primary btn-sm right" runat="server" OnClick="btnAddUpdater_Click"><small class="fa fa-plus icon-mr"></small>Add new updater</asp:LinkButton>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 cont-holder" style="overflow:auto">
                                        <asp:GridView ID="gridUpdaters" Width="100%" CssClass="table" Font-Size="12px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AutoGenerateColumns="False" DataSourceID="SqlDataSource6" OnSelectedIndexChanged="gridUpdaters_SelectedIndexChanged">
                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                            
                                            <RowStyle Height="41px"></RowStyle>
                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" ControlStyle-CssClass="btn btn-block btn-xs btn-default" SelectText="Edit" ControlStyle-Font-Size="11px" ButtonType="Button" />
                                                <asp:BoundField DataField="#" HeaderText="#" ReadOnly="True" SortExpression="#" HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="1px"/>
                                                <asp:BoundField DataField="Employee" HeaderText="Employee" ReadOnly="True" SortExpression="Employee" />
                                                <asp:BoundField DataField="Ext" HeaderText="Ext" SortExpression="Ext" ReadOnly="True" />
                                                <asp:BoundField DataField="Cell" HeaderText="Cell" SortExpression="Cell" ReadOnly="True" />
                                                <asp:BoundField DataField="EMail" HeaderText="EMail" SortExpression="EMail" ReadOnly="True" />
                                                <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" ReadOnly="True" />
                                                <asp:BoundField DataField="Section" HeaderText="Section" SortExpression="Section" ReadOnly="True" />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Standby_ViewUpdaters" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
