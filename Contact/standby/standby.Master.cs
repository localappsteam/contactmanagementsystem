﻿using System;
using System.Data;
using System.Web;
using System.Security.Principal;

namespace Contact.standby
{
    public partial class standby : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionBuilder start = new SessionBuilder();
            AuthenticateUser();
            loadUserProfile();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int AdminRoleID = 9;
            access obj = new access();

            if (!obj.checkRoles(UserName, 10, AdminRoleID)) //Standby List Update
            {
                lkupdatelist.Visible = false;
            }
            else
            {
                lkupdatelist.Visible = true;
            }

            obj = new access();
            if (!obj.checkRoles(UserName, 12, AdminRoleID)) //Sections
            {
                lksections.Visible = false;
            }
            else
            {
                lksections.Visible = true;
            }

            obj = new access();
            if (!obj.checkRoles(UserName, 13, AdminRoleID)) //Duties
            {
                lkduties.Visible = false;
            }
            else
            {
                lkduties.Visible = true;
            }

            obj = new access();
            if (!obj.checkRoles(UserName, 0, AdminRoleID)) //System users
            {
                useraccounts.Visible = false;
            }
            else
            {
                useraccounts.Visible = true;
            }
        }

        void loadUserProfile()
        {
            if (Session["MillID"] != null)
            {

                WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String UserName = UserSecurityPrincipal.Name;


                lblSelectedOffice.Text = Session["SelectedOffice"].ToString();
                lblUsername.Text = UserName;

                access obj = new access();
                DataSet ds = obj.getUserLogged(UserName);
                String employeeID = "0";

                if (ds.Tables[0].Rows.Count > 0)
                {
                    employeeID = ds.Tables[0].Rows[0]["EmployeeID"].ToString();
                }

                Session["EmployeeID"] = employeeID;
                Session["UserName"] = UserName;

                ADManager ad = new ADManager();
                UserName = UserName.Substring(UserName.LastIndexOf(@"\") + 1).Trim();
                lblUserOffice.Text = ad.getUserLocation(UserName);

            }
        }
    }
}