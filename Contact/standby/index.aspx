﻿<%@ Page Title="" Language="C#" MasterPageFile="~/standby/standby.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Contact.standby.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Standby</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Dashboard</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row text-center">
                            <div class="col-md-12 text-center" style="padding-right: 0px">
                                <div class="menu-circle-container">
                                    <a href="list.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/Actions-view-calendar-list-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Standby list</p>
                                        </div>
                                    </a>
                                </div>
                                <div id="divUpdateList" runat="server" class="menu-circle-container">
                                    <a href="update-list.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/document-edit-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Update list</p>
                                        </div>
                                    </a>
                                </div>
                                <div id="divUpdaters" runat="server" class="menu-circle-container">
                                    <a href="updaters.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/Supervisor-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Updaters</p>
                                        </div>
                                    </a>
                                </div>
                                <div id="divSections" runat="server" class="menu-circle-container">
                                    <a href="sections.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/companies-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Sections</p>
                                        </div>
                                    </a>
                                </div>
                                <div id="divDuties" runat="server" class="menu-circle-container">
                                    <a href="duties.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/document-construction-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">Duties</p>
                                        </div>
                                    </a>
                                </div>
                                <div id="divSystemUsers" runat="server" class="menu-circle-container">
                                    <a href="users.aspx" class="dash-link">
                                        <div class="menu-circle menu-circle-sm text-center">
                                            <img src="../images/user-group-icon.png" class="img-menu" />
                                            <p class="text-center m-t-10">System Users</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
