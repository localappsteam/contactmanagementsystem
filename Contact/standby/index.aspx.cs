﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Principal;

namespace Contact.standby
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int AdminRoleID = 9;
            access obj = new access();

            if (!obj.checkRoles(UserName, 10, AdminRoleID)) //Standby List Update
            {
                divUpdateList.Visible = false;
            }
            else
            {
                divUpdateList.Visible = true;
            }

            obj = new access();
            if (!obj.checkRoles(UserName, 12, AdminRoleID)) //Sections
            {
                divSections.Visible = false;
            }
            else
            {
                divSections.Visible = true;
            }

            obj = new access();
            if (!obj.checkRoles(UserName, 13, AdminRoleID)) //Duties
            {
                divDuties.Visible = false;
            }
            else
            {
                divDuties.Visible = true;
            }

            obj = new access();
            if (!obj.checkRoles(UserName, 0, AdminRoleID)) //System users
            {
                divSystemUsers.Visible = false;
            }
            else
            {
                divSystemUsers.Visible = true;
            }
        }
    }
}