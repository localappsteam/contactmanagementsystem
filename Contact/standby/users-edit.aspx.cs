﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Principal;

namespace Contact.standby
{
    public partial class users_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 0; //Admin only
                int AdminRoleID = 9;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
                else
                {
                    loadUser();
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        void loadUser()
        {
            if(Session["SelectedUserID"] != null)
            {
                lblFullname.Text = Session["SelectedFullname"].ToString();
                lblUsername.Text = Session["SelectedUsername"].ToString();
            }
            else
            {
                Response.Redirect("users.aspx");
            }
        }

        protected void btnAddRoles_Click(object sender, EventArgs e)
        {
            divAddRoles.Visible = true;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            assignRoles();
        }
        void assignRoles()
        {
            if (Session["SelectedUserID"] != null)
            {

                bool isSelected = false;

                for (int i=0; i< chkRoles.Items.Count; i++)
                {
                    if(chkRoles.Items[i].Selected)
                    {
                        isSelected = true;
                        break;
                    }
                }

                if (!isSelected)
                {
                    ShowAlert(2, "Selected roles first.");
                }
                else
                {
                    access obj;
                    
                    for (int i=0; i<chkRoles.Items.Count; i++)
                    {
                        if (chkRoles.Items[i].Selected)
                        {
                            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                            String UserName = UserSecurityPrincipal.Name;

                            obj = new access();
                            obj.assignUserRoles(UserName, Convert.ToInt32(Session["SelectedUserID"].ToString()), Convert.ToInt32(chkRoles.Items[i].Value.Trim()), 2);
                        }
                    }

                    chkRoles.DataBind();
                    gridRoles.DataBind();
                    divAddRoles.Visible = false;
                    ShowAlert(1, "Roles assigned successfully.");
                }

            }
            else
            {
                Response.Redirect("users.aspx");
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divAddRoles.Visible = false;
        }

        protected void gridRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            removeRoles();
        }

        void removeRoles()
        {
            if (Session["SelectedUserID"] != null)
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;

                access obj = new access();
                obj.removeUserRoles(SystemUserName, Convert.ToInt64(gridRoles.SelectedRow.Cells[1].Text.Trim()));

                chkRoles.DataBind();
                gridRoles.DataBind();
            }
            else
            {
                Response.Redirect("users.aspx");
            }
        }

        protected void btnDeleteUser_Click(object sender, EventArgs e)
        {
            divDelete.Visible = true;
        }

        protected void btnDeleteYes_Click(object sender, EventArgs e)
        {
            deleteAccount();
        }

        void deleteAccount()
        {
            if (Session["SelectedUserID"] != null)
            {
                try
                {
                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    access obj = new access();
                    obj.deleteUser(SystemUserName, Convert.ToInt32(Session["SelectedUserID"].ToString()), 2);
                    divDelete.Visible = false;
                    Session.Remove("SelectedUserID");

                    ShowAlert(1, "User account deleted successfully");
                }
                catch (Exception ex)
                {
                    ShowAlert(2, ex.Message);
                }
                
            }
            else
            {
                Response.Redirect("users.aspx");
            }
        }

        protected void btnDeleteNo_Click(object sender, EventArgs e)
        {
            divDelete.Visible = false;
        }

        protected void btnEditUsername_Click(object sender, EventArgs e)
        {
            viewEditUsername();
        }

        void viewEditUsername()
        {
            String un = lblUsername.Text;
            un = un.Substring(un.IndexOf(@"\") + 1);

            txtEditUsername.Text = un;

            lblUsername.Visible = false;
            btnEditUsername.Visible = false;

            divEditUsername.Visible = true;

        }

        protected void btnSaveUsername_Click(object sender, EventArgs e)
        {
            updateUsername();
        }

        protected void btnCancelUsername_Click(object sender, EventArgs e)
        {
            divEditUsername.Visible = false;
            lblUsername.Visible = true;
            btnEditUsername.Visible = true;
        }

        void updateUsername()
        {

            if (Session["SelectedUserID"] != null)
            {
                try
                {
                    if (txtEditUsername.Text.Trim() == String.Empty)
                    {
                        ShowAlert(2, "Username is required!");
                    }
                    else
                    {
                        System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String SystemUserName = UserSecurityPrincipal.Name;

                        access obj = new access();
                        obj.updateUsername(SystemUserName, Convert.ToInt32(Session["SelectedUserID"].ToString()), txtEditUsername.Text.Trim(), 4);

                        Session.Remove("SelectedUserID");
                        ShowAlert(1, "Username updated successfully.");
                    }
                }
                catch (Exception ex)
                {

                    ShowAlert(2, ex.Message);
                }

            }
            else
            {
                Response.Redirect("users.aspx");
            }


        }
        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["SelectedUserID"] == null)
            {
                Response.Redirect("users.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        
    }
}