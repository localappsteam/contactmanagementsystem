﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.standby
{
    public partial class updaters_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 11;
                int AdminRoleID = 9;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            submit();
        }

        void submit()
        {
            if(gridEveryone.SelectedIndex < 0)
            {
                ShowAlert(2, "Select a person first.");
            }
            else if(drdDepartment.SelectedIndex < 1)
            {
                ShowAlert(2, "Select a department first.");
            }
            else
            {
                try
                {
                    Int64 EmployeeID = Convert.ToInt64(gridEveryone.SelectedRow.Cells[1].Text.Trim());
                    String Emptype = gridEveryone.SelectedRow.Cells[6].Text.Trim();
                    int DepartmentID = Convert.ToInt32(drdDepartment.SelectedValue.Trim());

                    access obj = new access();

                    if (obj.checkIfStandbyUpdaterExist(EmployeeID, Emptype))
                    {
                        ShowAlert(2, "This person already exist on the list of updaters.");
                    }
                    else
                    {
                        System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String SystemUserName = UserSecurityPrincipal.Name;

                        obj = new access();
                        obj.addNewStandbyUpdater(SystemUserName, EmployeeID, Emptype, DepartmentID);

                        txtSearch.Text = "";
                        gridEveryone.DataBind();
                        gridEveryone.SelectedIndex = -1;
                        drdDepartment.SelectedIndex = 0;

                        ShowAlert(1, "Standby list updater added successfully");
                    }
                }
                catch (Exception ex)
                {

                    ShowAlert(2, ex.Message);
                }
            }
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }
}