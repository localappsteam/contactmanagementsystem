﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.standby
{
    public partial class sections : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 12;
                int AdminRoleID = 9;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void btnAddSection_Click(object sender, EventArgs e)
        {
            Response.Redirect("sections-add.aspx");
        }

        protected void gridSections_SelectedIndexChanged(object sender, EventArgs e)
        {
            editSection();
        }

        void editSection()
        {
            Session["SelectedSectionID"] = gridSections.SelectedRow.Cells[1].Text.Trim();
            Session["SelectedSectionSortingOrder"] = gridSections.SelectedRow.Cells[2].Text.Trim();
            Session["SelectedSectionName"] = gridSections.SelectedRow.Cells[3].Text.Trim();

            Response.Redirect("sections-edit.aspx");
        }
    }
}