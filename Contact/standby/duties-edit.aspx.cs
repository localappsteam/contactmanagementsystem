﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Contact.standby
{
    public partial class duties_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 13;
                int AdminRoleID = 9;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
                else
                {
                    loadDuty();
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        void loadDuty()
        {
            if(Session["SelectedStandbyDutyID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    access obj = new access();
                    DataSet ds = obj.getSelectStandbyDuty(Convert.ToInt32(Session["SelectedStandbyDutyID"].ToString()));

                    txtDutyNo.Text = ds.Tables[0].Rows[0]["DutyNo"].ToString();
                    txtDuty.Text = ds.Tables[0].Rows[0]["Description"].ToString();
                    txtBleeper.Text = ds.Tables[0].Rows[0]["Bleeper"].ToString();
                    drdDepartment.SelectedValue = ds.Tables[0].Rows[0]["DepartmentID"].ToString();
                    txtStandbyCell.Text = ds.Tables[0].Rows[0]["DutyContactCellphone"].ToString();
                    rdActiveOnList.SelectedValue = ds.Tables[0].Rows[0]["isActiveOnTemplate"].ToString();
                    rdShowOfficeTelephone.SelectedValue = ds.Tables[0].Rows[0]["ShowOfficeExtOnList"].ToString();

                }
            }
            else
            {
                Response.Redirect("duties.aspx");
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            updateDuty();
        }

        void updateDuty()
        {
            if (Session["SelectedStandbyDutyID"] != null)
            {
                if (txtDutyNo.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Enter a duty number first.");
                }
                else if (txtDuty.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Enter a duty first.");
                }
                else if (drdDepartment.SelectedIndex < 1)
                {
                    ShowAlert(2, "Select a department first.");
                }
                else
                {
                    try
                    {
                        int DutyID = Convert.ToInt32(Session["SelectedStandbyDutyID"].ToString());
                        String DutyNo = txtDutyNo.Text.Trim();
                        String Duty = txtDuty.Text.Trim();
                        String Bleeper = txtBleeper.Text.Trim();
                        int DepartmentID = Convert.ToInt32(drdDepartment.SelectedValue.Trim());
                        String StandbyCell = txtStandbyCell.Text.Trim();
                        int ActiveOnList = Convert.ToInt32(rdActiveOnList.SelectedValue.Trim());
                        int ShowTelephoneNumber = Convert.ToInt32(rdShowOfficeTelephone.SelectedValue.Trim());

                        System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String SystemUserName = UserSecurityPrincipal.Name;

                        access obj = new access();
                        obj.editStandbyDuty(SystemUserName, DutyID, DutyNo, Duty, Bleeper, DepartmentID, StandbyCell, ShowTelephoneNumber, ActiveOnList);

                        ShowAlert(1, "Standby duty updated successfully.");
                    }
                    catch (Exception ex)
                    {

                        ShowAlert(2, ex.Message);
                    }
                    
                }
            }
            else
            {
                Response.Redirect("duties.aspx");
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            divDelete.Visible = true;
        }
        protected void btnDeleteYes_Click(object sender, EventArgs e)
        {
            deleteDuty();
        }

        void deleteDuty()
        {
            if (Session["SelectedStandbyDutyID"] != null)
            {
                try
                {
                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    access obj = new access();
                    obj.deleteStandbyDuty(SystemUserName, Convert.ToInt32(Session["SelectedStandbyDutyID"].ToString()));
                    Session.Remove("SelectedStandbyDutyID");
                    ShowAlert(1, "Standby duty deleted successfully.");
                }
                catch (Exception ex)
                {
                    ShowAlert(2, ex.Message);
                }
            }
        }

        protected void btnDeleteNo_Click(object sender, EventArgs e)
        {
            divDelete.Visible = false;
        }
        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["SelectedStandbyDutyID"] == null)
            {
                Response.Redirect("duties.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        
    }
}