﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.standby
{
    public partial class updaters : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 11;
                int AdminRoleID = 9;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    btnAddUpdater.Visible = false;
                    gridUpdaters.Columns[0].Visible = false;
                }
                else
                {
                    btnAddUpdater.Visible = true;
                    gridUpdaters.Columns[0].Visible = true;
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void btnAddUpdater_Click(object sender, EventArgs e)
        {
            Response.Redirect("updaters-add.aspx");
        }

        protected void gridUpdaters_SelectedIndexChanged(object sender, EventArgs e)
        {
            editUpdater();
        }

        void editUpdater()
        {
            Session["SelectedUpdaterID"] = gridUpdaters.SelectedRow.Cells[1].Text.Trim();
            Session["SelectedUpdaterName"] = gridUpdaters.SelectedRow.Cells[2].Text.Trim();
            Session["SelectedUpdaterSection"] = gridUpdaters.SelectedRow.Cells[7].Text.Trim();

            Response.Redirect("updaters-edit.aspx");
        }
    }
}