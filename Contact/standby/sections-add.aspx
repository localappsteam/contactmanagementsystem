﻿<%@ Page Title="" Language="C#" MasterPageFile="~/standby/standby.Master" AutoEventWireup="true" CodeBehind="sections-add.aspx.cs" Inherits="Contact.standby.sections_add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Standby / Sections / Add</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add Section</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-5">
                                        <p>Enter a section name<span class="text-danger icon-ml">*</span></p>
                                        <asp:TextBox ID="txtSection" CssClass="form-control" placeholder="Section name..." runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-md-5">
                                        <p>Enter a position for sorting<span class="text-danger icon-ml">*</span></p>
                                        <asp:TextBox ID="txtSortOrder" TextMode="Number" CssClass="form-control" placeholder="Sorting order..." runat="server"></asp:TextBox>
                                        <small>NB: This is for determining the position for the section to appear on the standby list.<br />Eg when you enter 1, the section will appear first and all the employees on that sections, and when you enter 10 it will appear last on the list.</small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 btn-fix-width">
                                        <asp:LinkButton ID="btnSubmit" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnSubmit_Click"><p class="fa fa-save icon-mr"></p>Submit</asp:LinkButton>
                                    </div>
                                    <div class="col-md-2 btn-fix-width">
                                       <a href="sections.aspx" class="btn btn-default btn-sm btn-block"><p class="fa fa-times icon-mr"></p>Close</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click" ><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
