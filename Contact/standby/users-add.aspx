﻿<%@ Page Title="" Language="C#" MasterPageFile="~/standby/standby.Master" AutoEventWireup="true" CodeBehind="users-add.aspx.cs" Inherits="Contact.standby.users_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Standby / Users / Add</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add New User</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row">
                                    <div class="col-md-6">
                                       <p>Search for the person to add below</p>
                                <asp:TextBox ID="txtSearch" CssClass="form-control form-control-sm" Width="75%" Style="float: left" Placeholder="Search employee or contractor..." runat="server"></asp:TextBox>
                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server" OnClick="btnSearch_Click">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                            </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 cont-holder" style="overflow:auto">
                                        <asp:GridView ID="gridEveryone" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None"  runat="server" EmptyDataText="No data found." AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataSourceID="SqlDataSource2">
                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                            
                                            <RowStyle Height="41px"></RowStyle>
                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" ControlStyle-CssClass="btn-block" SelectText="Select" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                                <asp:BoundField DataField="#" HeaderText="#" ReadOnly="True" SortExpression="#"  HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="1px"/>
                                                <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" ReadOnly="True" />
                                                <asp:BoundField DataField="Firstnames" HeaderText="Firstnames" SortExpression="Firstnames" ReadOnly="True" />
                                                <asp:BoundField DataField="ADUserName" HeaderText="ADUserName" SortExpression="ADUserName" ReadOnly="True" />
                                                <asp:BoundField DataField="CompanyNo#" HeaderText="CompanyNo#" SortExpression="CompanyNo#" ReadOnly="True" />
                                                <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" ReadOnly="True" />
                                                <asp:BoundField DataField="EMail" HeaderText="EMail" SortExpression="EMail" ReadOnly="True" />
                                                <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" ReadOnly="True" />
                                                <asp:BoundField DataField="EmpType" HeaderText="EmpType" SortExpression="EmpType" ReadOnly="True" />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_System_Users_ViewEmployees" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                <asp:ControlParameter ControlID="txtSearch" DefaultValue="" Name="SearchValue" PropertyName="Text" Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        Enter the person's user name
                                       <asp:TextBox ID="txtUsername" CssClass="form-control" Placeholder="Enter user name...." runat="server"></asp:TextBox>
                                     </div>
                                    <div class="col-md-10">
                                        <hr />
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-md-2 btn-fix-width">
                                         <asp:LinkButton ID="btnSubmit" CssClass="btn btn-primary btn-sm btn-block" runat="server" OnClick="btnSubmit_Click"><p class="fa fa-save icon-mr"></p>Submit</asp:LinkButton>
                                     </div>
                                    <div class="col-md-2 btn-fix-width">
                                        <a href="users.aspx" class="btn btn-default btn-sm btn-block">
                                            <p class="fa fa-times icon-mr"></p>
                                            Close</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click" ><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
