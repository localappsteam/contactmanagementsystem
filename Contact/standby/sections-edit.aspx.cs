﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.standby
{
    public partial class sections_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 12;
                int AdminRoleID = 9;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
                else
                {
                    loadSection();
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        void loadSection()
        {
            if(Session["SelectedSectionID"] != null)
            {
                if(!Page.IsPostBack)
                {
                    txtSectionName.Text = Session["SelectedSectionName"].ToString().Trim();
                    txtSortingOrder.Text = Session["SelectedSectionSortingOrder"].ToString().Trim();
                }
               
            }
            else
            {
                Response.Redirect("sections.aspx");
            }
            
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            divDelete.Visible = true;
        }

        protected void btnDeleteYes_Click(object sender, EventArgs e)
        {
            deleteSection();
        }

        protected void btnDeleteNo_Click(object sender, EventArgs e)
        {
            divDelete.Visible = false;
        }
        void deleteSection()
        {
            try
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;

                access obj = new access();
                obj.deleteStandbySection(SystemUserName, Convert.ToInt32(Session["SelectedSectionID"].ToString()));
                Session.Remove("SelectedSectionID");
                ShowAlert(1, "Section deleted successfully.");
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            updateSection();
        }

        void updateSection()
        {
            if (Session["SelectedSectionID"] != null)
            {
                if(txtSectionName.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "A section name is required.");
                }
                else if (txtSortingOrder.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "A sorting order is required.");
                }
                else
                {

                    int SectionID = Convert.ToInt32(Session["SelectedSectionID"].ToString());
                    String Description = txtSectionName.Text.Trim();
                    int SortingOrder = Convert.ToInt32(txtSortingOrder.Text.Trim());

                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    access obj = new access();
                    obj.editStandbySection(SystemUserName, SectionID, Description, SortingOrder);

                    Session["SelectedSectionName"] = txtSectionName.Text.Trim();
                    Session["SelectedSectionSortingOrder"] = txtSortingOrder.Text.Trim();

                    ShowAlert(1, "Standby section updated successfully.");
                }
            }
            else
            {
                Response.Redirect("sections.aspx");
            }
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if(Session["SelectedSectionID"] == null)
            {
                Response.Redirect("sections.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        
    }
}