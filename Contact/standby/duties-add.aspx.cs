﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace Contact.standby
{
    public partial class duties_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 13;
                int AdminRoleID = 9;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            addDuty();
        }

        void addDuty()
        {
            try
            {
                if (txtDutyNo.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Enter a duty number first.");
                }
                else if (txtDuty.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Enter a duty first.");
                }
                else if (drdDepartment.SelectedIndex < 1)
                {
                    ShowAlert(2, "Select a department first.");
                }
                else
                {
                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    access obj = new access();
                    obj.addStandbyDuty(SystemUserName, txtDutyNo.Text.Trim(), txtDuty.Text.Trim(), txtBleeper.Text.Trim(), Convert.ToInt32(drdDepartment.SelectedValue.Trim()), txtStandbyCell.Text.Trim(), Convert.ToInt32(rdShowOfficeTelephone.SelectedValue.Trim()), Convert.ToInt32(rdActiveOnList.SelectedValue.Trim()));

                    txtDuty.Text = String.Empty;
                    txtDutyNo.Text = String.Empty;
                    txtBleeper.Text = String.Empty;
                    txtStandbyCell.Text = String.Empty;
                    drdDepartment.SelectedIndex = 0;

                    ShowAlert(1, "Standby duty added successfully.");
                }
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }
}