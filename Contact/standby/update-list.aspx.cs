﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Contact.standby
{
    public partial class update_list : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 10;
                int AdminRoleID = 9;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        protected void drdDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            hideAllDivs();
            if(drdDepartment.SelectedIndex > 0)
            {
                divDuties.Visible = true;
                gridDuties.SelectedIndex = -1;
            }
            
        }
       

        protected void gridDuties_SelectedIndexChanged(object sender, EventArgs e)
        {
            getStandbylist();
        }

        void getStandbylist()
        {
            hideAllDivs();
            divStandbyHeaders.Visible = true;
            divStandbyList.Visible = true;
            gridPersons.SelectedIndex = -1;

            lblSelectedDepartment.Text = drdDepartment.SelectedItem.Text.Trim();
            lblSelectedDutyNo.Text = gridDuties.SelectedRow.Cells[2].Text.Trim();
            lblSelectedDuty.Text = gridDuties.SelectedRow.Cells[3].Text.Trim();
        }

        void hideAllDivs()
        {
            divDuties.Visible = false;
            divStandbyHeaders.Visible = false;
            divStandbyList.Visible = false;
            divAddStandby.Visible = false;
            divEditStandby.Visible = false;
            divSwap.Visible = false;
        }
        
        protected void btnAddStandby_Click(object sender, EventArgs e)
        {
            hideAllDivs();
            divStandbyHeaders.Visible = true;
            divAddStandby.Visible = true;
            txtSearch.Text = "";
        }

        protected void btnViewDuties_Click(object sender, EventArgs e)
        {
            hideAllDivs();
            if (drdDepartment.SelectedIndex > 0)
            {
                divDuties.Visible = true;
                gridDuties.SelectedIndex = -1;
            }
        }


        protected void btnCancelAddStandby_Click(object sender, EventArgs e)
        {
            hideAllDivs();
            divStandbyHeaders.Visible = true;
            divStandbyList.Visible = true;
            gridPersons.SelectedIndex = -1;
        }

        protected void btnSubmitAddStandby_Click(object sender, EventArgs e)
        {
            submitNewStandby();
        }

        void submitNewStandby()
        {
            DateTime val;

            if (gridEveryone.SelectedIndex < 0)
            {
                ShowAlert(2, "Select a person first.");
            }
            else if (txtDates.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Select a starting date first.");
            }
            else if (txtTime.Text.Trim() == String.Empty)
            {
                ShowAlert(2, "Enter starting time first.");
            }
            else if (DateTime.TryParse(txtDates.Text.Trim(), out val) == false)
            {
                ShowAlert(2, "Enter a valid date. Format 'dd/MM/yyyy OR yyyy-MM-dd'");
            }
            else if (DateTime.TryParse(txtDates.Text.Trim() + " " + txtTime.Text.Trim(), out val) == false)
            {
                ShowAlert(2, "Enter a valid time. Format '00:00'");
            }
            else
            {
                try
                {
                    access obj = new access();

                    CultureInfo culture = new CultureInfo("en-US");

                    Boolean validSchedule = false;
                    String scheduleDescr = "";
                    String MillDescr = "";

                    String Date = "";  

                    if (txtDates.Text.Trim().Contains("/"))
                    {
                        Date = txtDates.Text.Trim();
                        Date = Date.Substring(6) + "-" + Date.Substring(3,2) + "-" + Date.Substring(0,2);
                    }
                    else
                    {
                        Date  = Convert.ToDateTime(txtDates.Text.Trim(), culture).ToString("yyyy-MM-dd");
                    }

                    String Time = txtTime.Text.Trim();
                    Int64 EmpID = Convert.ToInt64(gridEveryone.SelectedRow.Cells[1].Text.Trim());
                    String EmpType = gridEveryone.SelectedRow.Cells[6].Text.Trim();
                    int MillID = Convert.ToInt32(Session["MillID"].ToString());
                    int DutyID = Convert.ToInt32(gridDuties.SelectedRow.Cells[1].Text.Trim());
                    int Recurring = Convert.ToInt32(drdRecurring.SelectedValue.Trim());

                    System.Data.DataSet ds = obj.getStandbySchedules(Convert.ToInt32(Session["MillID"].ToString()));

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        String schedule = ds.Tables[0].Rows[i]["Schedule"].ToString().Trim();
                        String input = Convert.ToDateTime(Date, culture).DayOfWeek.ToString().ToUpper() + " " + Time.Trim();
                        MillDescr = ds.Tables[0].Rows[i]["Mill"].ToString().Trim();

                        if (i > 0)
                        {
                            scheduleDescr = scheduleDescr + "<br/>" + (i + 1).ToString() + ". " + schedule;
                        }
                        else
                        {
                            scheduleDescr = scheduleDescr + "<br/>" + "1. " + schedule;
                        }


                        if (input == schedule)
                        {
                            validSchedule = true;
                        }
                    }

                    if (!validSchedule && ds.Tables[0].Rows.Count > 0)
                    {
                        String msg = "The date and time you have entered is not valid on " + MillDescr + "'s standby list schedule. The date and time must be on one of the following:" + scheduleDescr;
                        ShowAlert(2, msg);
                    }
                    else
                    {

                        System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String SystemUserName = UserSecurityPrincipal.Name;

                        obj.addEmployeeToStandbyList(SystemUserName, EmpID, EmpType, MillID, Date, Time, DutyID, Recurring);

                        hideAllDivs();
                        gridPersons.DataBind();
                        divStandbyHeaders.Visible = true;
                        divStandbyList.Visible = true;
                        obj = new access();
                        obj.autoUpdateStandbyList();

                        drdRecurring.SelectedIndex = 0;
                        clearFields();

                        ShowAlert(1, "Standby created successfully.");
                    }
                }
                catch (Exception ex)
                {

                    ShowAlert(2, ex.Message);
                }
               
            }
        }


        void clearFields()
        {
            txtDates.Text = "";
            txtDateEdit.Text = "";
            txtTime.Text = "";
            txtTimeEdit.Text = "";
            txtSearch.Text = "";
            txtSearch2.Text = "";
            gridEveryone.DataBind();
            gridEveryone2.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            gridEveryone.SelectedIndex = -1;
        }

        protected void gridPersons_SelectedIndexChanged(object sender, EventArgs e)
        {
            viewEditStandby();
        }
        void viewEditStandby()
        {
            hideAllDivs();
            divStandbyHeaders.Visible = true;
            divEditStandby.Visible = true;

            divDeleteStandby.Visible = false;
            btnDeleteStandby.Visible = true;

            divEditReccurrence.Visible = false;
            divChangePerson.Visible = false;

            lblSelectedStandbyID.Text = gridPersons.SelectedRow.Cells[1].Text.Trim();
            lblSelectedStandbyPerson.Text = gridPersons.SelectedRow.Cells[2].Text.Trim();
            lblSelectedStandbyDate.Text = gridPersons.SelectedRow.Cells[6].Text.Trim();
            lblSelectedStandbyRecurrence.Text = gridPersons.SelectedRow.Cells[5].Text.Trim();
            
        }

        protected void btnDeleteStandby_Click(object sender, EventArgs e)
        {
            btnDeleteStandby.Visible = false;
            divDeleteStandby.Visible = true;
        }

        protected void btnYesDelete_Click(object sender, EventArgs e)
        {
            deleteStandby();
        }

        void deleteStandby()
        {
            try
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;

                access obj = new access();
                obj.deleteStandby(SystemUserName, Convert.ToInt64(gridPersons.SelectedRow.Cells[1].Text.Trim()));

                gridPersons.DataBind();

                hideAllDivs();
                divStandbyHeaders.Visible = true;
                divStandbyList.Visible = true;
                clearFields();

                ShowAlert(1, "Standby deleted successfully.");
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }

            divDeleteStandby.Visible = false;
            btnDeleteStandby.Visible = true;
        }

        protected void btnNoDelete_Click(object sender, EventArgs e)
        {
            divDeleteStandby.Visible = false;
            btnDeleteStandby.Visible = true;
        }

        protected void btnEditRecurrence_Click(object sender, EventArgs e)
        {
            divEditReccurrence.Visible = true;
        }

        protected void btnSubmitReccurreChange_Click(object sender, EventArgs e)
        {
            updateRecurrence();
        }

        void updateRecurrence()
        {
            try
            {
                System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String SystemUserName = UserSecurityPrincipal.Name;

                access obj = new access();

                Int64 StandbyID = Convert.ToInt64(gridPersons.SelectedRow.Cells[1].Text.Trim());
                int Recurrence = Convert.ToInt32(drdEditReccurrence.SelectedValue.Trim());
                obj.updateRecurrence(SystemUserName, StandbyID, Recurrence);

                gridPersons.DataBind();

                divEditReccurrence.Visible = false;
                lblSelectedStandbyRecurrence.Text = Recurrence.ToString();
                ShowAlert(1, "Update successful.");
                drdEditReccurrence.SelectedIndex = 0;
                clearFields();

            }
            catch (Exception ex)
            {
                ShowAlert(2, ex.Message);
            }
        }

        protected void btnEditStartingDate_Click(object sender, EventArgs e)
        {
            txtDateEdit.Text = Convert.ToDateTime(lblSelectedStandbyDate.Text).ToString("yyyy-MM-dd");
            txtTimeEdit.Text = Convert.ToDateTime(lblSelectedStandbyDate.Text).ToString("HH:mm");
            divEditStartingDate.Visible = true;
        }

        protected void btnCancelStartingDateEdit_Click(object sender, EventArgs e)
        {
            divEditStartingDate.Visible = false;
        }

        protected void btnSubmitStartingDateEdit_Click(object sender, EventArgs e)
        {
            updateStartingDate();
        }
        void updateStartingDate()
        {
            try
            {
                DateTime val;

                if (txtDateEdit.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Select a starting date first.");
                }
                else if (txtTimeEdit.Text.Trim() == String.Empty)
                {
                    ShowAlert(2, "Enter time first.");
                }
                else if (DateTime.TryParse(txtDateEdit.Text.Trim(), out val) == false)
                {
                    ShowAlert(2, "Enter a valid date. Format 'dd/MM/yyyy OR yyyy-MM-dd'");
                }
                else if (DateTime.TryParse(txtDateEdit.Text.Trim() + " " + txtTimeEdit.Text.Trim(), out val) == false)
                {
                    ShowAlert(2, "Enter a valid time. Format '00:00'");
                }
                else
                {
                    
                    access obj = new access();

                    CultureInfo culture = new CultureInfo("en-US");

                    Boolean validSchedule = false;
                    String scheduleDescr = "";
                    String MillDescr = "";

                    Int64 StandbyID = Convert.ToInt64(gridPersons.SelectedRow.Cells[1].Text.Trim());
                  
                    String Date = "";

                    if (txtDateEdit.Text.Trim().Contains("/"))
                    {
                        Date = txtDateEdit.Text.Trim();
                        Date = Date.Substring(6) + "-" + Date.Substring(3, 2) + "-" + Date.Substring(0, 2);
                    }
                    else
                    {
                        Date = Convert.ToDateTime(txtDateEdit.Text.Trim(), culture).ToString("yyyy-MM-dd");
                    }


                    String Time = txtTimeEdit.Text.Trim();

                    System.Data.DataSet ds = obj.getStandbySchedules(Convert.ToInt32(Session["MillID"].ToString()));

                    for (int i=0; i<ds.Tables[0].Rows.Count; i++)
                    {
                        String schedule = ds.Tables[0].Rows[i]["Schedule"].ToString().Trim();
                        String input = Convert.ToDateTime(Date, culture).DayOfWeek.ToString().ToUpper() +" "+ Time.Trim();
                        MillDescr = ds.Tables[0].Rows[i]["Mill"].ToString().Trim();

                        if (i > 0)
                        {
                            scheduleDescr = scheduleDescr + "<br/>" + (i + 1).ToString() + ". " + schedule;
                        }
                        else
                        {
                            scheduleDescr = scheduleDescr + "<br/>" + "1. " + schedule;
                        }

                        if (input == schedule)
                        {
                            validSchedule = true;
                        }
                    }

                    if (!validSchedule && ds.Tables[0].Rows.Count > 0)
                    {
                        String msg = "The date and time you have entered is not valid on "+MillDescr+"'s standby list schedule. The date and time must be on one of the following:" + scheduleDescr;
                        ShowAlert(2, msg);
                    }
                    else
                    {

                        obj = new access();
                        System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String SystemUserName = UserSecurityPrincipal.Name;

                        lblSelectedStandbyDate.Text = obj.updateStartingDate(SystemUserName, StandbyID, Date, Time);

                        gridPersons.DataBind();

                        divEditStartingDate.Visible = false;

                        obj = new access();
                        obj.autoUpdateStandbyList();
                        clearFields();

                        ShowAlert(1, "Update successful.");
                    }
                }
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
        }

        protected void btnCancelReccurreChange_Click(object sender, EventArgs e)
        {
            divEditReccurrence.Visible = false;
        }

        protected void btnChangePerson_Click(object sender, EventArgs e)
        {
            divChangePerson.Visible = true;
            divSwap.Visible = false;
        }
        protected void btnSwapPerson_Click(object sender, EventArgs e)
        {
            divSwap.Visible = true;
            divChangePerson.Visible = false;
            txtSearch2.Text = "";
            gridSwap.DataBind();
        }

        protected void btnCancelPersonChange_Click(object sender, EventArgs e)
        {
            divChangePerson.Visible = false;
            txtSearch2.Text = "";
        }
        protected void btnCancelSwap_Click(object sender, EventArgs e)
        {
            divSwap.Visible = false;
            ckApplyToUpcomingSchedules.Checked = false;
            gridSwap.SelectedIndex = -1;
        }

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            gridEveryone2.SelectedIndex = -1;
        }

        protected void btnSubmitPersonChange_Click(object sender, EventArgs e)
        {
            changeStandbyPerson();
        }

        void changeStandbyPerson()
        {
            if(gridEveryone2.SelectedIndex < 0)
            {
                ShowAlert(2, "Select a person first.");
            }
            else
            {
                try
                {
                    access obj = new access();
                    Int64 StandbyID = Convert.ToInt64(lblSelectedStandbyID.Text.Trim());
                    Int64 EmployeeID = Convert.ToInt64(gridEveryone2.SelectedRow.Cells[1].Text.Trim());
                    String EmpType = gridEveryone2.SelectedRow.Cells[5].Text.Trim();

                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    obj.changeStandbyPerson(SystemUserName, StandbyID, EmployeeID, EmpType);

                    gridPersons.DataBind();

                    hideAllDivs();
                    divStandbyHeaders.Visible = true;
                    divStandbyList.Visible = true;
                    clearFields();

                    ShowAlert(1, "Standby person changed successfully.");
                }
                catch (Exception ex)
                {

                    ShowAlert(2, ex.Message);
                }
            }
        }

        protected void btnApplySwap_Click(object sender, EventArgs e)
        {
            swapPerson();
        }
        void swapPerson()
        {
            if (gridSwap.SelectedIndex < 0)
            {
                ShowAlert(2, "Select a person to swap with first.");
            }
            else
            {
                try
                {
                    Int64 StandbyID1 = Convert.ToInt64(lblSelectedStandbyID.Text.Trim());
                    Int64 StandbyID2 = Convert.ToInt64(gridSwap.SelectedRow.Cells[1].Text.Trim());

                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    access obj = new access();
                    obj.swapStandbyPerson(SystemUserName, StandbyID1, StandbyID2, ckApplyToUpcomingSchedules.Checked ? 1 : 0);

                    gridPersons.DataBind();

                    hideAllDivs();
                    divStandbyHeaders.Visible = true;
                    divStandbyList.Visible = true;
                    gridSwap.DataBind();
                    ckApplyToUpcomingSchedules.Checked = false;
                    gridSwap.SelectedIndex = -1;
                    clearFields();

                    ShowAlert(1, "Standby person swapped successfully.");
                }
                catch (Exception ex)
                {

                    ShowAlert(2, ex.Message);
                }
            }

        }
        protected void btnCloseEdit_Click(object sender, EventArgs e)
        {
            hideAllDivs();
            divStandbyHeaders.Visible = true;
            divStandbyList.Visible = true;
        }
        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        protected void gridPersons_DataBound(object sender, EventArgs e)
        {
            for (int i=0; i<gridPersons.Rows.Count; i++)
            {
                if(gridPersons.Rows[i].Cells[7].Text.ToUpper().Trim() == "ACTIVE")
                {
                    gridPersons.Rows[i].Cells[7].Attributes.Add("style", "font-weight:bold; color:green");

                }
                else
                {
                    gridPersons.Rows[i].Cells[7].Attributes.Remove("style");
                }
            }
        }

        
    }
}