﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Principal;
using System.Drawing;

namespace Contact.standby
{
    public partial class report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionBuilder start = new SessionBuilder();
            loadList();
        }

        void loadList()
        {
            access obj;

            if (Session["MillID"] == null)
            {

                WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String UserName = UserSecurityPrincipal.Name;

                UserName = UserName.Substring(UserName.LastIndexOf(@"\") + 1).Trim();

                ADManager ad = new ADManager();
                String office = ad.getUserLocation(UserName);

                if (office != String.Empty)
                {
                    if (Session["SelectedOffice"] == null)
                    {
                        obj = new access();
                        DataSet ds1 = obj.GetMill(office);

                        if (ds1.Tables[0].Rows.Count > 0)
                        {
                            Session["SelectedOffice"] = office;
                            Session["MillID"] = ds1.Tables[0].Rows[0]["MillID"].ToString().ToUpper();
                        }
                        else
                        {
                            Session["SelectedOffice"] = office;
                            Session["MillID"] = 0;
                        }
                    }
                }

            }

            int MillID = Convert.ToInt32(Session["MillID"].ToString());


            obj = new access();

            Session["StandbyScheduleDS"] = obj.getStandbyScheduleDates(MillID);
            bool dateExist = false;

            try
            {
                for (int x = 0; x < ((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows.Count; x++)
                {
                    if (Session["SelectedStandbyScheduleDate"].ToString() == ((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows[x][0].ToString())
                    {
                        dateExist = true;
                        break;
                    }
                }
            }
            catch (Exception)
            {
            }
           
            

            if (Session["SelectedStandbyScheduleDate"] == null || !dateExist)
            {
                Session["SelectedStandbyScheduleDate"] = ((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows[0][0].ToString();
            }

            if (((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows.Count > 1)
            {
                btnNext.Visible = true;
                btnPrevious.Visible = true;

                int arrLenth = ((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows.Count;
                String dt = Session["SelectedStandbyScheduleDate"].ToString();

                for (int i = 0; i < ((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows.Count; i++)
                {

                    if (((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows[i][0].ToString() == dt)
                    {
                        btnNext.Attributes.Remove("class");
                        btnPrevious.Attributes.Remove("class");

                        if (i < arrLenth - 1)
                        {
                            btnNext.Attributes.Add("class", "btn btn-default btn-xs");
                        }
                        else
                        {
                            btnNext.Attributes.Add("class", "btn btn-default btn-xs disabled");
                        }

                        if (i > 0)
                        {
                            btnPrevious.Attributes.Add("class", "btn btn-default m-r-20 btn-xs");
                        }
                        else
                        {
                            btnPrevious.Attributes.Add("class", "btn btn-default btn-xs m-r-20 disabled");
                        }

                        break;
                    }


                }
            }
            else
            {
                btnNext.Visible = false;
                btnPrevious.Visible = false;
            }

            

            try
            {
                lblDateError.Visible = false;

                String StandbyTime = Convert.ToDateTime(Session["SelectedStandbyScheduleDate"].ToString()).ToShortTimeString();
                String StandbyDate = "";

                if (txtSearch.Text.Trim() == String.Empty)
                {
                    StandbyDate = Session["SelectedStandbyScheduleDate"].ToString();
                }
                else
                {
                    if (txtSearch.Text.Contains("/"))
                    {
                        String[] dateFormat = txtSearch.Text.Trim().Split('/');
                        StandbyDate = (dateFormat[2] + "-" + dateFormat[1] + "-" + dateFormat[0] + " " + StandbyTime).Trim();
                    }
                    else
                    {
                        StandbyDate = (txtSearch.Text.Trim() + " " + StandbyTime).Trim();
                    }
                }

                DateTime sDate = Convert.ToDateTime(StandbyDate);

                if (sDate.DayOfWeek != DayOfWeek.Friday)
                {
                    if(sDate.DayOfWeek == DayOfWeek.Saturday)
                        StandbyDate = sDate.AddDays(-1).ToString("yyyy-MM-dd HH:mm");
                    else if (sDate.DayOfWeek == DayOfWeek.Sunday)
                        StandbyDate = sDate.AddDays(-2).ToString("yyyy-MM-dd HH:mm");
                    else if (sDate.DayOfWeek == DayOfWeek.Monday)
                        StandbyDate = sDate.AddDays(-3).ToString("yyyy-MM-dd HH:mm");
                    else if (sDate.DayOfWeek == DayOfWeek.Tuesday)
                        StandbyDate = sDate.AddDays(-4).ToString("yyyy-MM-dd HH:mm");
                    else if (sDate.DayOfWeek == DayOfWeek.Wednesday)
                        StandbyDate = sDate.AddDays(-5).ToString("yyyy-MM-dd HH:mm");
                    else if (sDate.DayOfWeek == DayOfWeek.Thursday)
                        StandbyDate = sDate.AddDays(-6).ToString("yyyy-MM-dd HH:mm");
                }

                lblMill.Text = Session["SelectedOffice"].ToString().ToUpper();
                lblMill2.Text = Session["SelectedOffice"].ToString().ToUpper();

                obj = new access();
                DataSet ds2 = obj.getStandyListDates(StandbyDate);

                lblStartDate.Text = ds2.Tables[0].Rows[0]["StartDate"].ToString();
                lblEndDate.Text = ds2.Tables[0].Rows[0]["EndDate"].ToString();

                //LOAD EMERGENCY NUMBERS ##################################################
                obj = new access();
                ds2 = obj.getStandyListEmergencyNumbers(MillID);

                if (ds2.Tables[0].Rows.Count > 0)
                {
                    lblEmergencyNumber.Text = "Emergency Number : " + ds2.Tables[0].Rows[0][0].ToString();
                }
                else
                {
                    lblEmergencyNumber.Text = "Emergency Number : n/a";
                }

                //LOAD DEPARTMENTS AND PERSONS
                obj = new access();
                DataSet ds3 = obj.viewStandyListDepartment(MillID);

                TableRow[] dpr = new TableRow[ds3.Tables[0].Rows.Count];
                TableCell[] dpc = new TableCell[ds3.Tables[0].Rows.Count];

                for (int i = 0; i < dpr.Length; i++)
                {
                    //DEPARTMENT HEADINGS
                    dpr[i] = new TableRow();
                    dpc[i] = new TableCell();
                    dpc[i].Text = ds3.Tables[0].Rows[i]["Description"].ToString();
                    dpc[i].CssClass = "list-table-department bg-blue-grey text-center";
                    dpc[i].ColumnSpan = 6;
                    dpr[i].Cells.Add(dpc[i]);
                    tblPerson.Rows.Add(dpr[i]);

                    //CONTACT PERSONS
                    int DepartmentID = Convert.ToInt32(ds3.Tables[0].Rows[i]["DepartmentID"].ToString());

                    obj = new access();
                    DataSet ds4 = obj.viewStandyListContactPersons(MillID, DepartmentID, StandbyDate);

                    TableRow[] pr = new TableRow[ds4.Tables[0].Rows.Count];
                    TableCell[] pc = new TableCell[ds4.Tables[0].Columns.Count];

                    int countPersons = 0;

                    for (int r = 0; r < pr.Length; r++)
                    {
                        pr[r] = new TableRow();

                        for (int c = 1; c < pc.Length; c++)
                        {
                            pc[c] = new TableCell();
                            pc[c].Text = ds4.Tables[0].Rows[r][c].ToString();
                            pc[c].CssClass = "list-table-cell";

                            pc[c].Style.Add("font-size", "11px !important");
                            pr[r].Cells.Add(pc[c]);
                        }
                        tblPerson.Rows.Add(pr[r]);

                        countPersons++;
                    }

                    if (countPersons < 1)
                    {
                        tblPerson.Rows.Remove(dpr[i]);
                    }
                }
            }
            catch (Exception)
            {
                lblDateError.Visible = true;
            }
            

            //PRIAMRY UPDATER EMAILS
            obj = new access();
            DataSet pu = obj.getPrimaryUpdaters(MillID);
            Literal[] lt = new Literal[pu.Tables[0].Rows.Count];

            if (pu.Tables[0].Rows.Count > 0)
            {
                Literal caption = new Literal();
                caption.Text = "Please mail standby list changes to one of the following <br/>";
                lkPrimaryUpdaters.Controls.Add(caption);
            }

            for (int i=0; i< pu.Tables[0].Rows.Count; i++)
            {
                lt[i] = new Literal();
                lt[i].Text = "<a style='margin-right:10px' href='mailto: " + pu.Tables[0].Rows[i][0].ToString() + "'>" + pu.Tables[0].Rows[i][1].ToString() + "</a>";
                lkPrimaryUpdaters.Controls.Add(lt[i]);
            }

        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            next();
        }

        void next()
        {
            try
            {
                if (Session["StandbyScheduleDS"] != null)
                {
                    int arrLenth = ((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows.Count;
                    String dt = Session["SelectedStandbyScheduleDate"].ToString();

                    for (int i = 0; i < ((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows.Count; i++)
                    {
                        if (((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows[i][0].ToString() == dt)
                        {
                            Session["SelectedStandbyScheduleDate"] = ((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows[i + 1][0].ToString();
                            Response.Redirect("report.aspx");
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            prev();
        }
        void prev()
        {
            try
            {
                if (Session["StandbyScheduleDS"] != null)
                {
                    int arrLenth = ((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows.Count;
                    String dt = Session["SelectedStandbyScheduleDate"].ToString();

                    for (int i = 0; i < ((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows.Count; i++)
                    {
                        if (((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows[i][0].ToString() == dt)
                        {
                            Session["SelectedStandbyScheduleDate"] = ((Session["StandbyScheduleDS"]) as DataSet).Tables[0].Rows[i - 1][0].ToString();
                            Response.Redirect("report.aspx");
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {


            }

        }
    }
}