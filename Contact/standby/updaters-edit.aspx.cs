﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contact.standby
{
    public partial class updaters_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            if (Session["UserName"] != null)
            {
                String UserName = Session["UserName"].ToString();
                int RoleID = 11;
                int AdminRoleID = 9;

                access obj = new access();
                if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
                {
                    Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                    Response.Redirect("access-denied.aspx");
                }
                else
                {
                    loadUpdater();
                }
            }
            else
            {
                Response.Redirect("index.aspx");
            }
        }

        void loadUpdater()
        {
            if(Session["SelectedUpdaterID"]!=null)
            {
                lblUpdater.Text = Session["SelectedUpdaterName"].ToString();
                lblSection.Text = Session["SelectedUpdaterSection"].ToString();
            }
            else
            {
                Response.Redirect("updaters.aspx");
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            updateSection();
        }
        void updateSection()
        {
            try
            {
                if (Session["SelectedUpdaterID"] != null)
                {
                    if (drdDepartment.SelectedIndex < 1)
                    {
                        ShowAlert(2, "Selected a section first");
                    }
                    else
                    {

                        int StandbyUpdaterID = Convert.ToInt32(Session["SelectedUpdaterID"].ToString());
                        int SectionID = Convert.ToInt32(drdDepartment.SelectedValue.Trim());

                        System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                        String SystemUserName = UserSecurityPrincipal.Name;

                        access obj = new access();
                        obj.updateStandbyUpdaterSection(SystemUserName, StandbyUpdaterID, SectionID);

                        Session["SelectedUpdaterSection"] = drdDepartment.SelectedItem.Text.Trim();
                        drdDepartment.SelectedIndex = 0;

                        ShowAlert(1, "Changes saved successfully");

                    }
                }
                else
                {
                    Response.Redirect("updaters.aspx");
                }
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
            

        }

        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["SelectedUpdaterID"] == null)
            {
                Response.Redirect("updaters.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            divDelete.Visible = true;
        }

        protected void btnDeleteYes_Click(object sender, EventArgs e)
        {
            deleteUpdater();
        }
        void deleteUpdater()
        {
            try
            {
                if (Session["SelectedUpdaterID"] != null)
                {

                    int StandbyUpdaterID = Convert.ToInt32(Session["SelectedUpdaterID"].ToString());

                    System.Security.Principal.WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                    String SystemUserName = UserSecurityPrincipal.Name;

                    access obj = new access();
                    obj.deleteStandbyUpdater(SystemUserName, StandbyUpdaterID);
                    divDelete.Visible = false;

                    ShowAlert(1, "Standby updater deleted successfully");

                    Session.Remove("SelectedUpdaterID");
                }
                else
                {
                    Response.Redirect("updaters.aspx");
                }
            }
            catch (Exception ex)
            {

                ShowAlert(2, ex.Message);
            }
        }

        protected void btnDeleteNo_Click(object sender, EventArgs e)
        {
            divDelete.Visible = false;
        }
    }
}