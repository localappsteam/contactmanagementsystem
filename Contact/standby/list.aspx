﻿<%@ Page Title="" Language="C#" MasterPageFile="~/standby/standby.Master" AutoEventWireup="true" CodeBehind="list.aspx.cs" Inherits="Contact.standby.list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Standby</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Current Standby</h2>
                </div>
                <div id="divList" runat="server" class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 p-b-10 text-center hidden">
                                <img src="images/under-construction-2408061_640.png" width="130" />
                            </div>
                            <div class="col-md-12 text-center" style="margin-bottom: 10px">
                                <h3 class="m-b-20" style="margin-top: 0px">
                                    <asp:Label ID="lblMill" runat="server" Text=""></asp:Label>
                                    STANDBY LIST</h3>
                                <h4>
                                    <asp:Label ID="lblMill2" runat="server" Text=""></asp:Label>
                                    NEW SCHEDULE</h4>
                                <h4>
                                    <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                                    <span class="m-r-10"></span>
                                    -
                                    <span class="m-r-10"></span>
                                    <asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                                </h4>
                                <asp:LinkButton ID="btnPrevious" ToolTip="Click here to view alternate standby start and end times if it differs during the selected week above." runat="server" OnClick="btnPrevious_Click"><p class="fa fa-backward icon-mr"></p> Previous list</asp:LinkButton>
                                <asp:LinkButton ID="btnNext" ToolTip="Click here to view alternate standby start and end times if it differs during the selected week above." runat="server" OnClick="btnNext_Click">Next list <p class="fa fa-forward icon-ml"></p></asp:LinkButton>
                            </div>
                            <div class="col-md-12 cont-holder">
                            </div>
                            <div class="col-md-12 cont-holder-plain" style="margin-top: -10px">
                                <div class="row m-l-20 m-r-20">
                                    <div class="col-md-12 text-center bg-red" style="margin-bottom: 10px; padding-bottom: 0px">
                                        <asp:Label ID="lblEmergencyNumber" Font-Size="14px" Font-Bold="false" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="body table-responsive" style="padding-top: 0px; padding-bottom: 0px; margin-bottom: 0px; margin-top: -10px">
                                    <asp:Table ID="tblPerson" CssClass="table table-bordered=" Style="font-size: 12px;" runat="server">
                                        <asp:TableHeaderRow CssClass="bg-indigo">
                                            <asp:TableHeaderCell Text="Duty no#" CssClass="list-table-cell">
                                            </asp:TableHeaderCell>
                                            <asp:TableHeaderCell Text="Contact Person" CssClass="list-table-cell">
                                            </asp:TableHeaderCell>
                                            <asp:TableHeaderCell Text="Position" CssClass="list-table-cell">
                                            </asp:TableHeaderCell>
                                            <asp:TableHeaderCell Text="Address" CssClass="list-table-cell">
                                            </asp:TableHeaderCell>
                                            <asp:TableHeaderCell Text="Tel / Ext" CssClass="list-table-cell">
                                            </asp:TableHeaderCell>
                                            <asp:TableHeaderCell Text="Cellphone" CssClass="list-table-cell">
                                            </asp:TableHeaderCell>
                                        </asp:TableHeaderRow>
                                    </asp:Table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-4 p-l-40">
                            <h4>Please select date to view standby list</h4>
                             <asp:TextBox ID="txtSearch" Placeholder="dd/mm/yyyy" CssClass="form-control form-control-sm" Width="75%" Style="float: left" TextMode="Date" runat="server"></asp:TextBox>
                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server">View<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                        </div>
                            <div class="col-md-4 p-l-40">
                             <asp:Label ID="lblDateError" Visible="false" ForeColor="Red" runat="server" Text="Enter a valid date. Formart(dd/mm/yyyy)"></asp:Label>
                           </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
