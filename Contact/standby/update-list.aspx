﻿<%@ Page Title="" Language="C#" MasterPageFile="~/standby/standby.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="update-list.aspx.cs" Inherits="Contact.standby.update_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="block-header">
        <span>
            <p class="fa fa-home m-r-10"></p>
            Standby / Update list</span>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Update List</h2>
                </div>
                <div class="body">
                    <div class="panel">
                        <div class="row">
                            <div class="col-md-12 cont-holder-plain">
                                <div class="row" id="divDepartment">
                                    <div class="col-md-5">
                                        <p>Select a department to proceed.<span class="text-danger icon-ml">*</span></p>
                                        <asp:DropDownList ID="drdDepartment" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Description" AutoPostBack="true" DataValueField="DepartmentID" OnSelectedIndexChanged="drdDepartment_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Standby_SelectDepartments" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                                <div class="row" id="divDuties" runat="server" visible="false">
                                    <div class="col-md-8 cont-holder" style="overflow: auto">
                                        <asp:GridView ID="gridDuties" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None" runat="server" EmptyDataText="No data found." AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataKeyNames="#" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="gridDuties_SelectedIndexChanged">
                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                            <RowStyle Height="41px"></RowStyle>
                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" ControlStyle-CssClass="btn-block" SelectText="Select" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                                <asp:BoundField DataField="#" HeaderText="#" SortExpression="#" HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="1px" InsertVisible="False" ReadOnly="True" />
                                                <asp:BoundField DataField="DutyNo" HeaderText="DutyNo" SortExpression="DutyNo" />
                                                <asp:BoundField DataField="Duty" HeaderText="Duty" SortExpression="Duty" />
                                                <asp:BoundField DataField="Bleeper" HeaderText="Bleeper" SortExpression="Bleeper" />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Standby_UpdateList_ViewDuties" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="drdDepartment" Name="DepartmentID" PropertyName="SelectedValue" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                                <div class="row" id="divStandbyHeaders" visible="false" runat="server">
                                    <div class="col-md-12">
                                        <h5>Department:
                                            <asp:Label ID="lblSelectedDepartment" runat="server" Text=""></asp:Label></h5>
                                        <h5>Duty No#:
                                            <asp:Label ID="lblSelectedDutyNo" runat="server" Text="sdsdsd"></asp:Label></h5>
                                        <h5>Duty:
                                            <asp:Label ID="lblSelectedDuty" runat="server" Text="sdsdsd"></asp:Label></h5>
                                    </div>
                                </div>
                                <div class="row" id="divStandbyList" visible="false" runat="server">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-2 btn-fix-width" style="margin-bottom: 0px;">
                                                <asp:LinkButton ID="btnAddStandby" CssClass="btn btn-block btn-primary btn-sm" runat="server" OnClick="btnAddStandby_Click"><p class="fa fa-user-plus icon-mr"></p> Add new standby</asp:LinkButton>
                                            </div>
                                            <div class="col-md-2 btn-fix-width" style="margin-bottom: 0px;">
                                                <asp:LinkButton ID="btnViewDuties" CssClass="btn btn-block btn-primary btn-sm" runat="server" OnClick="btnViewDuties_Click"><p class="fa fa-list icon-mr"></p> View duties</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 cont-holder" style="overflow: auto">
                                        <asp:GridView ID="gridPersons" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None"   runat="server" EmptyDataText="No data found." AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataSourceID="SqlDataSource3" OnSelectedIndexChanged="gridPersons_SelectedIndexChanged" OnDataBound="gridPersons_DataBound">
                                            <AlternatingRowStyle BackColor="#f5f5f5" />

                                            <RowStyle Height="41px"></RowStyle>
                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" ControlStyle-CssClass="btn-block" SelectText="Edit" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                                <asp:BoundField DataField="#" HeaderText="#" SortExpression="#" HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="1px" ReadOnly="True" />
                                                <asp:BoundField DataField="Person" HeaderText="Person" SortExpression="Person" ReadOnly="True" />
                                                <asp:BoundField DataField="Position" HeaderText="Position" SortExpression="Position" ReadOnly="True" />
                                                <asp:BoundField DataField="Mill" HeaderText="Mill" SortExpression="Mill" ReadOnly="True" />
                                                <asp:BoundField DataField="WeeklyRecurrence" HeaderText="WeeklyRecurrence" ReadOnly="True" SortExpression="WeeklyRecurrence" />
                                                <asp:BoundField DataField="StartDate" HeaderText="StartDate" ReadOnly="True" SortExpression="StartDate" />
                                                <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Standby_UpdateList_ViewEmployees" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="gridDuties" Name="DutyID" PropertyName="SelectedValue" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </div>
                                </div>
                                <div class="row" id="divAddStandby" visible="false" runat="server">
                                    <div class="col-md-12">
                                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <hr />
                                                <h4>Add person to standby</h4>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <p>Select a person<span class="text-danger icon-ml">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <asp:TextBox ID="txtSearch" CssClass="form-control form-control-sm" Width="78%" Style="float: left" Placeholder="Search employee or contractor..." runat="server"></asp:TextBox>
                                                        <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-sm btn-search" runat="server" OnClick="btnSearch_Click">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                                                        <br />
                                                        <br />
                                                        <div class="row">
                                                            <div class="cont-holder" style="overflow: auto">
                                                                <asp:GridView ID="gridEveryone" Width="100%" CssClass="table" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None" runat="server" EmptyDataText="No data found." AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataSourceID="SqlDataSource4">
                                                                    <AlternatingRowStyle BackColor="#f5f5f5" />

                                                                    <RowStyle Height="41px"></RowStyle>
                                                                    <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                                                    <Columns>
                                                                        <asp:CommandField ShowSelectButton="True" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                                                        <asp:BoundField DataField="#" HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="0px" HeaderText="#" ReadOnly="True" SortExpression="#" />
                                                                        <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" ReadOnly="True" />
                                                                        <asp:BoundField DataField="Firstnames" HeaderText="Firstnames" SortExpression="Firstnames" ReadOnly="True" />
                                                                        <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" ReadOnly="True" />
                                                                        <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" ReadOnly="True" />
                                                                        <asp:BoundField DataField="EmpType" HeaderText="EmpType" SortExpression="EmpType" ReadOnly="True" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_EmployeeAndContractorSearch" SelectCommandType="StoredProcedure">
                                                                    <SelectParameters>
                                                                        <asp:ControlParameter ControlID="txtSearch" DefaultValue="" Name="SearchValue" PropertyName="Text" Type="String" />
                                                                        <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                                    </SelectParameters>
                                                                </asp:SqlDataSource>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>Starting date and time<span class="text-danger icon-ml">*</span></p>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <small>Date</small>
                                                                        <asp:TextBox ID="txtDates" AutoPostBack="true" Placeholder="dd/mm/yyyy" CssClass="form-control" TextMode="Date" runat="server"></asp:TextBox>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <small>Time</small>
                                                                        <asp:TextBox ID="txtTime" AutoPostBack="true" Placeholder="00:00" CssClass="form-control" TextMode="Time" runat="server"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <p>(Recurring) Begin standby after every<span class="text-danger icon-ml">*</span></p>
                                                                <asp:DropDownList ID="drdRecurring" AutoPostBack="true" CssClass="form-control" runat="server" DataSourceID="SqlDataSource5" DataTextField="Description" DataValueField="NumberOfWeeks">
                                                                </asp:DropDownList>
                                                                <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Standby_SelectWeeklyRecurrence" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 cont-holder" style="overflow: auto">
                                                        <p style="margin:5px"><b>Standby dates</b></p>
                                                        <asp:GridView ID="gridUpcoming" Width="100%" CssClass="table" Font-Size="11px" ShowHeader="true" AutoGenerateColumns="false" AllowSorting="True" BorderStyle="None" GridLines="None" runat="server" ShowHeaderWhenEmpty="true" DataSourceID="SqlDataSource7">
                                                            <AlternatingRowStyle BackColor="#f5f5f5" />

                                                            <RowStyle Height="41px"></RowStyle>
                                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                                            <Columns>
                                                                <asp:BoundField DataField="Begins" HeaderText="Begins" ReadOnly="True" SortExpression="Begins" />
                                                                <asp:BoundField DataField="Ends" HeaderText="Ends" SortExpression="Ends" ReadOnly="True" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_StandBy_UpcomingStandbyDates" SelectCommandType="StoredProcedure">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="txtDates" Name="Date" PropertyName="Text" DbType="String" />
                                                                <asp:ControlParameter ControlID="txtTime" DbType="String" Name="Time" PropertyName="Text" />
                                                                <asp:ControlParameter ControlID="drdRecurring" Name="Recurrence" PropertyName="SelectedValue" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div class="row">
                                            <div class="col-md-2 btn-fix-width-sm">
                                                <asp:LinkButton ID="btnSubmitAddStandby" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSubmitAddStandby_Click"><p class="fa fa-save icon-mr"></p>Submit</asp:LinkButton>
                                            </div>
                                            <div class="col-md-2 btn-fix-width-sm">
                                                <asp:LinkButton ID="btnCancelAddStandby" CssClass="btn btn-default btn-block btn-sm" runat="server" OnClick="btnCancelAddStandby_Click"><p class="fa fa-times icon-mr"></p>Cancel</asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row" id="divEditStandby" runat="server" visible="false">
                                    <div class="col-md-8">
                                        <h4>Edit standby</h4>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <div class="row">

                                                    <div class="col-md-2 btn-fix-width">
                                                        <asp:LinkButton ID="btnDeleteStandby" CssClass="btn btn-danger btn-block btn-sm" runat="server" OnClick="btnDeleteStandby_Click"><p class="fa fa-trash icon-mr"></p>Delete standby</asp:LinkButton>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <asp:LinkButton ID="btnCloseEdit" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnCloseEdit_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                                                    </div>
                                                    <div class="col-md-12" id="divDeleteStandby" runat="server" visible="false">
                                                        <h5 class="text-danger">Delete standby!</h5>
                                                        <p>Are you sure you want to delete this standby?</p>
                                                        <asp:LinkButton ID="btnYesDelete" CssClass="btn btn-danger btn-sm" runat="server" OnClick="btnYesDelete_Click"><p class="fa fa-check icon-mr"></p>Yes</asp:LinkButton>
                                                        <asp:LinkButton ID="btnNoDelete" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnNoDelete_Click"><p class="fa fa-times icon-mr"></p>No</asp:LinkButton>

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p>
                                                            Standby ID:
                                                            <asp:Label ID="lblSelectedStandbyID" CssClass="m-l-10" runat="server" Text=""></asp:Label>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <p>
                                                            Starting date:
                                                            <asp:Label ID="lblSelectedStandbyDate" CssClass="m-l-10" runat="server" Text=""></asp:Label>
                                                            <asp:LinkButton ID="btnEditStartingDate" CssClass="btn btn-default btn-xs m-l-10" runat="server" OnClick="btnEditStartingDate_Click"><small class="fa fa-edit icon-mr"></small>Edit</asp:LinkButton>
                                                        </p>
                                                        <div class="row" id="divEditStartingDate" runat="server" visible="false">
                                                            <div class="col-md-6">
                                                                <small>Date</small>
                                                                <asp:TextBox ID="txtDateEdit" AutoPostBack="true" Placeholder="dd/mm/yyyy" CssClass="form-control" TextMode="Date" runat="server"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <small>Time</small>
                                                                <asp:TextBox ID="txtTimeEdit" AutoPostBack="true" CssClass="form-control" Placeholder="00:00" TextMode="Time" runat="server"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-2 btn-fix-width-sm">
                                                                <asp:LinkButton ID="btnSubmitStartingDateEdit" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSubmitStartingDateEdit_Click"><p class="fa fa-save icon-mr"></p>Submit</asp:LinkButton>
                                                            </div>
                                                            <div class="col-md-2 btn-fix-width-sm">
                                                                <asp:LinkButton ID="btnCancelStartingDateEdit" CssClass="btn btn-default btn-block btn-sm" runat="server" OnClick="btnCancelStartingDateEdit_Click"><p class="fa fa-times icon-mr"></p>Cancel</asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <p>
                                                            Weekly recurrence:
                                                            <asp:Label ID="lblSelectedStandbyRecurrence" CssClass="m-l-10" runat="server" Text=""></asp:Label>
                                                            <asp:LinkButton ID="btnEditRecurrence" CssClass="btn btn-default btn-xs m-l-10" runat="server" OnClick="btnEditRecurrence_Click"><small class="fa fa-edit icon-mr"></small>Edit</asp:LinkButton>
                                                        </p>
                                                        <div class="row" id="divEditReccurrence" runat="server" visible="false">
                                                            <div class="col-md-12">
                                                                <asp:DropDownList ID="drdEditReccurrence" Style="max-width: 300px" CssClass="form-control" runat="server" DataSourceID="SqlDataSource5" DataTextField="Description" DataValueField="NumberOfWeeks">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-md-2 btn-fix-width-sm">
                                                                <asp:LinkButton ID="btnSubmitReccurreChange" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSubmitReccurreChange_Click"><p class="fa fa-save icon-mr"></p>Submit</asp:LinkButton>
                                                            </div>
                                                            <div class="col-md-2 btn-fix-width-sm">
                                                                <asp:LinkButton ID="btnCancelReccurreChange" CssClass="btn btn-default btn-block btn-sm" runat="server" OnClick="btnCancelReccurreChange_Click"><p class="fa fa-times icon-mr"></p>Cancel</asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <p>
                                                            Standby person:
                                                            <asp:Label ID="lblSelectedStandbyPerson" CssClass="m-l-10" runat="server" Text=""></asp:Label>
                                                            <asp:LinkButton ID="btnChangePerson" CssClass="btn btn-default btn-xs m-l-10" runat="server" OnClick="btnChangePerson_Click"><small class="fa fa-edit icon-mr"></small>Change</asp:LinkButton>
                                                            <asp:LinkButton ID="btnSwapPerson" CssClass="btn btn-default btn-xs m-l-10" runat="server" OnClick="btnSwapPerson_Click"><small class="fa fa-exchange icon-mr"></small>Swap</asp:LinkButton>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row" id="divChangePerson" runat="server" visible="false">
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtSearch2" CssClass="form-control form-control-sm" Width="78%" Style="float: left" Placeholder="Search employee or contractor..." runat="server"></asp:TextBox>
                                                        <asp:LinkButton ID="btnSearch2" CssClass="btn btn-primary btn-sm btn-search" runat="server" OnClick="btnSearch2_Click">Search<p class="fa fa-search icon-ml"></p></asp:LinkButton>
                                                        <br />
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-12 cont-holder" style="overflow: auto">
                                                        <asp:GridView ID="gridEveryone2" Width="100%" CssClass="table" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None" runat="server" EmptyDataText="No data found." AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataSourceID="SqlDataSource6">
                                                            <AlternatingRowStyle BackColor="#f5f5f5" />
                                                            <RowStyle Height="41px"></RowStyle>
                                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                                            <Columns>
                                                                <asp:CommandField ShowSelectButton="True" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                                                <asp:BoundField DataField="#" HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="0px" HeaderText="#" ReadOnly="True" SortExpression="#" />
                                                                <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" ReadOnly="True" />
                                                                <asp:BoundField DataField="Firstnames" HeaderText="Firstnames" SortExpression="Firstnames" ReadOnly="True" />
                                                                <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" ReadOnly="True" />
                                                                <asp:BoundField DataField="EmpType" HeaderText="EmpType" SortExpression="EmpType" ReadOnly="True" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_EmployeeAndContractorSearch" SelectCommandType="StoredProcedure">
                                                            <SelectParameters>
                                                                <asp:SessionParameter Name="MillID" SessionField="MillID" Type="Int32" />
                                                                <asp:ControlParameter ControlID="txtSearch2" DefaultValue="" Name="SearchValue" PropertyName="Text" Type="String" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                                </div>
                                                            </div>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <asp:LinkButton ID="btnSubmitPersonChange" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSubmitPersonChange_Click"><p class="fa fa-save icon-mr"></p>Submit</asp:LinkButton>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <asp:LinkButton ID="btnCancelPersonChange" CssClass="btn btn-default btn-block btn-sm" runat="server" OnClick="btnCancelPersonChange_Click"><p class="fa fa-times icon-mr"></p>Cancel</asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <hr />
                                                    </div>
                                                </div>
                                                <div class="row" id="divSwap" runat="server" visible="false">
                                                    <div class="col-md-5">
                                                        <div class="row">
                                                        <div class="col-md-12 cont-holder" style="overflow: auto;">
                                                            <h5 style="margin-left:5px">Select a person to swap with:</h5>
                                                        <asp:GridView ID="gridSwap" Width="100%" CssClass="table table-condensed" Font-Size="11px" AllowSorting="True" BorderStyle="None" GridLines="None" runat="server" EmptyDataText="No data found." AllowPaging="True" PageSize="25" AutoGenerateColumns="False" DataSourceID="SqlDataSource9">
                                                            <AlternatingRowStyle BackColor="#f5f5f5" />

                                                            <RowStyle Height="41px"></RowStyle>
                                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                                            <Columns>
                                                                <asp:CommandField ShowSelectButton="True" ControlStyle-CssClass="btn-block" SelectText="Select" ControlStyle-ForeColor="#333333" ButtonType="Button" />
                                                                <asp:BoundField DataField="#" HeaderText="#" SortExpression="#" HeaderStyle-ForeColor="Transparent" ItemStyle-ForeColor="Transparent" ItemStyle-Font-Size="1px" ReadOnly="True" />
                                                                <asp:BoundField DataField="Person" HeaderText="Person" SortExpression="Person" ReadOnly="True" />
                                                                <asp:BoundField DataField="StartDate" HeaderText="StartDate" SortExpression="StartDate" ReadOnly="True" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:SqlDataSource ID="SqlDataSource9" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_Standby_UpdateList_ViewEmployeesSwap" SelectCommandType="StoredProcedure">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="lblSelectedStandbyID" Name="SelectedStandbyID" PropertyName="Text" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                            </div>
                                                            </div>
                                                        <asp:CheckBox ID="ckApplyToUpcomingSchedules" runat="server" Text="Apply swapping to all upcoming standby schedules." />
                                                        <br />
                                                        <small class="text-danger">Please note that by checking this option, swapping will apply to all upcoming standby schedules for both people.<br />
                                                        </small>
                                                        <br />
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="col-md-3">
                                                                <asp:LinkButton ID="btnApplySwap" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnApplySwap_Click"><p class="fa fa-save icon-mr"></p>Submit</asp:LinkButton>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <asp:LinkButton ID="btnCancelSwap" CssClass="btn btn-default btn-block btn-sm" runat="server" OnClick="btnCancelSwap_Click"><p class="fa fa-times icon-mr"></p>Cancel</asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 cont-holder" style="overflow:auto">
                                                        <p style="margin:5px"><b>Standby dates</b></p>
                                                        <asp:GridView ID="gridUpcomingDatesEdit" Width="100%" CssClass="table" Font-Size="11px" AutoGenerateColumns="False" AllowSorting="True" BorderStyle="None" GridLines="None" runat="server" ShowHeaderWhenEmpty="True" DataSourceID="SqlDataSource8">
                                                            <AlternatingRowStyle BackColor="#f5f5f5" />

                                                            <RowStyle Height="41px"></RowStyle>
                                                            <SelectedRowStyle BackColor="#c5e3fc" ForeColor="Black" />
                                                            <Columns>
                                                                <asp:BoundField DataField="Begins" HeaderText="Begins" ReadOnly="True" SortExpression="Begins" />
                                                                <asp:BoundField DataField="Ends" HeaderText="Ends" SortExpression="Ends" ReadOnly="True" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:EmployeeContactConnectionString %>" SelectCommand="sp_Contact_StandBy_UpcomingStandbyDatesEditView" SelectCommandType="StoredProcedure">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="lblSelectedStandbyDate" Name="DateTime" PropertyName="Text" DbType="String" />
                                                                <asp:ControlParameter ControlID="lblSelectedStandbyRecurrence" Name="Recurrence" PropertyName="Text" Type="String" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="MessageBox" visible="false" class="MessageBox" runat="server">
        <div id="AlertBox" class="AlertBox"></div>
        <div class="AlertBoxContent">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="AlertHeader" runat="server" class="modal-header">
                        <h4 class="modal-title"><span id="spanAlert" runat="server"></span>
                            <asp:Label ID="lblAlertTitle" runat="server" Text="Title"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="false" Font-Size="Medium" Text="Message"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="btnCloseMessageBox" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnCloseMessageBox_Click"><p class="fa fa-times icon-mr"></p>Close</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
