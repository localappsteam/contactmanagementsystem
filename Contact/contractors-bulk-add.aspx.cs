﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Security.Principal;
using System.Net;
using System.Drawing;

namespace Contact
{
    public partial class contractors_bulk_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        void AuthenticateUser()
        {
            WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
            String UserName = UserSecurityPrincipal.Name;

            int RoleID = 18;
            int AdminRoleID = 3;

            access obj = new access();
            if (!obj.checkRoles(UserName, RoleID, AdminRoleID))
            {
                Session["Pagename"] = System.IO.Path.GetFileName(Request.Path).ToString().Trim();
                Response.Redirect("access-denied.aspx");
            }
            
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            uploadFile();
        }

        void uploadFile()
        {
            Session.Remove("BulkAdded");

            if (!FileUpload1.HasFile)
            {
                ShowAlert(2, "Select a file first.");
            }
            else if (FileUpload1.FileName.Trim() != "Contractors.csv")
            {
                ShowAlert(2, "Upload the Contractors.csv file.");
            }
            else
            {

                //CREATE UPLOAD FILE DIRECTORY AND SAVE FILE AS TXT FORMAT
                WindowsIdentity UserSecurityPrincipal = Request.LogonUserIdentity;
                String un = UserSecurityPrincipal.Name;

                un = un.Substring(un.IndexOf(@"\") + 1);
                String dir = Server.MapPath("uploads") + @"\" + un + @"\contractors";
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                dir = dir + @"\Contractors.txt";

                FileUpload1.SaveAs(dir);

                StreamReader rd = new StreamReader(dir);
                int total = File.ReadAllLines(dir).Length;

                String[] line;

                Char [] chars = { '`','~','!','#','$', '%','^','*','(',')','?', '"'};

                access obj = new access();
                obj.deleteTempBulkAddContractors(Convert.ToInt64(Session["EmployeeID"].ToString()));

                obj = new access();

                int countInserted = 0;

                for (int i = 0; i < total; i++)
                {
                    String data = rd.ReadLine();
                    for (int x=0; x< chars.Length; x++)
                    {
                        data = data.Replace(chars[x], '|');
                        data = data.Replace("|", "");
                    }
                    
                    line = data.Trim().Split(',');

                    try
                    {
                        if (i > 0)
                        {
                            obj.bulkAddContractorToTemp(Session["EmployeeID"].ToString(),
                            line[0].Trim(), line[1].Trim(), line[2].Trim(), line[3].Trim(),
                            line[4].Trim(), line[5].Trim(), line[6].Trim(), line[7].Trim(),
                            line[8].Trim(), line[9].Trim(), line[10].Trim(), line[11].Trim(),
                            line[12].Trim(), line[13].Trim(), line[14].Trim(), line[15].Trim(),
                            line[16].Trim(), line[17].Trim());

                            countInserted++;
                        }
                    }
                    catch (Exception)
                    {
                    }
                    
                }

                rd.Close();

                gridVerified.DataBind();

                lblTotalUploaded.Text = (total - 1).ToString();
                lblSkipped.Text = ((total - 1) - countInserted).ToString();

                divUpload.Visible = false;
                divVerification.Visible = true;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divUpload.Visible = true;
            divVerification.Visible = false;
        }
        protected void SqlDataSource1_Selected(object sender, SqlDataSourceStatusEventArgs e)
        {
            lblVerified.Text = e.AffectedRows.ToString();
            lblVerified2.Text = e.AffectedRows.ToString();
        }

        protected void SqlDataSource2_Selected(object sender, SqlDataSourceStatusEventArgs e)
        {
            lblFiltered.Text = e.AffectedRows.ToString();
            lblFiltered2.Text = e.AffectedRows.ToString();
        }

        void loadErrorFields()
        {
            for (int i = 0; i < gridFilteredOut.Rows.Count; i++)
            {
                for(int j=0; j<gridFilteredOut.Columns.Count; j++)
                {
                    if (gridFilteredOut.Rows[i].Cells[j].Text.ToLower().Contains("[already assigned]") || gridFilteredOut.Rows[i].Cells[j].Text.ToLower().Contains("[required]"))
                    {
                        gridFilteredOut.Rows[i].Cells[j].CssClass = "bg-danger";
                    }
                    else
                    {
                        gridFilteredOut.Rows[i].Cells[j].CssClass.Replace("bg-danger", "");
                    }
                }
            }
        }

        protected void gridFilteredOut_DataBound(object sender, EventArgs e)
        {
            loadErrorFields();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            sumbit();
        }

        void sumbit()
        {
            Session.Remove("BulkAdded");
            try
            {
                int added = Convert.ToInt32(lblVerified.Text.Trim());

                access obj = new access();
                obj.bulkAddContractors(Convert.ToInt64(Session["EmployeeID"].ToString()), Session["UserName"].ToString());

                Session["BulkAdded"] = true;
                ShowAlert(1, added + " contractors added successfully.");
            }
            catch (Exception ex)
            {
                ShowAlert(2, ex.Message);
            }
        }
        void ShowAlert(int title, String message)
        {
            if (title == 1)
            {
                lblAlertTitle.Text = "Success";
                lblAlertTitle.Attributes.Add("class", "text-success");
                spanAlert.Attributes.Add("class", "fa fa-check text-success");
            }
            else if (title == 2)
            {
                lblAlertTitle.Text = "Alert";
                lblAlertTitle.Attributes.Add("class", "text-danger");
                spanAlert.Attributes.Add("class", "fa fa-exclamation-triangle text-danger");
            }

            lblAlertMessage.Text = message;
            MessageBox.Visible = true;
        }

        void HideAlert()
        {
            lblAlertTitle.Text = "";
            lblAlertMessage.Text = "";
            MessageBox.Visible = false;

            if (Session["BulkAdded"] != null)
            {
                Session.Remove("BulkAdded");

                Response.Redirect("contractors-search.aspx");
            }
        }

        protected void btnCloseMessageBox_Click(object sender, EventArgs e)
        {
            HideAlert();
        }
    }
}